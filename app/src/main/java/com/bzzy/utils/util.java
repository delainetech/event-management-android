package com.bzzy.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.bzzy.SignInProcess.PinFragment;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.jsibbold.zoomage.ZoomageView;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class util {

    Context c;




    public util(Context c) {
        this.c = c;
    }
    public void Zoom_Image(final Context c, String imagepath,boolean isDownload) {

        Log.e("boolean",isDownload+"");
        final Dialog dialog=new Dialog(c,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.zoom_image);
        dialog.show();

        ZoomageView imageview=(ZoomageView)dialog.findViewById(R.id.myZoomageView);

        ImageView iv_download=(ImageView)dialog.findViewById(R.id.iv_download);

        if(isDownload)
            iv_download.setVisibility(View.VISIBLE);
        else
            iv_download.setVisibility(View.GONE);

        Picasso.get().load(imagepath).into(imageview);

        iv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadpdf(imagepath);
            }
        });

    }

    public long betweenDates(String FinalDate)
    {
        try {

            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String CurrentDate = df.format(c);

            Log.e("formattedDate",CurrentDate);

            Date date1;
            Date date2;

            SimpleDateFormat dates = new SimpleDateFormat("dd-MM-yyyy");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(FinalDate);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);

//            if(differenceDates<30){
//                new PinFragment.RenewDialog().showDialog(getActivity());
//            }
            //Convert long to String
//            String dayDifference = Long.toString(differenceDates);

            Log.e("HERE","HERE: " + differenceDates);

            return differenceDates;
        }
        catch (Exception exception) {

            Log.e("DIDN'T WORK", "exception " + exception);
            return 100;
        }
    }


    Dialog dialog;
    NumberProgressBar bar;
    String URL="";

    public void downloadpdf(String url)
    {

        this.c=c;
//        url=Constants.URL_Image+"admin/tests_images/"+Constants.ImageDirectory+".zip";

        Log.e("downloadpdf","call "+url);

        dialog=new Dialog(c);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_download);

        // inflate and adjust layout
        // dialog.setContentView(layout1);

        bar=(NumberProgressBar)dialog.findViewById(R.id.numberbar1);

        //adView=(AdView)dialog.findViewById(R.id.adView);
        dialog.show();
        dialog.setCancelable(true);

        //progtext=(TextView)dialog.findViewById(R.id.progresstextid);

        URL=url;

        String[] urlarr=URL.split("/");
        int len=urlarr.length;
        //Toast.makeText(c,"len "+len,200).show();
        URL=urlarr[len-1];

        new DownloadFile().execute(url);

    }

    public class DownloadFile extends AsyncTask<String, Integer, String> {

        String filepath;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub

                }
            }, 5000);

        }

        @Override
        protected String doInBackground(String... Url) {
            try {
                java.net.URL url = new URL(Url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                // Detect the file lenghth
                int fileLength = connection.getContentLength();

                // Locate storage location
                filepath = Environment.getExternalStorageDirectory().getAbsolutePath();
                //.getPath();

                File myDir = new File(filepath + "/Bzzy Images");
                myDir.mkdirs();
                File file = new File(myDir, URL);
                if (file.exists()) {

                    ((Activity)c).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(c,"Image already downloaded",Toast.LENGTH_SHORT).show();
                        }
                    });
//                    Message message = mHandler.obtainMessage(command, parameter);
//                    message.sendToTarget();
//                    Toast.makeText(c,"Image already downloaded",Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    // file.delete();
                }
                else {
                    // Download the file

                    InputStream input = new BufferedInputStream(url.openStream());

                    // Save the downloaded file

                    OutputStream output = new FileOutputStream(file);

                    byte data[] = new byte[1024];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // Publish the progress
                        publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                    // Close connection
                    output.flush();
                    output.close();
                    input.close();
                }
            } catch (Exception e) {
                // Error Log
                //Logg("response_code","e",e);
                Log.e("Error","e", e);
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
                dialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // Update the progress dialog
            bar.setProgress(progress[0]);

            // progtext.setText(String.valueOf(progress[0])+"%");

        }
    }


}
