package com.bzzy.utils;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Common {

    public static String CALLBACK_URL="https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";
    public static final String paytmurl ="http://139.59.88.42/Event-Managment/public/";
    public static String MID="gtuohp88083516982889";
    public static String CUST_ID = "CUST00";
    public static String INDUSTRY_TYPE_ID = "Retail";
    public static String CHANNEL_ID = "WAP";
    public static String WEBSITE = "DEFAULT"  ;//"shrwap";
//    public static String CALLBACK_URL="https://securegw.paytm.in/theia/processTransaction?ORDER_ID=";


   public static void loggg(String tag,String val){
        Log.e(tag,val);
    }

    public static String ParseString(JSONObject obj, String Param) throws JSONException {
        if (obj.has(Param)) {
            String lastSeen = obj.getString(Param);
            if (lastSeen != null && !TextUtils.isEmpty(lastSeen) && !lastSeen.equalsIgnoreCase("null") && lastSeen.length()>0)
                return obj.getString(Param);
            else
                return "";

        } else
            return "";
    }

}
