package com.bzzy.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.PresentAbsentActivity;
import com.bzzy.Adapters.PresentAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class PresentFragment extends Fragment implements ApiCalls {

    RecyclerView present_recycler;
    PresentAdapter prsntAdapter;
    JSONObject object1;
    String batch_id, id;

    TextView present_members, error_text;

    public PresentFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_present, container, false);

        error_text = (TextView) view.findViewById(R.id.error_text);
        present_members = (TextView) view.findViewById(R.id.present_members);
        present_recycler = (RecyclerView) view.findViewById(R.id.present_recycler);
        present_recycler.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        present_recycler.setLayoutManager(horizontalLayoutManager);

        id = getArguments().getString("attendance_id");
        batch_id = getArguments().getString("batch_id");
        Log.e("Batch_id_present", batch_id + "");

        // Intent data
        getdata();

        return view;
    }

    public void getdata() {

        PresentAbsentActivity.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getPrstAbsntList("");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                getPrstAbsntList(newText);
                return false;
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        getPrstAbsntList("");
    }

    public void getPrstAbsntList(String query) {

        RequestQueue MyRequestQueue = Volley.newRequestQueue(getContext());
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, batch_attendence, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response_present", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        present_recycler.setVisibility(View.VISIBLE);
                        JSONArray present_array = jObj.getJSONArray("present_users");
                        Log.e("present", present_array + "");

                        present_members.setText(present_array.length() + " " + "Members");

                        prsntAdapter = new PresentAdapter(getContext(), present_array);
                        present_recycler.setAdapter(prsntAdapter);


//                        Toast.makeText(AddUserActivity.this, "Congrats Shift is added successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        present_recycler.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "No result found!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("batch_id", batch_id);
                params.put("keyword", query);
                params.put("attendance_id", id);
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }
}
