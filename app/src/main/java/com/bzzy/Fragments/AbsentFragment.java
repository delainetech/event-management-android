package com.bzzy.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.PrstAbstAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class AbsentFragment extends Fragment implements ApiCalls {

    RecyclerView absent_recycler;
    PrstAbstAdapter abstAdapter;
    JSONObject object1;
    TextView absent_member, error_text;
    String batch_id, id;

    public AbsentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_absent, container, false);

        error_text = (TextView) view.findViewById(R.id.error_text);
        absent_recycler = (RecyclerView) view.findViewById(R.id.absent_recycler);
        absent_member = (TextView) view.findViewById(R.id.absent_members);
        absent_recycler.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        absent_recycler.setLayoutManager(horizontalLayoutManager);
// Intent data
        batch_id = getArguments().getString("batch_id");
        id=getArguments().getString("attendance_id");
        Log.e("Batch_id", batch_id + "");

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        getPrstAbsntList("");
    }

    public void getPrstAbsntList(String query) {

        RequestQueue MyRequestQueue = Volley.newRequestQueue(getContext());
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, batch_attendence, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response_absent", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        absent_recycler.setVisibility(View.VISIBLE);
                        JSONArray absent_array = jObj.getJSONArray("absent_users");
                        Log.e("present", absent_array + "");

                        absent_member.setText(absent_array.length() + " " + "Members");

                        abstAdapter = new PrstAbstAdapter(getContext(), absent_array);
                        absent_recycler.setAdapter(abstAdapter);

//                        Toast.makeText(AddUserActivity.this, "Congrats Shift is added successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        absent_recycler.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "No result found!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("batch_id", batch_id);
                params.put("keyword", query);
                params.put("attendance_id",id);
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }
}
