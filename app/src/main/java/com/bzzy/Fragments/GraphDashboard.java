//package com.sendcargo.Fragment;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.Dialog;
//import android.app.ProgressDialog;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.SharedPreferences;
//import android.database.Cursor;
//import android.graphics.Bitmap;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.media.ThumbnailUtils;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.os.Binder;
//import android.os.Bundle;
//import android.os.Environment;
//import android.os.StrictMode;
//import android.provider.MediaStore;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.DialogFragment;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentManager;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.Toast;
//import android.widget.VideoView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.NetworkError;
//import com.android.volley.NetworkResponse;
//import com.android.volley.NoConnectionError;
//import com.android.volley.ParseError;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.RetryPolicy;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.theartofdev.edmodo.cropper.CropImage;
//import com.theartofdev.edmodo.cropper.CropImageView;
//
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.URISyntaxException;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.HashMap;
//import java.util.Locale;
//import java.util.Map;
//import java.util.regex.Pattern;
//
//import droidninja.filepicker.FilePickerBuilder;
//import droidninja.filepicker.FilePickerConst;
//
//import static android.app.Activity.RESULT_OK;
//import static com.facebook.FacebookSdk.getApplicationContext;
//import static com.sendcargo.Util.ParamsKeys.Host;
//import static com.sendcargo.XMPP_service.XmppService.configure;
//
//public class ChatWindowFragment extends Fragment implements View.OnClickListener {
//
//    protected FragmentActivity mActivity;
//    DelayedProgressDialog pro;
//    int count=0;
//    boolean isresume=false;
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        mActivity = (FragmentActivity) activity;
//    }
//    RecyclerView recyclerview;
//    String videopath="";
//    ChatAdapter adapter;
//    public ImageView ivimage, ivsendButton, ivback;
//    Dialog mDialog,dialog;
//    LinearLayoutManager layoutManager;
//
//    ImageView pickbutton,sendbutton;
//    byte[] inputData=null;
//    EditText messageArea;
//    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
//    UserSharedPreferences user;
//    ArrayList<Chats_msg> chats_msgArrayList=new ArrayList<>();
//    int index=0;
//    Uri outputFileUri;
//    Bitmap bitmap;
//    File f;
//
//    ProgressDialog progressDialog;
//    ImageView play,ivsend;
//    VideoView videoView;
//    //xmpp chat
//
//    ArrayList<String> docPaths;
//    org.jivesoftware.smack.Chat chat;
//    Bitmap thumbnail;
//    ArrayList<String> staffuser=new ArrayList<>();
//    ImageView imageView;
//    View view;
//    public  XMPPConnection connection;
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        view=inflater.inflate(R.layout.chat_window,container,false);
//        mActivity=getActivity();
//        ;
//
//        user=new UserSharedPreferences(getActivity());
//        pro=new DelayedProgressDialog();
//
//
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//
//        isresume=false;
//
//
//        pickbutton=view.findViewById(R.id.pickbutton);
//        sendbutton=view.findViewById(R.id.sendButton);
//        recyclerview=view.findViewById(R.id.recyclerview);
//        layoutManager=new LinearLayoutManager(getActivity());
//        recyclerview.setLayoutManager(layoutManager);
//        messageArea=view.findViewById(R.id.messageArea);
//        imageView=view.findViewById(R.id.imageView);
//        adapter=new ChatAdapter(chats_msgArrayList,getActivity(),ChatWindowFragment.this);
//        recyclerview.setAdapter(adapter);
//        layoutManager.setStackFromEnd(true);
//        sendbutton.setOnClickListener(this);
//        pickbutton.setOnClickListener(this);
//
//
//
//
//        if (XmppService.connection!=null && !XmppService.connection.isConnected()){
//
//            Log.e( "onCreateView: ", "is connted");
//            getActivity().startService(new Intent(getActivity(),XmppService.class));
//        }else {
//            Log.e( "onCreateView: ","isConnected()+");
//        }
//
//        chat = XmppService.connection.getChatManager().
//                createChat("groupscstaff" + "@" + Host, null);
//
//        Log.e( "onCreateView: ","groupscstaff" + "@" + Host );
//
//        String username="groupscstaff" + "@" + Host;
//
//        user.setnoti(true);
//        getchathistory(getActivity(),index);
//        endlessRecyclerViewScrollListener=new EndlessRecyclerViewScrollListener(layoutManager) {
//            @Override
//            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//                if (chats_msgArrayList.size()%10==0){
//                    index++;
//                    Log.e( "onLoadMore: ",index+"" );
//                    getchathistory(getActivity(),index);
//                }
//            }
//        };
//
//        recyclerview.addOnScrollListener(endlessRecyclerViewScrollListener);
//
//
//        return view;
//    }
//
//
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()){
//
//            case R.id.sendButton:
//
//                if (messageArea.getText().length()>0) {
//
//                    settextmsg(messageArea.getText().toString(), "text","");
//
//                }
//
//                break;
//            case R.id.pickbutton:
//
//                openGellery();
//
//                break;
//
//
//        }
//
//
//    }
//
//    private void openGellery() {
//
//        final CharSequence[] items = {
//                "Choose from Image","Choose from Video","Other file's"};
//
//
//        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
//        builder.setTitle("Add Photo,Video and file's!");
//
//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @SuppressLint("RestrictedApi")
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                switch (which) {
//
//
//
//
//                    case 0:
//                        Opengallery();
//                        break;
//
//
//                    case 1:
//                        OpenVideo();
//                        break;
//
//                    case 2:
//                        openFilePicker();
//
//                        break;
//
//                }
//
//
//                dialog.dismiss();
//
//
//            }
//
//
//        });
//
//        builder.show();
//    }
//
//    private void OpenVideo() {
//        Intent intent = new Intent();
//        intent.setType("video/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Video"), 3);
//    }
//
//    private void Opengallery() {
//
////        Intent intent = new Intent();
////        intent.setType("image/*");
////        intent.setAction(Intent.ACTION_GET_CONTENT);
////        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
//        CropImage.activity()
//                .setGuidelines(CropImageView.Guidelines.ON)
//                .start(getContext(),this);
//
//    }
//
//    private void openCamera() {
//
//        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(cameraIntent, 0);
//    }
//
//    private void openFilePicker() {
//
//        FilePickerBuilder.getInstance().setMaxCount(1)
//
//                .setActivityTheme(R.style.LibAppTheme)
//                .pickFile(this,2);
//
//
//    }
//
//    private void settextmsg(String text,String type,String thumbnail){
//
//        try {
//            JSONObject object = new JSONObject();
//            object.put("msg", text);
//            object.put("type", type);
//            object.put("timestamp", (System.currentTimeMillis())+"");
//            object.put("sender_name", user.getname());
//            object.put("senderid", user.getuserId());
//            object.put("user_image",user.getimage());
//            object.put("message_type","client");
//
////            if (type.equalsIgnoreCase("video")){
//
//            object.put("video_thumb",thumbnail);
////            }
//
//
//            Log.e( "user image: ",user.getimage() );
//            Log.e( "settextmsg: connection",XmppService.connection.isConnected()+"" );
//            Message msg = new Message();
//            object.put("msg_id", msg.getPacketID());
//
//
//            Log.e("settextmsg:timestamp ",object.toString() );
//            msg.setBody(object.toString());
//            Log.e("chat",chat+"");
//            chat.sendMessage(msg);
//            if (type.equals("text")) {
//                chats_msgArrayList.add(new Chats_msg(text, "1", (System.currentTimeMillis()) + "",
//                        msg.getPacketID(),"image","",user.getimage(),user.getname()));
//                recyclerview.scrollToPosition(chats_msgArrayList.size()-1);
//            }else if (type.equals("document")){
//                chats_msgArrayList.add(new Chats_msg(text, "5", (System.currentTimeMillis()) + "",
//                        msg.getPacketID(),"document","",user.getimage(),user.getname()));
//                recyclerview.scrollToPosition(chats_msgArrayList.size()-1);
//
//            }
//            else if (type.equals("video")){
//                chats_msgArrayList.add(new Chats_msg(text, "7", (System.currentTimeMillis()) + "",
//                        msg.getPacketID(),"video",thumbnail,user.getimage(),user.getname()));
//                recyclerview.scrollToPosition(chats_msgArrayList.size()-1);
//
//            }
//            else {
//                chats_msgArrayList.add(new Chats_msg(text, "3", (System.currentTimeMillis()) + "",
//                        msg.getPacketID(),"image","",user.getimage(),user.getname()));
//                recyclerview.scrollToPosition(chats_msgArrayList.size()-1);
//
//            }
//            Collections.sort(chats_msgArrayList,new compare());
//            adapter.notifyDataSetChanged();
//
//            Log.e( "settextmsg: ","message send once" );
//            messageArea.setText("");
//
//
//        }catch (Exception e){
//
//            if (XmppService.connection!=null && !XmppService.connection.isConnected()){
//
////                getActivity().stopService(new Intent(getActivity(),XmppService.class));
//                Log.e( "onCreateView: ", "stop");
//                try {
//                    XmppService.connection.connect();
//
//                } catch (XMPPException e1) {
//                    Log.e( "settextmsg: ",e1+"" );
//                    e1.printStackTrace();
//                }
////                getActivity().startService(new Intent(getActivity(),XmppService.class));
//                Log.e( "settextmsg: ","start" );
//            }else {
//                Log.e( "onCreateView: ","isConnected()+");
//            }
//            Log.e("jsonexp","e",e);
//
////            Toast.makeText(getActivity(), "Please try after sometime", Toast.LENGTH_SHORT).show();
//
//        }
//    }
//
//
//    @Override
//    public void onPause() {
//
//        super.onPause();
//        isresume=true;
//        user.setnoti(false);
//        Log.e( "onPause: ",isresume+"" );
//        Log.e( "onPause: ",count+"" );
//        count=0;
//        getActivity().unregisterReceiver(receiver);
//
//    }
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent result) {
//        super.onActivityResult(requestCode, resultCode, result);
//        Log.e("requestCode",requestCode+"");
//        Log.e("resultCode",resultCode+"");
//        switch (requestCode) {
//
//            case 0:
//
//                break;
//            case 1:
//                if (resultCode == RESULT_OK) {
//                    outputFileUri = result.getData();
//
//
//                    if ((null == result) || (result.getData() == null)){
//                        new ImageCompressionAsyncTask(getActivity()).execute(outputFileUri.toString(),
//                                Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+getActivity().getPackageName()+"/media/images");
//                    }
//                    else {
//                        new ImageCompressionAsyncTask(getActivity()).execute(result.toString(),
//                                Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+getActivity().getPackageName()+"/media/images");
//                    }
////                    set_Image(outputFileUri);
//                }
//                break;
//
//            case 2:
//
//                if (resultCode==RESULT_OK){
//
//                    docPaths = new ArrayList<>();
//                    docPaths.addAll(result.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
//                    Log.e("filepath", docPaths.get(0));
//                    outputFileUri=Uri.fromFile(new File(docPaths.get(0)));
//                    Log.e( "onActivityResult: ",outputFileUri.toString() );
////                    sendFile(docPaths.get(0));
//                    if ((null == result) || (result.getData() == null)) {
//                        new DocumentAsyncTask(getActivity()).execute(outputFileUri.toString(),
//                                Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getActivity().getPackageName() + "/media/document");
//
//                    } else {
//                        new DocumentAsyncTask(getActivity()).execute(result.toString(),
//                                Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+getActivity().getPackageName()+"/media/document");
//
//
//                    }
//
//
//                }
//                break;
//            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
//
//
//                Log.e( "onActivityResult: ",CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE+"" );
//                CropImage.ActivityResult results = CropImage.getActivityResult(result);
//                if (resultCode == RESULT_OK) {
//                    outputFileUri = results.getUri();
//                    //poupup
//
//                    if ((null == result) || (result.getData() == null)){
//
//                        Log.e("outputstrim",outputFileUri+"");
//                        new ImageCompressionAsyncTask(getActivity()).execute(outputFileUri.toString(),
//                                Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+getActivity().getPackageName()+"/media/images");
//                    }
//                    else {
//                        new ImageCompressionAsyncTask(getActivity()).execute(result.toString(),
//                                Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+getActivity().getPackageName()+"/media/images");
//                    }
//
//
//
//                    Log.e("arraybit",inputData+"");
//                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                    Exception error = results.getError();
//                }
//                break;
//
//            case 3:
//
//                if (resultCode == RESULT_OK){
//
//                    Log.e( "onActivityResult: ",result.getData()+"" );
//
//                    outputFileUri=result.getData();
//                    thumbnail = ThumbnailUtils.createVideoThumbnail(getPath(outputFileUri), MediaStore.Images.Thumbnails.FULL_SCREEN_KIND);
//
//                    MediaDialog(thumbnail);
//
//                }
//
//
//
//                break;
//
//
//        }
//
//    }
//    public String getPath(Uri uri) {
//        Cursor cursor =getActivity().getContentResolver().query(uri, null, null, null, null);
//        cursor.moveToFirst();
//        String document_id = cursor.getString(0);
//        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
//        cursor.close();
//
//        cursor = getActivity().getContentResolver().query(
//                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
//                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
//        Log.e("cursercounty",cursor.getCount()+"");
//        cursor.moveToFirst();
//
//        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
//        cursor.close();
//
//        return path;
//    }
//
//    private void MediaDialog(Bitmap outputbitmap) {
//
//
//
//        dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
//        dialog.setContentView(R.layout.mediadialoglayout);
//        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//
//
//        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
//        final ImageView show_video=(ImageView)dialog.findViewById(R.id.show_video);
//
//        ivsend=(ImageView)dialog.findViewById(R.id.ivsend);
//        show_video.setImageBitmap(outputbitmap);
//        play=(ImageView)dialog.findViewById(R.id.play);
//        videoView=(VideoView)dialog.findViewById(R.id.videoview);
//
//
//        play.setVisibility(View.VISIBLE);
//        play.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                videoView.setVisibility(View.VISIBLE);
//                play.setVisibility(View.GONE);
//                show_video.setVisibility(View.GONE);
//                videoView.setVideoURI(outputFileUri);
//                videoView.start();
////                    Intent i=new Intent(c, PlayVideo.class);
////                    i.putExtra("url", media.get(position).getVideopath());
////                    c.startActivity(i);
//            }
//        });
//        iv_close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        ivsend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                //create destination directory
//                f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getActivity().getPackageName() + "/media/videos");
//                if (f.mkdirs() || f.isDirectory())
//                    Log.e("path",f.getPath());
//                //compress and output new video specs
//
////                pro.show(getFragmentManager(),"");
//
//                new VideoCompressionAsyncTask(getActivity()).execute(outputFileUri.toString());
//
//
//
//            }
//        });
//
//        dialog.show();
//    }
//    private void set_Image(Uri ImageUri) {
//
//        try {
//            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver() , ImageUri );
//
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
//            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
//            scaled.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//            byte[] b = baos.toByteArray();
//
//
////            img1 = Base64.encodeToString(b, Base64.DEFAULT);
////            Log.e("base64",img1);
//
//            imageView.setImageURI(ImageUri);
//
//        }
//
//        catch (Exception e) {
//            Log.e("e","e",e);
//            e.printStackTrace();
//        }
//    }
//
//
//    public void sendimage(final byte[] inputData, final String type) {
//
//
//        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constant.base_url+"upload_image", new Response.Listener<NetworkResponse>() {
//            @Override
//            public void onResponse(NetworkResponse response) {
//                String resultResponse = new String(response.data);
//                pro.cancel();
//                Log.e("registr", resultResponse+"");
//                try {
//                    JSONObject object=new JSONObject(resultResponse);
//                    if (object.getString("message").equals("Successfully Upload Image")){
//
//                        object.getString("data");
//                        settextmsg(object.getString("data"),type,"");
//                    }
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Log.e("error","e",e);
//                    pro.cancel();
//
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                volleyError.printStackTrace();
//                pro.cancel();
//
//                String message = null;
//                if (volleyError instanceof NetworkError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof ServerError) {
//                    message = "The server could not be found. Please try again after some time!!";
//                } else if (volleyError instanceof AuthFailureError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof ParseError) {
//                    message = "Parsing error! Please try again after some time!!";
//                } else if (volleyError instanceof NoConnectionError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof TimeoutError) {
//                    message = "Connection TimeOut! Please check your internet connection.";
//                }
//                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                Map<String, String> headers = new HashMap<String, String>();
//
//                return headers;
//            }
//
//
//            @Override
//            protected Map<String, DataPart> getByteData() {
//                Map<String, DataPart> params = new HashMap<>();
//
//
//                if (outputFileUri !=null) {
//                    //Important convert your image into byte Array
//                    params.put("image", new DataPart(System.currentTimeMillis() +"image.png", inputData));
//
//                }
//                Log.e("bytparam",params+"");
//                return params;
//            }
//        };
//
//        MySingleton.getInstance(getActivity()).addToRequestQueue(multipartRequest);
//    }
//
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//
//        user.setnoti(false);
//
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        count++;
//        user.setnoti(true);
//        if (isresume){
//            index=0;
//            chats_msgArrayList.clear();
//            getchathistory(getActivity(),index);
//            isresume=false;
//            Log.e( "onResume: ",isresume+"" );
//        }else {
//
//            Log.e( "onResume: ","nhi chla" );
//        }
//
//
////        getchathistory(getActivity(),index);
//        if (XmppService.connection!=null && !XmppService.connection.isConnected()){
//            getActivity().startService(new Intent(getActivity(),XmppService.class));
//        }
//        getActivity().registerReceiver(receiver, new IntentFilter("xmpp"));
//
//
//    }
//    public Uri getImageUri(Context inContext, Bitmap inImage) {
//        String path = null;
//        try {
//            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//            path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//
//        }catch (Exception e){
//            Log.e("e","e",e);
//        }
//        return Uri.parse(path);
//    }
//
//
//
//
//    BroadcastReceiver receiver=new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, final Intent intent) {
//            getActivity().runOnUiThread(new Runnable() {
//                @SuppressLint("WrongConstant")
//                @Override
//                public void run() {
//                    Log.e("mesagesdata",intent.getStringExtra("mesagesdata"));
//                    if (intent.hasExtra("mesagesdata")) {
//                        try {
//                            JSONObject object = new JSONObject(intent.getStringExtra("mesagesdata"));
//
//                            if (object.getString("type").equals("text")) {
//                                chats_msgArrayList.add(new Chats_msg(object.getString("msg"), "2",
//                                        object.getString("timestamp"), object.getString("msg_id"),
//                                        "image","",object.getString("user_image"),
//                                        object.getString("sender_name")));
//                                recyclerview.scrollToPosition(chats_msgArrayList.size()-1);
//                            } else if(object.getString("type").equals("document")){
//                                chats_msgArrayList.add(new Chats_msg(object.getString("msg"), "6",
//                                        object.getString("timestamp"), object.getString("msg_id"),
//                                        "document","",object.getString("user_image"),object.getString("sender_name")));
//                                recyclerview.scrollToPosition(chats_msgArrayList.size()-1);
//
//                            }
//                            else {
//                                chats_msgArrayList.add(new Chats_msg(object.getString("msg"), "4",
//                                        object.getString("timestamp"), object.getString("msg_id")
//                                        ,"image","",object.getString("user_image"),object.getString("sender_name")));
//                                recyclerview.scrollToPosition(chats_msgArrayList.size()-1);
//
//                            }
//                            Collections.sort(chats_msgArrayList,new compare());
//                            adapter.notifyDataSetChanged();
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Log.e("exeption","e",e);
//                        }
//                    }
//                }});
//        }
//    };
//
//    public void next(){
//        index++;
//        getchathistory(getActivity(),index);
//    }
//
//
//
//
//    class ImageCompressionAsyncTask extends AsyncTask<String, Void, String> {
//
//        Context mContext;
//
//        public ImageCompressionAsyncTask(Context context){
//            mContext = context;
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//
//            String filePath = SiliCompressor.with(mContext).compress(params[0], new File(params[1]));
//            return filePath;
//
//
//            /*
//            Bitmap compressBitMap = null;
//            try {
//                compressBitMap = SiliCompressor.with(mContext).getCompressBitmap(params[0], true);
//                return compressBitMap;
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return compressBitMap;
//            */
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            /*
//            if (null != s){
//                imageView.setImageBitmap(s);
//                int compressHieght = s.getHeight();
//                int compressWidth = s.getWidth();
//                float length = s.getByteCount() / 1024f; // Size in KB;
//                String text = String.format("Name: %s\nSize: %fKB\nWidth: %d\nHeight: %d", "ff", length, compressWidth, compressHieght);
//                picDescription.setVisibility(View.VISIBLE);
//                picDescription.setText(text);
//            }
//            */
//            try {
//
//
//                File imageFile = new File(s);
//                outputFileUri = Uri.fromFile(imageFile);
//
//                InputStream iStream =   getActivity().getContentResolver().openInputStream(  outputFileUri);
//
//                inputData = getBytes(iStream);
//                Log.e("arraybit",inputData+"");
//
//
//                mDialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
//
//                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                WindowManager.LayoutParams wlmp = mDialog.getWindow().getAttributes();
//                wlmp.gravity = Gravity.CENTER;
//                wlmp.dimAmount = 0.0F;
//                mDialog.getWindow().setAttributes(wlmp);
//                mDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//                mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                mDialog.setCancelable(false);
//                mDialog.setContentView(R.layout.chat_share_image);
//                mDialog.show();
//
//                ivback = mDialog.findViewById(R.id.ivback);
//                ivimage = mDialog.findViewById(R.id.ivimage);
//                ivsendButton = mDialog.findViewById(R.id.ivsendButton);
//
//
//                ivimage.setImageURI(outputFileUri);
//
//// Picasso.with(getActivity()).load(outputFileUri).into(ivimage);
//
//
////                if ( poupup.getDialog() != null ){
////                    poupup.getDialog().setCanceledOnTouchOutside(false);
////                }
////                Bundle b=new Bundle();
////                b.putString("uri",outputFileUri+"");
////                poupup.setArguments(b);
////                FragmentManager frm = getActivity().getSupportFragmentManager();
////                poupup.show(frm,"");
//
//                ivsendButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        pro.show(getFragmentManager(),"");
//                        sendimage(inputData,"image");
//
//                        mDialog.dismiss();
//                        Log.e( "onClick: ","send image" );
//
//                    }
//                });
//
//                ivback.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mDialog.dismiss();
//                    }
//                });
//
//
//
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), outputFileUri);
//                //
//                String name = imageFile.getName();
//                float length = imageFile.length() / 1024f; // Size in KB
//                int compressWidth = bitmap.getWidth();
//                int compressHieght = bitmap.getHeight();
//                String text = String.format(Locale.US, "Name: %s\nSize: %fKB\nWidth: %d\nHeight: %d", name, length, compressWidth, compressHieght);
//
//            }
//            catch (IOException e) {
//                pro.dismiss();
//                e.printStackTrace();
//                Log.e("error","e",e);
//            }
//
//        }
//    }
//
//    class VideoCompressionAsyncTask extends AsyncTask<String, Void, String> {
//
//        Context mContext;
//
//        public VideoCompressionAsyncTask(Context context){
//            mContext = context;
//        }
//
//        @Override
//        protected void onPreExecute()
//        {
//            //Create a new progress dialog
//            progressDialog = new ProgressDialog(getActivity());
//            //Set the progress dialog to display a horizontal progress bar
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            //Set the dialog title to 'Loading...'
//            progressDialog.setTitle("Loading...");
//            //Set the dialog message to 'Loading application View, please wait...'
//            progressDialog.setMessage("Uploading Video, please wait...");
//            //This dialog can't be canceled by pressing the back key
////            progressDialog.setCancelable(false);
//            //This dialog isn't indeterminate
////            progressDialog.setIndeterminate(false);
//            //The maximum number of items is 100
////            progressDialog.setMax(100);
////            //Set the current progress to zero
////            progressDialog.setProgress(0);
//            //Display the progress dialog
//            progressDialog.show();
//        }
//
//
//        @Override
//        protected String doInBackground(String... params) {
//
//            String filePath = null;
//            try {
//
//                videopath = SiliCompressor.with(mContext).compressVideo(getPath(outputFileUri), f.getPath());
//
//            } catch (URISyntaxException e) {
//                e.printStackTrace();
//                Log.e("compressionerror","e",e);
//            }
//            return  videopath;
//
//        }
//
//
//
//
//        @Override
//        protected void onPostExecute(String s) {
//
//
//            try {
//
//
//                File imageFile = new File(s);
////                outputFileUri = Uri.fromFile(imageFile);
//
//                InputStream iStream =   getActivity().getContentResolver().openInputStream(  outputFileUri);
//
//                inputData = getBytes(iStream);
//                Log.e("arraybit",inputData+"");
//
//                String filename = outputFileUri.toString().
//                        substring(outputFileUri.toString().lastIndexOf("/")+1);
//
//                sendVideo(inputData,"video",thumbnail);
//
////                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), outputFileUri);
////                //
////                String name = imageFile.getName();
////                float length = imageFile.length() / 1024f; // Size in KB
////                int compressWidth = bitmap.getWidth();
////                int compressHieght = bitmap.getHeight();
////                String text = String.format(Locale.US, "Name: %s\nSize: %fKB\nWidth: %d\nHeight: %d", name, length, compressWidth, compressHieght);
//
//            }
//            catch (IOException e) {
//                pro.dismiss();
//                e.printStackTrace();
//                Log.e("error","e",e);
//            }
//
//        }
//    }
//    private void sendVideo(final byte[] inputData, final String type, final Bitmap thumbnail) {
//
//
////        pro=new DelayedProgressDialog();
////
////        pro.show(getFragmentManager(),"");
//
//        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constant.base_url+"upload_image", new Response.Listener<NetworkResponse>() {
//            @Override
//            public void onResponse(NetworkResponse response) {
//                String resultResponse = new String(response.data);
//                progressDialog.dismiss();
//                Log.e("document upload", resultResponse+"");
//                try {
//                    JSONObject object=new JSONObject(resultResponse);
//                    if (object.getString("message").equals("Successfully Upload Image")){
//
//                        object.getString("data");
//                        dialog.dismiss();
////                        sendThumbliln(object.getString("data"));
//                        settextmsg(object.getString("data"),type,object.getString("thumbnail"));
//                    }
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Log.e("error","e",e);
//                    progressDialog.dismiss();
//
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                volleyError.printStackTrace();
//                progressDialog.dismiss();
//
//                String message = null;
//                if (volleyError instanceof NetworkError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof ServerError) {
//                    message = "The server could not be found. Please try again after some time!!";
//                } else if (volleyError instanceof AuthFailureError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof ParseError) {
//                    message = "Parsing error! Please try again after some time!!";
//                } else if (volleyError instanceof NoConnectionError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof TimeoutError) {
//                    message = "Connection TimeOut! Please check your internet connection.";
//                }
//                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("type","video");
//
//                return params;
//            }
//
//
//            @Override
//            protected Map<String, DataPart> getByteData() {
//                Map<String, DataPart> params = new HashMap<>();
//
//
//                if (outputFileUri !=null) {
//                    //Important convert your image into byte Array
//
//                    params.put("image", new DataPart(System.currentTimeMillis() +"video.mp4", inputData));
////                    params.put("image1",new DataPart(System.currentTimeMillis() +"image.png",getFileDataFromDrawable(thumbnail)));
//                }
//                Log.e("bytparam",params.toString()+"");
//                return params;
//            }
//        };
//
//        MySingleton.getInstance(getActivity()).addToRequestQueue(multipartRequest);
//
//    }
//
//
//
//
//    class DocumentAsyncTask extends AsyncTask<String, Void, String> {
//
//        Context mContext;
//
//        public DocumentAsyncTask(Context context){
//            mContext = context;
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//
//            Log.e( "doInBackground: ",params[0]+"" );
//
//            String filePath = params[0];
//            return filePath;
//
//
//            /*
//            Bitmap compressBitMap = null;
//            try {
//                compressBitMap = SiliCompressor.with(mContext).getCompressBitmap(params[0], true);
//                return compressBitMap;
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return compressBitMap;
//            */
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            /*
//            if (null != s){
//                imageView.setImageBitmap(s);
//                int compressHieght = s.getHeight();
//                int compressWidth = s.getWidth();
//                float length = s.getByteCount() / 1024f; // Size in KB;
//                String text = String.format("Name: %s\nSize: %fKB\nWidth: %d\nHeight: %d", "ff", length, compressWidth, compressHieght);
//                picDescription.setVisibility(View.VISIBLE);
//                picDescription.setText(text);
//            }
//            */
//            try {
//
//
//                File imageFile = new File(s);
//                Log.e( "onPostExecute: ",imageFile+"      "+s );
////                outputFileUri = Uri.fromFile(imageFile);
//                Log.e("onPostExecute: ",outputFileUri+"" );
////                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver() , outputFileUri );
//
//
//                String filename = outputFileUri.toString().
//                        substring(outputFileUri.toString().lastIndexOf("/")+1);
//
//                Log.e( "filename: ",filename );
//                InputStream iStream =   getActivity().getContentResolver().openInputStream(  outputFileUri);
//
//                inputData = getBytes(iStream);
//                Log.e("arraybit",inputData+"");
//
//
////                mDialog = new Dialog(getActivity());
////                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
////                WindowManager.LayoutParams wlmp = mDialog.getWindow().getAttributes();
////                wlmp.gravity = Gravity.CENTER;
////                wlmp.dimAmount = 0.0F;
////                mDialog.getWindow().setAttributes(wlmp);
////                mDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
////                mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
////                mDialog.setCancelable(false);
////                mDialog.setContentView(R.layout.chat_share_image);
////                mDialog.show();
////
////                ivback = mDialog.findViewById(R.id.ivback);
////                ivimage = mDialog.findViewById(R.id.ivimage);
////                ivsendButton = mDialog.findViewById(R.id.ivsendButton);
////
////
////                ivimage.setImageURI(outputFileUri);
//
//// Picasso.with(getActivity()).load(outputFileUri).into(ivimage);
//
//
////                if ( poupup.getDialog() != null ){
////                    poupup.getDialog().setCanceledOnTouchOutside(false);
////                }
////                Bundle b=new Bundle();
////                b.putString("uri",outputFileUri+"");
////                poupup.setArguments(b);
////                FragmentManager frm = getActivity().getSupportFragmentManager();
////                poupup.show(frm,"");
////
////                ivsendButton.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////
////                        sendimage();
////
////                        mDialog.dismiss();
////                        Log.e( "onClick: ","send image" );
////
////                    }
////                });
////
////                ivback.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////                        mDialog.dismiss();
////                    }
////                });
//
//
//                sendDocument(inputData,"document",filename);
//
//
////                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), outputFileUri);
////                //
////                String name = imageFile.getName();
////                float length = imageFile.length() / 1024f; // Size in KB
////                int compressWidth = bitmap.getWidth();
////                int compressHieght = bitmap.getHeight();
////                String text = String.format(Locale.US, "Name: %s\nSize: %fKB\nWidth: %d\nHeight: %d", name, length, compressWidth, compressHieght);
//
//            }
//            catch (IOException e) {
//                e.printStackTrace();
//                Log.e("error","e",e);
//            }
//
//        }
//    }
//
//    private void sendDocument(final byte[] inputData, final String type,final String filename) {
//        final DelayedProgressDialog progressDialog=new DelayedProgressDialog();
//
//        progressDialog.show(getFragmentManager(),"");
//
//        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constant.base_url+"upload_image", new Response.Listener<NetworkResponse>() {
//            @Override
//            public void onResponse(NetworkResponse response) {
//                String resultResponse = new String(response.data);
//                progressDialog.cancel();
//                Log.e("document upload", resultResponse+"");
//                try {
//                    JSONObject object=new JSONObject(resultResponse);
//                    if (object.getString("message").equals("Successfully Upload Image")){
//
//                        object.getString("data");
//                        settextmsg(object.getString("data"),type,"");
//                    }
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Log.e("error","e",e);
//                    progressDialog.cancel();
//
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                volleyError.printStackTrace();
//                progressDialog.cancel();
//
//                String message = null;
//                if (volleyError instanceof NetworkError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof ServerError) {
//                    message = "The server could not be found. Please try again after some time!!";
//                } else if (volleyError instanceof AuthFailureError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof ParseError) {
//                    message = "Parsing error! Please try again after some time!!";
//                } else if (volleyError instanceof NoConnectionError) {
//                    message = "Cannot connect to Internet...Please check your connection!";
//                } else if (volleyError instanceof TimeoutError) {
//                    message = "Connection TimeOut! Please check your internet connection.";
//                }
//                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                Map<String, String> headers = new HashMap<String, String>();
//
//                return headers;
//            }
//
//
//
//
//            @Override
//            protected Map<String, DataPart> getByteData() {
//                Map<String, DataPart> params = new HashMap<>();
//
//
//                if (outputFileUri !=null) {
//                    //Important convert your image into byte Array
//                    Log.e( "getByteData: ",filename );
//                    params.put("image", new DataPart(filename, inputData));
//
//                }
//                Log.e("bytparam",params.toString()+"");
//                return params;
//            }
//        };
//
//        MySingleton.getInstance(getActivity()).addToRequestQueue(multipartRequest);
//    }
//
//
//    public byte[] getBytes(InputStream inputStream) throws IOException {
//        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
//        int bufferSize = 1024;
//        byte[] buffer = new byte[bufferSize];
//
//        int len = 0;
//        while ((len = inputStream.read(buffer)) != -1) {
//            byteBuffer.write(buffer, 0, len);
//        }
//        return byteBuffer.toByteArray();
//    }
//
//    private void getchathistory(final Context mcontext , final int index)
//    {
//
//        Log.e( "getchathistory: ",index+"" );
////    chats_msgArrayList.clear();
////        final DelayedProgressDialog pro=new DelayedProgressDialog();
////
////        pro.show(getSupportFragmentManager(),"");
//
//        final StringRequest request = new StringRequest(Request.Method.POST, Constant.base_url+ "private-messages", new Response.Listener<String>() {
//            @Override
//            public void onResponse(String s) {
////                pro.cancel();
//                Log.e("requestresponse", s);
//                try {
//
//                    JSONObject object1 = new JSONObject(s);
//                    if (object1.getString("message").equals("true")){
//
//
//                        JSONArray jsonArray=object1.getJSONArray("data");
//
//
//                        for (int i=0;i<jsonArray.length();i++){
//                            JSONObject object2=jsonArray.getJSONObject(i);
//                            JSONObject object = new JSONObject(object2.getString("txt"));
//
//                            if (object.getString("senderid").equals(user.getuserId())) {
//                                if (object.getString("type").equals("text")) {
//                                    chats_msgArrayList.add(0,new Chats_msg(object.getString("msg"), "1",
//                                            object.getString("timestamp"), object.getString("msg_id")
//                                            ,"image","",object.getString("user_image")
//                                            ,object.getString("sender_name")));
//                                    recyclerview.scrollToPosition(chats_msgArrayList.size() - 1);
//                                } else if (object.getString("type").equals("document")){
//                                    chats_msgArrayList.add(new Chats_msg(object.getString("msg"), "5",
//                                            object.getString("timestamp"),
//                                            object.getString("msg_id"),"document","",
//                                            object.getString("user_image"),object.getString("sender_name")));
//
//                                    recyclerview.scrollToPosition(chats_msgArrayList.size()-1);
//                                }else if (object.getString("type").equals("video")){
//                                    chats_msgArrayList.add(new Chats_msg(object.getString("msg"), "7",
//                                            object.getString("timestamp"),
//                                            object.getString("msg_id"),"video",
//                                            object.getString("video_thumb"),
//                                            object.getString("user_image"),object.getString("sender_name")));
//
//                                    recyclerview.scrollToPosition(chats_msgArrayList.size()-1);
//                                }
//
////                                else {
////
////                                    Log.e( "onResponse: ",object.getString("type") );
////                                }
//                                else if (object.getString("type").equalsIgnoreCase("image")){
//                                    chats_msgArrayList.add(0,new Chats_msg(object.getString("msg"), "3",
//                                            object.getString("timestamp"), object.getString("msg_id"),"image",
//                                            "",object.getString("user_image"),object.getString("sender_name")));
//                                    recyclerview.scrollToPosition(chats_msgArrayList.size() - 1);
//
//                                }
//                            }else {
//                                if (object.getString("type").equals("text")) {
//                                    chats_msgArrayList.add(0,new Chats_msg(object.getString("msg"), "2",
//                                            object.getString("timestamp"), object.getString("msg_id")
//                                            ,"image","",object.getString("user_image"),object.getString("sender_name")));
//                                    recyclerview.scrollToPosition(chats_msgArrayList.size() - 1);
//                                } else if (object.getString("type").equals("document")){
//                                    chats_msgArrayList.add(new Chats_msg(object.getString("msg"), "6",
//                                            object.getString("timestamp"),
//                                            object.getString("msg_id"),"document","",object.getString("user_image"),object.getString("sender_name")));
//
//                                    recyclerview.scrollToPosition(chats_msgArrayList.size()-1);
//                                }else if (object.getString("type").equals("video")){
//                                    chats_msgArrayList.add(new Chats_msg(object.getString("msg"), "8",
//                                            object.getString("timestamp"),
//                                            object.getString("msg_id"),"video",object.getString("video_thumb"),
//                                            object.getString("user_image"),object.getString("sender_name")));
//
//                                    recyclerview.scrollToPosition(chats_msgArrayList.size()-1);
//                                }  else {
//                                    chats_msgArrayList.add(0,new Chats_msg(object.getString("msg"), "4",
//                                            object.getString("timestamp"), object.getString("msg_id")
//                                            ,"image","",object.getString("user_image"),
//                                            object.getString("sender_name")));
//                                    recyclerview.scrollToPosition(chats_msgArrayList.size() - 1);
//
//                                }
//                            }
//                        }
//                        Collections.sort(chats_msgArrayList,new compare());
//
//                        Log.e("lastitem",chats_msgArrayList.get(chats_msgArrayList.size()-1).getMessages());
//                        adapter.notifyDataSetChanged();
//
//                    }
//
//                }
//                catch (JSONException e)
//                {
//                    e.printStackTrace();
//                    Log.e("e", "e", e);
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//
//                Log.e("error", volleyError.getMessage() + "e");
////                pro.cancel();
//
//            }
//        })
//
//        {
//            @Override
//            protected Map<String, String> getParams() {
//                //set post params
//                Map<String, String> params = new HashMap<>();
//                params.put("from",user.getuserId()+"scuser");
//                params.put("to","groupscstaff");
//                params.put("index",index+"");
//
//
//                Log.e("msgparams",params+"");
//                return params;
//            }
//
//        };
//        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        MySingleton.getInstance(mcontext).addToRequestQueue(request);
//    }
//
//
//    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
//        return byteArrayOutputStream.toByteArray();
//    }
//
//
//
//    class compare implements Comparator<Chats_msg> {
//
//        @Override
//        public int compare(Chats_msg lhs, Chats_msg rhs) {
//
//            return lhs.getTimestamp().compareTo(rhs.getTimestamp());
//
//        }
//    }
//
//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//    }
//}