package com.bzzy.SignInProcess;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment implements ApiCalls, View.OnClickListener {

    TextView submit;
    View view;
    EditText name, phone, email, city;
    ProgressDialog pDialog;
    TextView phone_no;
    RequestQueue MyRequestQueue;
    StringRequest MyStringRequest;

    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        initialization();

        return view;
    }

    public void initialization() {
        submit = (TextView) view.findViewById(R.id.submit_button);
        submit.setOnClickListener(this);
        name = (EditText) view.findViewById(R.id.enter_name);
        phone = (EditText) view.findViewById(R.id.enter_phone);
        email = (EditText) view.findViewById(R.id.enter_email);
        city = (EditText) view.findViewById(R.id.enter_city);
    }

    @Override
    public void onClick(View v) {
        if (v == submit) {
            validations();
        }
    }

    // Validations of EditText data
    public void validations() {

        if (city.getText().toString().trim().equalsIgnoreCase("") ||
                email.getText().toString().trim().equalsIgnoreCase("") ||
                name.getText().toString().trim().equalsIgnoreCase("") ||
                phone.getText().toString().trim().equalsIgnoreCase("")) {

            name.setError("All fields can't be blank.");

        } else if (name.getText().toString().trim().equalsIgnoreCase("")) {
            name.setError("Name field can't be blank.");
        } else if (email.getText().toString().trim().equalsIgnoreCase("")) {
            email.setError("Name field can't be blank.");
        } else if (!isValidEmail(email.getText().toString().trim())) {
            email.setError("Email is not valid");
            // Toast.makeText(getContext(), "Enter registered email", T
        } else if (!isValidMobile(phone.getText().toString().trim())) {
            email.setError("Mobile no. is Invalid!");
            // Toast.makeText(getContext(), "Enter registered email", T
        } else if (phone.getText().toString().trim().equalsIgnoreCase("")) {
            phone.setError("Name field can't be blank.");
        } else if (city.getText().toString().trim().equalsIgnoreCase("")) {
            city.setError("Name field can't be blank.");
        } else {
            signUp();
        }
    }


    private boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    // Check Email validations
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    //     POST api SignUp method
    public void signUp() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.show();
        MyRequestQueue = Volley.newRequestQueue(getContext());
         MyStringRequest = new StringRequest(Request.Method.POST, sign_up, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                pDialog.dismiss();  //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        JSONObject object = jObj.getJSONObject("data");
                        // Launch login activity

                        name.getText().clear();
                        email.getText().clear();
                        phone.getText().clear();
                        city.getText().clear();

                        ViewDialog alert = new ViewDialog();
                        alert.showDialog(getActivity());
                    } else {
                        Toast.makeText(getContext(), "Sorry! Email is already registered with us.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("email", email.getText().toString().trim());
                params.put("name", name.getText().toString().trim());
                params.put("phone", phone.getText().toString().trim());
                params.put("city", city.getText().toString().trim());

                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public class ViewDialog {

        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog);

            phone_no=(TextView) dialog.findViewById(R.id.text);

            setPhoneNo();

            Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Login_screen login_screen = new Login_screen();
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.main_screen_frame, login_screen).commit();
                }
            });
            dialog.show();
        }
    }

    public void setPhoneNo() {
        MyRequestQueue = Volley.newRequestQueue(getContext());
        MyStringRequest = new StringRequest(Request.Method.POST, contact_us, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
           //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        JSONArray array = jObj.getJSONArray("data");

                        for (int i=0;i<array.length();i++){
                            JSONObject object=array.getJSONObject(i);
//                            phone_no.setText("Please call us on "+ object.getString("phone"));
                        }
                    } else {
                        Toast.makeText(getContext(), "No contact found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

}
