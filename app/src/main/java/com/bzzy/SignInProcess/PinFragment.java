package com.bzzy.SignInProcess;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.AdminProfile;
import com.bzzy.Activities.Dashboard;
import com.bzzy.Activities.PaymentTransaction;
import com.bzzy.Adapters.EventDueDateAdapter;
import com.bzzy.Adapters.NotificationAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.philio.pinentry.PinEntryView;

/**
 * A simple {@link Fragment} subclass.
 */
public class PinFragment extends Fragment implements ApiCalls, View.OnClickListener {

    TextView login, username, forgotpin;
    ProgressDialog pDialog;
    View view;

    UserSharedPreferences sharedPreferences;
    EditText pin1, pin2, pin3, pin4;
    Dialog dialog,updatedialog;
    Spinner event_spin;
    ArrayList<String> eventName = new ArrayList<>();
    ArrayAdapter<String> adapter;
    String event_id = null;
    TextView notification_text;
    ImageView notification_image;
    RecyclerView notification_recycler,eventduedate_recycler;
    private PinEntryView pinEntryView;
    String pin_enter = null,renew_date="";
    ProgressBar progressBar;
    TextView event_name;

    RequestQueue MyRequestQueue;   StringRequest MyStringRequest;

    public PinFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_pin, container, false);

        sharedPreferences = new UserSharedPreferences(getContext());

        initialization();
        return view;
    }

    public void initialization() {


        notification_recycler = (RecyclerView) view.findViewById(R.id.notification_recycler);
        eventduedate_recycler=(RecyclerView)view.findViewById(R.id.eventduedate_recycler);

        login = (TextView) view.findViewById(R.id.login);
        login.setOnClickListener(this);
        event_spin = (Spinner) view.findViewById(R.id.group_spinner);
        forgotpin = (TextView) view.findViewById(R.id.forgot_pin);
        forgotpin.setOnClickListener(this);
//        notification_image = view.findViewById(R.id.notification_image);
        notification_text = view.findViewById(R.id.notification_text);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        pinEntryView = (PinEntryView) view.findViewById(R.id.pin_entry_simple);
        event_name= (TextView) view.findViewById(R.id.event_name);
        pinEntryView.setOnPinEnteredListener(new PinEntryView.OnPinEnteredListener() {
            @Override
            public void onPinEntered(String pin) {
                pin_enter = pin;
//                Toast.makeText(getContext(), "Pin entered: " + pin_enter, Toast.LENGTH_LONG).show();
            }
        });

        username = (TextView) view.findViewById(R.id.user_name);

        if (!sharedPreferences.getname().equalsIgnoreCase(null) || sharedPreferences.getname().length() > 4) {
            username.setText("Welcome" + ", " + sharedPreferences.getname());
        } else username.setText("Welcome, User");

//        pinEnter();

        notification_recycler.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        notification_recycler.setLayoutManager(horizontalLayoutManager3);

        eventduedate_recycler.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager horizontalLayoutManager4 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        eventduedate_recycler.setLayoutManager(horizontalLayoutManager4);

    }

    @Override
    public void onResume() {
        super.onResume();
        statusEmail();
    }

    // Click Events ...........................
    @Override
    public void onClick(View v) {
        if (v == login) {
            validationPin();
        } else if (v == forgotpin) {
            ViewDialog alert = new ViewDialog();
            alert.showDialog(getActivity());
        }
    }

    public void validationPin() {

        if (pin_enter == null) {
            Toast.makeText(getContext(), "Please check PIN and Event name", Toast.LENGTH_SHORT).show();
        } else if (event_id == null) {
            Toast.makeText(getContext(), "Please select Event!", Toast.LENGTH_SHORT).show();
        } else {
            loginApp();

        }
    }

    //         POST api logIn method

    public void loginApp() {
        progressBar.setVisibility(View.VISIBLE);
         MyRequestQueue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));
         MyStringRequest = new StringRequest(Request.Method.POST, login_pin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                 //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar.setVisibility(View.GONE);
                        JSONObject object = jObj.getJSONObject("data");
                        sharedPreferences.seteventId(jObj.getString("event_id"));
                        sharedPreferences.setname(object.getString("name"));
                        sharedPreferences.setimage(object.getString("image"));
                        sharedPreferences.setEventName(jObj.getString("event_name"));
                        renew_date=jObj.getString("renew_date");
                        Log.e("event_name", jObj.getString("event_name") + "");
                        sharedPreferences.setMasterPin(object.getString("master_pin"));
                        sharedPreferences.setusernumber(object.getString("phone"));

                        sharedPreferences.setSelectedevent(event_spin.getSelectedItemPosition()+"");
                        Log.e("position",event_spin.getSelectedItemPosition()+"");
                        Log.e("selected save",sharedPreferences.getSelectedevent());
                        // Launch login activity
                        Intent intent = new Intent(getContext(), Dashboard.class);
                        intent.putExtra("renew_date",renew_date);
                        startActivity(intent);
                        Objects.requireNonNull(getActivity()).finish();

                    } else {
                        if (jObj.getString("type").equalsIgnoreCase("subscription ended")){

                            RenewDialog alert = new RenewDialog();
                            alert.showDialog(getActivity());
                        }
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), jObj.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("email", sharedPreferences.getemail());
                params.put("pin", pin_enter + "");
                params.put("event_id", event_id);

                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }


    // update dialog

    public void UpdateDialog(){

        updatedialog = new Dialog(Objects.requireNonNull(getActivity()));
        updatedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        updatedialog.setCanceledOnTouchOutside(false);
        updatedialog.setCancelable(true);
        WindowManager.LayoutParams wlmp = Objects.requireNonNull(updatedialog.getWindow()).getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.dimAmount = 0.0F;
        updatedialog.getWindow().setAttributes(wlmp);
        updatedialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        updatedialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        updatedialog.getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 1.00);
        int screenHeight = (int) (metrics.heightPixels * 1.00);
        updatedialog.getWindow().setLayout(screenWidth, screenHeight);
        updatedialog.setContentView(R.layout.update_app_pop);

        TextView tv_cancel=updatedialog.findViewById(R.id.tv_cancel);
        TextView tv_update=updatedialog.findViewById(R.id.tv_update);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatedialog.dismiss();
            }
        });

        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.bzzy")));
            }
        });

        updatedialog.show();

    }

    @Override
    public void onPause() {
        super.onPause();
        if ((updatedialog != null) && updatedialog.isShowing())
            updatedialog.dismiss();
    }

    // Forgot View and Dialog
    public class ViewDialog {
        void showDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.forgot_dialog);


            EditText enter_email = (EditText) dialog.findViewById(R.id.enter_email);

            Button dialogButton = (Button) dialog.findViewById(R.id.submit);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (enter_email.getText().toString().trim().equalsIgnoreCase("")) {
                        enter_email.setError("Enter valid email");
                    } else if (!isValidEmail(enter_email.getText().toString().trim())) {
                        enter_email.setError("Email is Invalid!");
                    } else {
                        forgotPIN();
                    }
                }
            });
            dialog.show();
        }
    }




    //         POST api logIn method
    public void forgotPIN() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.show();
         MyRequestQueue = Volley.newRequestQueue(getContext());
         MyStringRequest = new StringRequest(Request.Method.POST, forgot_pin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                pDialog.dismiss();  //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
//                        JSONObject object=jObj.getJSONObject("data");
                        // Launch login activity
                        Toast.makeText(getContext(), "PIN successfully sent to your email.", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getContext(), "Sorry! no email found!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", sharedPreferences.getemail());
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    // Check Email validations
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    Double feess=0.0;
    Double discount=0.0;
    Double total_amount=0.0;
    EditText fees, city;
    Spinner time_spinner,discount_spinner;
    ImageView partner_image, renew_partner;
    TextView tvamount;
    EditText no_discount;
    TextView text2;
    JSONObject object,object1;
    ArrayList<String> time = new ArrayList<>();
    ArrayList<String> monthINT = new ArrayList<>();
    ArrayList<String> disList = new ArrayList<>();

    ArrayAdapter<String> timeAdapter;
    ArrayAdapter<String> discountAdapter;
    //     POST api logIn method
    public class RenewDialog {
        void showDialog(Activity activity) {

            total_amount=0.0;
            feess=0.0;
            discount=0.0;

            Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.renew_admin_dialog);

            time_spinner = (Spinner) dialog.findViewById(R.id.month_spinner);
            discount_spinner=(Spinner)dialog.findViewById(R.id.discount_spinner);
            fees = (EditText) dialog.findViewById(R.id.enter_fee);
            renew_partner = (ImageView) dialog.findViewById(R.id.renew);
            tvamount=(TextView)dialog.findViewById(R.id.amount);
            no_discount=(EditText)dialog.findViewById(R.id.no_discount);
            text2=(TextView)dialog.findViewById(R.id.text2);

            renewTime();

            renew_partner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sharedPreferences.seteventId(event_id);
                    dialog.dismiss();
                    DecimalFormat df = new DecimalFormat("###.#");
                    Intent intent=new Intent(getContext(), PaymentTransaction.class);
                    intent.putExtra("amount",df.format(total_amount));
                    intent.putExtra("from",monthINT.get(time_spinner.getSelectedItemPosition()));
//                    intent.putExtra("discount",discount_spinner.getSelectedItem().toString());
                    intent.putExtra("purpose","Renew");
                    startActivity(intent);

//
//                    WalletDialog alert = new WalletDialog();
//                    alert.showDialog(AdminProfile.this);
                }
            });

            dialog.show();
        }
    }

    public void statusEmail() {
        eventName.clear();
        progressBar.setVisibility(View.VISIBLE);
         MyRequestQueue = Volley.newRequestQueue(getContext());
         MyStringRequest = new StringRequest(Request.Method.POST, email_check, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
              //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {

                        progressBar.setVisibility(View.GONE);
//                        notification_image.setVisibility(View.VISIBLE);
                        // Launch login activity
                        JSONArray jsonArray = jObj.getJSONArray("events");
                        JSONObject notification_obj = jObj.getJSONObject("data");
                        JSONObject versionObject=jObj.getJSONObject("version");
                        Log.e("image",notification_obj+"");
                        Log.e("image",jsonArray.toString()+"");

                        String versionCode= versionObject.getString("version");

                        CheckUpdate(versionCode);

//                        Picasso.get().load(advertising_images + notification_obj.getString("image")).fit().error(R.drawable.error_circle).into(notification_image);
//                        notification_text.setText("Your payment will be due on " + notification_obj.getString("renew_date"));

                        renew_date=notification_obj.getString("renew_date");

                        NotificationAdapter notificationAdapter = new NotificationAdapter(getContext(), notification_obj, jsonArray);
                        notification_recycler.setAdapter(notificationAdapter);

                        Log.e("result", jsonArray.length() + "");
//                        eventName.add("Select Event");
//                        if (jsonArray.length()== 1){
//                            JSONObject c = jsonArray.getJSONObject(0);
//                            event_name.setVisibility(View.VISIBLE);
//                            event_spin.setVisibility(View.GONE);
//                            event_name.setText(c.getString("name"));
//                            event_id = c.getString("id");
//                        }else {
                            event_name.setVisibility(View.GONE);
                            event_spin.setVisibility(View.VISIBLE);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject c = jsonArray.getJSONObject(i);
                                eventName.add(c.getString("name"));
                                adapter = new ArrayAdapter<>(getContext(), R.layout.spinner, eventName);
                                event_spin.setAdapter(adapter);
                            }

                            if (sharedPreferences.getSelectedevent().length()>0){

                                event_spin.setSelection(Integer.parseInt(sharedPreferences.getSelectedevent()));
                            }

                            EventDueDateAdapter eventDueDateAdapter = new EventDueDateAdapter(getContext(), notification_obj, jsonArray);
                            eventduedate_recycler.setAdapter(eventDueDateAdapter);
                            getData(jsonArray);
//                        }
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "Sorry! No events found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("email", sharedPreferences.getemail());

                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    private void CheckUpdate(String versionCode) {

        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = pInfo.versionName;

            if (!version.equalsIgnoreCase(versionCode)){

                UpdateDialog();

            }

            Log.e("current veron",version);
            Log.e("backend versn",versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("package error",e.toString());
            e.printStackTrace();
        }


    }


    private void renewTime() {
        time.clear();
        disList.clear();
        MyRequestQueue = Volley.newRequestQueue(getActivity());
        MyStringRequest = new StringRequest(Request.Method.POST, get_renew_time, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {

                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        JSONArray array = jObj.getJSONArray("data");
                        JSONArray discount_array=jObj.getJSONArray("discounts");
                        if (getActivity()!=null){
                        JSONObject object = null;
                        for (int i = 0; i < array.length(); i++) {
                            object = array.getJSONObject(i);
                            Log.e("GetList object", object + "");

                            time.add(object.getString("name"));
                            monthINT.add(object.getString("months"));
                            Log.e("month",object.getString("months"));
                            timeAdapter = new ArrayAdapter<>(getActivity(), R.layout.expense_spinner, time);
                            time_spinner.setAdapter(timeAdapter);
                        }

                        for (int i = 0; i < discount_array.length(); i++) {
                            object = discount_array.getJSONObject(i);
                            Log.e("GetList object", object + "");

                            disList.add(object.getString("name"));
                            discountAdapter = new ArrayAdapter<>(getActivity(), R.layout.expense_spinner, disList);
                            discount_spinner.setAdapter(discountAdapter);
                        }

                        if (discount_array.length()>0) {


                            discount_spinner.setVisibility(View.VISIBLE);
                            no_discount.setVisibility(View.GONE);

                            text2.setVisibility(View.VISIBLE);
                        }else {
                            discount=0.0;
                            text2.setVisibility(View.GONE);
                            no_discount.setVisibility(View.GONE);
                            discount_spinner.setVisibility(View.GONE);


                        }
                        passValues(array, discount_array);
                        }
                    } else {
//                        Toast.makeText(AdminProfile.this, "", Toast.LENGTH_SHORT).show();


                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }
    // getData from URl to spinner
    private void getData(JSONArray array) {
        event_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                JSONObject object = null;
                try {
                    object = array.getJSONObject(i);
                    event_id = object.getString("id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("Spinner object", event_id + "  " + object + " ");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void passValues(JSONArray array, JSONArray discount_array) {


        DecimalFormat df = new DecimalFormat("###.#");
        time_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String batch_name = time_spinner.getItemAtPosition(time_spinner.getSelectedItemPosition()).toString();
                Log.e("batch_name", batch_name + "");
                try {
                    object = array.getJSONObject(i);
                    Log.e("Spinner object", object + "");
                    fees.setText(object.getString("amount"));
                    feess=Double.parseDouble(object.getString("amount"));
                    total_amount=feess-discount;
                    tvamount.setText("Total fees ₹ "+df.format(total_amount));
                } catch (JSONException e) {
                    Log.e("eeee",e.toString());
                    e.printStackTrace();
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });

        discount_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String batch_name = time_spinner.getItemAtPosition(time_spinner.getSelectedItemPosition()).toString();
                Log.e("batch_name", batch_name + "");
                try {
                    object1 = discount_array.getJSONObject(i);
                    Log.e("Spinner object", object1 + "");
//                    fees.setText(object1.getString("amount"));
//                    feess=Double.parseDouble(object.getString("amount"));
                    discount=object1.getDouble("amount");
                    total_amount=feess-discount;
                    tvamount.setText("Total fees ₹ "+df.format(total_amount));
                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.e("e",e.toString());
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }
}


//