package com.bzzy.SignInProcess;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;
import io.fabric.sdk.android.Fabric;

public class MainScreen extends AppCompatActivity {

    UserSharedPreferences sharedPreferences;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main_screen);

        sharedPreferences = new UserSharedPreferences(MainScreen.this);

        if (sharedPreferences.getemail().equals("")) {
            Login_screen login_screen = new Login_screen();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_screen_frame, login_screen).commit();
        } else {
            PinFragment fragment = new PinFragment();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_screen_frame, fragment).commit();
        }


    }
}
