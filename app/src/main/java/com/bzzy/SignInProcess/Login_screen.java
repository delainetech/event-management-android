package com.bzzy.SignInProcess;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class Login_screen extends Fragment implements ApiCalls, View.OnClickListener {

    TextView new_account, submit;
    UserSharedPreferences sharedPreferences;
    ProgressDialog pDialog;
    EditText enter_email;
    View view;

    public Login_screen() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login_screen, container, false);

        sharedPreferences = new UserSharedPreferences(getContext());

        initialization();

        return view;
    }

    public void initialization() {
        new_account = (TextView) view.findViewById(R.id.new_account);
        new_account.setOnClickListener(this);
        submit = (TextView) view.findViewById(R.id.submit);
        submit.setOnClickListener(this);
        enter_email = (EditText) view.findViewById(R.id.enter_email);
    }

    @Override
    public void onClick(View v) {
        if (v == submit) {
            validations();
        } else if (v == new_account) {
            SignUpFragment login_screen = new SignUpFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_screen_frame, login_screen).addToBackStack(null).commit();
        }
    }

    public void validations() {
        if (enter_email.getText().toString().trim().equals("") || enter_email.length() < 0) {
            enter_email.setError("Email can't be blank");
        } else if (!isValidEmail(enter_email.getText().toString().trim())) {
            enter_email.setError("Sorry! Email is not valid");
            // Toast.makeText(getContext(), "Enter registered email", Toast.LENGTH_SHORT).show();
        } else {
            loginApp();
        }
    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    //     POST api logIn method
    public void loginApp() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.show();
        RequestQueue MyRequestQueue = Volley.newRequestQueue(getContext());
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, email_check, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                pDialog.dismiss();  //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        JSONObject object = jObj.getJSONObject("data");

                        enter_email.getText().clear();

                        if (object.getString("pin").equalsIgnoreCase("null")) {
                            Toast.makeText(getContext(), "Sorry! You are not verified by ADMIN!", Toast.LENGTH_SHORT).show();
                        } else {
                            sharedPreferences.setname(object.getString("name"));
                            sharedPreferences.setemail(object.getString("email"));
                            sharedPreferences.setuserID(object.getString("id"));
                            sharedPreferences.setusernumber(object.getString("phone"));

                            PinFragment pinFragment = new PinFragment();
                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.main_screen_frame, pinFragment).addToBackStack(null).commit();
                        }

                    } else {
                        Toast.makeText(getContext(), "Sorry! Email is not registered with us.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                String mail = enter_email.getText().toString();
                params.put("email", mail);

                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

}
