package com.bzzy.Interface;

import android.view.View;

import org.json.JSONException;

public interface ClickListener {

    void onClick(View view, int position);

    void onLongClick(View view, int position) throws JSONException;

}
