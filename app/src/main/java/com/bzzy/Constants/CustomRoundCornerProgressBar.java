package com.bzzy.Constants;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.bzzy.R;


/**
 * Created by root on 18/02/2017.
 */

public class CustomRoundCornerProgressBar extends RoundCornerProgressBar {
    private Drawable rcProgressDrawable;

    public CustomRoundCornerProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray attributes = context.obtainStyledAttributes( R.styleable.CustomDrawableProgressBar);

        Drawable progressDrawable = attributes.getDrawable(R.styleable.CustomDrawableProgressBar_rcProgressDrawable);
        if(progressDrawable != null){
            rcProgressDrawable = progressDrawable;
        }

        attributes.recycle();
    }
    public CustomRoundCornerProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CustomDrawableProgressBar, defStyleAttr, 0);

        Drawable progressDrawable = attributes.getDrawable(R.styleable.CustomDrawableProgressBar_rcProgressDrawable);
        if(progressDrawable != null){
            rcProgressDrawable = progressDrawable;
        }

        attributes.recycle();
    }
    @SuppressWarnings("deprecation")
    @Override
    protected void drawProgress(LinearLayout layoutProgress, float max, float progress, float totalWidth,
                                int radius, int padding, int colorProgress, boolean isReverse) {

        super.drawProgress(layoutProgress, max, progress, totalWidth, radius, padding, colorProgress, isReverse);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if(rcProgressDrawable != null){
                layoutProgress.setBackground(rcProgressDrawable);
            }
        }
    }
}