package com.bzzy.Constants;

import android.content.Context;
import android.content.SharedPreferences;


public class UserSharedPreferences {

    private static Context context;
    private SharedPreferences userPreferences;
    private static UserSharedPreferences userSharedPreferences;
    private SharedPreferences.Editor edit;
    private static final String APP_PREFS = "Event Management";
    private static final String PREF_NAME = "userPref";

    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    String id;

    public UserSharedPreferences(Context context) {
        this.context = context;
        userPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        edit = userPreferences.edit();
    }

    public UserSharedPreferences(Context context, String id) {
        this.context = context;
        this.id = id;
        sharedPreferences = context.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void removeUser() {
        edit.clear().commit();
    }

    public static UserSharedPreferences getInstance(Context context) {
        if (userSharedPreferences == null) {
            userSharedPreferences = new UserSharedPreferences(context);
        }
        return userSharedPreferences;
    }

    public void Clear() {
        edit.clear();
        edit.commit();
    }

    public void ClearEditor() {
        editor.clear();
        editor.commit();
    }

    public void setuserID(String userid) {
        edit.putString("userid", userid);
        edit.commit();
    }

    public String getuserID() {
        return userPreferences.getString("userid", "");
    }

    public String getwallet() {
        return userPreferences.getString("wallet", "");
    }

    public void setwallet(String amount) {
        edit.putString("wallet", amount);
        edit.apply();
    }


    public String getTicket() {
        return userPreferences.getString("ticket", "");
    }

    public void setTicket(String ticket) {
        edit.putString("ticket", String.valueOf(ticket));
        edit.commit();
    }

    public String getTime() {
        return userPreferences.getString("time", "");
    }

    public void setTime(String time) {
        edit.putString("time", time);
        edit.commit();
    }


    public String getDate() {
        return userPreferences.getString("date", "");
    }

    public void setDate(String date) {
        edit.putString("date", String.valueOf(date));
        edit.commit();
    }

    public String getimage() {
        return userPreferences.getString("image", "abcd");
    }

    public void setimage(String image) {
        edit.putString("image", image);
        edit.commit();
    }

    public void setemail(String email) {
        edit.putString("email", email);
        edit.commit();
    }

    public String getemail() {
        return userPreferences.getString("email", "");
    }

    public void setname(String name) {
        edit.putString("name", name);
        edit.commit();
    }

    public String getname() {
        return userPreferences.getString("name", "");
    }

    public String getToken() {
        return userPreferences.getString("token", "");
    }

    public void setToken(String token) {
        edit.putString("token", token);
        edit.commit();
    }

    public void setlastname(String lastname) {
        edit.putString("last_name", lastname);
        edit.commit();
    }

    public String getlastname() {
        return userPreferences.getString("last_name", "");
    }

    public void setid(String id) {
        edit.putString("id", id);
        edit.commit();
    }

    public String getid() {
        return userPreferences.getString("id", "");
    }

    public String getPassword() {
        return userPreferences.getString("password", "");
    }

    public void setPassword(String password) {
        edit.putString("password", password);
        edit.commit();
    }

    public String gettype() {
        return userPreferences.getString("type", "");
    }

    public void settype(String desc) {
        edit.putString("type", desc);
        edit.commit();
    }

    public void setplayerid(String playerid) {
        edit.putString("player_id", playerid);
        edit.commit();
    }

    public String getplayerid() {
        return userPreferences.getString("player_id", "");
    }

    public String getCountry() {
        return userPreferences.getString("country", "");
    }

    public void setCountry(String country) {
        edit.putString("country", country);
        edit.commit();
    }


    public String getCountryTicket() {
        return userPreferences.getString("location", "");
    }

    public void setCountryTicket(String location) {
        edit.putString("location", location);
        edit.commit();
    }


    public void setCountryCode(String code) {
        edit.putString("code", code);
        edit.commit();
    }

    public String getCountryCode() {
        return userPreferences.getString("code", "");
    }

    public void setUnix(String unix) {
        edit.putString("unix", unix);
        edit.commit();
    }

    public String getUnix() {
        return userPreferences.getString("unix", "");
    }

    public void setAmount(String amount) {
        edit.putString("amount", amount);
        edit.commit();
    }

    public void setlat(String lat) {
        edit.putString("lat",lat);
        edit.commit();
    }

    public String getlat() {
        return  userPreferences.getString("lat","0.0");
    }

    public void setlng(String lng) {
        edit.putString("lng",lng);
        edit.commit();
    }

    public String getlng() {
        return  userPreferences.getString("lng","0.0");
    }


    public String getAmount() {
        return userPreferences.getString("amount", "");
    }

    public void setOTP(String OTP) {
        edit.putString("otp", OTP);
        edit.commit();
    }

    public String getOTP() {
        return userPreferences.getString("otp", "");
    }

    public void setVerify(String verify) {
        edit.putString("verify", verify);
        edit.commit();
    }

    public String getVerify() {
        return userPreferences.getString("verify", "");
    }

    public void setTomorrow(String milliseconds) {
        edit.putString("tomorrow", milliseconds);
        edit.commit();
    }

    public String getTomorrow() {
        return userPreferences.getString("tomorrow", "");
    }

    public void setCurrent_time(String milliseconds) {
        edit.putString("current_time", milliseconds);
        edit.commit();
    }

    public String getCurrent_time() {
        return userPreferences.getString("current_time", "");
    }

    public void setStatus(int status) {
        edit.putInt("locked", status).commit();
    }

    public int getStatus() {
        return userPreferences.getInt("locked", 0);
    }

    public void setAppStatus(String appStatus) {
        edit.putString("token_in", appStatus).commit();
    }

    public String getAppStatus() {
        return userPreferences.getString("token_in", "");
    }

    public void setGames(String games) {
        edit.putString("games", games).commit();
    }

    public String getGames() {
        return userPreferences.getString("games", "");
    }

    public void setWin(String win) {
        edit.putString("win", win).commit();
    }

    public String getWin() {
        return userPreferences.getString("win", "");
    }

    public void setLoss(String loss) {
        edit.putString("losses", loss).commit();
    }

    public String getLoss() {
        return userPreferences.getString("losses", "");
    }

    public void setAmountWon(String amountWon) {
        edit.putString("amount", amountWon).commit();
    }

    public String getAmountWon() {
        return userPreferences.getString("amount", "");
    }

    public void setemailForgot(String email) {
        edit.putString("emailforgot", email);
        edit.commit();
    }

    public String getemailForgot() {
        return userPreferences.getString("emailforgot", "");
    }

    public void setTicketNo(String ticketNo) {
        edit.putString("ticket", ticketNo);
        edit.commit();
    }

    public String getTicketNo() {
        return userPreferences.getString("ticket", "");
    }

    public void setAltitude(String altitude) {
        edit.putString("altitude", altitude);
        edit.commit();
    }
    public  String getAltitude(){
        return userPreferences.getString("altitude","");
    }

    public void setCoinId(String id) {
        edit.putString("id", id);
        edit.commit();
    }
    public String getCoinId() {return userPreferences.getString("id","");}

    public String geteventId() {
        return userPreferences.getString("event_id","");
    }

    public void seteventId(String event_id) {
        edit.putString("event_id", event_id);
        edit.commit();
    }


    public String getEventName() {
        return userPreferences.getString("event_name","");
    }

    public void setEventName(String event_name) {
        edit.putString("event_name", event_name);
        edit.commit();
    }


    public String getusernumber() {
        return userPreferences.getString("usernumber","");
    }

    public void setusernumber(String event_name) {
        edit.putString("usernumber", event_name);
        edit.commit();
    }


    public void setMasterPin(String master_pin) {
        edit.putString("master_pin", master_pin);
        edit.commit();
    }

    public String getMasterPin() {
        return userPreferences.getString("master_pin","");
    }

    public void setSelectedevent(String master_pin) {
        edit.putString("Selectedevent", master_pin);
        edit.commit();
    }

    public String getSelectedevent() {
        return userPreferences.getString("Selectedevent","");
    }
}
