package com.bzzy.Constants;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;

public interface  Constants {

    public static byte[]  set_Image_bytearray(Uri ImageUri, Context context)
    {
        byte[] b = new byte[0];
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver() , ImageUri );

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
            scaled.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            b = baos.toByteArray();
            Log.e("byteArray",b+"");

        }

        catch (Exception e) {
            Log.e("e","e",e);
            e.printStackTrace();
        }
        return b;
    }

}
