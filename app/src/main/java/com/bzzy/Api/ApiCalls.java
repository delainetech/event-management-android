package com.bzzy.Api;

public interface ApiCalls {
    String ROOT_URL = "http://139.59.88.42/Event-Managment/public/api/";
    String email_check = ROOT_URL + "email_check";
    String sign_up = ROOT_URL + "signup";
    String login_pin = ROOT_URL + "pin_check";
    String forgot_pin = ROOT_URL + "forget_password";
    String add_batch = ROOT_URL + "new_batch";
    String getBatchList = ROOT_URL + "batch_list";
    String user_profile = ROOT_URL + "student_profile";
    String batch_images = "http://139.59.88.42/Event-Managment/public/images/batch_user/";
    String advertising_images = "http://139.59.88.42/Event-Managment/public/images/";
    String expenditure_images = "http://139.59.88.42/Event-Managment/public/images/expenditure/";
    String all_users = ROOT_URL + "members_list";
    String add_expense_bill = ROOT_URL + "add_expenditure";
    String add_new_user = ROOT_URL + "add_batch_user";
    String categories = ROOT_URL + "category_list";
    String get_expenditure_home = ROOT_URL + "expenditure_home";
    String bottom_cards = ROOT_URL + "cards";
    String my_earnings = ROOT_URL + "my_earnings";
    String my_expense = ROOT_URL + "my_expenditure";
    String filter_expenditure = ROOT_URL + "filter_expenditure";

    String filter_earnings = ROOT_URL + "filter_earnings";
    String batch_users = ROOT_URL + "batch_users";
    String add_attendence = ROOT_URL + "add_attendance";
    String partner_profile = ROOT_URL + "partner_profile";
    String pending_approvals = ROOT_URL + "pending_approvels";

    String remove_partner = ROOT_URL + "remove_partner";
    String change_profile=ROOT_URL+"change_profile";

    String respond_approvals = ROOT_URL + "respond_approvel";
    String batch_attendence = ROOT_URL + "batch_attendance";
    String add_category = ROOT_URL + "add_category";
    String expenditure_list = ROOT_URL + "expenditure_list";
    String withdraw_home = ROOT_URL + "withdraw_home";
    String withdraw_request = ROOT_URL + "withdraw_request";
    String withdraw_respond = ROOT_URL + "respond_withdraw_request";
    String add_partner_admin = ROOT_URL + "add_partner";
    String get_renew_time = ROOT_URL + "view_renewal";
    String renew_partner_account = ROOT_URL + "renew_account";  // still pending for gateway
    String disable_batch = ROOT_URL + "disable_batch";
    String change_pin_mst = ROOT_URL + "change_pin";
    String get_data_mail = ROOT_URL + "get_data";
    String feedback = ROOT_URL + "give_feedback";
    String report_problem = ROOT_URL + "report";
    String update_seat = ROOT_URL + "update_chair";
    String student_account_renew = ROOT_URL + "student_account_renew";
    String renew_student = ROOT_URL + "renew_student";
    String disable_student = ROOT_URL + "disable_student";
    String delete_expenditure = ROOT_URL + "delete_expenditure";
    String conversion_report = ROOT_URL + "month_report";
    String contact_us = ROOT_URL + "contact_us";
    String send_sms=ROOT_URL+"send_domain_sms";
    String previous_sms=ROOT_URL+"sms_history";
    String full_search = ROOT_URL + "search_content";
    String attendence_batch_all=ROOT_URL+"attendance_batches";
    String previous_record=ROOT_URL+"check_previous_record";
    String generate_card=ROOT_URL+"generate_card";
    String seat_status=ROOT_URL+"seat_status";
    // Constant Methods
// ₹
}
