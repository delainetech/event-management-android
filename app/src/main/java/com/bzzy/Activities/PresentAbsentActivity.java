package com.bzzy.Activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bzzy.Adapters.ViewPagerAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Fragments.AbsentFragment;
import com.bzzy.Fragments.PresentFragment;
import com.bzzy.R;

public class PresentAbsentActivity extends AppCompatActivity implements View.OnClickListener, ApiCalls {

    public ViewPager viewPager;
    String batch_id, id;
    TextView present_btn, absent_btn;
    public static SearchView searchView;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_present_absent);

        batch_id = getIntent().getStringExtra("batch_id");
        id=getIntent().getStringExtra("attendance_id");
        Log.e("Batch_id", batch_id);

        initialization();

    }

    public void initialization() {
        present_btn = (TextView) findViewById(R.id.present_btn);
        present_btn.setOnClickListener(this);
        absent_btn = (TextView) findViewById(R.id.absent_btn);
        absent_btn.setOnClickListener(this);

        searchView = (SearchView) findViewById(R.id.search_view);
//         Search bar ... searching by name and ID
        searchView.setQueryHint("Type a name or ID number");
        searchView.setIconified(false);
        searchView.setEnabled(true);
        searchView.clearFocus();
        searchView.setIconifiedByDefault(false);

        setViewpager(batch_id);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    // Setting pager in Home Activity
    public void setViewpager(String batch_id) {

        viewPager = (ViewPager) findViewById(R.id.pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        // Add Fragments to adapter one by one

        Bundle b = new Bundle();
        PresentFragment mf = new PresentFragment();
        b.putString("batch_id", batch_id + "");
        b.putString("attendance_id",id);
        mf.setArguments(b);
        adapter.addFragment(mf, "");

        AbsentFragment lvf = new AbsentFragment();
        b.putString("batch_id", batch_id + "");
        b.putString("attendance_id",id);

        lvf.setArguments(b);
        adapter.addFragment(lvf, "");

        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onClick(View v) {
        if (v == present_btn) {
            viewPager.setCurrentItem(0);

        } else if (v == absent_btn) {
            viewPager.setCurrentItem(1);
        }

    }
}
