package com.bzzy.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.HistoryAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.Interface.ClickListener;
import com.bzzy.Popus.ExpensBillEdit;
import com.bzzy.Popus.ExpenseBill;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewExpenditureBills extends AppCompatActivity implements ApiCalls {

    RecyclerView expenditure_history_recy;
    HistoryAdapter adapter;
    Spinner category_spinner, partner_spinner;
    TextView error_text, grand_total;
    ArrayList<String> cat_name = new ArrayList<>();
    ArrayList<String> partner_name = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    UserSharedPreferences sharedPreferences;
    JSONArray jsonArray = new JSONArray();
    ProgressBar progressBar;
    RequestQueue MyRequestQueue;
    ProgressDialog progressDialog;
    StringRequest MyStringRequest;
    JSONObject cat_object = null;
    JSONObject partner_object = null;
    JSONArray partner_array;
    JSONArray cat_data;
    Dialog dialog,editDialog;
    ImageView user_image, id_image, pop_image;
    ProgressBar progressBar1, progressBar2, progressBar3;
    ImageView delete_user, edit_user, expanded_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_expenditure_bills);
        sharedPreferences = new UserSharedPreferences(ViewExpenditureBills.this);

        initializations();
    }

    public void initializations() {
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        grand_total = (TextView) findViewById(R.id.grand_total);
        error_text = (TextView) findViewById(R.id.error_text);
        category_spinner = (Spinner) findViewById(R.id.cat_spinner);
        partner_spinner = (Spinner) findViewById(R.id.partner_spinner);
        expenditure_history_recy = (RecyclerView) findViewById(R.id.expenditure_history_recycler);
        expenditure_history_recy.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(ViewExpenditureBills.this, LinearLayoutManager.VERTICAL, false);
        expenditure_history_recy.setLayoutManager(horizontalLayoutManager3);

        getCategories();




        expenditure_history_recy.addOnItemTouchListener(new RecyclerViewItemClickListener(this, expenditure_history_recy, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //triggers when click

//                Toast.makeText(ViewExpenditureBills.this, "on Click", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) throws JSONException {
                //triggers when you long press

                if (cat_data.getJSONObject(position).getString("user_id").equalsIgnoreCase(sharedPreferences.getuserID())){

                    editUser alert = new editUser();
                    alert.showDialog(ViewExpenditureBills.this, cat_data.getJSONObject(position));

                }




//                Toast.makeText(ViewExpenditureBills.this, "on long Click", Toast.LENGTH_SHORT).show();
            }
        }));

    }
    public class editUser {
        void showDialog(Activity activity, JSONObject object) {


          AlertDialog.Builder builder = new AlertDialog.Builder(ViewExpenditureBills.this);
            builder.setTitle("Modify Expense");

            builder.setMessage("Do you want to modify expense?\n" +
                    "(Note: If you Edit any expense, approval will be sent to your partner, if any, before adding it.)");
            builder.setPositiveButton("Delete",
                    new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            try {
                        disableUser(object.getString("id"));
                                dialog.cancel();
                    } catch (JSONException e) {
                        Log.e("ee",e.toString());
                        e.printStackTrace();
                    }

                        }
                    });

            builder.setNeutralButton("Cancel",
                    new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
//                            context.startActivity(new Intent(context, Setup.class));
                            dialog.cancel();
                        }
                    });

            builder.setNegativeButton("Edit",
                    new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {

                            FragmentManager fm2 = getSupportFragmentManager();
                            ExpensBillEdit dialog1 = new ExpensBillEdit();
                            Bundle bundle = new Bundle();
                            bundle.putString("object", object.toString());
                            dialog1.setArguments(bundle);
                            dialog1.show(fm2, "");

                            dialog.cancel();
                        }
                    });
            builder.create().show();
//            dialog = new Dialog(activity);
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.setCancelable(true);
//            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
//            wlmp.gravity = Gravity.CENTER;
//            wlmp.dimAmount = 0.0F;
//            dialog.getWindow().setAttributes(wlmp);
//            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            dialog.getWindow()
//                    .getAttributes().windowAnimations = R.style.DialogAnimation;
//
//            DisplayMetrics metrics = getResources().getDisplayMetrics();
//            int screenWidth = (int) (metrics.widthPixels * 1.00);
//            int screenHeight = (int) (metrics.heightPixels * 1.00);
//            dialog.getWindow().setLayout(screenWidth, screenHeight);
//            dialog.setContentView(R.layout.expenditure_delete_popup);
//
//
//            TextView text=dialog.findViewById(R.id.text);
//            TextView text1=dialog.findViewById(R.id.text1);
//            text.setVisibility(View.GONE);
//            text1.setVisibility(View.GONE);
//            progressBar2 = dialog.findViewById(R.id.progress_bar);
//            progressBar2.setVisibility(View.GONE);
//            pop_image = dialog.findViewById(R.id.user_image);
//            delete_user = (ImageView) dialog.findViewById(R.id.delete_btn);
//            edit_user = (ImageView) dialog.findViewById(R.id.edit_btn);
//
//
//            Log.e("object",object.toString());
//
//            delete_user.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    try {
//                        disableUser(object.getString("id"));
//                    } catch (JSONException e) {
//                        Log.e("ee",e.toString());
//                        e.printStackTrace();
//                    }
//                }
//            });
//
//
//            edit_user.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
////                    editExpenditureBills();
//
//                    FragmentManager fm2 = getSupportFragmentManager();
//                    ExpensBillEdit dialog1 = new ExpensBillEdit();
//                    Bundle bundle = new Bundle();
//                    bundle.putString("object", object.toString());
//                    dialog1.setArguments(bundle);
//                    dialog1.show(fm2, "");
//                    dialog.dismiss();
////                    dialog.dismiss();
//                }
//            });
//
//
//            dialog.show();
        }
    }

//    private void editExpenditureBills() {
//        editDialog = new Dialog(this);
//        editDialog.setCanceledOnTouchOutside(false);
//        editDialog.setCancelable(true);
//        WindowManager.LayoutParams wlmp = editDialog.getWindow().getAttributes();
//        wlmp.gravity = Gravity.CENTER;
//        wlmp.dimAmount = 0.0F;
//        editDialog.getWindow().setAttributes(wlmp);
//        editDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        editDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
//        editDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        editDialog.getWindow()
//                .getAttributes().windowAnimations = R.style.DialogAnimation;
//
//        DisplayMetrics metrics = getResources().getDisplayMetrics();
//        int screenWidth = (int) (metrics.widthPixels * 1.00);
//        int screenHeight = (int) (metrics.heightPixels * 1.00);
//        editDialog.getWindow().setLayout(screenWidth, screenHeight);
//        editDialog.setContentView(R.layout.expense_bill);
//        editDialog.show();
//
//
//    }

    public void disableUser(String id) {

        progressDialog = ProgressDialog.show(ViewExpenditureBills.this, null, null);
        ProgressBar spinner = new android.widget.ProgressBar(ViewExpenditureBills.this, null,android.R.attr.progressBarStyleSmall);
        spinner.getIndeterminateDrawable().setColorFilter(Color.parseColor("#8F2AFB"), android.graphics.PorterDuff.Mode.SRC_IN);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setContentView(spinner);
        progressDialog.setCancelable(false);
        progressDialog.show();

        RequestQueue MyRequestQueue = Volley.newRequestQueue(ViewExpenditureBills.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, delete_expenditure, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response_seat", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                   JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressDialog.dismiss();
                        // Launch login activity
                        dialog.dismiss();
                        getCategoriesData("", "", "");

//                        getUserData(userId);
//                        Intent intent = new Intent(UserProfile.this, SelectMemberActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);

//                        finish();

//                        Toast.makeText(ViewExpenditureBills.this, "User is Deleted successfully!", Toast.LENGTH_SHORT).show();
                    } else {
                        progressDialog.dismiss();
//                        Toast.makeText(ViewExpenditureBills.this, "Sorry! you can't delete User.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressDialog.dismiss();
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                progressBar2.setVisibility(View.GONE);

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(ViewExpenditureBills.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("expenditure_id",id );
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }
    public void setDataUsers(String cat_id, String catname, String user_id) {
        getCategoriesData(cat_id, catname, user_id);
    }

    // Partners API DATA
    public void getCategories() {
        progressBar.setVisibility(View.VISIBLE);
        cat_name.clear();
        MyRequestQueue = Volley.newRequestQueue(ViewExpenditureBills.this);
        MyStringRequest = new StringRequest(Request.Method.POST, categories, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity

//                        Log.e("GetList array  ", array + "");
                        // Partner Details for approvals
                        partner_array = jObj.getJSONArray("partners");
                        JSONObject object1 = null;
                        partner_name.add("All");
                        for (int i = 0; i < partner_array.length(); i++) {
                            object1 = partner_array.getJSONObject(i);
                            Log.e("GetList object", object1 + "");
                            partner_name.add(object1.getString("name"));
                            arrayAdapter = new ArrayAdapter<>(ViewExpenditureBills.this, R.layout.expense_spinner, partner_name);
                            partner_spinner.setAdapter(arrayAdapter);
                        }
                        setPartnerData(partner_array);

       //--------------------------------------------------------
                        JSONArray cat_array = jObj.getJSONArray("data");
                        JSONObject object = null;

                        cat_name.add("All");
                        for (int i = 0; i < cat_array.length(); i++) {
                            object = cat_array.getJSONObject(i);
//                            Log.e("GetList object", object + "");
                            if (object.getString("status").equalsIgnoreCase("1")) {

                                cat_name.add(object.getString("name"));
                                arrayAdapter = new ArrayAdapter<>(ViewExpenditureBills.this, R.layout.expense_spinner, cat_name);
                                category_spinner.setAdapter(arrayAdapter);
                                jsonArray.put(object);
                            }
                        }
                        setDataSpinner(jsonArray);

//                        Toast.makeText(AddUserActivity.this, "Congrats Shift is added successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ViewExpenditureBills.this, "Sorry! event is not found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ViewExpenditureBills.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id", sharedPreferences.geteventId());
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    // Get Values from Array through spinner
    public void setDataSpinner(JSONArray array) {
        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String category_selected = String.valueOf(category_spinner.getSelectedItemId() + 1);
                String partner_selected = String.valueOf(partner_spinner.getSelectedItemId() + 1);

                if (!partner_selected.equalsIgnoreCase("1") && category_selected.equalsIgnoreCase("1")) {
                    try {
                        Log.e("in", "start");
                        if (partner_object.getString("id").equalsIgnoreCase("null")) {
                            setDataUsers("", category_selected, "");
                            cat_object = array.getJSONObject(i);
                        } else {
                            setDataUsers("", category_selected, partner_object.getString("id"));
                            cat_object = array.getJSONObject(i);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (cat_name.get(i).equalsIgnoreCase("All") && category_selected.equalsIgnoreCase("1")) {
                    Log.e("in", "start");
                    getCategoriesData("", "", "");

                } else {
                    int d = i - 1;
                    Log.e("position", d + "");
                    try {
                        cat_object = array.getJSONObject(d);
//                        Log.e("sub", array + "");
//                        Log.e("cat_selectd", category_selected + " " + partner_selected);
                        if (partner_selected.equalsIgnoreCase("1")) {
//                            Log.e("selected",category_selected+" "+partner_selected);
                            setDataUsers(cat_object.getString("id"), category_selected, "");
                        } else if (category_selected.equalsIgnoreCase("1")) {
//                            Log.e("selected",category_selected+" "+partner_selected);
                            setDataUsers("", category_selected, partner_object.getString("id"));
                        } else {
//                            Log.e("selected",category_selected+" "+partner_selected);
                            setDataUsers(cat_object.getString("id"), category_selected, partner_object.getString("id"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(ViewExpenditureBills.this, "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Get Partner from Array through spinner
    public void setPartnerData(JSONArray array) {
        partner_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String partner_selected = String.valueOf(partner_spinner.getSelectedItemId() + 1);
                String cat_selected = String.valueOf(category_spinner.getSelectedItemId() + 1);
                if (partner_selected.equalsIgnoreCase("1") && !cat_selected.equalsIgnoreCase("1")) {
                    try {
                        Log.e("in", "start");
                        setDataUsers(cat_object.getString("id"), cat_selected, " ");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (partner_name.get(i).equalsIgnoreCase("All") && partner_selected.equalsIgnoreCase("1")) {
                    getCategoriesData(" ", " ", " ");
                } else {
                    int d = i - 1;
                    try {
                        partner_object = array.getJSONObject(d);
//                        Log.e("sub", array + "");

                        Log.e("part_selectd", partner_selected + " " + cat_selected);

                        if (cat_selected.equalsIgnoreCase("1")) {
                            Log.e("selected", cat_selected + " " + partner_selected);
                            setDataUsers(" ", cat_selected, partner_object.getString("id"));
                        } else if (partner_selected.equalsIgnoreCase("1")) {
                            Log.e("selected", cat_selected + " " + partner_selected);
                            setDataUsers(cat_object.getString("id"), cat_selected, " ");
                        } else {
                            Log.e("selected", cat_selected + " " + partner_selected);
                            setDataUsers(cat_object.getString("id"), cat_selected, partner_object.getString("id"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(ViewExpenditureBills.this, "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getCategoriesData(String cat_id, String cat_name, String user_id) {
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(ViewExpenditureBills.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, expenditure_list, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar.setVisibility(View.GONE);
                        error_text.setVisibility(View.GONE);
                        expenditure_history_recy.setVisibility(View.VISIBLE);
                        // Launch login activity
                         cat_data = jObj.getJSONArray("data");

                         Log.e("cat array",cat_data.toString());

                        grand_total.setText("₹ " + String.format("%.2f", Double.parseDouble(jObj.getString("grand_total")))) ;

                        if (cat_data.length() > 0) {
                            adapter = new HistoryAdapter(ViewExpenditureBills.this, cat_data, cat_name);
                            expenditure_history_recy.setAdapter(adapter);
                        }
                    } else {
                        grand_total.setText("₹ 00");
                        error_text.setVisibility(View.VISIBLE);
                        expenditure_history_recy.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
//                        Toast.makeText(ViewExpenditureBills.this, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ViewExpenditureBills.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id", sharedPreferences.geteventId());
                params.put("category_id", cat_id);
                params.put("user_id", user_id);
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }



    public class RecyclerViewItemClickListener implements RecyclerView.OnItemTouchListener {

        //GestureDetector to detect touch event.
        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerViewItemClickListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    //Find child on x and y position relative to screen
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        try {
                            clickListener.onLongClick(child, recyclerView.getChildLayoutPosition(child));
                        } catch (JSONException e1) {
                            Log.e("e",e.toString());
                            e1.printStackTrace();
                        }
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            //On Touch event
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildLayoutPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
