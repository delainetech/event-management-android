package com.bzzy.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.MonthlyGraphAdapter;
import com.bzzy.Adapters.WeekAdapter;
import com.bzzy.Adapters.WeekGraphAdapter2;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MyEarnings extends AppCompatActivity implements ApiCalls, View.OnClickListener {

    TextView current_month, estimate_amt, total_earning,estimate_text;
    UserSharedPreferences sharedPreferences;
    Button next, previous;
   public int year, ct_month, previous_month, previous_year, current_day;
    RecyclerView weekly_recycler;
    WeekGraphAdapter2 graphAdapter;
    MonthlyGraphAdapter graphAdapter1;
    JSONObject jObj;
    int amount_total = 0, amount_weekly = 0, amount_monthly = 0;
    Spinner filter_data_spin;
    Calendar c;
    public TextView earning_type, amount_type;
    CardView earning_detail;
    private ArrayList<String> year_list = new ArrayList<>();
    int[] months = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    public ArrayList<String> xVals = new ArrayList<String>();
    JSONObject weekly;
    WeekAdapter weekAdapter;
    public ArrayList<String> weeks = new ArrayList<>();
    public ArrayList<String> weeks_days = new ArrayList<>();
    LinearLayoutManager horizontalLayoutManager3;
    public int month_count;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_earnings);

        sharedPreferences = new UserSharedPreferences(MyEarnings.this);

        getDate();
        initialization();
    }

    public void initialization() {
        amount_type = (TextView) findViewById(R.id.weekly_amount);
        filter_data_spin = (Spinner) findViewById(R.id.filter_data_spinner);
        earning_detail=(CardView)findViewById(R.id.expenses_detai);
        weekly_recycler = (RecyclerView) findViewById(R.id.weekly_recycler);
        earning_type = (TextView) findViewById(R.id.type_of_earning);
        next = (Button) findViewById(R.id.next);
        next.setOnClickListener(this);
        previous = (Button) findViewById(R.id.previous);
        previous.setOnClickListener(this);
        current_month = (TextView) findViewById(R.id.month);
        estimate_amt = (TextView) findViewById(R.id.estimated_amount);
        estimate_text=(TextView) findViewById(R.id.estimated_text);
        total_earning = (TextView) findViewById(R.id.total_earning);

        weekly_recycler.setItemAnimator(new DefaultItemAnimator());
        horizontalLayoutManager3 = new LinearLayoutManager(MyEarnings.this, LinearLayoutManager.VERTICAL, false);
        weekly_recycler.setLayoutManager(horizontalLayoutManager3);
        weekly_recycler.setNestedScrollingEnabled(true);

        for (int i = 0; i < months.length; i++) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
            cal.set(Calendar.MONTH, months[i]);
            String month_name = month_date.format(cal.getTime());

            Log.e("Month_name", month_name + "");
            xVals.add(month_name);
        }
        xVals.clear();
        xVals.add("January");
        xVals.add("February");
        xVals.add("March");
        xVals.add("April");
        xVals.add("May");
        xVals.add("June");
        xVals.add("July");
        xVals.add("August");
        xVals.add("September");
        xVals.add("October");
        xVals.add("November");
        xVals.add("December");
        getEarnings(ct_month, year);
    }

    public void getDate() {
        c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        previous_year = c.get(Calendar.YEAR) - 1;
        ct_month = c.get(Calendar.MONTH) + 1;
        current_day = c.get(Calendar.DAY_OF_MONTH);
        Log.e("Month " + ct_month + " " + previous_month, "prev " + previous_year + "  Year " + year);
        month_count = ct_month;
    }

        @Override
        public void onClick(View v) {
            if (v == next) {
                if (month_count < ct_month) {
                    month_count++;
                    earning_detail.setVisibility(View.GONE);
                    Log.e("next", month_count + "");
                    getEarnings(month_count, year);
                }
            } else if (v == previous) {
                if (month_count > 1) {
                    month_count--;
                    earning_detail.setVisibility(View.GONE);
                    Log.e("previousMonth", month_count + "");
                    getEarnings(month_count, year);
                }
            }
        }

    // Data from API
    public void getEarnings(int month, int year) {
        year_list.clear();
        RequestQueue MyRequestQueue = Volley.newRequestQueue(MyEarnings.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, my_earnings, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity

                        JSONObject months_array = jObj.getJSONObject("months");

                        JSONObject object = jObj.getJSONObject("days");
                        Log.e("day", current_day + "");

                        current_month.setText(jObj.getString("current_month"));
                        total_earning.setText("₹ " + jObj.getString("total_earning"));

                        if (month == ct_month) {
                            estimate_amt.setVisibility(View.VISIBLE);
                            estimate_text.setVisibility(View.VISIBLE);
                            estimate_amt.setText("₹ " + jObj.getString("estimated_earning"));
                        }else{
                            estimate_amt.setVisibility(View.GONE);
                            estimate_text.setVisibility(View.GONE);
                        }
                        weekly = jObj.getJSONObject("weeks");
                        Log.e("day", current_day + "");

                        ArrayAdapter<String> adapter;
                        String[] menuArray = getResources().getStringArray(R.array.filter_data);

                        for (int k = 0; k < menuArray.length; k++) {
                            year_list.add(menuArray[k]);
                            adapter = new ArrayAdapter<>(MyEarnings.this, R.layout.cat_spinner, year_list);
                            filter_data_spin.setAdapter(adapter);
                        }
                        setData();
                    } else {
                        Toast.makeText(MyEarnings.this, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(MyEarnings.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("event_id", sharedPreferences.geteventId());
                params.put("user_id", sharedPreferences.getuserID());
                params.put("month", month + "");
                params.put("year", year + "");

                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    // Select To filter data
    public void setData() {

        filter_data_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String month1 = String.valueOf(filter_data_spin.getSelectedItem());
                Log.e("spinner", month1);
// Daily Earning
                earning_detail.setVisibility(View.GONE);
                if (filter_data_spin.getSelectedItemPosition() == 0) {
                    amount_total = 0;
                    earning_type.setText("Daily Earning");
                    try {

                        if (ct_month != month_count) {
                        Log.e("If","days");
                            for (int k = 1; k <=jObj.getJSONObject("days").length() ; k++) {
                                try {
                                    jObj.getJSONObject("days").getString(k + "");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                amount_total = amount_total + Integer.parseInt(jObj.getJSONObject("days").getString(k + ""));
                            }

                            graphAdapter = new WeekGraphAdapter2(MyEarnings.this, jObj,
                                    jObj.getJSONObject("days").length(), amount_total,earning_detail);
                            weekly_recycler.setAdapter(graphAdapter);
                            weekly_recycler.scrollToPosition(graphAdapter.getItemCount() - 1);

                        } else {
                            for (int k = 1; k <= current_day; k++) {
                                try {
                                    jObj.getJSONObject("days").getString(k + "");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                amount_total = amount_total + Integer.parseInt(jObj.getJSONObject("days").getString(k + ""));
                            }

                            graphAdapter = new WeekGraphAdapter2(MyEarnings.this, jObj,
                                    current_day, amount_total, earning_detail);
                            weekly_recycler.setAdapter(graphAdapter);
                            weekly_recycler.scrollToPosition(graphAdapter.getItemCount() - 1);
                        }
                    } catch (JSONException e) {
                        Log.e("days ee ",e.toString());
                        e.printStackTrace();
                    }
// Monthly Earning data
                } else if (filter_data_spin.getSelectedItemPosition() == 2) {
                    amount_monthly = 0;
                    earning_type.setText("Monthly Earning");
                    try {
                        for (int k = 0; k < ct_month; k++) {
                            Log.e("month",jObj.getJSONObject("months")+"    "+xVals.get(k).toString());
                            try {
                                jObj.getJSONObject("months").getString(xVals.get(k).toString());
                            } catch (JSONException e) {

                                e.printStackTrace();
                            }
                            amount_monthly = amount_monthly + Integer.parseInt(jObj.getJSONObject("months").getString(xVals.get(k).toString()));
                        }
                        Log.e("amoount", amount_monthly + "");
                        graphAdapter1 = new MonthlyGraphAdapter(MyEarnings.this,
                                jObj, ct_month, amount_monthly,earning_detail);
                        weekly_recycler.setAdapter(graphAdapter1);
                        weekly_recycler.scrollToPosition(graphAdapter1.getItemCount() - 1);
                    } catch (JSONException e) {
                        Log.e("month ee ",e.toString());
                        e.printStackTrace();
                    }
// Weekly earning data
                } else {
                    amount_weekly = 0;
                    weeks_days.clear();
                    weeks.clear();
                    earning_type.setText("Weekly Earning");
                    String header = null;
                    try {
                        weekly = jObj.getJSONObject("weeks");

                        Iterator<String> key = weekly.keys();
                        while (key.hasNext()) {
                            header = key.next().toString();
                            Log.e("Key : ", header + " value : "
                                    + weekly.getString(header));
                            amount_weekly = amount_weekly + Integer.parseInt(weekly.getString(header));
                            if (weekly.getString(header).equalsIgnoreCase("0")) {
                                Log.e("not", "added");
                            } else {
                                weeks.add(weekly.getString(header));
                                weeks_days.add(header);
                            }
                        }
                        weekAdapter = new WeekAdapter(MyEarnings.this, jObj,
                                Double.parseDouble(amount_weekly+""), weeks, weeks_days, 1, earning_detail,true);
                        weekly_recycler.setAdapter(weekAdapter);
                        weekly_recycler.scrollToPosition(weekAdapter.getItemCount() - 1);
                    } catch (JSONException e) {
                        Log.e("Weekly",e.toString());
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
}
