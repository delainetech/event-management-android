package com.bzzy.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bzzy.utils.util;
import com.crashlytics.android.Crashlytics;
import com.bzzy.Adapters.AppSearchAdapter;
import com.bzzy.Adapters.DashboardBarAdapter;
import com.bzzy.Adapters.ExpenditureAdapterBottom;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.Popus.FollowerCustomDialog;
import com.bzzy.R;
import com.bzzy.Swipe.OnSwipeTouchListener;
import com.github.florent37.tutoshowcase.TutoShowcase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.bzzy.Api.ApiCalls.full_search;
import static com.bzzy.Api.ApiCalls.getBatchList;

public class Dashboard extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout main;
    RecyclerView.LayoutManager layoutManager;
    ExpenditureAdapterBottom adapterBottom;
    ImageView add_user, add_shift, add_shift1;
    public SearchView searchView;
    String renew_date="";
    long date_diff=100;
    AppCompatEditText search_data;
    RecyclerView batch_bar;
    ProgressDialog progressDialog;
    UserSharedPreferences sharedPreferences;
    DashboardBarAdapter barAdapter;
    Calendar c;
    int month, year, current_day,current_month;
    ProgressBar progressBar;
    public View view;
    public RecyclerView search_recycler;
    FollowerCustomDialog dialogg;
    ArrayAdapter<String> adapter;
    ArrayList<String> suggestions = new ArrayList<>();
    ImageView notification_bell;
    Thread splashTread;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Fabric.with(this, new Crashlytics());
        sharedPreferences = new UserSharedPreferences(Dashboard.this);
        Utility.checkPermission(Dashboard.this);
        initialization();
        getDate();
        renew_date=getIntent().getStringExtra("renew_date");


        date_diff=new util(Dashboard.this).betweenDates(renew_date);

        if(date_diff<3)
            new RenewDialog().showDialog(Dashboard.this);

//        displayTuto();
    }

    protected void displayTuto() {
        TutoShowcase.from(Dashboard.this)
                .setListener(new TutoShowcase.Listener() {
                    @Override
                    public void onDismissed() {
                        Toast.makeText(Dashboard.this, "Tutorial dismissed", Toast.LENGTH_SHORT).show();
                    }
                })
                .setContentView(R.layout.showcase_items)
                .setFitsSystemWindows(false)


                .on(R.id.add_progress)
                .addCircle()
                .withBorder()
                .onClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Dashboard.this, "Add event timings", Toast.LENGTH_SHORT).show();
                    }
                })

                .on(R.id.add_new_user)
                .addCircle()
                .onClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Dashboard.this, "Add new user here", Toast.LENGTH_SHORT).show();
                    }
                })

                .on(R.id.swipable1)
                .displaySwipableRight()
                .animated(false)
                .delayed(300)
                .duration(300)
                .onClickContentView(R.id.swipable1, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Dashboard.this, "Members", Toast.LENGTH_SHORT).show();
                    }
                })

                .on(R.id.swipable)
                .displaySwipableLeft()
                .animated(false)
                .delayed(300)
                .duration(300)
                .onClickContentView(R.id.swipable1, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Dashboard.this, "Expenditure", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    public void initialization() {
        view = findViewById(R.id.include);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        add_user = (ImageView) findViewById(R.id.add_new_user);
        add_user.setOnClickListener(this);

        notification_bell=findViewById(R.id.notification_bell);
        notification_bell.setOnClickListener(this);
        main = (RelativeLayout) findViewById(R.id.main_layout);
        main.setOnClickListener(this);
        add_shift1 = (ImageView) findViewById(R.id.add_progress1);
        add_shift1.setOnClickListener(this);

//        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);
//        Glide.with(this).load(R.raw.oie_rounded_corners).into(notification_bell);

        searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setQueryHint("Search in "+ sharedPreferences.getEventName());
        searchView.setIconified(true);
        searchView.setEnabled(true);
        searchView.clearFocus();
        searchView.onActionViewCollapsed();
        searchView.setIconifiedByDefault(false);

        ImageView closeBtn = (ImageView) searchView.findViewById(R.id.search_close_btn);
        closeBtn.setEnabled(false);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("close","close");
                closeBtn.setEnabled(false);
                searchView.setQuery("", false);
                searchView.clearFocus();
             searchApp("");

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
//                searchApp(s);

                Log.e("khsbg","sihdfb");
                closeBtn.setEnabled(false);

                searchView.setQuery("", false);
//                searchView.clearFocus();
                searchView.setQueryHint("Search in "+ sharedPreferences.getEventName());
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                searchApp(s);
                closeBtn.setEnabled(true);
                searchView.setQueryHint("Search in "+ sharedPreferences.getEventName());
                return false;
            }
        });

        batch_bar = (RecyclerView) findViewById(R.id.batch_bar_recycler);
        batch_bar.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(Dashboard.this, LinearLayoutManager.VERTICAL, false);
        batch_bar.setLayoutManager(horizontalLayoutManager3);


        search_recycler = (RecyclerView) findViewById(R.id.search_recycler);
        search_recycler.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(Dashboard.this, LinearLayoutManager.VERTICAL, false);
        search_recycler.setLayoutManager(horizontalLayoutManager);


        search_data = (AppCompatEditText) findViewById(R.id.autoCompleteTextView);
        search_data.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchApp(s + "");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        swipeGesture();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBatchData();
        view.setVisibility(View.VISIBLE);
        searchView.clearFocus();
        searchView.setQueryHint("Search in "+ sharedPreferences.getEventName());
    }

    @SuppressLint("ClickableViewAccessibility")
    public void swipeGesture() {
        main.setOnTouchListener(new OnSwipeTouchListener(Dashboard.this) {
            public void onSwipeTop() {
//                Toast.makeText(Dashboard.this, "top", Toast.LENGTH_SHORT).show();
                FragmentActivity activity = (FragmentActivity) (Dashboard.this);
                FragmentManager fm2 = activity.getSupportFragmentManager();
                dialogg = new FollowerCustomDialog();
                view.setVisibility(View.GONE);
                dialogg.show(fm2, "");
            }

            public void onSwipeRight() {
                Intent intent = new Intent(Dashboard.this, ExpenditureActivity.class);
                startActivity(intent);
            }

            public void onSwipeLeft() {
                Intent intent = new Intent(Dashboard.this, SelectMemberActivity.class);
                startActivity(intent);
            }

            public void onSwipeBottom() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == add_user) {
            Intent intent = new Intent(Dashboard.this, AddUserActivity.class);
            startActivity(intent);
        } else if (v == add_shift1) {
            Intent intent = new Intent(Dashboard.this, AddBatch.class);

            startActivity(intent);
        }else if (v==main){
            Log.e("clikcl","s");
            searchView.onActionViewCollapsed();
            searchView.setQueryHint("Set");
        }else if (v==notification_bell){
            Intent intent = new Intent(Dashboard.this, MyEarningsDetails.class);


            intent.putExtra("day", current_day+"");
            intent.putExtra("month", month + "");
            intent.putExtra("year", year + "");
            intent.putExtra("first_date", "1-1-2019");
            intent.putExtra("second_date", "1-1-2019");
            intent.putExtra("type", "day");
            startActivity(intent);

        }
    }

    public void getBatchData() {
        progressBar.setVisibility(View.VISIBLE);

        RequestQueue MyRequestQueue = Volley.newRequestQueue(Dashboard.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, getBatchList, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response BatchList", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        JSONArray array = jObj.getJSONArray("data");
                        Log.e("GetList array  ", array + "");
                        progressBar.setVisibility(View.GONE);

                        add_shift1.setVisibility(View.GONE);
                        batch_bar.setVisibility(View.VISIBLE);
//                        add_shift.setVisibility(View.VISIBLE);
//                        if (array.length() > 0) {
                        barAdapter = new DashboardBarAdapter(Dashboard.this, array);
                        batch_bar.setAdapter(barAdapter);

                    } else {
                        batch_bar.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        add_shift1.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Dashboard.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
                progressBar.setVisibility(View.GONE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id", sharedPreferences.geteventId());
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }


    // Check permission method for Picture
    public static class Utility {
        static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

        static boolean checkPermission(final Context context) {
            int currentAPIVersion = Build.VERSION.SDK_INT;
            if (currentAPIVersion >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Permission necessary");
                        alertBuilder.setMessage("External storage permission is necessary");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();
                    } else {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
    }

    public void searchApp(String query) {
        progressBar.setVisibility(View.VISIBLE);

        RequestQueue MyRequestQueue = Volley.newRequestQueue(Dashboard.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, full_search, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response BatchList", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        progressBar.setVisibility(View.GONE);
                        search_recycler.setVisibility(View.VISIBLE);
                        JSONArray array = jObj.getJSONArray("data");
                        Log.e("GetList array  ", array + "");

                        AppSearchAdapter adapter = new AppSearchAdapter(Dashboard.this, array);
                        search_recycler.setAdapter(adapter);
                    } else {
                        search_recycler.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
//                        Toast.makeText(Dashboard.this, "No result found!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(Dashboard.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
                progressBar.setVisibility(View.GONE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("keyword", query);
                params.put("event_id",sharedPreferences.geteventId());
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    Dialog dialog;
    // Forgot View and Dialog
    public class RenewDialog {

        void showDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation1;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.renew_alert_dialog);

            Button dialogButton = (Button) dialog.findViewById(R.id.submit);
            TextView tv_dialog=(TextView)dialog.findViewById(R.id.text_dialog);
            if(date_diff<1)
                tv_dialog.setText("Your Subscription for "+sharedPreferences.getEventName()+" expired on "+renew_date);
            else
                tv_dialog.setText("Your Subscription for "+sharedPreferences.getEventName()+" will expire on "+renew_date);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Dashboard.this, AdminProfile.class);
                    startActivity(intent);
                    dialog.dismiss();
                }
            });
            dialog.show();


            splashTread = new Thread() {
                @Override
                public void run() {
                    try {
                        synchronized (this) {
                            wait(4000);

                            if(dialog.isShowing())
                                dialog.dismiss();

                        }

                    } catch (InterruptedException e) {
                        Log.e("exception", "", e);
                    }
                }
            };

            splashTread.start();

        }
    }


    public void getDate() {
        c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        current_day = c.get(Calendar.DAY_OF_MONTH);

        Log.e("Month " + month, " " + "Year " + year);

    }

}
