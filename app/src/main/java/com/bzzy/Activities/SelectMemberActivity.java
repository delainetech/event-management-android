package com.bzzy.Activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.ContactsAdapter;
import com.bzzy.Adapters.DueDateAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.RecyclerSectionItemDecoration;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class SelectMemberActivity extends AppCompatActivity implements View.OnClickListener, ApiCalls {

    RecyclerView dueRecycler, contactRecycler;
    DueDateAdapter adapter;
    public ContactsAdapter contactsAdapter;
    ImageView back;
    SearchView searchView;
    TextView total_members, add_user;
    UserSharedPreferences sharedPreferences;
    ProgressBar progressBar;
    String date_cr;
    public int year, current_day, month;
    boolean flagDecoration=false;
    ArrayList<String> Alphabets = new ArrayList<>();
    JSONArray list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_member);

        if (isPermissionGranted()) {
            Log.e("permission", "granted");
        }

        sharedPreferences = new UserSharedPreferences(SelectMemberActivity.this);
        initialization();
    }

    public void initialization() {
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        total_members = (TextView) findViewById(R.id.total_users);

        add_user = (TextView) findViewById(R.id.add_user);
        add_user.setOnClickListener(this);

// Search bar ... searching by name and ID
        searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setQueryHint("Type a name or ID number");
        searchView.setIconified(true);
        searchView.setEnabled(true);
        searchView.clearFocus();
        searchView.onActionViewCollapsed();
        searchView.setIconifiedByDefault(false);

        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageView closeBtn = searchView.findViewById(R.id.search_close_btn);
        closeBtn.setEnabled(false);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeBtn.setEnabled(false);
                searchView.setQuery("", false);
                searchView.clearFocus();
                if (contactRecycler.getItemDecorationCount() > 0) {
                    contactRecycler.removeItemDecorationAt(0);
                }
                flagDecoration=false;
                getMemberList("","");
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do so me magic
                closeBtn.setEnabled(true);
                searchView.clearFocus();

                if (contactRecycler.getItemDecorationCount() > 0) {
                    contactRecycler.removeItemDecorationAt(0);
                }

                flagDecoration=false;
                contactRecycler.invalidate();
                Log.e("subit",flagDecoration+"");
                Log.e("submit",searchView.getQuery().toString());
                getMemberList(searchView.getQuery().toString(),"");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (contactRecycler.getItemDecorationCount() > 0) {
                    contactRecycler.removeItemDecorationAt(0);
                }

                Log.e("change",flagDecoration+"");
                flagDecoration=false;
//                contactRecycler.invalidate();
                getMemberList(newText,"");
                closeBtn.setEnabled(true);
                return false;
            }
        });


        date_cr = getDate();
        Log.e("date", date_cr);

        contactRecycler = (RecyclerView) findViewById(R.id.contactsRecycler);
        dueRecycler = (RecyclerView) findViewById(R.id.due_date_recycler);
        dueRecycler.setItemAnimator(new DefaultItemAnimator());
//        contactRecycler.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(SelectMemberActivity.this, LinearLayoutManager.HORIZONTAL, false);
        dueRecycler.setLayoutManager(horizontalLayoutManager3);

        LinearLayoutManager layoutManager = new LinearLayoutManager(SelectMemberActivity.this, LinearLayoutManager.VERTICAL, false);
        contactRecycler.setLayoutManager(layoutManager);

        getMemberList("","");
    }

    @Override
    protected void onResume() {
        super.onResume();
//        flagDecoration=false;
//        contactRecycler.invalidate();
//        getMemberList("","");
    }


    @Override
    public void onBackPressed() {
        if (searchView.getQuery().toString().length()>0){


            searchView.setQuery("", false);
            searchView.clearFocus();
            if (contactRecycler.getItemDecorationCount() > 0) {
                contactRecycler.removeItemDecorationAt(0);
            }
            flagDecoration=false;
            getMemberList("","");
        }else {

            super.onBackPressed();
        }

    }

    @Override
    public void onClick(View v) {
        if (v == add_user) {
//
//            Intent intent = new Intent(SelectMemberActivity.this, AddUserActivity.class);
//            startActivity(intent);
            PopupMenu popup = new PopupMenu(this, v);
            popup.inflate(R.menu.menu_filter);
            popup.show();

            // This activity implements OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.name:
                            if (contactRecycler.getItemDecorationCount() > 0) {
                                contactRecycler.removeItemDecorationAt(0);
                            }
                            flagDecoration=false;
                            getMemberList("","");
//                            Toast.makeText(SelectMemberActivity.this, "name", Toast.LENGTH_SHORT).show();
                            return true;
                        case R.id.trial:

                            if (contactRecycler.getItemDecorationCount() > 0) {
                                contactRecycler.removeItemDecorationAt(0);
                            }
                            flagDecoration=false;
                            getMemberList("","trial");

//                            Toast.makeText(SelectMemberActivity.this, "trial", Toast.LENGTH_SHORT).show();
                            return true;
                        case R.id.latest:
                            if (contactRecycler.getItemDecorationCount() > 0) {
                                contactRecycler.removeItemDecorationAt(0);
                            }
                            flagDecoration=false;

                            getMemberList("","latest");
//                            Toast.makeText(SelectMemberActivity.this, "latest", Toast.LENGTH_SHORT).show();

                            return true;
                        case R.id.oldest:

                           if (contactRecycler.getItemDecorationCount() > 0) {
                            contactRecycler.removeItemDecorationAt(0);
                        }

                            flagDecoration=false;
                            getMemberList("","oldest");
//                            Toast.makeText(SelectMemberActivity.this, "oldest", Toast.LENGTH_SHORT).show();
                            return true;
                        default:
                            return false;
                    }

                }
            });


        }
    }


    public void getMemberList(String query,String filter) {
        list=new JSONArray();
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(SelectMemberActivity.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, all_users, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar.setVisibility(View.GONE);
                        contactRecycler.setVisibility(View.VISIBLE);
                        dueRecycler.setVisibility(View.VISIBLE);
                        // Launch login activity
                        Log.e("success", "su");
                        list = jObj.getJSONArray("member_list");
                        contactsAdapter = new ContactsAdapter(SelectMemberActivity.this, list, date_cr, false);
                        contactRecycler.setAdapter(contactsAdapter);

                        Log.e("inside deco",flagDecoration+"");
                        if (!flagDecoration) {
                            list=new JSONArray();
                            Log.e("inside deco",flagDecoration+"");
                            list = jObj.getJSONArray("member_list");
                            Log.e("size",list.toString()+"      "+list.length()+"");
                            RecyclerSectionItemDecoration sectionItemDecoration =
                                    new RecyclerSectionItemDecoration(50,
                                            true,
                                            getSectionCallback(list));
                            contactRecycler.addItemDecoration(sectionItemDecoration);
                            flagDecoration=true;
                        }
                       // total members
                        total_members.setText(list.length() + " " + "Members");


                        // Due date Adapter
                        JSONArray due_list_array = jObj.getJSONArray("due_list");

                        adapter = new DueDateAdapter(SelectMemberActivity.this, due_list_array, date_cr);
                        dueRecycler.setAdapter(adapter);
                    } else {

                        total_members.setText(  "0 " + "Members");
                        progressBar.setVisibility(View.GONE);
                        contactRecycler.setVisibility(View.GONE);
                        dueRecycler.setVisibility(View.GONE);
                        Toast.makeText(SelectMemberActivity.this, "Member not found!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                progressBar.setVisibility(View.GONE);

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(SelectMemberActivity.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(Sele.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("event_id", sharedPreferences.geteventId());
                params.put("keyword", query);
                params.put("filter",filter);
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();

                } else {
//                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }


    public String getDate() {
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        current_day = c.get(Calendar.DAY_OF_MONTH);
        Log.e("Month " + month, " " + "Year " + year);
        String date = current_day + "-" + month + "-" + year;
        return date;
    }


    public RecyclerSectionItemDecoration.SectionCallback getSectionCallback(JSONArray arrayList) {
        return new RecyclerSectionItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int position) {
                Log.e("pos",position+" ");
                if (arrayList.length() > 0 && position >= 0 && position < arrayList.length()) {

                    try {

                        Log.e("name name",arrayList.getJSONObject(position).getString("student_name").substring(0, 1));
                        return position == 0
                                || arrayList.getJSONObject(position).getString("student_name").substring(0, 1).toUpperCase()
                                .charAt(0) != arrayList.getJSONObject(position - 1).getString("student_name").substring(0, 1).toUpperCase()
                                .charAt(0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else
                    return position == 0;
                return false;
            }

            @Override
            public CharSequence getSectionHeader(int position) {
                CharSequence s = "";
                if (arrayList.length() > 0 && position >= 0 && position < arrayList.length()) {
                    try {
                        s = arrayList.getJSONObject(position).getString("student_name").substring(0, 1).toUpperCase();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    return s;

                } else {
                    return s;
                }
            }
        };
    }


}
