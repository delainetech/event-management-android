package com.bzzy.Activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

//import com.instamojo.android.Instamojo;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.AdminCardsAdapter;
import com.bzzy.Adapters.ApprovalAdminAdapter;
import com.bzzy.Adapters.BannerAdapter;
import com.bzzy.Adapters.CarouselEffectTransformer;
import com.bzzy.Adapters.LinearViewTransformation;
import com.bzzy.Adapters.WithdrawAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.Constants.VolleyMultipartRequest;
import com.bzzy.R;
import com.bzzy.utils.Common;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.philio.pinentry.PinEntryView;

public class AdminProfile extends AppCompatActivity implements View.OnClickListener, ApiCalls
         {

    BannerAdapter bannerAdapter;
    ViewPager banner_viewpager;
    ViewPager data_cards;
    AdminCardsAdapter cardsAdapter;
    RecyclerView pending_approvals;
    ApprovalAdminAdapter adminAdapter;
    TextView admin_name;
    RecyclerView withdraw_recycler;
    WithdrawAdapter adapter;
    UserSharedPreferences sharedPreferences;
    RelativeLayout renew, get_data, withdraw, add_partner;
    EditText amount_withdraw, remark,no_discount;
    TextView submit, upload_image,text2;
    EditText pin1, pin2, pin3, pin4, pin5, pin6;
    EditText partner_name, mobile_no, email, master_pin, fees, city;
    ImageView partner_image, renew_partner;
    RelativeLayout card;
    ProgressBar progressBar;
    TextView addPartner;
    String orderId="";
    Double feess=0.0;
    Double discount=0.0;
    Double total_amount=0.0;
    Bitmap bitmap1, bitmap;
    ArrayList<String> time = new ArrayList<>();
    ArrayList<String> monthINT = new ArrayList<>();
    ArrayList<String> disList = new ArrayList<>();
    ArrayAdapter<String> timeAdapter;
    ArrayAdapter<String> discountAdapter;
    Spinner time_spinner,discount_spinner;
    JSONObject object,object1;
    RequestQueue MyRequestQueue;
    StringRequest MyStringRequest;
    ImageView payment_button;
    TextView sent_to_mail;
    Dialog getDialog;
    TextView tvamount;
    JSONArray array;
    Dialog dialog;
    String discount_amount="";
    String date,st_datefrom="",st_dateto="";
    int month1,day1,year1;
    String pin_enter= null;
    private PinEntryView pinEntryView;
    ProgressBar get_progress;
    TextView total_fee,tv_datefrom,tv_dateto;
    int[] backgrounds = {R.drawable.banner_first_background,
            R.drawable.banner_background,
            R.drawable.banner_second
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Instamojo.getInstance().initialize(this, Instamojo.Environment.TEST);
        setContentView(R.layout.activity_admin_profile);

        sharedPreferences = new UserSharedPreferences(AdminProfile.this);
        initializations();
    }

    public void initializations() {
        // Set name on Admin profile
        admin_name = (TextView) findViewById(R.id.admin_name);
        admin_name.setText(sharedPreferences.getEventName());

        renew = (RelativeLayout) findViewById(R.id.renew);
        renew.setOnClickListener(this);
        get_data = (RelativeLayout) findViewById(R.id.getData);
        get_data.setOnClickListener(this);
        withdraw = (RelativeLayout) findViewById(R.id.withdraw);
        withdraw.setOnClickListener(this);
        add_partner = (RelativeLayout) findViewById(R.id.partner);
        add_partner.setOnClickListener(this);

        // RecyclerView to show pending requests
        pending_approvals = (RecyclerView) findViewById(R.id.pending_approval_recycler);
        pending_approvals.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(AdminProfile.this, LinearLayoutManager.VERTICAL, false);
        pending_approvals.setLayoutManager(horizontalLayoutManager3);

// ViewPager & Carousel effect bottom

        data_cards = (ViewPager) findViewById(R.id.cards_viewpager);
        data_cards.setClipChildren(false);
        data_cards.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.margin_20dp));
        data_cards.setOffscreenPageLimit(3);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            data_cards.setElevation(0.3f);
        }

        data_cards.setPageTransformer(false, new LinearViewTransformation(this)); // Set transformer

// ViewPager & Carousel effect bottom
        banner_viewpager = (ViewPager) findViewById(R.id.viewpagerTop);
        banner_viewpager.setClipChildren(false);
        banner_viewpager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen._minus25sdp));
        banner_viewpager.setOffscreenPageLimit(3);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            banner_viewpager.setElevation(0.3f);
        }
        banner_viewpager.setPageTransformer(false, new CarouselEffectTransformer(this)); // Set transformer
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserData();
    }

    // Admin Profile API DATA
    public void getUserData() {
        MyRequestQueue = Volley.newRequestQueue(AdminProfile.this);
        MyStringRequest = new StringRequest(Request.Method.POST, partner_profile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response in admiprofile", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // User admission details
                        JSONArray month_array = jObj.getJSONArray("months");
                        cardsAdapter = new AdminCardsAdapter(AdminProfile.this, month_array, jObj);
                        data_cards.setAdapter(cardsAdapter);
// Banner Data
                        JSONArray banner_array = jObj.getJSONArray("advertisement");
                        if (banner_array.length() > 0) {
                            bannerAdapter = new BannerAdapter(AdminProfile.this, backgrounds, banner_array);
                            banner_viewpager.setAdapter(bannerAdapter);
                            banner_viewpager.setCurrentItem(1);
                        }else {
                            bannerAdapter = new BannerAdapter(AdminProfile.this, backgrounds);
                            banner_viewpager.setAdapter(bannerAdapter);
                            banner_viewpager.setCurrentItem(1);
                        }
// Partner Details for approvals
                        JSONArray array = jObj.getJSONArray("partner");
                        adminAdapter = new ApprovalAdminAdapter(AdminProfile.this, array);
                        pending_approvals.setAdapter(adminAdapter);
                    } else {
                        Toast.makeText(AdminProfile.this, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(AdminProfile.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("event_id", sharedPreferences.geteventId());
                params.put("user_id", sharedPreferences.getuserID());
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }


    @Override
    public void onClick(View v) {
        if (v == renew) {
            RenewDialog alert = new RenewDialog();
            alert.showDialog(AdminProfile.this);
        } else if (v == get_data) {
            getDataDialog alert = new getDataDialog();
            alert.showDialog(AdminProfile.this);
        } else if (v == withdraw) {
            withdrawDialog alert = new withdrawDialog();
            alert.showDialog(AdminProfile.this);
        } else if (v == add_partner) {
            addPartnerDialog alert = new addPartnerDialog();
            alert.showDialog(AdminProfile.this);
        }
    }

//    @Override
//    public void onInstamojoPaymentComplete(String s, String s1, String s2, String s3) {
//
//    }
//
//    @Override
//    public void onPaymentCancelled() {
//
//    }
//
//    @Override
//    public void onInitiatePaymentFailure(String s) {
//
//    }

    // GetData Dialog
    public class getDataDialog {
        void showDialog(Activity activity) {

            Calendar c=Calendar.getInstance();
            month1=(c.get(Calendar.MONTH)+1);
            day1=c.get(Calendar.DAY_OF_MONTH);
            year1=c.get(Calendar.YEAR);
            getDialog = new Dialog(activity);
            getDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            getDialog.setCancelable(true);
            getDialog.setCanceledOnTouchOutside(false);
            WindowManager.LayoutParams wlmp = getDialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            getDialog.getWindow().setAttributes(wlmp);
            getDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            getDialog.getWindow().setLayout(screenWidth, screenHeight);
            getDialog.setContentView(R.layout.get_data_dialog);

            get_progress= (ProgressBar) getDialog.findViewById(R.id.progress_bar);
            get_progress.setVisibility(View.GONE);
            pinEntryView = (PinEntryView) getDialog.findViewById(R.id.pin_entry_simple);
            tv_datefrom=(TextView)getDialog.findViewById(R.id.tv_from);
            tv_dateto=(TextView)getDialog.findViewById(R.id.tv_to);
            tv_dateto.setHint("dd-mm-yyyy");
            tv_datefrom.setHint("dd-mm-yyyy");

            tv_datefrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDate(tv_datefrom,day1,month1,year1,true);
                }
            });

            tv_dateto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDate(tv_dateto,day1,month1,year1,false);
                }
            });

            pinEntryView.setOnPinEnteredListener(new PinEntryView.OnPinEnteredListener() {
                @Override
                public void onPinEntered(String pin) {
                    pin_enter = pin;
//                Toast.makeText(AdminProfile.this, "Pin entered: " + pin_enter, Toast.LENGTH_LONG).show();
                }
            });
            sent_to_mail = (TextView) getDialog.findViewById(R.id.submit);

            sent_to_mail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (st_datefrom.length()>0 && st_dateto.length()>0){

                    if (pin_enter == null) {
                        Toast.makeText(activity, "Please enter complete PIN!", Toast.LENGTH_SHORT).show();
                    } else if (!sharedPreferences.getMasterPin().equalsIgnoreCase(pin_enter)) {
                        Toast.makeText(activity, "Incorrect Master PIN!", Toast.LENGTH_SHORT).show();
                    } else {
                        setDataMail();
                    }
                }else {

                        Toast.makeText(AdminProfile.this, "Select Date", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            getDialog.show();
        }
    }


    public void getDate(final TextView et, final int day, int month, int year,boolean b){

        DatePickerDialog datePickerDialog =new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String d=dayOfMonth+"-"+(month+1)+"-"+year+" 00:00:00";

                if(b) {

                    day1 = dayOfMonth;
                    month1 = month + 1;
                    year1 = year;

                    st_datefrom=convertDate(d,"dd-MM-yyyy");
                }
                else
                    st_dateto=convertDate(d,"dd-MM-yyyy");

//                    tv_to.setText(utl.convertDate(d,"dd MMM-yyyy"));

                et.setText(convertDate(d,"dd-MMM-yyyy"));
            }
        },year,month-1,day);

        if(b){

        }
//            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        else {

//            String myDate = year+"/"+month+"/"+day;
            if (st_datefrom.length() > 0) {

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                Date date = null;
                try {
                    date = sdf.parse(st_datefrom);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long millis = date.getTime();

                datePickerDialog.getDatePicker().setMinDate(millis);
            }
        }
        datePickerDialog.show();
    }


    public String convertDate(String dt, String ft){

//        log("daaaaa",dt+"");
        if(dt.equalsIgnoreCase(null)|dt.equalsIgnoreCase("null")|dt.equalsIgnoreCase(""))
            return "";

        Date date1 = null;
        try {
            date1=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat formatter = new SimpleDateFormat(ft);
//        Date date = new Date();

        return formatter.format(date1);

    }
    // Request for data
    private void setDataMail() {
        get_progress.setVisibility(View.VISIBLE);
        MyRequestQueue = Volley.newRequestQueue(AdminProfile.this);
        MyStringRequest = new StringRequest(Request.Method.POST, get_data_mail, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        get_progress.setVisibility(View.GONE);
                        getDialog.dismiss();
                        Toast.makeText(AdminProfile.this, jObj.getString("message") + "", Toast.LENGTH_SHORT).show();
                    } else {
                        get_progress.setVisibility(View.GONE);
                        Toast.makeText(AdminProfile.this, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                get_progress.setVisibility(View.GONE);

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(AdminProfile.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("partner_id", sharedPreferences.getuserID());
                params.put("event_id", sharedPreferences.geteventId());
                params.put("from",st_datefrom);
                params.put("to",st_dateto);

                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    // Dialog for withdraw
    public class withdrawDialog {
        void showDialog(Activity activity) {
            Dialog dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.withdraw_dialog);

            amount_withdraw = (EditText) dialog.findViewById(R.id.amount);
            remark = (EditText) dialog.findViewById(R.id.remarks);

            submit = (TextView) dialog.findViewById(R.id.submit);

            withdraw_recycler = (RecyclerView) dialog.findViewById(R.id.withdraw_recycler);
            withdraw_recycler.setItemAnimator(new DefaultItemAnimator());
            LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(AdminProfile.this, LinearLayoutManager.VERTICAL, false);
            withdraw_recycler.setLayoutManager(horizontalLayoutManager3);

            respondWithdraw();

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (amount_withdraw.getText().toString().trim().equalsIgnoreCase("")) {
                        amount_withdraw.setError("Please enter Withdraw amount.");
                    } else {
                        requestWithdraw();
                    }
                }
            });
            dialog.show();
        }
    }

    public void setAdapterWithdraw(JSONArray array) {
        adapter = new WithdrawAdapter(AdminProfile.this, array);
        withdraw_recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    // Respond to withdraw request
    public void respondWithdraw() {
        MyRequestQueue = Volley.newRequestQueue(AdminProfile.this);
        MyStringRequest = new StringRequest(Request.Method.POST, withdraw_home, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        array = jObj.getJSONArray("data");
                        setAdapterWithdraw(array);
                    } else {
//                        Toast.makeText(AdminProfile.this, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(AdminProfile.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", sharedPreferences.getuserID());
                params.put("event_id", sharedPreferences.geteventId());
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    // Request for withdraw amount
    private void requestWithdraw() {
        MyRequestQueue = Volley.newRequestQueue(AdminProfile.this);
        MyStringRequest = new StringRequest(Request.Method.POST, withdraw_request, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        JSONObject obj = jObj.getJSONObject("data");
                        remark.getText().clear();
                        amount_withdraw.getText().clear();
                        respondWithdraw();

                        Toast.makeText(AdminProfile.this, "Withdraw request added successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AdminProfile.this, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(AdminProfile.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", sharedPreferences.getuserID());
                params.put("event_id", sharedPreferences.geteventId());
                params.put("amount", amount_withdraw.getText().toString().trim());
                if (remark.getText().toString().trim().equalsIgnoreCase("")) {
                    params.put("remark", "");
                } else params.put("remark", remark.getText().toString().trim());

                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    // Add partner dialog
    public class addPartnerDialog {
        void showDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.add_partner_dialog);

            progressBar = dialog.findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.GONE);

            partner_image = (ImageView) dialog.findViewById(R.id.user_image);
            card=(RelativeLayout) dialog.findViewById(R.id.card);
            upload_image=(TextView) dialog.findViewById(R.id.upload_image);
            partner_name = (EditText) dialog.findViewById(R.id.partner_name);
            mobile_no = (EditText) dialog.findViewById(R.id.mobile_no);
            email = (EditText) dialog.findViewById(R.id.enter_email);
            city = (EditText) dialog.findViewById(R.id.enter_city);
            master_pin = (EditText) dialog.findViewById(R.id.master_pin);
            addPartner = (TextView) dialog.findViewById(R.id.add_partner);
// Partner Image
            partner_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("click","start");

                    CropImage.activity().setAspectRatio(400,400)
                            .start(AdminProfile.this);
                }
            });


            // Submit Button
            addPartner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    validationsAdd();
                }
            });

            dialog.show();
        }
    }

    // Validations for Add Partner
    public void validationsAdd() {

        if (partner_name.getText().toString().trim().equalsIgnoreCase("")) {
            partner_name.setError("Please fill Partner name");
        } else if (mobile_no.getText().toString().trim().equalsIgnoreCase("")) {
            mobile_no.setError("Please fill Partner number");
        } else if (email.getText().toString().trim().equalsIgnoreCase("")) {
            email.setError("Please fill Partner email id");
        } else if (!isValidEmail(email.getText().toString().trim())) {
            email.setError("Sorry! Email is invalid.");
        } else if (master_pin.getText().toString().trim().equalsIgnoreCase("")) {
            master_pin.setError("Please enter master pin.");
        } else if (!master_pin.getText().toString().trim().equalsIgnoreCase(sharedPreferences.getMasterPin())) {
            master_pin.setError("Sorry! you can't add partner.");
        } else if (city.getText().toString().trim().equalsIgnoreCase("")) {
            city.setError("Please enter city name.");
        } else {
            addPartner(bitmap1);
        }
    }

    private void addPartner(Bitmap bitmap) {
        progressBar.setVisibility(View.VISIBLE);
        Log.e("gjg", "hgjg");
        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, add_partner_admin,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Log.e("Response", new String(response.data) + " ");
                        //This code is executed if the server responds, whether or not the response contains data.
                        try {
                            JSONObject jObj = new JSONObject(new String(response.data));
                            String status = jObj.getString("status");
                            if (status.equals("success")) {
                                progressBar.setVisibility(View.GONE);
                                JSONObject object = jObj.getJSONObject("data");
                                getUserData();
                                dialog.dismiss();
                                Toast.makeText(AdminProfile.this, jObj.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(AdminProfile.this, jObj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e1) {
                            progressBar.setVisibility(View.GONE);
                            Log.e("Exception", e1 + " ");
                            e1.printStackTrace();
                        }
                    }
                },
                error -> {


                    progressBar.setVisibility(View.GONE);
                    Log.e("onErrorResponse: ", error + "");
                    NetworkResponse networkResponse = error.networkResponse;
                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";
                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                        Toast.makeText(AdminProfile.this, errorMessage.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("event_id", sharedPreferences.geteventId());
                params.put("user_id", sharedPreferences.getuserID());
                params.put("name", partner_name.getText().toString().trim());
                params.put("master_pin", master_pin.getText().toString().trim());
                params.put("email", email.getText().toString().trim());
                params.put("phone", mobile_no.getText().toString().trim());
                params.put("city", city.getText().toString().trim());

                if (bitmap == null) {
                    params.put("image", "");
                }
                Log.e("getParams: ", params + "");
                return params;
            }

            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                if (bitmap != null) {
                    params.put("image", new DataPart(imagename + ".jpg", getFileDataFromDrawable(bitmap)));
                }
                Log.e("params", params + "");
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //adding the request to volley
        Volley.newRequestQueue(AdminProfile.this).add(volleyMultipartRequest);
    }

    // Check Email validations
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("codes", requestCode + "," + resultCode + "," + data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.e("uri", resultUri + "");


                try {
                    bitmap1 = MediaStore.Images.Media.getBitmap(AdminProfile.this.getContentResolver(), resultUri);
                    bitmap=getResizedBitmap(bitmap1, 350);

                    partner_image.setImageBitmap(bitmap1 );

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("in", error + "");
            }
        }
    }

    // REsize Bitmap Method
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    //Renew Dialog
    public class RenewDialog {
        void showDialog(Activity activity) {

            total_amount=0.0;
            feess=0.0;
            discount=0.0;

            Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.renew_admin_dialog);

            time_spinner = (Spinner) dialog.findViewById(R.id.month_spinner);
            discount_spinner=(Spinner)dialog.findViewById(R.id.discount_spinner);
            fees = (EditText) dialog.findViewById(R.id.enter_fee);
            renew_partner = (ImageView) dialog.findViewById(R.id.renew);
            tvamount=(TextView)dialog.findViewById(R.id.amount);
            no_discount=(EditText)dialog.findViewById(R.id.no_discount);
            text2=(TextView)dialog.findViewById(R.id.text2);

            renewTime();

            renew_partner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    DecimalFormat df = new DecimalFormat("###.#");
                    Intent intent=new Intent(AdminProfile.this,PaymentTransaction.class);
                    intent.putExtra("amount",df.format(total_amount));
                    intent.putExtra("from",monthINT.get(time_spinner.getSelectedItemPosition()));
//                    intent.putExtra("discount",discount_spinner.getSelectedItem().toString());
                    intent.putExtra("purpose","Renew");
                    startActivity(intent);

//
//                    WalletDialog alert = new WalletDialog();
//                    alert.showDialog(AdminProfile.this);
                }
            });

            dialog.show();
        }
    }

    public class WalletDialog {
        void showDialog(Activity activity) {


            Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.wallet_dialog);
            dialog.show();

            payment_button=dialog.findViewById(R.id.payment_button);

            payment_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {





//                    Instamojo.getInstance().initiatePayment(AdminProfile.this, "e377f5a35f204ad5bad328a6c87c79c3", AdminProfile.this);
//                    PaymentMethod();
                }
            });



        }


    }




    private String initOrderId() {

        Random r = new Random(System.currentTimeMillis());
        String orderId = "ORDER" + (1 + r.nextInt(2)) * 10000
                + r.nextInt(10000);
//		EditText orderIdEditText = (EditText) findViewById(R.id.order_id);
//		orderIdEditText.setText(orderId);
        return orderId;
    }






    private void renewTime() {
        time.clear();
        disList.clear();
        MyRequestQueue = Volley.newRequestQueue(AdminProfile.this);
        MyStringRequest = new StringRequest(Request.Method.POST, get_renew_time, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        JSONArray array = jObj.getJSONArray("data");
                        JSONArray discount_array=jObj.getJSONArray("discounts");

                        JSONObject object = null;
                        for (int i = 0; i < array.length(); i++) {
                            object = array.getJSONObject(i);
                            Log.e("GetList object", object + "");

                            time.add(object.getString("name"));
                            monthINT.add(object.getString("months"));
                            Log.e("month",object.getString("months"));
                            timeAdapter = new ArrayAdapter<>(AdminProfile.this, R.layout.expense_spinner, time);
                            time_spinner.setAdapter(timeAdapter);
                        }

                        for (int i = 0; i < discount_array.length(); i++) {
                            object = discount_array.getJSONObject(i);
                            Log.e("GetList object", object + "");

                            disList.add(object.getString("name"));
                            discountAdapter = new ArrayAdapter<>(AdminProfile.this, R.layout.expense_spinner, disList);
                            discount_spinner.setAdapter(discountAdapter);
                        }

                        if (discount_array.length()>0) {


                            discount_spinner.setVisibility(View.VISIBLE);
                            no_discount.setVisibility(View.GONE);

                            text2.setVisibility(View.VISIBLE);
                        }else {
                            discount=0.0;
                            text2.setVisibility(View.GONE);
                            no_discount.setVisibility(View.GONE);
                            discount_spinner.setVisibility(View.GONE);


                        }
                        passValues(array, discount_array);

                    } else {
//                        Toast.makeText(AdminProfile.this, "", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(AdminProfile.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public void passValues(JSONArray array, JSONArray discount_array) {


        DecimalFormat df = new DecimalFormat("###.#");
        time_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String batch_name = time_spinner.getItemAtPosition(time_spinner.getSelectedItemPosition()).toString();
                Log.e("batch_name", batch_name + "");
                try {
                    object = array.getJSONObject(i);
                    Log.e("Spinner object", object + "");
                    fees.setText(object.getString("amount"));
                    feess=Double.parseDouble(object.getString("amount"));
                    total_amount=feess-discount;
                    tvamount.setText("Total fees ₹ "+df.format(total_amount));
                } catch (JSONException e) {
                    Log.e("eeee",e.toString());
                    e.printStackTrace();
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(AdminProfile.this, "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });

        discount_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String batch_name = time_spinner.getItemAtPosition(time_spinner.getSelectedItemPosition()).toString();
                Log.e("batch_name", batch_name + "");
                try {
                    object1 = discount_array.getJSONObject(i);
                    Log.e("Spinner object", object1 + "");
//                    fees.setText(object1.getString("amount"));
//                    feess=Double.parseDouble(object.getString("amount"));
                    discount=object1.getDouble("amount");
                    total_amount=feess-discount;
                    tvamount.setText("Total fees ₹ "+df.format(total_amount));
                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.e("e",e.toString());
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(AdminProfile.this, "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void renewPartner() {
        MyRequestQueue = Volley.newRequestQueue(AdminProfile.this);
        MyStringRequest = new StringRequest(Request.Method.POST, renew_partner_account, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        JSONObject object = jObj.getJSONObject("data");

                        if (object.getString("month").equalsIgnoreCase("1")) {
                            Toast.makeText(AdminProfile.this, "Congrats! Account is renewed for" + object.getString("month") + " month.", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(AdminProfile.this, "Congrats! Account is renewed for" + object.getString("month") + " months.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AdminProfile.this, "Sorry! event is not found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(AdminProfile.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", sharedPreferences.getuserID());
                params.put("date", "");
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }
}
