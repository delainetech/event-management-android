package com.bzzy.Activities;

import android.Manifest;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;
import com.bzzy.utils.Common;
import com.bzzy.utils.FileDownloader;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class IdentityCardActivity extends AppCompatActivity implements ApiCalls {


    FloatingActionButton iv_download,iv_share;
    String user_id="",pdf_link,pdf_name;
    UserSharedPreferences user;
    private ProgressDialog pDialog;
    WebView webview;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.identity_card_webbview);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        user=new UserSharedPreferences(this);
        user_id = getIntent().getExtras().getString("user_id");
        pDialog = new ProgressDialog(IdentityCardActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        iv_download=findViewById(R.id.iv_download);
        webview=(WebView)findViewById(R.id.webview);
        iv_share=findViewById(R.id.iv_share);
        getUserData(user_id);
        webview.loadUrl("http://139.59.88.42/Event-Managment/public/student_card/" + user_id+"/"+user.geteventId());

        iv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pdf_link.length()>0 && pdf_name.length()>0){

                    if (haveStoragePermission()) {



                        downloadFile(pdf_link,pdf_name,IdentityCardActivity.this);

                    }else {


                    }
                }

            }
        });

        iv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!checkdownload(pdf_name)){
                    if (haveStoragePermission()) {



                        downloadFile(pdf_link,pdf_name,IdentityCardActivity.this);


                        open_File(pdf_name);


                    }

                }else {

                    open_File(pdf_name);
                }

            }
        });

    }

    public static boolean checkdownload(String filename) {


        File f = new File(filename);

        if(!f.exists()) {
            //Log.e("file://","file not exist");
            return false;
            //f.mkdirs();
        }
        else {
            //Log.e("file://","file exist");
            return true;
        }
    }

    public void downloadFile(String filePath,String filename,Context context)
    {
        Log.e("check","file download");
        new DownloadFileFromURL(filePath,filename,context).execute();
    }


    public  boolean haveStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.e("Permission error","You have permission");
                return true;
            } else {

                Log.e("Permission error","You have asked for permission");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //you dont need to worry about these stuff below api level 23
            Log.e("Permission error","You already have the permission");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            //you have the permission now.
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(pdf_link));
            request.setTitle(pdf_name);
            request.setDescription(pdf_name);
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            String filename = URLUtil.guessFileName(pdf_name, null, MimeTypeMap.getFileExtensionFromUrl(pdf_link));
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename);
            DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
        }
    }
    public void getUserData(String user_id) {

        pDialog.show();
        RequestQueue MyRequestQueue = Volley.newRequestQueue(IdentityCardActivity.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, generate_card, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                  JSONObject  jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {


                        pDialog.dismiss();
                        JSONObject object=jObj.getJSONObject("data");


                        webview.getSettings().setJavaScriptEnabled(true);
                        String pdf = "http://139.59.88.42/Event-Managment/public/pdf/"+object.getString("id")+".pdf";
                        pdf_name=object.getString("id")+".pdf";
                        pdf_link="http://139.59.88.42/Event-Managment/public/pdf/"+object.getString("id")+".pdf";


                        Log.e("card",pdf);



                    } else {

                        pDialog.dismiss();
                        Toast.makeText(IdentityCardActivity.this, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {

                    pDialog.dismiss();
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                pDialog.dismiss();
                String message = null;

                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(IdentityCardActivity.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("event_id",user.geteventId());
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }



    public  class DownloadFileFromURL extends AsyncTask<String, Integer, String> {

        String filepath1;
        ProgressBar progressBar;
        String Filename;
        Context context;
        String Url = "";

        public DownloadFileFromURL(String filepath, String filename, Context context) {
            this.filepath1 = filepath;
            this.Filename = filename;
            Url = filepath;
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub

                }
            }, 1000);

        }

        @Override
        protected String doInBackground(String... Urll) {
            try {
                Log.e("url1111", Url + "");
//                java.net.URL url = new URL(Url[0]);
//                Log.e("url234",url+"");
                URL url = new URL(Url);

                URLConnection connection = url.openConnection();
                connection.connect();

                // Detect the file lenghth
                int fileLength = connection.getContentLength();
                Log.e("fileLength", fileLength + " ");
                // Locate storage location
                String filepath = Environment.getExternalStorageDirectory().getAbsolutePath();
                //.getPath();

                File myDir = new File(filepath + "/BZZY");
                myDir.mkdirs();
                File file = new File(myDir, Filename);
                if (file.exists()) {
                     file.delete();

                    InputStream input = new BufferedInputStream(url.openStream());

                    // Save the downloaded file

                    OutputStream output = new FileOutputStream(file);

                    byte data[] = new byte[1024];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // Publish the progress
                        publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                    // Close connection
                    output.flush();
                    output.close();
                    input.close();
                    Toast.makeText(context, "File Downloaded", Toast.LENGTH_SHORT).show();

                } else {
                    // Download the file

                    InputStream input = new BufferedInputStream(url.openStream());

                    // Save the downloaded file

                    OutputStream output = new FileOutputStream(file);

                    byte data[] = new byte[1024];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // Publish the progress
                        publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                    // Close connection
                    output.flush();
                    output.close();
                    input.close();
                    Toast.makeText(context, "File Downloaded", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                // Error Log
                //Log.e("response_code","e",e);
                //Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("s", s + "file downloaded");




        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);


        }


    }

    public void open_File(String filename) {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/BZZY", filename);

        if (file.exists()) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("application/pdf");
            startActivity(intent);
        } else {
            Log.e("DEBUG", "File doesn't exist");
        }
    }

}
