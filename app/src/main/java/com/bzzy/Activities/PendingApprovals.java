package com.bzzy.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.PendingApprovalAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import me.philio.pinentry.PinEntryView;

public class PendingApprovals extends AppCompatActivity implements ApiCalls {
    RecyclerView expenditure_history_recy;
    PendingApprovalAdapter adapter;
    TextView partner_name,sent_to_mail;
    ImageView partner_image;
    String jsonString;
    JSONObject object;
    TextView error_text;
    ImageView delete_btn;
    Dialog getDialog;
    ProgressBar d_progressbar;
    ProgressBar get_progress;
    UserSharedPreferences sharedPreferences;
    PinEntryView pinEntryView;
    String pin_enter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_approvals);

        sharedPreferences = new UserSharedPreferences(PendingApprovals.this);
        try {

            Intent intent = getIntent();
            jsonString = intent.getStringExtra("jsonObject");
            Log.e("jsonString", jsonString);

            initializations();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void initializations() throws JSONException {


        object = new JSONObject(jsonString);
        partner_image = (ImageView) findViewById(R.id.partner_image);
        partner_name = (TextView) findViewById(R.id.partner_name);
        error_text=(TextView) findViewById(R.id.error_text);
        delete_btn=(ImageView)findViewById(R.id.delete_btn);
        d_progressbar=(ProgressBar)findViewById(R.id.d_progressbar);

        if (object.getString("image").length() > 4)
            Picasso.get().load(advertising_images + object.getString("image")).fit().error(R.drawable.error_circle).into(partner_image);

        partner_name.setText(object.getString("name"));

        expenditure_history_recy = (RecyclerView) findViewById(R.id.expenditure_history_recycler);
        expenditure_history_recy.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(PendingApprovals.this, LinearLayoutManager.VERTICAL, false);
        expenditure_history_recy.setLayoutManager(horizontalLayoutManager3);



        delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataDialog alert = new getDataDialog();
                alert.showDialog(PendingApprovals.this);

            }
        });

    }

    public static int convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int dp = (int) (px / (metrics.densityDpi / 160f));
        return dp;
    }


    public class getDataDialog {
        void showDialog(Activity activity) {


            getDialog = new Dialog(activity);
            getDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            getDialog.setCancelable(true);
            getDialog.setCanceledOnTouchOutside(false);
            WindowManager.LayoutParams wlmp = getDialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            getDialog.getWindow().setAttributes(wlmp);
            getDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            getDialog.getWindow().setLayout(screenWidth, screenHeight);
            getDialog.setContentView(R.layout.master_pin_dialog);

            get_progress= (ProgressBar) getDialog.findViewById(R.id.progress_bar);
            get_progress.setVisibility(View.GONE);
            pinEntryView = (PinEntryView) getDialog.findViewById(R.id.pin_entry_simple);


            pinEntryView.setOnPinEnteredListener(new PinEntryView.OnPinEnteredListener() {
                @Override
                public void onPinEntered(String pin) {
                    pin_enter = pin;
//                Toast.makeText(AdminProfile.this, "Pin entered: " + pin_enter, Toast.LENGTH_LONG).show();
                }
            });
            sent_to_mail = (TextView) getDialog.findViewById(R.id.submit);

            sent_to_mail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                        if (pin_enter == null) {
                            Toast.makeText(activity, "Please enter complete PIN!", Toast.LENGTH_SHORT).show();
                        } else if (!sharedPreferences.getMasterPin().equalsIgnoreCase(pin_enter)) {
                            Toast.makeText(activity, "Incorrect Master PIN!", Toast.LENGTH_SHORT).show();
                        } else {
                          if (pin_enter.equalsIgnoreCase(sharedPreferences.getMasterPin())){

                              AlertDialog myQuittingDialogBox =new AlertDialog.Builder(PendingApprovals.this)
                                      //set message, title, and icon
                                      .setTitle("Delete")
                                      .setMessage("Do you want to Delete")
                                      .setIcon(R.drawable.ic_delete)

                                      .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                                          public void onClick(DialogInterface dialog, int whichButton) {
                                              //your deleting code
                                              dialog.dismiss();
                                              getDialog.dismiss();

                                              try {
                                                  DeleteApprovals(object.getString("id"));
                                              } catch (JSONException e) {
                                                  e.printStackTrace();
                                              }

                                          }

                                      })

                                      .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                          public void onClick(DialogInterface dialog, int which) {

                                              dialog.dismiss();

                                          }
                                      })
                                      .create();
                              myQuittingDialogBox.show();
                          }else {

                              Toast.makeText(activity, "Enter Correct Master pin", Toast.LENGTH_SHORT).show();
                          }




                        }


                }
            });

            getDialog.show();
        }
    }
    public void MasterpinCheck(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PendingApprovals.this);
        alertDialog.setTitle("Enter Master Password");
        alertDialog.setMessage("Enter Password");

        final EditText input = new EditText(PendingApprovals.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(convertPixelsToDp(15,this), 0, convertPixelsToDp(15,this), 0);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setIcon(R.drawable.ic_vpn_key_black_24dp);

        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                      String  password = input.getText().toString();
                        if (password.compareTo("") == 0) {
                            if (password.equals(password)) {
                                Toast.makeText(getApplicationContext(),
                                        "Password Matched", Toast.LENGTH_SHORT).show();
//                                Intent myIntent1 = new Intent(view.getContext(),
//                                        Show.class);
//                                startActivityForResult(myIntent1, 0);
                            } else {
                                Toast.makeText(getApplicationContext(),
                                        "Wrong Password!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }




    private void DeleteApprovals(String id) {

        d_progressbar.setVisibility(View.VISIBLE);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(PendingApprovals.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, remove_partner, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {


                        d_progressbar.setVisibility(View.GONE);
                        Toast.makeText(PendingApprovals.this, jObj.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {

                        d_progressbar.setVisibility(View.GONE);
                        Toast.makeText(PendingApprovals.this, jObj.getString("message"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e1) {
                    d_progressbar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(PendingApprovals.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("event_id", sharedPreferences.geteventId());
                params.put("master_pin",sharedPreferences.getMasterPin());
                params.put("removel_id",id);
                params.put("user_id", sharedPreferences.getuserID() );
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            pendingApprovals(object.getString("id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void pendingApprovals(String id) {
        RequestQueue MyRequestQueue = Volley.newRequestQueue(PendingApprovals.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, pending_approvals, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        JSONArray array = jObj.getJSONArray("data");
                        Log.e("Data in pending", array + "");
                        adapter = new PendingApprovalAdapter(PendingApprovals.this, array);
                        expenditure_history_recy.setAdapter(adapter);
                    } else {
                        expenditure_history_recy.setVisibility(View.GONE);
                        error_text.setVisibility(View.VISIBLE);
//                        Toast.makeText(PendingApprovals.this, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(PendingApprovals.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("event_id", sharedPreferences.geteventId());
                params.put("user_id", id );
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }
}
