package com.bzzy.Activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ContactUs extends AppCompatActivity implements ApiCalls, View.OnClickListener {

    TextView back;
    StringRequest MyStringRequest;
    RequestQueue MyRequestQueue;
    ImageView mobile_no, email;
    JSONObject object;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        back=(TextView) findViewById(R.id.toolbar);
        back.setOnClickListener(this);
        email=findViewById(R.id.email);
        email.setOnClickListener(this);
        mobile_no=findViewById(R.id.phone);
        mobile_no.setOnClickListener(this);
        progressBar=findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);


        setPhoneNo();
    }

    public void setPhoneNo() {
        progressBar.setVisibility(View.VISIBLE);
        MyRequestQueue = Volley.newRequestQueue(ContactUs.this);
        MyStringRequest = new StringRequest(Request.Method.POST, contact_us, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar.setVisibility(View.GONE);
                        JSONArray array = jObj.getJSONArray("data");

                        for (int i=0;i<array.length();i++){
                             object=array.getJSONObject(i);
//                            phone_no.setText("Please call us on "+ object.getString("phone"));
                        }

                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ContactUs.this, "No contact found!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ContactUs.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v==back){
            finish();
        }else if (v==email){
            Intent intent = null;
            try {
                intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto",object.getString("email"), null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(intent, "Choose an Email client :"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else if (v==mobile_no){
            Uri uri = null;
            try {
                uri = Uri.parse("tel:" + object.getString("phone"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Intent DialIntent = new Intent(Intent.ACTION_DIAL, uri);
            DialIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
          startActivity(DialIntent);
        }

    }
}
