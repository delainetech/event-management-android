package com.bzzy.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bzzy.Adapters.InvestmentExpenseAdapter;
import com.bzzy.Adapters.MonthlyExpendiAdapter;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.Constants.VolleyMultipartRequest;
import com.bzzy.Popus.ExpenseBill;
import com.bzzy.R;
import com.bzzy.SignInProcess.MainScreen;
import com.jsibbold.zoomage.ZoomageView;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import live.utkarshdev.maincontentslidingnavview.ContentSlidingDrawerLayout;

import static com.bzzy.Api.ApiCalls.add_category;
import static com.bzzy.Api.ApiCalls.advertising_images;
import static com.bzzy.Api.ApiCalls.change_pin_mst;
import static com.bzzy.Api.ApiCalls.change_profile;
import static com.bzzy.Api.ApiCalls.get_expenditure_home;

public class ExpenditureActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    RecyclerView invest_expenditure, monthly_expenses;
    InvestmentExpenseAdapter adapter;
    RoundCornerProgressBar progressBar, income_bar;
    ImageView drawer, add_expense, add_monthly_expense, admin_profile;
    MonthlyExpendiAdapter expendiAdapter;
    TextView total_income, total_expense, current_month, admin_name_drawer, event_name_drawer;
    ImageView admin_image_drawer, drawer_icon;
    UserSharedPreferences sharedPreferences;
    TextView onetime_total, monthly_total;
    Dialog dialog,perDialog;
    ContentSlidingDrawerLayout dLayout;
    ProgressBar progressBar1;
    Spinner pin_spinner;
    ArrayList<String> pin_list = new ArrayList<>();
    String change;
    EditText old_pin, new_pin, confirm_pin;
    TextView submit_pin;
    RequestQueue MyRequestQueue;
    StringRequest MyStringRequest;
    JSONArray array;
    JSONArray data;
    ProgressBar change_pin_prgress;


    int[] investment_cards = {
            R.drawable.white_card_background,
            R.drawable.white_card_background,
            R.drawable.white_card_background,
            R.drawable.white_card_background,
    };

    int[] monthly_expense = {
            R.drawable.white_card_background,
            R.drawable.white_card_background,
            R.drawable.white_card_background,
            R.drawable.white_card_background,
    };
    Bitmap bitmap = null, bitmap1 = null;
    final List<String> cardList = new ArrayList(Arrays.asList(investment_cards));
    final List<String> expenseList = new ArrayList(Arrays.asList(monthly_expense));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenditure);

        Utility.checkPermission(ExpenditureActivity.this);
        sharedPreferences = new UserSharedPreferences(ExpenditureActivity.this);

        initialization();
    }

    public void initialization() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        progressBar1 = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar1.setVisibility(View.GONE);

        dLayout = (ContentSlidingDrawerLayout) findViewById(R.id.drawer_layout); // initiate a DrawerLayout

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        dLayout.init(this, navigationView, getSupportActionBar());

        drawer = (ImageView) findViewById(R.id.drawer);
        drawer.setOnClickListener(this);
        admin_name_drawer = (TextView) header.findViewById(R.id.tvUserName);
        admin_name_drawer.setOnClickListener(this);
        admin_image_drawer = (ImageView) header.findViewById(R.id.ProfileImages);
        event_name_drawer = (TextView) header.findViewById(R.id.event_name);

        monthly_total = (TextView) findViewById(R.id.monthly_total_amount);
        onetime_total = (TextView) findViewById(R.id.investment_amount);
        current_month = (TextView) findViewById(R.id.month);
        total_expense = (TextView) findViewById(R.id.total_expenditure);
        total_income = (TextView) findViewById(R.id.total_investment);

        admin_profile = (ImageView) findViewById(R.id.admin_profile);
        admin_profile.setOnClickListener(this);

        if (sharedPreferences.getimage().length() > 4)
            Picasso.get().load(advertising_images + sharedPreferences.getimage()).fit().error(R.drawable.error_circle).into(admin_profile);

        add_expense = (ImageView) findViewById(R.id.add_expenditure);
        add_expense.setOnClickListener(this);

        add_monthly_expense = (ImageView) findViewById(R.id.add_monthly_expense);
        add_monthly_expense.setOnClickListener(this);

        invest_expenditure = (RecyclerView) findViewById(R.id.investmentRecycler);
        monthly_expenses = (RecyclerView) findViewById(R.id.monthly_Recycler);

        invest_expenditure.setItemAnimator(new DefaultItemAnimator());
        monthly_expenses.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(ExpenditureActivity.this, LinearLayoutManager.HORIZONTAL, false);
        invest_expenditure.setLayoutManager(horizontalLayoutManager3);

        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        monthly_expenses.setLayoutManager(manager);

        progressBar = (RoundCornerProgressBar) findViewById(R.id.progress_bar_statics);
        progressBar.getProgress();
        progressBar.setProgress(0);
        income_bar = (RoundCornerProgressBar) findViewById(R.id.income_bar);
        income_bar.getProgress();
        income_bar.setProgress(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showExpenditureHome();

        if (!sharedPreferences.getname().equalsIgnoreCase("null")) {
            admin_name_drawer.setText(sharedPreferences.getname());
        }
        event_name_drawer.setText(sharedPreferences.getEventName());

        if (sharedPreferences.getimage().length() > 4)
            Picasso.get().load(advertising_images + sharedPreferences.getimage()).fit().error(R.drawable.error_circle).into(admin_image_drawer);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                dLayout.openDrawer(GravityCompat.START);  // OPEN DRAWER
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.about_us) {
            Intent intent = new Intent(ExpenditureActivity.this, AboutUs.class);
            startActivity(intent);
            dLayout.closeDrawer(Gravity.START);
        } else if (id == R.id.change_pin) {
            Change_pin alert = new Change_pin();
            alert.showDialog(ExpenditureActivity.this);
            dLayout.closeDrawer(Gravity.START);
        } else if (id == R.id.earn_with_us) {
            Intent intent = new Intent(ExpenditureActivity.this, EarnWithUs.class);
            startActivity(intent);
            dLayout.closeDrawer(Gravity.START);
        } else if (id == R.id.contact_us) {
            Intent intent = new Intent(ExpenditureActivity.this, ContactUs.class);
            startActivity(intent);
            dLayout.closeDrawer(Gravity.START);
        } else if (id == R.id.suggestion) {
            Intent intent = new Intent(ExpenditureActivity.this, Suggestion.class);
            startActivity(intent);
            dLayout.closeDrawer(Gravity.START);
        } else if (id == R.id.report) {
            Intent intent = new Intent(ExpenditureActivity.this, ReportProblem.class);
            startActivity(intent);
            dLayout.closeDrawer(Gravity.START);
        } else if (id == R.id.sign_out) {
            LogoutDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v == drawer) {                                                  // Open Drawer
            dLayout.openDrawer(Gravity.START);
        } else if (v == add_expense) {                                      // Adding One time investment Expense card
            AddCategoryDialog alert = new AddCategoryDialog();
            alert.showDialog(ExpenditureActivity.this, 1);
        } else if (v == add_monthly_expense) {                            // Adding Monthly Expense card
            AddCategoryDialog alert = new AddCategoryDialog();
            alert.showDialog(ExpenditureActivity.this, 2);
        } else if (v == admin_profile) {
            // Admin Profile
//            Intent intent = new Intent(ExpenditureActivity.this, AdminProfile.class);
//            startActivity(intent);

            PerviewDialog();


        } else if (v == admin_name_drawer) {
            Intent intent = new Intent(ExpenditureActivity.this, AdminProfile.class);
            startActivity(intent);
            dLayout.closeDrawer(Gravity.START);
        }
    }


    private void PerviewDialog() {

        perDialog = new Dialog(this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        perDialog.setCanceledOnTouchOutside(false);
        perDialog.setCancelable(true);
        WindowManager.LayoutParams wlmp = perDialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.dimAmount = 0.0F;
        perDialog.getWindow().setAttributes(wlmp);
        perDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        perDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        perDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        perDialog.getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
//
//        DisplayMetrics metrics = getResources().getDisplayMetrics();
//        int screenWidth = (int) (metrics.widthPixels * 1.00);
//        int screenHeight = (int) (metrics.heightPixels * 1.00);
//        perDialog.getWindow().setLayout(screenWidth, screenHeight);
        perDialog.setContentView(R.layout.preview_image);
        perDialog.show();

        ZoomageView imageview=(ZoomageView) perDialog.findViewById(R.id.myZoomageView);

        ImageView iv_edit=(ImageView)perDialog.findViewById(R.id.iv_edit);
        ImageView iv_back=(ImageView)perDialog.findViewById(R.id.iv_back);


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                perDialog.dismiss();
            }
        });
        Glide.with(ExpenditureActivity.this)
                .load(advertising_images + sharedPreferences.getimage())
                .placeholder(R.drawable.error_circle)
                .into(imageview);

        iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(ExpenditureActivity.this);
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("codes", requestCode + "," + resultCode + "," + data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.e("uri", resultUri + "");
//                image_path.setVisibility(View.VISIBLE);
//                image_path.setText(resultUri + "");
//                profile_picture.setImageURI(resultUri);
                try {
                    bitmap1 = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                    bitmap = getResizedBitmap(bitmap1, 512);
                    UpdateAdminProfilePic();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("in", error + "");
            }
        }
    }
    ProgressDialog pDialog;
    private void UpdateAdminProfilePic() {


        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, change_profile,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        Log.e("Response", new String(response.data) + " ");
                        //This code is executed if the server responds, whether or not the response contains data.
                        try {
                            JSONObject jObj = new JSONObject(new String(response.data));
                            String status = jObj.getString("status");

                            Toast.makeText(ExpenditureActivity.this, jObj.getString("message"), Toast.LENGTH_SHORT).show();
                            if (status.equals("success")) {
                                pDialog.dismiss();
                                perDialog.dismiss();
                                JSONObject object = jObj.getJSONObject("data");

                                sharedPreferences.setname(object.getString("name"));
                                sharedPreferences.setimage(object.getString("image"));
                                sharedPreferences.setMasterPin(object.getString("master_pin"));

//                                Picasso.get().load(advertising_images + sharedPreferences.getimage()).fit().error(R.drawable.error_circle).into(admin_image_drawer);
                                Glide.with(ExpenditureActivity.this)
                                        .load(advertising_images + sharedPreferences.getimage())
                                        .centerCrop()
                                        .placeholder(R.drawable.error_circle)
                                        .into(admin_profile);


                            } else {
                                pDialog.dismiss();

                            }
                        } catch (JSONException e1) {
                            pDialog.dismiss();
                            Log.e("Exception", e1 + " ");
                            e1.printStackTrace();
                        }
                    }
                },
                error -> {
                    pDialog.dismiss();


                    Log.e("onErrorResponse: ", error + "");
                    NetworkResponse networkResponse = error.networkResponse;
                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";
                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }

                        Toast.makeText(ExpenditureActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("user_id", sharedPreferences.getuserID());


                if (bitmap == null) {
                    params.put("image", "");
                }
                Log.e("getParams: ", params + "");
                return params;
            }

            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                if (bitmap != null) {
                    params.put("image", new DataPart(imagename + ".jpg", getFileDataFromDrawable(bitmap)));
                }
                Log.e("params", params + "");
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    public static byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    // Logout from app
    public void LogoutDialog() {
        dLayout.closeDrawer(Gravity.START);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle("Warning!");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("Logout",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        new UserSharedPreferences(getApplicationContext()).removeUser();
                        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = mPrefs.edit();
                        editor.clear();
                        editor.commit();

                        Toast.makeText(getApplicationContext(), "You have successfully logout", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ExpenditureActivity.this, MainScreen.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                        finishAffinity();
                        dLayout.closeDrawer(GravityCompat.START);
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    // Check permission method for Picture
    public static class Utility {
        static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

        static boolean checkPermission(final Context context) {
            int currentAPIVersion = Build.VERSION.SDK_INT;
            if (currentAPIVersion >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Permission necessary");
                        alertBuilder.setMessage("External storage permission is necessary");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();
                    } else {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
    }

    public void showExpenditureHome() {
        progressBar1.setVisibility(View.VISIBLE);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(ExpenditureActivity.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, get_expenditure_home, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar1.setVisibility(View.GONE);
// Set Home values
                        setValues(jObj);
// One time adapter
                        array = jObj.getJSONArray("one_time");

                        adapter = new InvestmentExpenseAdapter(ExpenditureActivity.this, cardList, array);
                        invest_expenditure.setAdapter(adapter);
// monthly adapter data
                        data = jObj.getJSONArray("monthly");
                        expendiAdapter = new MonthlyExpendiAdapter(ExpenditureActivity.this, expenseList, data);
                        monthly_expenses.setAdapter(expendiAdapter);

//                        Toast.makeText(ExpenditureActivity.this, "Congrats Shift is added successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        progressBar1.setVisibility(View.GONE);
                        Toast.makeText(ExpenditureActivity.this, "Sorry! Email is not registered with us.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar1.setVisibility(View.GONE);
                Toast.makeText(ExpenditureActivity.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id", sharedPreferences.geteventId());
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    // Add new Category api
    public void addCategory(String cat_name, int type) {
        MyRequestQueue = Volley.newRequestQueue(ExpenditureActivity.this);
        MyStringRequest = new StringRequest(Request.Method.POST, add_category, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        JSONObject object = jObj.getJSONObject("data");
                        Log.e("Data in Shift", object + "");

                        if (object.getString("type").equalsIgnoreCase("1")) {
                            Log.e("if", "111");
//                            adapter.notifyDataSetChanged();
                            showExpenditureHome();
                        } else {
                            Log.e("else", "00");
//                            expendiAdapter.notifyDataSetChanged();
                            showExpenditureHome();
                        }
                        Log.e("type", object.getString("type") + "");
                        dialog.dismiss();
                        Toast.makeText(ExpenditureActivity.this, "Congrats! new category is added.", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(ExpenditureActivity.this, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ExpenditureActivity.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("event_id", sharedPreferences.geteventId());
                params.put("name", cat_name);
                params.put("type", type + "");
                params.put("sub_cat", "other");
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);

    }

    // Home values
    public void setValues(JSONObject jObj) throws JSONException {
        float value_monthly= Float.parseFloat(jObj.getString("onetime_total"))+Float.parseFloat(jObj.getString("mothly_total"));
       String value=String.format("%.0f", value_monthly);
        total_expense.setText("₹ " + value);
        total_income.setText("₹ " + jObj.getString("total_income"));
        monthly_total.setText("₹ " + jObj.getString("mothly_total"));



        onetime_total.setText("₹ " +  String.format("%02.0f",Double.parseDouble( jObj.getString("onetime_total_comp"))));
        current_month.setText(jObj.getString("current_month"));

// progress expenditure
        float total_value = value_monthly + Float.parseFloat(jObj.getString("total_income"));
        float progress = (value_monthly / total_value) * 100;
        progressBar.setProgress(progress);
        Log.e("expenditure progress", progress + "");

//  Progress income
        float total_value_income = value_monthly + Float.parseFloat(jObj.getString("total_income"));
        float income_progress = (Float.parseFloat(jObj.getString("total_income")) / total_value_income) * 100;
        income_bar.setProgress(income_progress);
        Log.e("income progress", income_progress + "");

    }

    public class AddCategoryDialog {
        void showDialog(Activity activity, int status) {
            dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.add_category);


            TextView add = (TextView) dialog.findViewById(R.id.addCat_btn);
            EditText add_Category = (EditText) dialog.findViewById(R.id.add_category);

            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (add_Category.getText().toString().trim().equalsIgnoreCase("")) {
                        add_Category.setError("Please provide category name");
                    } else {
                        addCategory(add_Category.getText().toString().trim(), status);
                    }
                }
            });

            dialog.show();
        }
    }

    public class Change_pin {
        void showDialog(Activity activity) {
            pin_list.clear();
            dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.activity_change_pin);

            old_pin = (EditText) dialog.findViewById(R.id.old_pin);
            new_pin = (EditText) dialog.findViewById(R.id.new_pin);
            confirm_pin = (EditText) dialog.findViewById(R.id.confirm_pin);
            submit_pin = (TextView) dialog.findViewById(R.id.submit);
            change_pin_prgress=(ProgressBar) dialog.findViewById(R.id.progress_bar);
            change_pin_prgress.setVisibility(View.GONE);
            pin_spinner = (Spinner) dialog.findViewById(R.id.pin_spinner);
            // Create an ArrayAdapter that will contain all list items
            ArrayAdapter<String> adapter;

            String[] menuArray = getResources().getStringArray(R.array.change_pin);

            for (int k = 0; k < menuArray.length; k++) {
                pin_list.add(menuArray[k]);
                adapter = new ArrayAdapter<>(ExpenditureActivity.this, R.layout.expense_spinner, pin_list);
                pin_spinner.setAdapter(adapter);
            }
// Get spinner value
            pin_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    change = String.valueOf(pin_spinner.getSelectedItemId());
//                    Toast.makeText(activity, change, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    Toast.makeText(ExpenditureActivity.this, "Nothing selected", Toast.LENGTH_SHORT).show();
                }
            });


            submit_pin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (change.equalsIgnoreCase("0")){
                        Toast.makeText(activity, "Please select option for Change PIN!", Toast.LENGTH_SHORT).show();
                    }
                   else if (change.equalsIgnoreCase("1")) {
                        validations(change);
                    } else if (change.equalsIgnoreCase("2")) {
                        validations2(change);
                    }

                }

                // validations
                private void validations(String type) {
                    if (old_pin.getText().toString().trim().equalsIgnoreCase("")) {
                        old_pin.setError("Please enter Old PIN");
                    } else if (old_pin.getText().toString().trim().length() != 4) {
                        old_pin.setError("Please enter 4 Digit PIN");
                    } else if (new_pin.getText().toString().trim().equalsIgnoreCase("")) {
                        new_pin.setError("Please enter New PIN");
                    } else if (new_pin.getText().toString().trim().length() != 4) {
                        new_pin.setError("Please enter 4 Digit PIN");
                    } else if (confirm_pin.getText().toString().trim().equalsIgnoreCase("")) {
                        confirm_pin.setError("Please re_enter New PIN");
                    } else if (!new_pin.getText().toString().equals(confirm_pin.getText().toString())) {
                        confirm_pin.setError("Password didn't match.");
                    } else {
                        changePin(type);
                    }
                }

            // validations
            private void validations2(String type) {
                if (old_pin.getText().toString().trim().equalsIgnoreCase("")) {
                    old_pin.setError("Please enter old Master PIN");
                } else if (old_pin.getText().toString().trim().length() != 6) {
                    old_pin.setError("Please enter 6 Digit Master PIN");
                } else if (new_pin.getText().toString().trim().equalsIgnoreCase("")) {
                    new_pin.setError("Please enter new Master PIN");
                } else if (new_pin.getText().toString().trim().length() != 6) {
                    new_pin.setError("Please enter 6 Digit Master PIN");
                } else if (confirm_pin.getText().toString().trim().equalsIgnoreCase("")) {
                    confirm_pin.setError("Please re_enter new Master PIN");
                } else if (!new_pin.getText().toString().equals(confirm_pin.getText().toString())) {
                    confirm_pin.setError("Password didn't match.");
                } else {
                    changePin(type);
                }
            }
        });

            dialog.show();
        }
    }

    // Change new PIN api
    public void changePin(String type) {
        change_pin_prgress.setVisibility(View.VISIBLE);
        MyRequestQueue = Volley.newRequestQueue(ExpenditureActivity.this);
        MyStringRequest = new StringRequest(Request.Method.POST, change_pin_mst, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        change_pin_prgress.setVisibility(View.GONE);
                        // Launch login activity
                        JSONObject object = jObj.getJSONObject("data");
                        dialog.dismiss();
                        Toast.makeText(ExpenditureActivity.this, "Your PIN is successfully Changed!", Toast.LENGTH_LONG).show();
                    } else {
                        change_pin_prgress.setVisibility(View.GONE);
                        Toast.makeText(ExpenditureActivity.this, "Sorry! PIN can't change or wrong PIN entered!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e1) {
                    change_pin_prgress.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                change_pin_prgress.setVisibility(View.GONE);
                Toast.makeText(ExpenditureActivity.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", sharedPreferences.getuserID());
                params.put("pin", confirm_pin.getText().toString().trim());
                params.put("type", type);
                params.put("old_pin", old_pin.getText().toString().trim());
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

}
