package com.bzzy.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.BatchAttendenceAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.EndlessRecyclerViewScrollListener;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Attendence extends AppCompatActivity implements ApiCalls, View.OnClickListener {
    RecyclerView batch_recycler;
    BatchAttendenceAdapter attendenceAdapter;
    TextView mark_attendence;
    Dialog dialog;
    UserSharedPreferences sharedPreferences;
    ArrayList<String> batch_name = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    Spinner batch_spinner;
    ImageView dialogButton;
    JSONObject object = null;
    JSONArray new_array = null;
    SearchView searchView;
    LinearLayoutManager horizontalLayoutManager;

    int[] backgrounds = {
            R.drawable.card_attendence_pink,
            R.drawable.voilet_square_card,
            R.drawable.light_green_square_card,
            R.drawable.blue_square_card

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendence);

        sharedPreferences = new UserSharedPreferences(this);
        initialization();
    }

    public void initialization() {
        mark_attendence = (TextView) findViewById(R.id.attendance);
        mark_attendence.setOnClickListener(this);
        batch_recycler = (RecyclerView) findViewById(R.id.batch_details);

        batch_recycler.setItemAnimator(new DefaultItemAnimator());

        horizontalLayoutManager = new LinearLayoutManager(Attendence.this, LinearLayoutManager.VERTICAL, false);
        batch_recycler.setLayoutManager(horizontalLayoutManager);
        batch_recycler.setNestedScrollingEnabled(false);


// Search bar ... searching by name and ID
        searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setQueryHint("Type a name or ID number");
        searchView.setIconified(false);
        searchView.setEnabled(true);
        searchView.clearFocus();
        searchView.setIconifiedByDefault(false);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN |
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
////        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getBatchList(searchView.getQuery().toString());
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                getBatchList(newText);
                return false;
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == mark_attendence) {
            Log.e("Start", "ed");
            ViewDialog alert = new ViewDialog();
            alert.showDialog(Attendence.this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        searchView.clearFocus();
        getBatchList("");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.e("finish", "fi");
        finish();
    }

    // Forgot View and Dialog
    public class ViewDialog {
        void showDialog(Activity activity) {
            Log.e("Start", "ed");
            dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.select_batch_popup);

            batch_spinner = (Spinner) dialog.findViewById(R.id.batch_spinner);

            getBatchData();

            dialogButton = (ImageView) dialog.findViewById(R.id.submit_button);

            dialog.show();
        }
    }

    public void getBatchData() {
        batch_name.clear();
        RequestQueue MyRequestQueue = Volley.newRequestQueue(Attendence.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, getBatchList, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {

                        // Launch login activity
                        JSONArray array = jObj.getJSONArray("data");
                        Log.e("GetList array  ", array + "");
                        JSONObject object = null;
                        for (int i = 0; i < array.length(); i++) {
                            object = array.getJSONObject(i);
                            Log.e("GetList object", object + "");

                            batch_name.add(object.getString("name"));
                            arrayAdapter = new ArrayAdapter<>(Attendence.this, R.layout.batch_spin_attendence, batch_name);
                            batch_spinner.setAdapter(arrayAdapter);
                        }

                        passValues(array);

//                        Toast.makeText(AddUserActivity.this, "Congrats Shift is added successfully.", Toast.LENGTH_SHORT).show();
                    } else {
//                        Toast.makeText(Attendence.this, "No Attendance taken yet.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(Attendence.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("event_id", sharedPreferences.geteventId());

                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public void getBatchList(String query) {
        RequestQueue MyRequestQueue = Volley.newRequestQueue(Attendence.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, attendence_batch_all, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        batch_recycler.setVisibility(View.VISIBLE);
                        // Launch login activity
                        JSONArray array = jObj.getJSONArray("data");


                        showAllData(array);

//                        Toast.makeText(AddUserActivity.this, "Congrats Shift is added successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        batch_recycler.setVisibility(View.GONE);
//                        Toast.makeText(Attendence.this, "No Attendance taken yet.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Attendence.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id", sharedPreferences.geteventId());
                params.put("keyword", query);
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }


    public void showAllData(JSONArray array) {

        attendenceAdapter = new BatchAttendenceAdapter(Attendence.this, array, backgrounds);
        batch_recycler.setAdapter(attendenceAdapter);

//        batch_recycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(horizontalLayoutManager) {
//            @Override
//            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//                Log.e("page", page + "," + array.length());
//                if (array.length() >= 10) {
//                    try {
//                        getBatchMore(array.getJSONObject(0).getString("id"));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        });
    }

    public void getBatchMore(String id) {
        RequestQueue MyRequestQueue = Volley.newRequestQueue(Attendence.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, attendence_batch_all, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        batch_recycler.setVisibility(View.VISIBLE);
                        // Launch login activity
                        JSONArray array = jObj.getJSONArray("data");

                        attendenceAdapter.notifyDataSetChanged();

//                        Toast.makeText(AddUserActivity.this, "Congrats Shift is added successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        batch_recycler.setVisibility(View.GONE);
//                        Toast.makeText(Attendence.this, "No Attendance taken yet.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Attendence.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id", sharedPreferences.geteventId());
                params.put("keyword", "");
                params.put("page", id + "");
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }


    public void passValues(JSONArray array) {
        batch_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String batch_name = batch_spinner.getItemAtPosition(batch_spinner.getSelectedItemPosition()).toString();
                Log.e("batch_name", batch_name + "");
                try {
                    object = array.getJSONObject(i);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("Spinner object", object + "");

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (batch_name.equals("")) {
                            Toast.makeText(Attendence.this, "Please select batch.", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                Intent intent = new Intent(Attendence.this, SelectAttendence.class);
                                intent.putExtra("batch_id", object.getString("id") + "");
                                startActivity(intent);

                                dialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(Attendence.this, "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
