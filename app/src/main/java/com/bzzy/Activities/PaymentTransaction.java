package com.bzzy.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;
import com.bzzy.utils.Common;
//import com.instamojo.android.helpers.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class PaymentTransaction extends Activity implements ApiCalls {

    WebView w;
    WebSettings set;
//    String type,postid;
    UserSharedPreferences pref;
    String test_name="";

    String st_name,st_email="",st_phone="",st_amount="",st_purpose="",st_from;


    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction);



        w = (WebView)findViewById(R.id.webView1);

        w.setWebViewClient(new MyBrowser());

        pref=new UserSharedPreferences(this);

        st_email=pref.getemail();
        st_name=pref.getname();
        st_phone=pref.getCurrent_time();

        Intent in=getIntent();

//        postid=in.getStringExtra("cate_id");
//        test_name=in.getStringExtra("test_name");
//        type=in.getStringExtra("type");

        st_amount=in.getStringExtra("amount");
        st_purpose=in.getStringExtra("purpose");
        st_from=in.getStringExtra("from");

        Log.e("test_name",st_purpose+" "+st_amount);

//		in.putExtra("purpose",jsondata.getString("title"));

        set = w.getSettings();

        set.setJavaScriptEnabled(true);

        volley();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("call_onDestroy()","Call onDestroy()");
        deleteCache(this);
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        String successurl= w.getUrl();
//        Log.e("successurl",successurl);

        if(successurl.equals(""))
        {

        }

        if ((keyCode == KeyEvent.KEYCODE_BACK) && w.canGoBack()) {
            w.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private class MyBrowser extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {


            // TODO Auto-generated method stub

            Log.e("onPage startedurl",url);

            if(url.contains("139.59.88.42")){
//                http://139.59.88.42/Event-Managment/public/loading
                Uri uri = Uri.parse(url);

                String pay_id=uri.getQueryParameter("payment_id");
                String pay_req_id=uri.getQueryParameter("payment_request_id");
                String status=uri.getQueryParameter("payment_status");

//                dbh.payment_int(postid,type,pay_id,pay_req_id,"false");

                volleyinsertpaymentinfo(pay_id,pay_req_id,status);

            }

//				  if(url.contains("status=AUTHORIZATION_FAILED")){
//				  	  payment_fail=false;
//				  }
//
//				  if(url.contains("https://www.instamojo.com/payment/status/")&payment_fail){
//
//					  Log.e("payment_success","payment_success");
//				  }
//
//
//	        	if(url.equals(""))
//	  		    {
//						try {
//
//							volleyinsertpaymentinfo();
//
//							//JSONObject jsonobj=new JSONObject(Utility.data.getJsonfield());
//
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//
//	  		    }

            super.onPageStarted(view, url, favicon);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if(dialog!=null&dialog.isShowing()){
                dialog.dismiss();
            }

//            if(url.contains("status=AUTHORIZATION_FAILED")){
//                payment_fail=false;
//                Log.e("on_page_finished_failed",url);
//            }
//            if(url.contains("https://www.instamojo.com/payment/status/")&url.contains("status=CHARGED")){
//
//                Log.e("on_page_finished_succes",url);
//            }
//            else
            Log.e("on_page_finished",url);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            Log.e("should Override url",url);



//				  if(url.contains("status=AUTHORIZATION_FAILED")){
//					  payment_fail=false;
//				  }
//
//				  if(url.contains("https://www.instamojo.com/payment/status/")&payment_fail){
//
//					  Log.e("payment_success","payment_success");
//				  }

            return true;

        }
    }

    public void volleyinsertpaymentinfo(final String pay_id, final String pay_req_id, String status)
    {


        RequestQueue queue= Volley.newRequestQueue(PaymentTransaction.this);
        JSONObject json =new JSONObject();

        String url= ROOT_URL+"webhook";
//        http://139.59.88.42/Event-Managment/public/api/webhook
        url=url.replaceAll(" ","%20");

        Log.e("payment_by_instamojo",url);

        StringRequest jsonreq=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                Log.e("response",s);
                try {
                    dialog.dismiss();
                    JSONObject res=new JSONObject(s);

                    if(res.getString("status").equals("success"))
                    {

                        Toast.makeText(PaymentTransaction.this, res.getString("message"),Toast.LENGTH_SHORT).show();


//                        dbh.payment_upd(postid,type,"true");
                    finish();

                    }
                    else {
                        Toast.makeText(PaymentTransaction.this,res.getString("message"),Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //Log.e("payment_exception","e",e);
                    dialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

//				post_id="+postid+"&email="+Constants.Email+"&type="+type+"&amount="+st_amount

                Map<String, String> param=new HashMap<String, String>();

//                Log.e("postid",postid+"");
                Log.e("email",pref.getemail());
//                Log.e("type",type+"");
                Log.e("amount",st_amount+"");
                Log.e("pay_id",pay_id+"");
                Log.e("pay_req_id",pay_req_id);

//                param.put("post_id",postid+"");
                param.put("buyer_phone",pref.getusernumber());
                param.put("status",status);
                param.put("email",pref.getemail());
                param.put("type","android");
                param.put("amount",st_amount+"");
                param.put("payment_id",pay_id+"");
                param.put("payment_request_id",pay_req_id+"");

                Log.e("param",param+"");
                return param;
            }
        };



        queue.add(jsonreq);
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonreq.setRetryPolicy(policy);

    }


    public void volley()
    {

        dialog = new ProgressDialog(PaymentTransaction.this,R.style.AppTheme);
        dialog.setMessage("Loading..Please wait.");
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

        RequestQueue queue= Volley.newRequestQueue(getApplicationContext());
        final String username;
        String url;

        url= ROOT_URL+"order_request";



        url=url.replace(" ","%20");

        Log.e("name", url);


        StringRequest jsonreq1=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {


                Log.e("res", s);

                try {

                    JSONObject res=new JSONObject(s);

                    w.loadUrl(res.getJSONObject("data").getString("longurl"));


                } catch (JSONException e) {

                    e.printStackTrace();
                    Log.e("payment_exception","e",e);
                    dialog.dismiss();
                }

//				dialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("payment_exception","e",volleyError);
                dialog.dismiss();
            }
        })

        {

            @Override
            protected Map<String, String> getParams() {

                Log.e("name",st_name+" ");
                Log.e("email",st_email+" ");
                Log.e("phone",st_phone+"8950177852");
                Log.e("amount",st_amount+" ");
                Log.e("purpose",st_purpose+" ");

                Map<String,String> param=new HashMap<>();

                param.put("name",st_name);
                param.put("email",st_email);
                param.put("user_id",pref.getuserID());
                param.put("phone",pref.getusernumber());
                param.put("months",st_from);
                param.put("amount",st_amount);
                param.put("purpose",st_purpose);
                param.put("event_id",pref.geteventId());


                Log.e("param ",param+"");

                return param;
            }
        };


        queue.add(jsonreq1);
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonreq1.setRetryPolicy(policy);
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

}

