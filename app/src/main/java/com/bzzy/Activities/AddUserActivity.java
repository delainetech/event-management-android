package com.bzzy.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.CarouselEffectTransformer;
import com.bzzy.Adapters.EditPagerAdapter;
import com.bzzy.Adapters.MyPagerAdapter;
import com.bzzy.Adapters.RegistrationAdapter;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.bzzy.Adapters.EditPagerAdapter.setIdImage.sIdImage;
import static com.bzzy.Adapters.EditPagerAdapter.setUserImage.sUserImage;
import static com.bzzy.Adapters.MyPagerAdapter.IdImage.setIdImage;
import static com.bzzy.Adapters.MyPagerAdapter.UserImage.setUserImage;
import static com.bzzy.Api.ApiCalls.getBatchList;

public class AddUserActivity extends AppCompatActivity {

    public Bitmap userBitmap=null, userBitmap1=null, idBitmap=null, bmap=null, idmap=null, userBitmap_edit=null;
    RecyclerView addUser_recycler;
    RegistrationAdapter adapter;
    public ViewPager registration;
    ImageView back;
    MyPagerAdapter pagerAdapter;
    EditPagerAdapter editPagerAdapter;
    Spinner batch_spinner;
    ArrayList<String> batch_name = new ArrayList<>();
    public ArrayAdapter<String> arrayAdapter;
    UserSharedPreferences sharedPreferences;
    public TextView batch_time, batch_fee, total_seats, vacant, occupied;
    TextView total_users;
    JSONArray img_array = new JSONArray();
    String user_details;
    JSONObject user_object;
    String date_cr;
    public int year, current_day, month;
    JSONArray domain_array;
    ProgressBar progressBar;
    RequestQueue MyRequestQueue;
    StringRequest MyStringRequest;
    Context mcontext;
    JSONObject main_data = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);

        sharedPreferences = new UserSharedPreferences(AddUserActivity.this);
        Utility.checkPermission(AddUserActivity.this);
        user_details = getIntent().getStringExtra("object");
        Log.e("user_details", user_details + "");

        if (user_details != null) {
            try {
                user_object = new JSONObject(user_details);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        try {
            initialization();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * Initialize all required variables
     */

    public void initialization() throws JSONException {
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        total_users = (TextView) findViewById(R.id.total_users);
        batch_spinner = (Spinner) findViewById(R.id.batch_spinner);
        batch_fee = (TextView) findViewById(R.id.batch_fee);
        batch_time = (TextView) findViewById(R.id.batch_time);
        total_seats = (TextView) findViewById(R.id.no_of_seats);
        occupied = (TextView) findViewById(R.id.no_of_occupied);
        vacant = (TextView) findViewById(R.id.no_of_vacant);
        back = (ImageView) findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        addUser_recycler = (RecyclerView) findViewById(R.id.registration_recycler);
        addUser_recycler.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(AddUserActivity.this, LinearLayoutManager.HORIZONTAL, false);
        addUser_recycler.setLayoutManager(horizontalLayoutManager3);


// ViewPager & Carousel effect bottom
        registration = (ViewPager) findViewById(R.id.viewpagerTop);
        registration.setClipChildren(false);
        registration.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.margin_5dp));
        registration.setOffscreenPageLimit(3);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            registration.setElevation(0.3f);
        }
        registration.setPageTransformer(false, new CarouselEffectTransformer(this)); // Set transformer

        date_cr = getDate();
        Log.e("date pawan", date_cr);

        if (user_details != null) {
            getBatchList();
        } else {
            getBatchData();
        }

    }

    public void getBatchList() {
        progressBar.setVisibility(View.VISIBLE);

        MyRequestQueue = Volley.newRequestQueue(AddUserActivity.this);
        MyStringRequest = new StringRequest(Request.Method.POST, getBatchList, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Responsebatch", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {

                        progressBar.setVisibility(View.GONE);
                        // Launch login activity
                        domain_array = jObj.getJSONArray("domain");
                        JSONArray array = jObj.getJSONArray("data");
                        Log.e("GetList array  ", array + "");
//                        progressDialog.dismiss();
                        JSONObject object = null;
                        for (int i = 0; i < array.length(); i++) {
                            object = array.getJSONObject(i);
                            Log.e("GetList object", object + "");

                            batch_name.add(object.getString("name"));

                            arrayAdapter = new ArrayAdapter<>(AddUserActivity.this, R.layout.spinner_batch, batch_name);
                            batch_spinner.setAdapter(arrayAdapter);
                        }
                        setEditSpinner(array, domain_array);


                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(AddUserActivity.this, "Sorry! event is not found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
//                    progressDialog.dismiss();
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(AddUserActivity.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");Log.e("Error", volleyError + "");
                Log.e("Error", volleyError + "");
//                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id", sharedPreferences.geteventId());
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public void getBatchData() {
        progressBar.setVisibility(View.VISIBLE);

        MyRequestQueue = Volley.newRequestQueue(AddUserActivity.this);
        MyStringRequest = new StringRequest(Request.Method.POST, getBatchList, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar.setVisibility(View.GONE);
                        // Launch login activity
                        JSONArray domain_array = jObj.getJSONArray("domain");
                        JSONArray array = jObj.getJSONArray("data");
                        Log.e("GetList array  ", array + "");
//                        progressDialog.dismiss();

                        for (int i = 0; i < array.length(); i++) {
                            main_data = array.getJSONObject(i);
                            Log.e("GetList object", main_data + "");
                            batch_name.add(main_data.getString("name"));
                            arrayAdapter = new ArrayAdapter<>(AddUserActivity.this, R.layout.spinner_batch, batch_name);
                            batch_spinner.setAdapter(arrayAdapter);
                        }
                        setDataSpinner(array, domain_array);

                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(AddUserActivity.this, "Create Batch to Add Members.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
//                    progressDialog.dismiss();
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(AddUserActivity.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");Log.e("Error", volleyError + "");
                progressBar.setVisibility(View.GONE);
//                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id", sharedPreferences.geteventId());
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public void setImageAdapter(JSONArray array) {
        Log.e("array image", array + "");
        adapter = new RegistrationAdapter(AddUserActivity.this, array);
        addUser_recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    // Get Values from Array through spinner
    public void setEditSpinner(JSONArray array, JSONArray domain_array) {
        try{
        if (batch_name.contains(user_object.getString("name"))) {
            Log.e("in if", user_object.getString("name") + "");
            String name = user_object.getString("name");
            batch_spinner.setSelection(batch_name.indexOf(name));
        }
        }catch (Exception e){
         Log.e("exception","",e);
        }

        batch_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    if (batch_name.contains(user_object.getString("name"))) {

                        Log.e("array", array + "");
                        JSONObject object = array.getJSONObject(i);

                        Log.e("domain_array", domain_array + "");

                        batch_fee.setText("Fee Rs" + " " + object.getString("fee"));
                        batch_time.setText(object.getString("start_time") + " to " + object.getString("end_time"));

                        Log.e("upper time",object.getString("start_time") + " to " + object.getString("end_time"));
                        total_seats.setText(object.getString("seats"));
                        occupied.setText(object.getString("occupied"));
                        int vacant_seat = Integer.parseInt(object.getString("seats")) - Integer.parseInt(object.getString("occupied"));

                        if (vacant_seat > 0) {
                            vacant.setText(vacant_seat + "");
                        } else vacant.setText(0 + "");

                        Log.e("value change","value change");
                        editPagerAdapter = new EditPagerAdapter(AddUserActivity.this, user_object, domain_array, object);
                        registration.setAdapter(editPagerAdapter);

                        // Setting values of data
//                        setValues(object, domain_array);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(AddUserActivity.this, "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });

//        batch_spinner.setEnabled(false);
    }

    // Get Values from Array through spinner
    public void setDataSpinner(JSONArray array, JSONArray domain_array) {
        batch_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                img_array = new JSONArray();
                try {
                    JSONObject object = array.getJSONObject(i);
                    Log.e("Spinner object", object + "");
                    // Set Spinner Data
                    // Adapter to set images of users
                    JSONArray array1 = object.getJSONArray("users");
                    if (array1.length() > 0) {
                        addUser_recycler.setVisibility(View.VISIBLE);
                        for (int k = 0; k < array1.length(); k++) {
                            if (k < 5) {
                                Log.e("jim", "if");
                                img_array.put(array1.getString(k));
                                total_users.setVisibility(View.GONE);
                            } else {
                                Log.e("jim", "else");
                                total_users.setVisibility(View.VISIBLE);
                                int value = array1.length() - 5;
                                total_users.setText("+" + value + "");
                            }
                            setImageAdapter(img_array);
                        }
                    } else {
                        addUser_recycler.setVisibility(View.GONE);
                        total_users.setVisibility(View.GONE);
                        Log.e("length", "else");
                    }
                    // Setting values of data
                    setValues(object, domain_array);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(AddUserActivity.this, "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setValues(JSONObject obj, JSONArray domain_array) {

        try {
            batch_fee.setText("Fee Rs" + " " + obj.getString("fee"));
            batch_time.setText(obj.getString("start_time") + " to " + obj.getString("end_time"));
            total_seats.setText(obj.getString("seats"));
            occupied.setText(obj.getString("occupied"));
            int vacant_seat = Integer.parseInt(obj.getString("seats")) - Integer.parseInt(obj.getString("occupied"));

            if (vacant_seat > 0) {
                vacant.setText(vacant_seat + "");
            } else vacant.setText(0 + "");

// Log.e("array_length", array.length() + "");
            pagerAdapter = new MyPagerAdapter(AddUserActivity.this, obj, domain_array, date_cr);
            registration.setAdapter(pagerAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void editValues(JSONObject obj, JSONArray domain_array) {

        try {
            batch_fee.setText("Fee Rs" + " " + obj.getString("fee"));
            batch_time.setText(obj.getString("start_time") + " to " + obj.getString("end_time"));
            total_seats.setText(obj.getString("seats"));
            occupied.setText(obj.getString("occupied"));
            int vacant_seat = Integer.parseInt(obj.getString("seats")) - Integer.parseInt(obj.getString("occupied"));

            if (vacant_seat > 0) {
                vacant.setText(vacant_seat + "");
            } else vacant.setText(0 + "");

// Log.e("array_length", array.length() + "");
            pagerAdapter = new MyPagerAdapter(AddUserActivity.this, obj, domain_array, date_cr);
            registration.setAdapter(pagerAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // OnActivity Result for Images
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("codes", requestCode + "," + resultCode + "," + data);
        if (requestCode == 251) {
            Log.e("code", requestCode + "");
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.e("uri", resultUri + "");

                try {
                    userBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                    userBitmap1 = getResizedBitmap(userBitmap, 512);
                    Log.e("userBitmap", userBitmap + "");
//                    MyPagerAdapter.user_image.setImageURI(resultUri);

                       setUserImage(userBitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("exception", e + "");
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else if (requestCode == 212) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
//                MyPagerAdapter.done.setVisibility(View.VISIBLE);
//                MyPagerAdapter.done.setText("uploaded successfully.");
                if (data != null) {
                    Uri resultUri = result.getUri();
                    Log.e("uri", resultUri + "");

                    try {
                        idBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                        idmap = getResizedBitmap(idBitmap, 512);
                        Log.e("idBitmap", idBitmap + "");
//                        MyPagerAdapter.id_card.setImageURI(resultUri);

                           setIdImage(idBitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e("exception", e + "");
                    }
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("error", error + "");
                MyPagerAdapter.done.setVisibility(View.VISIBLE);
                MyPagerAdapter.done.setText("Error uploading");
            }
        } else if (requestCode == 250) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.e("uri", resultUri + "");

                try {
                    idBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                    bmap = getResizedBitmap(idBitmap, 512);
//                    EditPagerAdapter.id_card.setImageURI(resultUri);
                    sIdImage(idBitmap);
                } catch (IOException e) {
                    Log.e("exception", e + "");
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("error", error + "");
            }
        } else if (requestCode == 211) {
            Log.e("code", requestCode + "");
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.e("uri", resultUri + "");

                try {
                    userBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                    userBitmap_edit = getResizedBitmap(userBitmap, 512);
                    Log.e("userBitmap", userBitmap + "");
//                    EditPagerAdapter.user_image.setImageURI(resultUri);
                    sUserImage(userBitmap);
                } catch (IOException e) {
                    Log.e("exception", e + "");
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void setUriNull(){

        userBitmap=null;
        userBitmap1=null;
        idBitmap=null;
        bmap=null;
        idmap=null;
        userBitmap_edit=null;

        Log.e("bitmap null","null set ");


    }

    public static byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }




    public String getDate() {
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        current_day = c.get(Calendar.DAY_OF_MONTH);
//        current_day=1;
//        month=1;
        Log.e("Month " + month, " " + "Year " + year+" date "+current_day);

        String date = null;
      String  date_string = String.format("%02d-%02d-%02d", current_day, month, year);
        Log.e("final_date_string",date_string);
//        if (current_day <= 9 && month <= 9) {
//            date = "0" + current_day + "-" + "0" + month + "-" + year;
//            Log.e("final date1",date);
//        } else if (current_day > 9 && month > 9) {
//            date = current_day + "-" + month + "-" + year;
//            Log.e("final date2",date);
//        }else if (current_day >9 && month<9){
//            date = current_day + "-" + "0" + month + "-" + year;
//            Log.e("final date3",date);
//        }else {
//            date = "0" + current_day + "-" + month + "-" + year;
//            Log.e("final date4",date);
//        }
//        Log.e("final date",date);
        return date_string;
    }

    @Override
    public void onBackPressed() {
        if (user_details != null) {
            backDialogEdit();
        } else {
            backDialog();
        }

    }

// backfrom New Registration
    public void backDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddUserActivity.this);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle("Warning!");
        builder.setMessage("Do you want to Quit New Registration?");
        builder.setPositiveButton("Continue",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                    }
                });
        builder.setNegativeButton("Quit",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();

                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

// Back from edit
    public void backDialogEdit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddUserActivity.this);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle("Wait!");
        builder.setMessage("Do you want to Quit User details Editing?");
        builder.setPositiveButton("Continue",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                    }
                });
        builder.setNegativeButton("Quit",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

// Check permission method for Picture
    public static class Utility {
        static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

        static boolean checkPermission(final Context context) {
            int currentAPIVersion = Build.VERSION.SDK_INT;
            if (currentAPIVersion >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Permission necessary");
                        alertBuilder.setMessage("External storage permission is necessary");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();
                    } else {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
    }
}
