package com.bzzy.Activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.AttendenceContactAdapter;
import com.bzzy.Adapters.ImageViewerAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SelectAttendence extends AppCompatActivity implements ApiCalls {

    RecyclerView contact_image_recycler, contacts_recycler;
    public ImageViewerAdapter adapter;
    AttendenceContactAdapter contactAdapter;
    public TextView total_members, submit_attendance, all_users_count;
    String batch_id;
    SearchView searchView;
   public SparseBooleanArray checkedItems = new SparseBooleanArray();
    public JSONArray id_array = new JSONArray();
  public   JSONArray img_array = new JSONArray();
  public  ArrayList<String> dataList=new ArrayList<>();
  public ArrayList<String> image_array=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_attendence);

        batch_id = getIntent().getStringExtra("batch_id");
        Log.e("batch_id", batch_id);

        initializations();
    }

    public void initializations() {
        all_users_count = (TextView) findViewById(R.id.total_users);
        submit_attendance = (TextView) findViewById(R.id.submit_button);
        total_members = (TextView) findViewById(R.id.total_members);
        contact_image_recycler = (RecyclerView) findViewById(R.id.image_recycler);
        contacts_recycler = (RecyclerView) findViewById(R.id.contactsRecycler);

        contact_image_recycler.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(SelectAttendence.this, LinearLayoutManager.HORIZONTAL, false);
        contact_image_recycler.setLayoutManager(horizontalLayoutManager3);

        contacts_recycler.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(SelectAttendence.this, LinearLayoutManager.VERTICAL, false);
        contacts_recycler.setLayoutManager(layoutManager);

        // Search bar ... searching by name and ID
        searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setQueryHint("Type a name or ID number");
        searchView.setIconified(false);
        searchView.setEnabled(true);
        searchView.clearFocus();
        searchView.setIconifiedByDefault(false);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                getMemberList(newText);
                return false;
            }
        });

        getMemberList("");
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void setImageAdapter(ArrayList<String> array) {
        Log.e("array image", array + "");
//        if(array.size()6){
        adapter = new ImageViewerAdapter(SelectAttendence.this, array);
        contact_image_recycler.setAdapter(adapter);
        contact_image_recycler.setLayoutFrozen(true);
        adapter.notifyDataSetChanged();
//        }
    }

    public void getMemberList(String query) {
        RequestQueue MyRequestQueue = Volley.newRequestQueue(SelectAttendence.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, batch_users, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        contacts_recycler.setVisibility(View.VISIBLE);
                        // Launch login activity
                        Log.e("success", "su");
                        JSONArray member_details = jObj.getJSONArray("data");

                        total_members.setText(member_details.length() + " " + "Members");

                        contactAdapter = new AttendenceContactAdapter(SelectAttendence.this, member_details, batch_id, submit_attendance, adapter, contact_image_recycler);
                        contacts_recycler.setAdapter(contactAdapter);
                    } else {
                        contacts_recycler.setVisibility(View.GONE);
                        Toast.makeText(SelectAttendence.this, "No member added yet!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(SelectAttendence.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("batch_id", batch_id);
                params.put("keyword", query);
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    @Override
    public void onBackPressed() {
        backDialog();
    }

    // Stop taking attendance from app
    public void backDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SelectAttendence.this);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle("Warning!");
        builder.setMessage("Do you want to Stop taking Attendance?");
        builder.setPositiveButton("Continue",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                    }
                });
        builder.setNegativeButton("Stop",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }
}
