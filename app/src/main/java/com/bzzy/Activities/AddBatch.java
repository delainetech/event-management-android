package com.bzzy.Activities;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.CarouselEffectTransformer;
import com.bzzy.Adapters.ExpenditureAdapterBottom;
import com.bzzy.Adapters.ShiftPagerAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddBatch extends AppCompatActivity implements View.OnClickListener, ApiCalls {

    ViewPager shift_pager;
   public ImageView back, add_batch;
    String batch_data;
    JSONObject object;
    boolean status=false;
    JSONArray array;
    TextView batch_text;

    UserSharedPreferences sharedPreferences;
    int[] backgrounds = {R.drawable.shift_card_first,
            R.drawable.shift_background_10,
            R.drawable.shift_screen_3,
            R.drawable.shift_screen_4,
            R.drawable.shift_card_first,
            R.drawable.shift_background_10,
            R.drawable.shift_screen_3,
            R.drawable.shift_screen_4
    };

    int[] text_back = {
            R.drawable.shift_edittext_back,
            R.drawable.shift_back_2,
            R.drawable.shift_back_3,
            R.drawable.shift_back_4,
            R.drawable.shift_edittext_back,
            R.drawable.shift_back_2,
            R.drawable.shift_back_3,
            R.drawable.shift_back_4
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_batch);

        sharedPreferences=new UserSharedPreferences(this);
        batch_text= findViewById(R.id.add_batch_text);
        batch_data = getIntent().getStringExtra("array");
        Log.e("batch_obj", batch_data + "");

//        if (batch_data == null) {
//            Log.e("batch_obj_if", batch_data + "");
//        } else {
//            batch_text.setText("Edit Batch");
//            Log.e("batch_obj", batch_data + "");
//            try {
//                array = new JSONArray(batch_data);
//
//            } catch (JSONException e) {
//                Log.e("array",e.toString());
//                e.printStackTrace();
//            }
//        }

        Bundle bundle=this.getIntent().getExtras();

        if (bundle!=null){
            status=bundle.getBoolean("status");

        }
        initialization();


    }

    public void initialization() {
        add_batch=(ImageView) findViewById(R.id.add_new_batch);
        add_batch.setVisibility(View.INVISIBLE);
        back = (ImageView) findViewById(R.id.back_button);
        back.setOnClickListener(this);
        shift_pager = (ViewPager) findViewById(R.id.shiftPager);
        shift_pager.setClipChildren(false);
        shift_pager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.margin_5dp));
        shift_pager.setOffscreenPageLimit(3);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            shift_pager.setElevation(0.3f);
        }
        shift_pager.setPageTransformer(false, new CarouselEffectTransformer(this)); // Set transformer


        getBatchData();

//        if (array.length() == 0) {
//            Log.e("null","=null");
//            ShiftPagerAdapter adapter = new ShiftPagerAdapter(AddBatch.this, backgrounds, text_back,array);
//            shift_pager.setAdapter(adapter);
//        } else {
//            Log.e("null","=null not");
//            ShiftPagerAdapter adapter = new ShiftPagerAdapter(AddBatch.this, backgrounds, text_back, array);
//            shift_pager.setAdapter(adapter);
//        }

        setupViewPager();
    }

    /**
     * Setup viewpager and it's events
     */
    private void setupViewPager() {

        // Set Top ViewPager Adapter
        shift_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int index = 0;

            @Override
            public void onPageSelected(int position) {
                index = position;
                Log.e("Page selected", index + "");
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == back) {
            if (status){
                status=false;

                getBatchData();
            }else {

                super.onBackPressed();
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        getBatchData();
    }



    @Override
    public void onBackPressed() {
        if (status){
            status=false;

            getBatchData();
        }else {

            super.onBackPressed();
        }

    }


    public void getBatchData() {


        array=new JSONArray();
        RequestQueue MyRequestQueue = Volley.newRequestQueue(this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, getBatchList, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response BatchList", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String statu = jObj.getString("status");
                    if (statu.equals("success")) {
                        // Launch login activity
                        batch_text.setText("Edit Batch");
                        array = jObj.getJSONArray("data");
                        Log.e("GetList array  ", array + "");
                        Log.e("staus",status+"");
                        if (status) {
                            array=new JSONArray();
                            batch_text.setText("Add Batch");
                        }


                        ShiftPagerAdapter adapter = new ShiftPagerAdapter(AddBatch.this, array);
                        shift_pager.setAdapter(adapter);

                    } else {

                        ShiftPagerAdapter adapter = new ShiftPagerAdapter(AddBatch.this,array);
                        shift_pager.setAdapter(adapter);

                    }
                } catch (JSONException e1) {

                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(Dashboard.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id", sharedPreferences.geteventId());
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }



}
