package com.bzzy.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.bzzy.Api.ApiCalls.feedback;

public class Suggestion extends AppCompatActivity implements View.OnClickListener {

    ImageView submit;
    TextView back;
    EditText enter_suggestion;
    UserSharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion);

        sharedPreferences = new UserSharedPreferences(Suggestion.this);
        submit = (ImageView) findViewById(R.id.submit);
        submit.setOnClickListener(this);
        enter_suggestion = (EditText) findViewById(R.id.suggestions);
        back = (TextView) findViewById(R.id.toolbar);
        back.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if (v == back) {
            finish();
        } else if (v == submit) {

            if (enter_suggestion.getText().toString().trim().equalsIgnoreCase("")) {
                enter_suggestion.setError("Please enter your suggestion.");
            } else if (enter_suggestion.length() < 100) {
                enter_suggestion.setError("Suggestion should be of more than 100 characters!");
                enter_suggestion.requestFocus();
            } else {
                String text = enter_suggestion.getText().toString().trim();
// Suggestion call
                suggestion(text);
            }
        }
    }

    public void suggestion(String suggestion) {
        RequestQueue MyRequestQueue = Volley.newRequestQueue(Suggestion.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, feedback, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response Suggestion", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
//                        JSONObject object = jObj.getJSONObject("data");
                        enter_suggestion.getText().clear();
                        Toast.makeText(Suggestion.this, "Thank you for your suggestion!", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(Suggestion.this, "Sorry! suggestion can't be submitted.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(Suggestion.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("partner_id", sharedPreferences.getuserID());
                params.put("feedback", suggestion);
//                params.put("type", type);
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }
}
