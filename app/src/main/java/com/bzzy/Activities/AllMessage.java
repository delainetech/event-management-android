package com.bzzy.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.MessagePreviousAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AllMessage extends AppCompatActivity implements ApiCalls, View.OnClickListener {

    Spinner message_spinner;
    EditText write_message;
    ImageView submit_message;
    TextView message_sent, total_users;
    RecyclerView previous_message;
    ArrayList<String> domain_name = new ArrayList<>();
    public ArrayAdapter<String> arrayAdapter;
    UserSharedPreferences sharedPreferences;
    RequestQueue MyRequestQueue;
    StringRequest MyStringRequest;
    int year, month, current_day;
    String date;
    String domain_id;
    ProgressBar progressBar;
    MessagePreviousAdapter messagePreviousAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_message);
        sharedPreferences = new UserSharedPreferences(AllMessage.this);
        initializations();
    }

    public void initializations() {
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        message_spinner = (Spinner) findViewById(R.id.message_spinner);
        message_sent = (TextView) findViewById(R.id.text);
        write_message = (EditText) findViewById(R.id.message_text);
        submit_message = (ImageView) findViewById(R.id.send_sms);
        submit_message.setOnClickListener(this);
        total_users = (TextView) findViewById(R.id.total_users);
        previous_message = (RecyclerView) findViewById(R.id.message_previous_recycler);
        previous_message.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(AllMessage.this, LinearLayoutManager.VERTICAL, false);
        previous_message.setLayoutManager(layoutManager);

        date = getDate();

        getBatchList();
    }


    @Override
    protected void onResume() {
        super.onResume();

        previousMessage();
    }

    public void validations() {
        if (write_message.getText().toString().trim().equalsIgnoreCase("")) {
            write_message.setError("Please enter message in box.");
        } else if (write_message.length() < 50 ) {

            write_message.setError("Message should be of more than 50 characters!");
            write_message.requestFocus();
        } else if (write_message.length() > 140 ) {

            write_message.setError("Message should be of less than 140 characters!");
            write_message.requestFocus();
        } else {
            sendSms();
        }
    }

    public void setData(JSONArray array) {

        message_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    JSONObject object = null;
                    Log.e("Arraylist", domain_name + "");
                    if (domain_name.get(position).equalsIgnoreCase("All")) {
                        object = array.getJSONObject(position);
                        Log.e("Spinner if", object + "");
                        domain_id = "all";
                        Log.e("Domain_id", domain_id + "");
                        int total_userss = 0;
                        for (int i = 0; i < array.length(); i++) {
                            total_userss = total_userss + Integer.parseInt(array.getJSONObject(i).getString("domain_users"));
                        }
                        Log.e("total_users", total_userss + "");
                        total_users.setText("Your message will be delivered to " + total_userss + " candidates");
                    } else {
                        int index = position - 1;
                        Log.e("Spinner else", index + "");
                        object = array.getJSONObject(index);
                        domain_id = array.getJSONObject(index).getString("id");
                        Log.e("Domain_id", domain_id + "");
                        total_users.setText("Your message will be delivered to " + object.getString("domain_users") + " candidates");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void getBatchList() {
        domain_name.clear();
        MyRequestQueue = Volley.newRequestQueue(AllMessage.this);
        MyStringRequest = new StringRequest(Request.Method.POST, getBatchList, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {

                        JSONArray domain_array = jObj.getJSONArray("domain");
                        Log.e("GetList array  ", domain_array + "");
                        JSONObject object1 = null;
                        domain_name.add("All");
                        for (int k = 0; k < domain_array.length(); k++) {
                            object1 = domain_array.getJSONObject(k);
                            domain_name.add(object1.getString("name"));
                            arrayAdapter = new ArrayAdapter<>(AllMessage.this, R.layout.message_spin, domain_name);
                            message_spinner.setAdapter(arrayAdapter);
                        }
                        setData(domain_array);

                    } else {
                        Toast.makeText(AllMessage.this, "Sorry! no member added yet!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {

                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(AllMessage.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id", sharedPreferences.geteventId());
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    // send sms  to users
    private void sendSms() {
        progressBar.setVisibility(View.VISIBLE);
        MyRequestQueue = Volley.newRequestQueue(AllMessage.this);
        MyStringRequest = new StringRequest(Request.Method.POST, send_sms, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    String message = jObj.getString("message");
                    if (status.equals("success")) {
                        progressBar.setVisibility(View.GONE);
                        // Launch login activity
                        write_message.getText().clear();
                        write_message.setText("");
                        write_message.clearFocus();
                        previousMessage();
                        messagePreviousAdapter.notifyDataSetChanged();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(AllMessage.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(AllMessage.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("partner_id", sharedPreferences.getuserID());
                params.put("event_id", sharedPreferences.geteventId());
                params.put("domain_id", domain_id + "");
                params.put("message", write_message.getText().toString().trim());
                params.put("date", date + "");
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    // Check previous sms
    private void previousMessage() {
        MyRequestQueue = Volley.newRequestQueue(AllMessage.this);
        MyStringRequest = new StringRequest(Request.Method.POST, previous_sms, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
//                    String message = jObj.getString("message");
                    if (status.equals("success")) {
                        // Launch login activity
                        JSONArray array = jObj.getJSONArray("data");
                        message_sent.setText("Previous sms (" + array.length() + "/5)");

                        messagePreviousAdapter = new MessagePreviousAdapter(AllMessage.this, array);
                        previous_message.setAdapter(messagePreviousAdapter);
                    } else {
                        Toast.makeText(AllMessage.this, "Error!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AllMessage.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("partner_id", sharedPreferences.getuserID());
                params.put("event_id", sharedPreferences.geteventId());
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == submit_message) {
            validations();
        }

    }

    public String getDate() {
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        current_day = c.get(Calendar.DAY_OF_MONTH);
        Log.e("Month " + month, " " + "Year " + year);
        String date = current_day + "-" + month + "-" + year;
        return date;
    }
}

