package com.bzzy.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.MonthlyGraphExpense;
import com.bzzy.Adapters.WeekAdapter;
import com.bzzy.Adapters.WeeklyGraphAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.Interface.ClickListener;
import com.bzzy.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MyExpenses extends AppCompatActivity implements ApiCalls, View.OnClickListener {

    UserSharedPreferences sharedPreferences;
    TextView current_month, estimate_amt, my_expenses;
   public int month, year, current_day;
    Double amount_total = 0.0,
            amount_weekly = 0.0, amount_monthly = 0.0;
    WeeklyGraphAdapter graphAdapter;
    RecyclerView weekly_recycler;
    MonthlyGraphExpense graphAdapter1;
    LinearLayoutManager horizontalLayoutManager3;
    JSONObject jObj;
    private ArrayList<String> year_list = new ArrayList<>();
    int[] months = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    Spinner filter_data_spin;
    public ArrayList<String> xVals = new ArrayList<String>();
    JSONObject weekly;
    WeekAdapter weekAdapter;
    Button previous, next;
    public TextView earning_type, amount_type;
    public ArrayList<String> weeks = new ArrayList<>();
    public ArrayList<String> weeks_days = new ArrayList<>();
    public int month_count;
    CardView expenses_detail;
    Calendar c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_expenses);
        getDate();
        sharedPreferences = new UserSharedPreferences(MyExpenses.this);

        initializations();
    }


    public void initializations() {
        filter_data_spin = (Spinner) findViewById(R.id.filter_data_spinner);
        earning_type = (TextView) findViewById(R.id.type_of_earning);
        amount_type = (TextView) findViewById(R.id.amount_type);
        weekly_recycler = (RecyclerView) findViewById(R.id.weekly_recycler);
        next = (Button) findViewById(R.id.next);
        next.setOnClickListener(this);
        expenses_detail=findViewById(R.id.earning_detail);
        previous = (Button) findViewById(R.id.previous);
        previous.setOnClickListener(this);
        weekly_recycler.setItemAnimator(new DefaultItemAnimator());

        horizontalLayoutManager3 = new LinearLayoutManager(MyExpenses.this, LinearLayoutManager.VERTICAL, false);
        weekly_recycler.setLayoutManager(horizontalLayoutManager3);
//        weekly_recycler.scrollToPosition( - 1);
        weekly_recycler.setNestedScrollingEnabled(true);

        current_month = findViewById(R.id.month);
        my_expenses = findViewById(R.id.my_expense);

        for (int i = 0; i < months.length; i++) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
            cal.set(Calendar.MONTH, months[i]);
            String month_name = month_date.format(cal.getTime());

            Log.e("Month_name", month_name + "");
            xVals.add(month_name);
        }

        xVals.clear();
        xVals.add("January");
        xVals.add("February");
        xVals.add("March");
        xVals.add("April");
        xVals.add("May");
        xVals.add("June");
        xVals.add("July");
        xVals.add("August");
        xVals.add("September");
        xVals.add("October");
        xVals.add("November");
        xVals.add("December");

        getExpense(month, year);

        weekly_recycler.addOnItemTouchListener(new RecyclerViewItemClickListener(this, weekly_recycler, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //triggers when click

//                Toast.makeText(ViewExpenditureBills.this, "on Click", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) throws JSONException {
                //triggers when you long press





//                Toast.makeText(ViewExpenditureBills.this, "on long Click", Toast.LENGTH_SHORT).show();
            }
        }));

    }

    public void getDate() {
        c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        current_day = c.get(Calendar.DAY_OF_MONTH);
        Log.e("Month " + month, " " + "Year " + year);
        month_count = month;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    // Data from API
    public void getExpense(int month, int year) {
        year_list.clear();
        RequestQueue MyRequestQueue = Volley.newRequestQueue(MyExpenses.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, my_expense, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        Log.e("success", "su");

                        current_month.setText(jObj.getString("current_month"));
                        my_expenses.setText("₹ " + jObj.getString("total_expensis"));
                        Log.e("day", current_day + "");
                        JSONObject months_array = jObj.getJSONObject("months");

                        JSONObject object = jObj.getJSONObject("days");

                        weekly = jObj.getJSONObject("weeks");
                        Log.e("day", current_day + "");

                        ArrayAdapter<String> adapter;
                        String[] menuArray = getResources().getStringArray(R.array.filter_data);

                        for (int k = 0; k < menuArray.length; k++) {
                            year_list.add(menuArray[k]);
                            adapter = new ArrayAdapter<>(MyExpenses.this, R.layout.cat_spinner, year_list);
                            filter_data_spin.setAdapter(adapter);
                        }
                        setData();

                    } else {
                        Toast.makeText(MyExpenses.this, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(MyExpenses.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("event_id", sharedPreferences.geteventId());
                params.put("user_id", sharedPreferences.getuserID());
                params.put("month", month + "");
                params.put("year", year + "");


                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    // Select To filter data
    public void setData() {
        filter_data_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String month1 = String.valueOf(filter_data_spin.getSelectedItem());
                Log.e("spinner", month1);
                expenses_detail.setVisibility(View.GONE);
                //Daily Expense
                if (filter_data_spin.getSelectedItemPosition() == 0) {
                    amount_total = 0.0;
                    earning_type.setText("Daily Expenses");
                    try {
                        if (month != month_count) {
                            Log.e("If", "days");
                            for (int k = 1; k <= jObj.getJSONObject("days").length(); k++) {
                                try {

                                    jObj.getJSONObject("days").getString(k + "");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                amount_total = amount_total + Double.parseDouble(jObj.getJSONObject("days").getString(k + ""));
                            }

                            graphAdapter = new WeeklyGraphAdapter(MyExpenses.this, jObj,
                                    jObj.getJSONObject("days").length(), amount_total,expenses_detail);
                            weekly_recycler.setAdapter(graphAdapter);
                            weekly_recycler.scrollToPosition(graphAdapter.getItemCount() - 1);
                        } else {
                            for (int k = 1; k <= current_day; k++) {
                                try {
                                    jObj.getJSONObject("days").getString(k + "");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                amount_total = amount_total + Double.parseDouble(jObj.getJSONObject("days").getString(k + ""));
                            }
                            Log.e("amoount_daily", amount_total + "");

                            graphAdapter = new WeeklyGraphAdapter(MyExpenses.this, jObj, current_day, amount_total, expenses_detail);
                            weekly_recycler.setAdapter(graphAdapter);
                            weekly_recycler.scrollToPosition(graphAdapter.getItemCount() - 1);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
// Monthly Expense Data
                } else if (filter_data_spin.getSelectedItemPosition() == 2) {
                    amount_monthly = 0.0;
                    earning_type.setText("Monthly Expenses");
                    try {
                        for (int k = 0; k < month; k++) {
                            try {
                                jObj.getJSONObject("months").getString(xVals.get(k).toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//                            Log.e("k", k + " " + jObj.getJSONObject("months").getString(xVals.get(k).toString()));
                            amount_monthly = amount_monthly + Double.parseDouble(jObj.getJSONObject("months").getString(xVals.get(k).toString()));
                        }
                        expenses_detail.setVisibility(View.GONE);
                        Log.e("amount_monthly", amount_monthly + "");
                        graphAdapter1 = new MonthlyGraphExpense(MyExpenses.this, jObj, month, amount_monthly,expenses_detail);
                        weekly_recycler.setAdapter(graphAdapter1);
                        weekly_recycler.scrollToPosition(graphAdapter1.getItemCount() - 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
// Week days Expense
                } else {
                    amount_weekly = 0.0;
                    weeks_days.clear();
                    weeks.clear();
                    earning_type.setText("Weekly Expenses");
                    String header = null;
                    try {
                        weekly = jObj.getJSONObject("weeks");
                        Iterator<String> key = weekly.keys();
                        while (key.hasNext()) {
                            header = key.next().toString();
//                            Log.e("Key : ", header + " value : "
//                                    + weekly.getString(header));
                            amount_weekly = amount_weekly + Double.parseDouble(weekly.getString(header));
                            if (weekly.getString(header).equalsIgnoreCase("0")) {
                                Log.e("not", "added");
                            } else {
                                weeks.add(weekly.getString(header));
                                weeks_days.add(header);
                            }
                        }
                        weekAdapter = new WeekAdapter(MyExpenses.this, jObj,
                                amount_weekly, weeks, weeks_days, 0,expenses_detail,false);
                        weekly_recycler.setAdapter(weekAdapter);
                        weekly_recycler.scrollToPosition(weekAdapter.getItemCount() - 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == next) {
            if (month_count < month) {
                month_count++;
                Log.e("next", month_count + "");
                getExpense(month_count, year);
                expenses_detail.setVisibility(View.GONE);
            }
        } else if (v == previous) {
            if (month_count > 1) {
                month_count--;

                expenses_detail.setVisibility(View.GONE);
                Log.e("previousMonth", month_count + "");
                getExpense(month_count, year);
            }
        }
    }
    public class RecyclerViewItemClickListener implements RecyclerView.OnItemTouchListener {

        //GestureDetector to detect touch event.
        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerViewItemClickListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    //Find child on x and y position relative to screen
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        try {
                            clickListener.onLongClick(child, recyclerView.getChildLayoutPosition(child));
                        } catch (JSONException e1) {
                            Log.e("e",e.toString());
                            e1.printStackTrace();
                        }
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            //On Touch event
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildLayoutPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


}
