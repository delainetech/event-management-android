package com.bzzy.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bzzy.Adapters.TransactionAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ViewTransactionHistory extends AppCompatActivity implements ApiCalls, View.OnClickListener {

    TextView username, user_Id, batch_time, batch_name, edit_btn, user_email, user_phone, days, topic, transactions;
    ImageView user_image, id_image;
    UserSharedPreferences sharedPreferences;
    JSONObject object;
    JSONArray array;
    String dataobject, amount_total;
    TextView total_amount;
    RecyclerView transactions_recycler;
    TransactionAdapter adapter;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_transaction_history);

        dataobject = getIntent().getStringExtra("object");
        amount_total = getIntent().getStringExtra("total_amount");
        Log.e("data_object", dataobject + "  " + amount_total);
        try {
            object = new JSONObject(dataobject);

            array = object.getJSONArray("payments");
            Log.e("array", array+"");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        initialization();
    }

    public void initialization() {
        progressBar= findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        total_amount = (TextView) findViewById(R.id.total_amount);
        if (amount_total.equalsIgnoreCase("null")) {
            total_amount.setText("₹ " + 0);
        } else total_amount.setText("₹ " + amount_total);

        username = (TextView) findViewById(R.id.admin_name);
        user_Id = (TextView) findViewById(R.id.admin_id);
        batch_time = (TextView) findViewById(R.id.batch_time);
        batch_name = (TextView) findViewById(R.id.batch_name);
          user_email = (TextView) findViewById(R.id.email);
        user_phone = (TextView) findViewById(R.id.mobile_no);
        topic = (TextView) findViewById(R.id.preparing_details);
        user_image = (ImageView) findViewById(R.id.admin_image);
        transactions_recycler = (RecyclerView) findViewById(R.id.fees_recycler);

        transactions_recycler.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(ViewTransactionHistory.this, LinearLayoutManager.VERTICAL, false);
        transactions_recycler.setLayoutManager(horizontalLayoutManager3);

        setValues();
    }

    public void setValues() {
        progressBar.setVisibility(View.GONE);
        try {
            username.setText(object.getString("student_name"));
            user_Id.setText("ID: " + object.getString("student_id"));
            batch_time.setText(object.getString("start_time") + " - " + object.getString("end_time"));
            batch_name.setText(object.getString("name") + " -");
            user_email.setText(object.getString("student_email"));
            user_phone.setText(object.getString("student_phone"));

            if (object.getString("image").length() > 4) {
                Log.e("pic", "user");
                Picasso.get().load(batch_images + object.getString("image"))
                        .fit().error(R.drawable.error_circle).into(user_image);
            }

            adapter=new TransactionAdapter(ViewTransactionHistory.this, array);
            transactions_recycler.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        array =new JSONArray();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        array =new JSONArray();
    }

    @Override
    public void onClick(View v) {

    }
}
