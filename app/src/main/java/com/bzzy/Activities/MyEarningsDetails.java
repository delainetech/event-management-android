package com.bzzy.Activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.EarningDetailDayAdapter;
import com.bzzy.Adapters.EarningDetailsAdapter;
import com.bzzy.Adapters.ExpensesDetailDayAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MyEarningsDetails extends AppCompatActivity implements ApiCalls,View.OnClickListener {
    RecyclerView expense_recycler,date_recycler;
    LinearLayoutManager manager,manager2;
    EarningDetailsAdapter adapter;
    TextView empty_view;
    EarningDetailDayAdapter adapterday;
    public ArrayList<String> xVals = new ArrayList<String>();
    int[] months = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    int month_count;
    Calendar c;
    JSONObject jObj;
    TextView tvdaily,tvmonth;
    UserSharedPreferences sharedPreferences;
    public  String Inday,Inyear,Inmonth,type;
    LinearLayout layout1;
    JSONObject weekly;
    ProgressDialog progressDialog;
    String first_date,second_date;
    TextView tv_to,tv_from;
    private ArrayList<String> year_list = new ArrayList<>();
    public ArrayList<String> weeks = new ArrayList<>();
    public ArrayList<String> weeks_days = new ArrayList<>();
    public  double amount;
    public TextView amount_text,tvcustom;
    int month, year, current_day,current_month;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myearning_deatils);
        sharedPreferences=new UserSharedPreferences(this);
        getDate();
        expense_recycler=findViewById(R.id.expense_recycler);
        layout1=findViewById(R.id.layout1);
        manager=new LinearLayoutManager(this);
        amount_text=findViewById(R.id.my_expense);
        tvdaily=findViewById(R.id.tvdaily);
        tvmonth=findViewById(R.id.tvmonth);
        tvcustom=findViewById(R.id.tvcustom);
        empty_view=findViewById(R.id.empty_view);
        date_recycler=findViewById(R.id.date_recycler);
        manager2=new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        date_recycler.setLayoutManager(manager2);
        expense_recycler.setLayoutManager(manager);

        tv_to=findViewById(R.id.tv_to);
        tv_from=findViewById(R.id.tv_from);


        expense_recycler.setAdapter(adapter);
        for (int i = 0; i < months.length; i++) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat month_date = new SimpleDateFormat("MMMM", Locale.getDefault());
            cal.set(Calendar.MONTH, months[i]);
            String month_name = month_date.format(cal.getTime());

            Log.e("Month_name", month_name + "");
            xVals.add(month_name);
        }
        xVals.clear();
        xVals.add("January");
        xVals.add("February");
        xVals.add("March");
        xVals.add("April");
        xVals.add("May");
        xVals.add("June");
        xVals.add("July");
        xVals.add("August");
        xVals.add("September");
        xVals.add("October");
        xVals.add("November");
        xVals.add("December");


        tvcustom.setOnClickListener(this);
        tvdaily.setOnClickListener(this);
        tvmonth.setOnClickListener(this);

        Bundle bundle=this.getIntent().getExtras();
        if (bundle!=null){

            Inday=bundle.getString("day");
            Inmonth=bundle.getString("month");
            Inyear=bundle.getString("year");
            type=bundle.getString("type");
            first_date=bundle.getString("first_date");
            second_date=bundle.getString("second_date");

            if (type.equals("day")){

                layout1.setVisibility(View.GONE);
                date_recycler.setVisibility(View.VISIBLE);
                tvdaily.setBackgroundResource(R.drawable.expense_bill_black_background);
                tvdaily.setTextColor(Color.parseColor("#ffffff"));

                if (month!=Integer.parseInt(Inmonth)){

                    current_month=numberOfDaysInMonth(Integer.parseInt(Inmonth),Integer.parseInt(Inyear));

                    Log.e("current","current");
                    adapterday=new EarningDetailDayAdapter(MyEarningsDetails.this
                            ,current_month,type,Inday,Inmonth,Inyear);
                    date_recycler.setAdapter(adapterday);
                     date_recycler.scrollToPosition(Integer.parseInt(Inday)-1);

                }else {

                    Log.e("current ","not current");
                    adapterday=new EarningDetailDayAdapter(MyEarningsDetails.this
                            ,current_day,type,Inday,Inmonth, Inyear);
                    date_recycler.setAdapter(adapterday);
                     date_recycler.scrollToPosition(Integer.parseInt(Inday)-1);

                }


            }else if (type.equals("month")){

                layout1.setVisibility(View.GONE);
                date_recycler.setVisibility(View.VISIBLE);
                tvmonth.setBackgroundResource(R.drawable.expense_bill_black_background);
                tvmonth.setTextColor(Color.parseColor("#ffffff"));

                Log.e("date",current_day+" c "+Inday+" d "+Inmonth+" m "+Inyear+ "y "+type+" f "+first_date+" s "+ second_date );


                if (month!=Integer.parseInt(Inmonth)){

                    current_month=month;

                    adapterday=new EarningDetailDayAdapter(MyEarningsDetails.this
                            ,current_month,type,Inday,Inmonth, Inyear);
                    date_recycler.setAdapter(adapterday);
                    date_recycler.scrollToPosition(Integer.parseInt(Inday)-1);

                }else {
                    adapterday=new EarningDetailDayAdapter(MyEarningsDetails.this
                            ,month,type,Inday,Inmonth, Inyear);
                    date_recycler.setAdapter(adapterday);
                    date_recycler.scrollToPosition(Integer.parseInt(Inday)-1);
                    Log.e("current ","not current");

                }


            }else {

                CustomFilter(false);

            }

        }

        getExpense(Inday,Inmonth, Inyear,type,first_date,second_date,false);


    }
    public void getDate() {
        c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        current_day = c.get(Calendar.DAY_OF_MONTH);

        Log.e("Month " + month, " " + "Year " + year);
        month_count = month;
    }
    public void getExpense(String day,String mont,
                           String year,String type,
                           String first_date,
                           String second_date,boolean isclick) {
        year_list.clear();
        empty_view.setVisibility(View.GONE);
        progressDialog = ProgressDialog.show(MyEarningsDetails.this, null, null);
        ProgressBar spinner = new android.widget.ProgressBar(MyEarningsDetails.this, null,android.R.attr.progressBarStyleSmall);
        spinner.getIndeterminateDrawable().setColorFilter(Color.parseColor("#8F2AFB"), android.graphics.PorterDuff.Mode.SRC_IN);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setContentView(spinner);
        progressDialog.setCancelable(false);
        progressDialog.show();
        RequestQueue MyRequestQueue = Volley.newRequestQueue(MyEarningsDetails.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, filter_earnings, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity

                        progressDialog.dismiss();
                        JSONArray array=jObj.getJSONArray("data");

                        amount=0.0;
                        for (int i=0;i<array.length();i++){

                            amount=amount+Double.parseDouble(array.getJSONObject(i).getString("amount"));
                        }
                        amount_text.setText(" ₹ "+String.format("%02.0f", amount));
                        adapter=new EarningDetailsAdapter(MyEarningsDetails.this,array);
                        expense_recycler.setAdapter(adapter);
//                        setData();

                        expense_recycler.setVisibility(View.VISIBLE);
                        empty_view.setVisibility(View.GONE);

                    } else {
                        expense_recycler.setVisibility(View.GONE);
                        empty_view.setVisibility(View.VISIBLE);
                        empty_view.setText(jObj.getString("message"));
                        Log.e("else day",day) ;
                        date_recycler.scrollToPosition(Integer.parseInt(day)-1);
                    Toast.makeText(MyEarningsDetails.this, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }



                    if (isclick) {
                        Log.e("scroll count",day);

                        if (type.equals("day")){

                            Log.e("scroll count",day);
                            adapterday = new EarningDetailDayAdapter(MyEarningsDetails.this
                                    , current_day, type, day, Inmonth, Inyear);
                            date_recycler.setAdapter(adapterday);
                            date_recycler.scrollToPosition(Integer.parseInt(day)-1);
                        }else if (type.equals("month")){


                            adapterday = new EarningDetailDayAdapter(MyEarningsDetails.this
                                    , current_month, type, mont, Inmonth, Inyear);
                            date_recycler.setAdapter(adapterday);
                            date_recycler.scrollToPosition(Integer.parseInt(day)-1);



                        }
                    }else {
                        date_recycler.scrollToPosition(Integer.parseInt(day)-1);
                        Log.e("ushg","wyiegbn");
                    }

                    progressDialog.dismiss();

                } catch (JSONException e1) {

                    progressDialog.dismiss();
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                progressDialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(MyEarningsDetails.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("event_id", sharedPreferences.geteventId());
                params.put("month", mont + "-"+year);
                params.put("date",day+"-"+mont+"-"+year);
                params.put("type",type);
                params.put("year", year + "");
                params.put("from",first_date);
                params.put("to",second_date);

                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public static int numberOfDaysInMonth(int month, int year) {
        Calendar monthStart = new GregorianCalendar(year, month-1, 1);
        return monthStart.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    @Override
    public void onClick(View v) {
        if (v == tvdaily) {

            DailyFilter();

        } else if (v == tvcustom) {

            CustomFilter(true);

        }else if (v==tvmonth){

            MonthFilter();


        }
    }

    private void CustomFilter(boolean isclick) {
        layout1.setVisibility(View.VISIBLE);
        date_recycler.setVisibility(View.GONE);


        tvdaily.setBackgroundResource(R.drawable.expense_bill_white_background);
        tvdaily.setTextColor(Color.parseColor("#000000"));

        tvmonth.setBackgroundResource(R.drawable.expense_bill_white_background);
        tvmonth.setTextColor(Color.parseColor("#000000"));
        tvcustom.setBackgroundResource(R.drawable.expense_bill_black_background);
        tvcustom.setTextColor(Color.parseColor("#ffffff"));
//        layout1.setVisibility(View.VISIBLE);
//        date_recycler.setVisibility(View.GONE);

        if (isclick){

            first_date=c.get(Calendar.DAY_OF_MONTH)+"-"+(c.get(Calendar.MONTH) + 1)+"-"+c.get(Calendar.YEAR);

            first_date="DD/MM/YYYY";
            tv_from.setText(first_date);
            tv_to.setText(first_date);

            amount_text.setText(" ₹ 0");
            expense_recycler.setVisibility(View.GONE);
            empty_view.setVisibility(View.VISIBLE);
            empty_view.setText("Select Date");

//            getExpense(current_day+"",month+"", Inyear,"day","","",false);


        }else {

            tv_from.setText(first_date);
            tv_to.setText(second_date);
        }

        tv_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                getCustomDate(tv_from,c.get(Calendar.DAY_OF_MONTH),c.get(Calendar.MONTH) + 1,c.get(Calendar.YEAR),true);

            }
        });


        tv_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (first_date.length()>0) {
                    getCustomDate(tv_to, c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.MONTH) + 1, c.get(Calendar.YEAR), false);

                }

            }
        });

    }

    public void getCustomDate(final TextView et, final int day, int month, int year, boolean b){
        type="custom";
        DatePickerDialog datePickerDialog =new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String d=dayOfMonth+"-"+(month+1)+"-"+year+" 00:00:00";

                if(b) {

                    first_date=dayOfMonth+"-"+(month+1)+"-"+year;

                    second_date="DD/MM/YYYY";
                    et.setText(first_date);
                    tv_from.setText(first_date);
                    tv_to.setText(second_date);
//                    day1 = dayOfMonth;
//                    month1 = month + 1;
//                    year1 = year;
//
//                    st_datefrom=convertDate(d,"dd-MM-yyyy");
                }
                else {

                    if (first_date.length()>0){

                        second_date=dayOfMonth+"-"+(month+1)+"-"+year;

                        getExpense(Inday,Inmonth, Inyear,type,first_date,second_date,false);

                        et.setText(second_date);
                    }

                }
//                    tv_to.setText(utl.convertDate(d,"dd MMM-yyyy"));

//                et.setText(convertDate(d,"dd-MMM-yyyy"));
            }
        },year,month-1,day);

//        if(b){
//
//        }
////            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//        else {
//
////            String myDate = year+"/"+month+"/"+day;
//            if (st_datefrom.length() > 0) {
//
//                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//
//                Date date = null;
//                try {
//                    date = sdf.parse(st_datefrom);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//                long millis = date.getTime();
//
//                datePickerDialog.getDatePicker().setMinDate(millis);
//            }
//        }
        datePickerDialog.show();
    }


    private void MonthFilter() {

        getDate();

        layout1.setVisibility(View.GONE);
        date_recycler.setVisibility(View.VISIBLE);
        tvdaily.setBackgroundResource(R.drawable.expense_bill_white_background);
        tvdaily.setTextColor(Color.parseColor("#000000"));

        tvmonth.setBackgroundResource(R.drawable.expense_bill_black_background);
        tvmonth.setTextColor(Color.parseColor("#ffffff"));


        tvcustom.setBackgroundResource(R.drawable.expense_bill_white_background);
        tvcustom.setTextColor(Color.parseColor("#000000"));

        date_recycler.setVisibility(View.VISIBLE);
        layout1.setVisibility(View.GONE);

        type="month";
        current_month=month;

//        current_day=Integer.parseInt(Inday);
        adapterday=new EarningDetailDayAdapter(MyEarningsDetails.this
                ,month,type,month+"",Inmonth, Inyear);
        date_recycler.setAdapter(adapterday);
        date_recycler.scrollToPosition(Integer.parseInt(Inday)-1);
        getExpense(current_day+"",month+"", Inyear,"month","","",false);



    }

    private void DailyFilter() {



        getDate();
        layout1.setVisibility(View.GONE);
        date_recycler.setVisibility(View.VISIBLE);
        tvdaily.setBackgroundResource(R.drawable.expense_bill_black_background);
        tvdaily.setTextColor(Color.parseColor("#ffffff"));

        tvmonth.setBackgroundResource(R.drawable.expense_bill_white_background);
        tvmonth.setTextColor(Color.parseColor("#000000"));


        tvcustom.setBackgroundResource(R.drawable.expense_bill_white_background);
        tvcustom.setTextColor(Color.parseColor("#000000"));

        date_recycler.setVisibility(View.VISIBLE);
        layout1.setVisibility(View.GONE);


        Log.e("date",current_day+" c "+Inday+" d "+Inmonth+" m "+Inyear+ "y "+type+" f "+first_date+" s "+ second_date );


        type="day";
//        current_day=Integer.parseInt(Inday);
        adapterday=new EarningDetailDayAdapter(MyEarningsDetails.this
                ,current_day,type,current_day+"",Inmonth, Inyear);
        date_recycler.setAdapter(adapterday);
        date_recycler.scrollToPosition(Integer.parseInt(Inday)-1);
        getExpense(current_day+"",month+"", Inyear,"day","","",false);

    }


}
