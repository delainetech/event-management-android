package com.bzzy.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.Volley;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.Constants.VolleyMultipartRequest;
import com.bzzy.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.bzzy.Api.ApiCalls.report_problem;

public class ReportProblem extends AppCompatActivity implements View.OnClickListener {

    ImageView submit, upload_image, image_preview;
    EditText enter_report;
    Bitmap bitmap,problem_bitmap;
    TextView back;
    UserSharedPreferences sharedPreferences;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_problem);
        sharedPreferences= new UserSharedPreferences(ReportProblem.this);

        submit = (ImageView) findViewById(R.id.submit);
        submit.setOnClickListener(this);
        upload_image = (ImageView) findViewById(R.id.report_image);
        upload_image.setOnClickListener(this);
        image_preview = (ImageView) findViewById(R.id.image_preview);
        enter_report = (EditText) findViewById(R.id.report_text);
        back = (TextView) findViewById(R.id.toolbar);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == submit) {
            validations();
        } else if (v == upload_image) {
            CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                    .start(ReportProblem.this);
        } else if (v == back) {
            finish();
        }
    }

    public void validations(){
        if (enter_report.getText().toString().trim().equalsIgnoreCase("")){
            enter_report.setError("Please enter your report!");
        }else if (enter_report.length() < 50) {
            enter_report.setError("Suggestion should be of more than 50 characters!");
            enter_report.requestFocus();
        } else {
            reportProblem(enter_report.getText().toString().trim());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("codes", requestCode + "," + resultCode + "," + data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.e("uri", resultUri + "");
                image_preview.setVisibility(View.VISIBLE);
//                image_preview.setImageURI(resultUri);
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(ReportProblem.this.getContentResolver(), resultUri);
                    problem_bitmap=getResizedBitmap(bitmap,300);
                    image_preview.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("in", error + "");
            }
        }
    }

    public static byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void reportProblem(String suggestion) {
        Log.e("gjg", "hgjg");
        pDialog = new ProgressDialog(ReportProblem.this);
        pDialog.setMessage("Please wait...");
        pDialog.show();
        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, report_problem,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        Log.e("Response", new String(response.data) + " ");
                        //This code is executed if the server responds, whether or not the response contains data.
                        try {
                            JSONObject jObj = new JSONObject(new String(response.data));
                            String status = jObj.getString("status");
                            if (status.equals("success")) {
                                pDialog.dismiss();
                                enter_report.getText().clear();
                                image_preview.setImageDrawable(getDrawable(R.drawable.saved_button));
                                Toast.makeText(ReportProblem.this, "Thank you for your Report!", Toast.LENGTH_SHORT).show();
                            } else {
                                pDialog.dismiss();
                                Toast.makeText(ReportProblem.this, "Sorry! Report can't be submitted.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e1) {
                            pDialog.dismiss();
                            Log.e("Exception", e1 + " ");
                            e1.printStackTrace();
                        }
                    }
                },
                error -> {
                    pDialog.dismiss();
                    Toast.makeText(ReportProblem.this, "Please check your Internet connection", Toast.LENGTH_SHORT).show();

                    Log.e("onErrorResponse: ", error + "");
                    NetworkResponse networkResponse = error.networkResponse;
                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";
                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("partner_id", sharedPreferences.getuserID());
                params.put("report", suggestion);
                if (problem_bitmap == null) {
                    params.put("image", "");
                }
                Log.e("getParams: ", params + "");
                return params;
            }

            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                if (problem_bitmap != null) {
                    params.put("image", new DataPart(imagename + ".jpg", getFileDataFromDrawable(problem_bitmap)));
                }
                Log.e("params", params + "");
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //adding the request to volley
        Volley.newRequestQueue(ReportProblem.this).add(volleyMultipartRequest);
    }
}
