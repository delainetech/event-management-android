package com.bzzy.Activities;

import android.animation.Animator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Adapters.AppSearchAdapter;
import com.bzzy.Adapters.AutoCompleteAdapter;
import com.bzzy.Adapters.SheatSearchAdapter;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.Models.searchresult;
import com.bzzy.R;
import com.bzzy.utils.Common;
import com.bzzy.utils.util;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.bzzy.utils.Common.loggg;

public class UserProfile extends AppCompatActivity implements ApiCalls, View.OnClickListener {

    TextView username, user_Id, batch_time, batch_name, edit_btn, user_email, user_phone,
            days, topic, transactions, seat_no,lock_no;
    ImageView user_image, id_image, pop_image;
    UserSharedPreferences sharedPreferences;
    String userId, batch_id;
    JSONObject object;
    JSONObject jObj;
    String selected_seat="";
    ProgressBar progressBar;
    Dialog dialog;
    RecyclerView search_recycler;
    EditText seatNo;
    private ArrayList<searchresult>  searchresultArrayList=new ArrayList<>();
    AutoCompleteTextView seatt_update;
    TextView add,tv_Fname,view_identity_card;
    ProgressBar progressBar1, progressBar2, progressBar3;
    ImageView delete_user, edit_user, expanded_image;
    private Animator mCurrentAnimatorEffect;
    private int mShortAnimationDurationEffect;
    AutoCompleteAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        sharedPreferences = new UserSharedPreferences(UserProfile.this);

        userId = getIntent().getExtras().getString("user_id");
        batch_id = getIntent().getExtras().getString("batch_id");

        Log.e("user_id", userId + " " + batch_id);

        initialization();

    }

    public void initialization() {
        progressBar = findViewById(R.id.progress_bar);

        seat_no = (TextView) findViewById(R.id.seat_no);
        seat_no.setOnClickListener(this);
        lock_no=(TextView)findViewById(R.id.lock_no);
        view_identity_card=(TextView)findViewById(R.id.view_identity_card);
        view_identity_card.setOnClickListener(this);
        lock_no.setOnClickListener(this);
        tv_Fname = (TextView) findViewById(R.id.tv_Fname);
        username = (TextView) findViewById(R.id.admin_name);
        user_Id = (TextView) findViewById(R.id.admin_id);
        batch_time = (TextView) findViewById(R.id.batch_time);
        batch_name = (TextView) findViewById(R.id.batch_name);
        edit_btn = (TextView) findViewById(R.id.edit_btn);
        edit_btn.setOnClickListener(this);
        user_email = (TextView) findViewById(R.id.email);
        user_phone = (TextView) findViewById(R.id.mobile_no);
        days = (TextView) findViewById(R.id.days);
        topic = (TextView) findViewById(R.id.preparing_details);
        user_image = (ImageView) findViewById(R.id.admin_image);
        user_image.setOnClickListener(this);
        id_image = (ImageView) findViewById(R.id.id_card_image);
        id_image.setOnClickListener(this);
        transactions = (TextView) findViewById(R.id.view_transactions);
        transactions.setOnClickListener(this);
        expanded_image = findViewById(R.id.expanded_image);
        transactions.setClickable(false);
        id_image.setClickable(false);
        user_image.setClickable(false);


    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserData(userId);
    }

    public void getUserData(String user_id) {
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(UserProfile.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, user_profile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar.setVisibility(View.GONE);
                        // Launch login activity
                        object = jObj.getJSONObject("data");
                        Log.e("Data in Shift", object + "");
                        transactions.setClickable(true);
                        username.setText(object.getString("student_name"));
                        user_Id.setText("ID: " + object.getString("student_id"));
                        batch_time.setText(object.getString("start_time") + " - " + object.getString("end_time"));
                        batch_name.setText(object.getString("name") + " -");
                        tv_Fname.setText(Common.ParseString(object,"father_name"));
                        days.setText(object.getString("days_before"));
                        user_email.setText(Common.ParseString(object,"id_number"));
                        user_phone.setText(object.getString("student_phone"));


                        Log.e("liwb",object.getString("seat")+"qoyfgbsdjf");

                        if (object.getString("seat") != null && !object.getString("seat").isEmpty()
                                && !object.getString("seat").equals("null"))
                            {
                            seat_no.setText(object.getString("seat"));
                        }else {
                            seat_no.setText("000");

                        }

                        if (!object.getString("locker").equalsIgnoreCase("null")){
                            lock_no.setText(object.getString("locker"));

                        }

                        if (object.getString("domain_name").equalsIgnoreCase("null")) {
                            topic.setText("Not found");
                        } else
                            topic.setText(object.getString("domain_name"));

                        if (object.getString("image").length() > 4) {
                            user_image.setClickable(true);
                            Picasso.get().load(batch_images + object.getString("image")).fit().error(R.drawable.error_circle).into(user_image);
                        }
                        if (object.getString("id_card").length() > 4) {
                            id_image.setClickable(true);
                            Picasso.get().load(batch_images + object.getString("id_card")).fit().into(id_image);
                        }
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(UserProfile.this, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {

        progressBar.setVisibility(View.GONE);
                String message = null;
                progressBar.setVisibility(View.GONE);
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(UserProfile.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("student_id", user_id);
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == edit_btn) {
            if (object != null) {
                editUser alert = new editUser();
                alert.showDialog(UserProfile.this, object);
            }
        } else if (v == transactions) {
            Intent intent = new Intent(UserProfile.this, ViewTransactionHistory.class);
            intent.putExtra("object", object + "");
            try {
                if (jObj.getString("total_amount") == null) {
                    intent.putExtra("total_amount", "0");
                } else
                    intent.putExtra("total_amount", jObj.getString("total_amount"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            startActivity(intent);
        } else if (v == seat_no) {
            AddCategoryDialog alert = new AddCategoryDialog();
            alert.showDialog(UserProfile.this,"seat");
        } else if (v==lock_no){

            AddCategoryDialog alert1=new AddCategoryDialog();
            alert1.showDialog(UserProfile.this,"locker");

        }

        else if (v == id_image) {
            try {
                if (object.getString("id_card").length() > 4) {
//                Picasso.get().load(batch_images + object.getString("id_card")).fit().into(id_image);
                    new util(UserProfile.this).Zoom_Image(UserProfile.this,batch_images+object.getString("id_card"),false);
//                    ViewDialog alert = new ViewDialog();
//                    alert.showDialog(UserProfile.this, object.getString("id_card"));
                } else id_image.setEnabled(false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (v == user_image) {
            try {
                if (object.getString("image").length() > 4) {

//                Picasso.get().load(batch_images + object.getString("id_card")).fit().into(id_image);
                    new util(UserProfile.this).Zoom_Image(UserProfile.this,batch_images+object.getString("image"),false);
//                    ViewDialog alert = new ViewDialog();
//                    alert.showDialog(UserProfile.this, object.getString("image"));

                } else id_image.setEnabled(false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if (v==view_identity_card){

            Intent intent=new Intent(this,IdentityCardActivity.class);
            intent.putExtra("user_id",userId);
            startActivity(intent);


        }
    }

    public class AddCategoryDialog {
        void showDialog(Activity activity,String type) {

            selected_seat="";
            dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
            );
            dialog.setContentView(R.layout.add_seat);


            add = (TextView) dialog.findViewById(R.id.submit);
            seatt_update = (AutoCompleteTextView) dialog.findViewById(R.id.seat_nno);
            progressBar1 = dialog.findViewById(R.id.progress_bar);
            progressBar1.setVisibility(View.GONE);
            search_recycler=dialog.findViewById(R.id.search_recycler);

            ImageView dropdown_icon=dialog.findViewById(R.id.dropdown_icon);

            dropdown_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    seatt_update.showDropDown();
                }
            });
            if (type.equals("seat")) {
                seatt_update.setHint("Select Seat");
//                Toast.makeText(activity, "Seat Already allotted!!", Toast.LENGTH_SHORT).show();
            }else {

                seatt_update.setHint("Select Locker");
//                Toast.makeText(activity, "Locker Already allotted!!", Toast.LENGTH_SHORT).show();

            }
            LinearLayoutManager manager=new LinearLayoutManager(UserProfile.this);
            search_recycler.setLayoutManager(manager);
            seatt_update.setThreshold(0);
//            seatt_update.setScrollBarSize(0);
//            seatt_update.setVerticalScrollBarEnabled(false);
             adapter = new AutoCompleteAdapter(UserProfile.this,android.R.layout.simple_dropdown_item_1line,
                    searchresultArrayList);
            seatt_update.setAdapter(adapter);

//           seatt_update.

            searchApp("",type);
            seatt_update.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    Log.e("sss",s+"");
                    selected_seat="";
                    seatt_update.invalidate();
//                    seatt_update.setText("     "+s);
//                    seatt_update.
                    searchApp(s+"",type);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

//                    searchApp("","seat");
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            seatt_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   seatt_update.showDropDown();
                }
            });

            seatt_update.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    seatt_update.dismissDropDown();
                    TextView textView=view.findViewById(R.id.search_text);
                    TextView typ=view.findViewById(R.id.type);
                    Log.e("size",searchresultArrayList.size()+"");
                    Log.e("seat",position+"     "+typ.getText().toString()+"  "+type);


                    if (typ.getText().toString().equals("false"))
                    {
                    selected_seat = textView.getText().toString();
                    Log.e("seat",selected_seat);
                }else {

                        seatt_update.setText("");
                        if (type.equals("seat")) {

                            seatt_update.setHint("Select Seat");
                            Toast.makeText(activity, "Seat Already allotted!!", Toast.LENGTH_SHORT).show();
                        }else {

                            seatt_update.setHint("Select Locker");
                            Toast.makeText(activity, "Locker Already allotted!!", Toast.LENGTH_SHORT).show();

                        }
                    }

                }
            });


            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selected_seat.trim().equalsIgnoreCase("") ) {

                        if (seatt_update.getText().toString().equals("")){
//
                            AlertDialog.Builder builder = new AlertDialog.Builder(UserProfile.this);
                            builder.setMessage("Do you want to unreserve member's "+type +" ?");

                            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing but close the dialog
                                    updateSeat(type);
                                    dialog.dismiss();
                                }
                            });

                            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    // Do nothing
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();
//                            Toast.makeText(activity, "inside blank", Toast.LENGTH_SHORT).show();
                        }else {

                            seatt_update.setError("Please select "+type+"  no. from drop down");
                        }

                    } else {


//                        Toast.makeText(activity, "blank", Toast.LENGTH_SHORT).show();
                        updateSeat(type);
                    }
                }
            });
            dialog.show();
        }
    }



    public static void hideSoftKeyboard(Activity activity) {
        final InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isActive()) {
            if (activity.getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }


    public void searchApp(String query,String type) {


        searchresultArrayList.clear();
        RequestQueue MyRequestQueue = Volley.newRequestQueue(UserProfile.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, seat_status, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response BatchList", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
//                        search_recycler.setVisibility(View.VISIBLE);
                        JSONArray array = jObj.getJSONArray("data");
                        Log.e("GetList array  ", array + "");


                        searchresultArrayList.clear();
                        for (int i=0;i<array.length();i++){

                            JSONObject object=array.getJSONObject(i);
                            searchresultArrayList.add(new searchresult(object.getString("id"),
                                    object.getString("status")));
                        }

                        Log.e("siE",searchresultArrayList.size()+"");
                        seatt_update.invalidate();
                         adapter = new AutoCompleteAdapter(UserProfile.this,android.R.layout.simple_dropdown_item_1line,
                                searchresultArrayList);
                        seatt_update.setAdapter(adapter);
//                        adapter.notifyDataSetChanged();
//                        seatt_update.showDropDown();
//                        seatt_update.requestFocus();

                    } else {
//                        search_recycler.setVisibility(View.GONE);
//                        Toast.makeText(Dashboard.this, "No result found!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(UserProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("seat_number", query);
                params.put("event_id",sharedPreferences.geteventId());
                params.put("type",type);
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public class editUser {
        void showDialog(Activity activity, JSONObject object) {
            dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.user_delete_popup);


            progressBar2 = dialog.findViewById(R.id.progress_bar);
            progressBar2.setVisibility(View.GONE);
            pop_image = dialog.findViewById(R.id.user_image);
            delete_user = (ImageView) dialog.findViewById(R.id.delete_btn);
            edit_user = (ImageView) dialog.findViewById(R.id.edit_btn);

            try {
                if (object.getString("image").length() > 4) {
                    Picasso.get().load(batch_images + object.getString("image")).fit().error(R.drawable.error_circle).into(pop_image);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            delete_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disableUser();
                }
            });


            edit_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(UserProfile.this, AddUserActivity.class);
                    intent.putExtra("object", object + "");
                    startActivityForResult(intent,12);
                    dialog.dismiss();
                }
            });


            dialog.show();
        }
    }

    public void disableUser() {
        progressBar2.setVisibility(View.VISIBLE);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(UserProfile.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, disable_student, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response_seat", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar2.setVisibility(View.GONE);
                        // Launch login activity
                        dialog.dismiss();
//                        getUserData(userId);
//                        Intent intent = new Intent(UserProfile.this, SelectMemberActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);

                        finish();

                        Toast.makeText(UserProfile.this, "User is Deleted successfully!", Toast.LENGTH_SHORT).show();
                    } else {
                        progressBar2.setVisibility(View.GONE);
                        Toast.makeText(UserProfile.this, "Sorry! you can't delete User.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar2.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                progressBar2.setVisibility(View.GONE);

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(UserProfile.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("student_id", userId);
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public void updateSeat(String type) {
        progressBar1.setVisibility(View.VISIBLE);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(UserProfile.this);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, update_seat, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response_seat", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar1.setVisibility(View.GONE);
                        // Launch login activity
                        dialog.dismiss();
                        seat_no.setText(object.getString("seat"));
                        getUserData(userId);
                        Toast.makeText(UserProfile.this, jObj.getString("message"), Toast.LENGTH_SHORT).show();
                    } else {
                        progressBar1.setVisibility(View.GONE);
                        Toast.makeText(UserProfile.this, jObj.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar1.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                    progressBar1.setVisibility(View.GONE);
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(UserProfile.this, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("student_id", userId);
                params.put("batch_id", batch_id);
                params.put("seat_number", selected_seat);
                params.put("type",type);
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public class ViewDialog {
        void showDialog(Activity activity, String image) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.view_bill);

            Log.e("imagename", image);

            progressBar3 = dialog.findViewById(R.id.progress_bar);
            progressBar3.setVisibility(View.VISIBLE);
            ImageView bill_image = (ImageView) dialog.findViewById(R.id.bill_image);
            TextView error_text = (TextView) dialog.findViewById(R.id.error_text);

            Picasso.get().load(batch_images + image).fit().error(R.drawable.error_img).into(bill_image);

            progressBar3.setVisibility(View.GONE);

            dialog.show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("requestCode=",requestCode+"_resultCode"+resultCode);
        if (requestCode==12 && resultCode==RESULT_OK)
        {
            if (data!=null){
                loggg("dataa",data.getStringExtra("data"));
                if (data.getStringExtra("data").length()>0){
                    userId=data.getStringExtra("data");
                    getUserData(userId);
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

}
