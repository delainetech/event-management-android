package com.bzzy.Models;

public class ModelTest {

    private String prop1, prop2, prop3,prop4,prop5,prop6;
    private boolean showBottomView;

    public ModelTest( boolean showBottomView) {
        this.showBottomView = showBottomView;
    }

    public boolean isShowBottomView() {
        return showBottomView;
    }

    public void setShowBottomView(boolean showBottomView) {
        this.showBottomView = showBottomView;
    }

}
