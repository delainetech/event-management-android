package com.bzzy.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bzzy.Api.ApiCalls;
import com.bzzy.Popus.ExpenseBill;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class InvestmentExpenseAdapter extends RecyclerView.Adapter<InvestmentExpenseAdapter.ViewHolder> implements ApiCalls {
    Context context;
    List<String> list = new ArrayList<>();
    JSONArray array;


    public InvestmentExpenseAdapter(Context context, List<String> cardList, JSONArray array) {
        this.context = context;
        this.list = cardList;
        this.array = array;
    }

    @NonNull
    @Override
    public InvestmentExpenseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_card, parent, false);
        return new InvestmentExpenseAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final InvestmentExpenseAdapter.ViewHolder holder, final int i) {

        try {
            JSONObject object = array.getJSONObject(i);

            holder.item_name.setText(object.getString("name"));
            Log.e("object adpt", object + "");

            holder.rate.setText("₹ "+object.getString("amount"));

            if (object.getString("image").length()>4)
            Picasso.get().load(advertising_images+object.getString("image")).error(R.drawable.other_icon).into(holder.product_image);
// Add data to card
            holder.main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentActivity activity = (FragmentActivity) (context);
                    FragmentManager fm2 = activity.getSupportFragmentManager();
                    ExpenseBill dialog = new ExpenseBill();
                    Bundle bundle = new Bundle();
                    try {
                        bundle.putString("item_id", object.getString("id"));
                        bundle.putString("item_name",object.getString("name"));
                        bundle.putString("cat_type",object.getString("type"));
                        bundle.putString("image", object.getString("image"));
                        bundle.putBoolean("status",false);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    dialog.setArguments(bundle);
                    dialog.show(fm2, "");
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return array.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView product_image;
        TextView item_name, rate;
        CardView main;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            main = (CardView) itemView.findViewById(R.id.main_layout);
            product_image = itemView.findViewById(R.id.shopimg);
            item_name = itemView.findViewById(R.id.shoptxt);
            rate = itemView.findViewById(R.id.price);
        }
    }
}
