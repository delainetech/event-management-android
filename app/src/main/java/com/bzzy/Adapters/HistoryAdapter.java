package com.bzzy.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bzzy.Activities.ViewExpenditureBills;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;
import com.bzzy.utils.util;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> implements ApiCalls {
    List<String> dataList;
    String cat_name;
    Context context;
    JSONArray data;
    Dialog dialog;
    ProgressBar progressBar;


    public HistoryAdapter(Context context, JSONArray array, String cat_name) {
        this.context = context;
        this.data = array;
        this.cat_name = cat_name;

    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_items, viewGroup, false);
        return new HistoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HistoryAdapter.ViewHolder holder, int i) {

        try {
            JSONObject object = data.getJSONObject(i);
            if (!object.getString("description").equalsIgnoreCase("null")) {
                holder.product_name.setText(object.getString("description"));
            } else if (!object.getString("sub_type").equalsIgnoreCase("null")) {
                if (object.getString("sub_type").equalsIgnoreCase("other")) {
                    holder.product_name.setText(object.getString("sub_type") + "(" + object.getString("cat_name") + ")");
                } else
                    holder.product_name.setText(object.getString("sub_type"));
            } else
                holder.product_name.setText(object.getString("cat_name"));

            holder.bill_date.setText(object.getString("date"));

            if (object.getString("remark").equalsIgnoreCase("null")) {
                holder.remark.setText(" ");
            } else
                holder.remark.setText(object.getString("remark"));

            if (object.getString("cat_image").length() > 4)
                Picasso.get().load(advertising_images + object.getString("cat_image")).error(R.drawable.other_icon).into(holder.product_image);

            holder.amount.setText("₹ " + object.getString("amount"));

            // Check bill image

            if (data.getJSONObject(i).getString("image").length() > 4) {
                holder.view_bill.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
Log.e("boolean","true");
//                        ViewDialog alert = new ViewDialog();
                        try {
                            new util(context).Zoom_Image(context,expenditure_images+data.getJSONObject(i).getString("image"),true);
//                            alert.showDialog((ViewExpenditureBills) context, data.getJSONObject(i).getString("image"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } else
                holder.view_bill.setTextColor(context.getResources().getColor(R.color.light_grey));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView product_name, bill_date, remark, view_bill, amount;
        ImageView product_image;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            product_image = itemView.findViewById(R.id.product_image);
            product_name = (TextView) itemView.findViewById(R.id.product_name);
            bill_date = (TextView) itemView.findViewById(R.id.bill_date);
            remark = (TextView) itemView.findViewById(R.id.remarks);
            view_bill = itemView.findViewById(R.id.view_bill);
            amount = (TextView) itemView.findViewById(R.id.bill_amount);
        }
    }

    public class ViewDialog {
        void showDialog(Activity activity, String image) {
            dialog = new Dialog(activity);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.view_bill);

            Log.e("imagename", image);
            progressBar = dialog.findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.VISIBLE);

            ImageView bill_image = (ImageView) dialog.findViewById(R.id.bill_image);
            TextView error_text = (TextView) dialog.findViewById(R.id.error_text);

            if (image.length() > 4) {
                Picasso.get().load(expenditure_images + image).fit().into(bill_image);
                progressBar.setVisibility(View.GONE);
            } else {
                bill_image.setVisibility(View.GONE);
                error_text.setVisibility(View.VISIBLE);
            }
            dialog.show();
        }
    }
}