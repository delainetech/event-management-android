package com.bzzy.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bzzy.Models.searchresult;
import com.bzzy.R;

import java.util.ArrayList;

public class Filter extends ArrayAdapter<searchresult> {
    public Filter(Context context, ArrayList<searchresult> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        searchresult user = getItem(position);
        Log.e("sfskdlfdlfd",position+"");
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.search_item, parent, false);
        }
        // Lookup view for data population

        TextView tvName = (TextView) convertView.findViewById(R.id.search_text);
//        TextView tvHome = (TextView) convertView.findViewById(R.id.tvHome);
        // Populate the data into the template view using the data object
        tvName.setText(user.getId());
//        tvHome.setText(user.hometown);
        // Return the completed view to render on screen
        return convertView;
    }
}
