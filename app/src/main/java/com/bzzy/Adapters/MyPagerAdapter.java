package com.bzzy.Adapters;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.AddUserActivity;
import com.bzzy.Activities.UserProfile;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.Constants.VolleyMultipartRequest;
import com.bzzy.R;
import com.bzzy.utils.Common;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import static com.bzzy.Activities.AddUserActivity.getFileDataFromDrawable;

public class MyPagerAdapter extends PagerAdapter implements ApiCalls {

    Context context;
    private String time;
    private JSONObject obje;
    static EditText time_from = null, time_to = null, fee = null, user_name = null, enter_mobile = null,
            enter_email = null,enter_father=null;
    static Spinner enter_topic_spinner = null;
    EditText trail_spinner;
    static String selected_trail1;
    public static ImageView user_image, id_card;
    public static TextView upload_id, done;
    UserSharedPreferences sharedPreferences;
    private ArrayList<String> trails_name = new ArrayList<>();
    ArrayAdapter<String> adapter;
    ProgressDialog pDialog;
    ImageView add_user = null;
    EditText id_number;
    View view = null;
    JSONArray domain_arr;
    ArrayList<String> domain_name = new ArrayList<>();
    public ArrayAdapter<String> arrayAdapter;
    String domain_name_select;
    JSONObject object = null;
    Dialog dialog;
    public static ImageView next, previous;
    String date;
    ProgressBar progressBar;
    ImageView previous_history, quit_registration;
    StringRequest MyStringRequest;
    RequestQueue MyRequestQueue;
    TextView record_details;
    int dayFinal, monthFinal, yearFinal, hourFinal, minuteFinal;
    int day, month, year;
    int mHour, mMinute;

    public MyPagerAdapter(Context context, JSONObject obj, JSONArray domain_array, String date) {
        this.context = context;
        this.obje = obj;
        this.domain_arr = domain_array;
        this.date = date;
        sharedPreferences = new UserSharedPreferences(context);
    }

    public MyPagerAdapter(Context context, JSONObject obj) {
        this.context = context;
        this.obje = obj;
        sharedPreferences = new UserSharedPreferences(context);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int i) {

// First View ................................
        if (i == 0) {
            view = LayoutInflater.from(context).inflate(R.layout.time_set, null, false);
            time_from = (EditText) view.findViewById(R.id.time_from);
            time_to = (EditText) view.findViewById(R.id.time_to);
            fee = (EditText) view.findViewById(R.id.batch_fee);



//            spinner
            enter_topic_spinner = (Spinner) view.findViewById(R.id.enter_topic);

            try {
                for (int k = 0; k < domain_arr.length(); k++) {
                    object = domain_arr.getJSONObject(k);
                    domain_name.add(object.getString("name"));
                    arrayAdapter = new ArrayAdapter<>(context, R.layout.domain_spinner, domain_name);
                    enter_topic_spinner.setAdapter(arrayAdapter);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            enter_topic_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        domain_name_select = domain_arr.getJSONObject(position).getString("id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("domain_name_select", domain_name_select + "");
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            trail_spinner = (EditText) view.findViewById(R.id.trails_spinner);
            next = view.findViewById(R.id.next);

            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getItem(i);
                }
            });
            // Create an ArrayAdapter that will contain all list items

            trail_spinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StartDateTimePicker(trail_spinner);
                }
            });

            try {
                time_from.setText(obje.getString("start_time"));
                time_to.setText(obje.getString("end_time"));
                fee.setText(obje.getString("fee"));

                time_from.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectTime(time_from);
                    }
                });

                time_to.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectTime(time_to);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
// Second View ................................
        } else if (i == 1) {
            view = LayoutInflater.from(context).inflate(R.layout.personal_details, null, false);
            user_name = (EditText) view.findViewById(R.id.enter_username);
            enter_mobile = (EditText) view.findViewById(R.id.enter_phone);
            enter_email = (EditText) view.findViewById(R.id.enter_email);
            enter_father = (EditText) view.findViewById(R.id.enter_father);
             next = view.findViewById(R.id.next);
            previous = view.findViewById(R.id.previous);

            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getItem(i + 1);
                }
            });

            previous.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getItem(i - 1);
                }
            });


            Log.e("GetList object", object + "");



// Third View ................................
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.upload_image, null, false);

            id_number = (EditText) view.findViewById(R.id.id_number);
            upload_id = (TextView) view.findViewById(R.id.upload_id);
            user_image = (ImageView) view.findViewById(R.id.user_image);
            add_user = (ImageView) view.findViewById(R.id.submit_details);
            done = (TextView) view.findViewById(R.id.done);
            id_card = (ImageView) view.findViewById(R.id.id_card);
            progressBar = view.findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.GONE);


            upload_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON).setAspectRatio(512, 350)
                            .getIntent(context);
                    ((AddUserActivity) context).startActivityForResult(intent, 212);
                }
            });

            user_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).setAspectRatio(400, 580)
                            .getIntent(context);
                    ((AddUserActivity) context).startActivityForResult(intent, 251);
                }
            });

            add_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    validations();
                }

                // Validations
                private void validations() {
//                    if (id_number.getText().toString().trim().equalsIgnoreCase("")) {
//                        id_number.setError("Enter ID number.");
//                    } else

                        if (time_from.getText().toString().trim().equalsIgnoreCase("")) {
                        time_from.setError("Please check start time");
                    } else if (time_to.getText().toString().trim().equalsIgnoreCase("")) {
                        time_to.setError("Please check end time.");
                    } else if (fee.getText().toString().trim().equalsIgnoreCase("")) {
                        fee.setError("Please provide fee");
                    } else if (trail_spinner.getText().toString().trim().equalsIgnoreCase("")) {
                        Toast.makeText(context, "Please select due date", Toast.LENGTH_SHORT).show();
                    } else if (user_name.getText().toString().trim().equalsIgnoreCase("")) {
                        user_name.setError("Add username");
                        Toast.makeText(context, "Add username", Toast.LENGTH_SHORT).show();
                    } else if (enter_father.getText().toString().trim().equalsIgnoreCase("")) {
                            enter_father.setError("Add father name");
                        Toast.makeText(context, "Add father name", Toast.LENGTH_SHORT).show();
                    } else if (enter_mobile.getText().toString().trim().equalsIgnoreCase("")) {
                        enter_mobile.setError("Enter mobile no.");
                        Toast.makeText(context, "Enter mobile no.", Toast.LENGTH_SHORT).show();
                    } else if (!isValidMobile(enter_mobile.getText().toString().trim())) {
                        enter_mobile.setError("Incorrect Mobile no.");
                        Toast.makeText(context, "Incorrect Mobile no.", Toast.LENGTH_SHORT).show();
                    }else if ((enter_email.getText().toString().trim().length()>0) && !isValidEmailId(enter_email.getText().toString().trim())) {
                                enter_email.setError("Email is not valid.");
                                Toast.makeText(context, "Incorrect Email Id", Toast.LENGTH_SHORT).show();
                        }else if (enter_topic_spinner.equals("")) {
                        Toast.makeText(context, "Please enter topic", Toast.LENGTH_SHORT).show();
                    }
//                    else if (((AddUserActivity) context).idmap == null) {
//                        Toast.makeText(context, "Please upload ID-card image.", Toast.LENGTH_SHORT).show();
//                    }else if (((AddUserActivity) context).userBitmap1 == null) {
//                        Toast.makeText(context, "Please upload User image.", Toast.LENGTH_SHORT).show();
//                    }
                    else {
                        previousRecord();
                    }
                }
            });
        }

        container.addView(view);
        return view;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    private boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    private boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    private void selectTime(EditText setTime) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                datetime.set(Calendar.MINUTE, selectedMinute);

                if (selectedHour >= 00 && selectedHour < 12) {
                    time = String.format("%02d:%02d", selectedHour, selectedMinute) + " AM";
                } else {
                    if (selectedHour == 12) {
                        time = String.format("%02d:%02d", selectedHour, selectedMinute) + " PM";
                    } else {
                        selectedHour = selectedHour - 12;
                        time = String.format("%02d:%02d", selectedHour, selectedMinute) + " PM";
                    }
                }
                setTime.setText(time);
            }
        }, hour, minute, false);
        mTimePicker.show();
    }

    private void addUser(Bitmap id_card1, Bitmap user_image_bit, int Status) {

        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Registering...");
        pDialog.show();
        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, add_new_user,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        Log.e("Response", new String(response.data) + " ");
                        //This code is executed if the server responds, whether or not the response contains data.
                        try {
                            JSONObject jObj = new JSONObject(new String(response.data));
                            String status = jObj.getString("status");

                            if (status.equals("success")) {
                                pDialog.dismiss();

                                if (Status == 0) {
                                    dialog.dismiss();
                                }

                                JSONObject object = jObj.getJSONObject("data");
//                                user_name.getText().clear();
//                                enter_mobile.getText().clear();
//                                enter_email.getText().clear();
//                                enter_father.getText().clear();
//                                trail_spinner.getText().clear();
//
//                                time_from.setText(obje.getString("start_time"));
//                                time_to.setText(obje.getString("end_time"));
//                                fee.setText(obje.getString("fee"));
//
//                                id_number.getText().clear();
//
//                                ((AddUserActivity) context).registration.setCurrentItem(0);
//                                done.setVisibility(View.GONE);
//                                ((AddUserActivity) context).idmap = null;
//                                ((AddUserActivity) context).userBitmap1 = null;
//                                int occupy = Integer.parseInt(obje.getString("occupied")) + 1;
//                                ((AddUserActivity) context).occupied.setText(occupy + "");
//
//                                int vaccant = Integer.parseInt(obje.getString("seats")) - (Integer.parseInt(obje.getString("occupied")) + 1);
//
//                                if (vaccant > 0) {
//                                    ((AddUserActivity) context).vacant.setText(vaccant + "");
//                                } else ((AddUserActivity) context).vacant.setText(0 + "");
//
//
//                                id_card.setImageDrawable(context.getResources().getDrawable(R.drawable.card_background_orange));
//                                user_image.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_upload_photo));

                                Intent intent = new Intent(context, UserProfile.class);
                                try {
                                    intent.putExtra("user_id", object.getString("id"));
                                    intent.putExtra("batch_id", object.getString("batch_id"));
                                } catch (JSONException e) {
                                    Log.e("laidhfgodfgb",e.toString());
                                    e.printStackTrace();
                                }
                                    notifyDataSetChanged();
                                ((AddUserActivity)context).setUriNull();
                                context.startActivity(intent);


                                Toast.makeText(context, "User added successfully.", Toast.LENGTH_SHORT).show();


                            } else if (jObj.getString("message").equals("User Deleted from library!!")){
                                showdialog(jObj.getString("delete_id"));
                            }else {
                                String message = jObj.getString("message");
                                pDialog.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e1) {
                            pDialog.dismiss();
                            Log.e("Exception", e1 + " ");
                            e1.printStackTrace();
                        }
                    }
                },
                error -> {
                    pDialog.dismiss();
                    Toast.makeText(context, "Please check your Internet connection", Toast.LENGTH_SHORT).show();

                    Log.e("onErrorResponse: ", error + "");
                    NetworkResponse networkResponse = error.networkResponse;
                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";
                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put("event_id", sharedPreferences.geteventId());
                    params.put("user_id", sharedPreferences.getuserID());
                    params.put("start_time", time_from.getText().toString().trim());
                    params.put("end_time", time_to.getText().toString().trim());
                    params.put("due_date", trail_spinner.getText().toString().trim());
                    params.put("student_name", user_name.getText().toString().trim());
                    params.put("amount", fee.getText().toString().trim());
                    params.put("batch_id", obje.getString("id"));
                    params.put("topic", domain_name_select);
                    params.put("student_id", " ");
                    params.put("father_name", enter_father.getText().toString().trim());
                    params.put("date", date);
//                    params.put("due_date", );
                    params.put("student_phone", enter_mobile.getText().toString().trim());
                    if ( enter_email.getText().toString().trim().length()>0) {
                        params.put("student_email", enter_email.getText().toString().trim());
                    }
                    params.put("id_number", id_number.getText().toString().trim());
                    if (user_image_bit == null) {
                        params.put("user_image", " ");
                    }
                    if (id_card1 == null) {
                        params.put("id_card", " ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("getParams: ", params + "");
                return params;
            }


            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
//                Log.e("id-card", imagename+"");
//                Log.e("userImage","profile_" + UUID.randomUUID().toString() + ".png");
                if (id_card1 != null && user_image_bit != null) {
                    params.put("id_card", new DataPart(imagename + ".png", getFileDataFromDrawable(id_card1)));
                    params.put("user_image", new DataPart("profile_" + UUID.randomUUID().toString() + ".png", getFileDataFromDrawable(user_image_bit)));
                    //                    params.put("image", new DataPart(System.currentTimeMillis() + "image.png", profile_image));
                } else if (user_image_bit != null) {
                    params.put("user_image", new DataPart("profile_" + UUID.randomUUID().toString() + ".png", getFileDataFromDrawable(user_image_bit)));
                } else if (id_card1 != null){
                    params.put("id_card", new DataPart(imagename + ".png", getFileDataFromDrawable(id_card1)));

                }
                Log.e("params", params + "");
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //adding the request to volley
        Volley.newRequestQueue(context).add(volleyMultipartRequest);
    }


    public void StartDateTimePicker(EditText date) {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        yearFinal = year;
                        monthFinal = month + 1;
                        dayFinal = dayOfMonth;

                        Log.e("Date", dayOfMonth + "-" + monthFinal + "-" + yearFinal);
                        String due_date = String.format("%02d-%02d-%02d", dayOfMonth, monthFinal, yearFinal);
                        date.setText(due_date);
                    }
                }, year, month, day);
//        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    private int getItem(int i) {
        return ((AddUserActivity) context).registration.getCurrentItem() - i;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("MyAdapter", "onActivityResult");
    }

    public class registrationCheck {
        void showDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.registration_submit);


            record_details = dialog.findViewById(R.id.previous_history);
            previous_history = dialog.findViewById(R.id.register_anyway);
            quit_registration = dialog.findViewById(R.id.quit_registration);
            quit_registration.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    user_name.getText().clear();
                    enter_mobile.getText().clear();
                    enter_email.getText().clear();
                    enter_father.getText().clear();
                    trail_spinner.getText().clear();
                    try {
                        time_from.setText(obje.getString("start_time"));

                        time_to.setText(obje.getString("end_time"));
                        fee.setText(obje.getString("fee"));

                        id_number.getText().clear();

                        done.setVisibility(View.GONE);
                        ((AddUserActivity) context).idmap = null;
                        ((AddUserActivity) context).userBitmap1 = null;

                        id_card.setImageDrawable(context.getResources().getDrawable(R.drawable.card_background_orange));
                        user_image.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_upload_photo));

                        ((AddUserActivity) context).registration.setCurrentItem(0);
                        dialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            previous_history.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addUser(((AddUserActivity) context).idmap, ((AddUserActivity) context).userBitmap1, 0);
                }
            });

            dialog.show();
        }
    }

    private void previousRecord() {
        progressBar.setVisibility(View.VISIBLE);
        MyRequestQueue = Volley.newRequestQueue(context);
        MyStringRequest = new StringRequest(Request.Method.POST, previous_record, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response in admiprofile", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar.setVisibility(View.GONE);
                        if (jObj.getString("data").equalsIgnoreCase("0")) {
                            addUser(((AddUserActivity) context).idmap, ((AddUserActivity) context).userBitmap1, 1);
                        } else if (jObj.getString("data").equalsIgnoreCase("1")) {
                            registrationCheck alert = new registrationCheck();
                            try {
                                alert.showDialog((AddUserActivity) context);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            record_details.setText("This member has been registered with " + jObj.getString("data") + " event in Last 3 MONTHS.");
                        } else {
                            registrationCheck alert = new registrationCheck();
                            try {
                                alert.showDialog((AddUserActivity) context);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            record_details.setText("This member has been registered with " + jObj.getString("data") + " events in Last 3 MONTHS.");
                        }
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(context, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("phone", enter_mobile.getText().toString().trim());
//                params.put("email", enter_email.getText().toString().trim());
                params.put("id_number", id_number.getText().toString().trim());
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public interface IdImage {
        static void setIdImage(Bitmap uri) {
            id_card.setImageBitmap(uri);
        }
    }

    public interface UserImage {
        static void setUserImage(Bitmap uri) {
            user_image.setImageBitmap(uri);
        }
    }

    // Back from edit
    public void showdialog(String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle("Wait!");
        builder.setMessage("Re-register your old Member?");
        builder.setPositiveButton("Continue",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                        unable_user(id);
                    }
                });
        builder.setNegativeButton("Quit",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();

//
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void unable_user(String id) {
        progressBar.setVisibility(View.VISIBLE);
        MyRequestQueue = Volley.newRequestQueue(context);
        MyStringRequest = new StringRequest(Request.Method.POST, disable_student, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response in admiprofile", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        Toast.makeText(context, "User Re-registered", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                        if (pDialog!=null){
                            pDialog.dismiss();
                        }
                        if(dialog!=null){
                            dialog.dismiss();
                        }
                        (  (AddUserActivity)context).finish();


                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("student_id", id);
//                params.put("email", enter_email.getText().toString().trim());
                params.put("status", "1");
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }


}
