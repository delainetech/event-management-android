package com.bzzy.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.AdminProfile;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WithdrawAdapter extends RecyclerView.Adapter<WithdrawAdapter.ViewHolder> implements ApiCalls {
    List<String> dataList;
    String letter;
    Context context;
    JSONArray data;
    Dialog dialog;
    UserSharedPreferences sharedPreferences;

    public WithdrawAdapter(Context context, JSONArray array) {
        this.context = context;
        this.data = array;
        sharedPreferences = new UserSharedPreferences(context);
    }

    @Override
    public WithdrawAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.withdraw_items, viewGroup, false);
        return new WithdrawAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final WithdrawAdapter.ViewHolder holder, int i) {
        try {
            JSONObject object = data.getJSONObject(i);
// Set details of expense
            if (data.getJSONObject(i).getString("status").equalsIgnoreCase("0")) {
                holder.partner_name.setText(object.getString("name"));
               if (data.getJSONObject(i).length()>4)
                Picasso.get().load(batch_images+object.getString("image")).fit().error(R.drawable.error_circle).into(holder.partner_image);

                holder.bill_date.setText(object.getString("created_at"));
                holder.main.setBackgroundColor(context.getResources().getColor(R.color.withdraw_card_back));
                if (object.getString("remark").equalsIgnoreCase("null")) {
                    holder.remark.setText("");
                } else
                    holder.remark.setText(object.getString("remark"));

                holder.amount.setText("₹ " + object.getString("amount"));
// Approve bill
                holder.approve.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (sharedPreferences.getuserID().equalsIgnoreCase(object.getString("partner_id"))) {
                                Toast.makeText(context, "Sorry, you can't approve own request.", Toast.LENGTH_SHORT).show();
                            } else {
                                WithdrawAdapter.confirmationDialog alert = new WithdrawAdapter.confirmationDialog();
                                alert.showDialog((AdminProfile) context, data.getJSONObject(i).getString("id"), 1, i);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
// Decline bill
                holder.decline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            if (sharedPreferences.getuserID().equalsIgnoreCase(object.getString("partner_id"))) {
                                WithdrawAdapter.declineDialog alert = new WithdrawAdapter.declineDialog();
                                alert.showDialog((AdminProfile) context, data.getJSONObject(i).getString("id"), 3, i);
                            } else {
                                WithdrawAdapter.declineDialog alert = new WithdrawAdapter.declineDialog();
                                alert.showDialog((AdminProfile) context, data.getJSONObject(i).getString("id"), 2, i);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else if (data.getJSONObject(i).getString("status").equalsIgnoreCase("1")) {
                holder.partner_name.setText(object.getString("name"));
                if (data.getJSONObject(i).length()>4)
                    Picasso.get().load(batch_images+object.getString("image")).fit().error(R.drawable.error_circle).into(holder.partner_image);

                holder.status.setVisibility(View.VISIBLE);
                holder.status.setText("Accepted");
                holder.bill_date.setText(object.getString("created_at"));
                if (object.getString("remark").equalsIgnoreCase("null")) {
                    holder.remark.setText("");
                } else
                    holder.remark.setText(object.getString("remark"));
                holder.amount.setText("₹ " + object.getString("amount"));
                holder.approve.setVisibility(View.INVISIBLE);
                holder.decline.setVisibility(View.INVISIBLE);
            } else if (data.getJSONObject(i).getString("status").equalsIgnoreCase("2")) {
                holder.partner_name.setText(object.getString("name"));
                if (data.getJSONObject(i).length()>4)
                    Picasso.get().load(batch_images+object.getString("image")).fit().error(R.drawable.error_circle).into(holder.partner_image);

                holder.status.setVisibility(View.VISIBLE);
                holder.status.setText("Rejected");
                holder.bill_date.setText(object.getString("created_at"));
                if (object.getString("remark").equalsIgnoreCase("null")) {
                    holder.remark.setText("");
                } else
                    holder.remark.setText(object.getString("remark"));
                holder.amount.setText("₹ " + object.getString("amount"));
                holder.approve.setVisibility(View.INVISIBLE);
                holder.decline.setVisibility(View.INVISIBLE);
            } else if (data.getJSONObject(i).getString("status").equalsIgnoreCase("3")) {
                holder.partner_name.setText(object.getString("name"));
                if (data.getJSONObject(i).length()>4)
                    Picasso.get().load(batch_images+object.getString("image")).fit().error(R.drawable.error_circle).into(holder.partner_image);

                holder.status.setVisibility(View.VISIBLE);
                holder.status.setText("Cancelled");
                holder.bill_date.setText(object.getString("created_at"));
                if (object.getString("remark").equalsIgnoreCase("null")) {
                    holder.remark.setText("");
                } else
                    holder.remark.setText(object.getString("remark"));
                holder.amount.setText("₹ " + object.getString("amount"));
                holder.approve.setVisibility(View.INVISIBLE);
                holder.decline.setVisibility(View.INVISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView partner_name, bill_date, remark, amount, decline, approve, status;
        ImageView partner_image;
        ConstraintLayout main;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            partner_image = itemView.findViewById(R.id.partner_image);
            partner_name = (TextView) itemView.findViewById(R.id.partner_name);
            bill_date = (TextView) itemView.findViewById(R.id.bill_date);
            remark = (TextView) itemView.findViewById(R.id.remarks);
            amount = (TextView) itemView.findViewById(R.id.amount);
            decline = (TextView) itemView.findViewById(R.id.decline);
            approve = (TextView) itemView.findViewById(R.id.approve);
            main = (ConstraintLayout) itemView.findViewById(R.id.main_layout);
            status = (TextView) itemView.findViewById(R.id.status);
        }
    }


    // Confirm bill
    public class confirmationDialog {
        void showDialog(Activity activity, String exp_id, int status, int pos) {
            dialog = new Dialog(activity);
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.confirmation_dialog);


            TextView approve = (TextView) dialog.findViewById(R.id.submit_button);
            TextView cancel = (TextView) dialog.findViewById(R.id.delete_btn);

            approve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    respondWithdraw(exp_id, status, pos);
                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    // Decline bill
    public class declineDialog {
        void showDialog(Activity activity, String exp_id, int status, int pos) {
            dialog = new Dialog(activity);
            dialog.setCancelable(false);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.decline_dialog);

            TextView decline = (TextView) dialog.findViewById(R.id.submit_button);
            TextView cancel = (TextView) dialog.findViewById(R.id.delete_btn);

            decline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    respondWithdraw(exp_id, status, pos);
                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }


    private void respondWithdraw(String exp_id, int status, int pos) {
        RequestQueue MyRequestQueue = Volley.newRequestQueue(context);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, withdraw_respond, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        JSONObject object = jObj.getJSONObject("data");

                        dialog.dismiss();
//                       data.remove(pos);
//                        notifyDataSetChanged();
                        ((AdminProfile)context).respondWithdraw();
                    } else {
                        Toast.makeText(context, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("withdraw_id", exp_id);
                params.put("user_id", sharedPreferences.getuserID());
                params.put("status", status + "");
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }
}