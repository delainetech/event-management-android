package com.bzzy.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConversionAdapter extends RecyclerView.Adapter<ConversionAdapter.ViewHolder> implements ApiCalls {
    Context context;
    JSONArray data;
    Dialog dialog;
    UserSharedPreferences sharedPreferences;

    int year;

    public ConversionAdapter(Context context) {
        this.context = context;
    }

    public ConversionAdapter(Context context, JSONArray array) {
        this.context = context;
        this.data = array;
        sharedPreferences = new UserSharedPreferences(context);
    }

    @Override
    public ConversionAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.conversion_data, viewGroup, false);
        return new ConversionAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ConversionAdapter.ViewHolder holder, int i) {

        try {
            JSONObject object = data.getJSONObject(i);

            holder.month_name.setText(object.getString("month_name"));

            String value = String.format("%.2f", Float.parseFloat(object.getString("admission_percentage")));
            holder.conversion.setText(value + " %");


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView month_name, conversion;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();

            month_name = (TextView) itemView.findViewById(R.id.month_name);
            conversion = (TextView) itemView.findViewById(R.id.percentage_conversion);

        }

    }

}