package com.bzzy.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.SelectAttendence;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AttendenceContactAdapter extends RecyclerView.Adapter<AttendenceContactAdapter.ViewHolder> implements ApiCalls {

    String letter;
    Context context;
    JSONArray data;
    String new_attendence_array, new_attendence_;
    String batch_id;
    UserSharedPreferences sharedPreferences;
    TextView submit_attnd;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    ImageViewerAdapter imageViewerAdapter;


    public AttendenceContactAdapter(Context context) {
        this.context = context;
    }

    public AttendenceContactAdapter(Context context, JSONArray array, String batch_id, TextView submit_attendance,
                                    ImageViewerAdapter adapter, RecyclerView recyclerView
    ) {
        this.context = context;
        this.data = array;

        this.batch_id = batch_id;
        this.submit_attnd = submit_attendance;
        this.imageViewerAdapter = adapter;
        this.recyclerView = recyclerView;
        sharedPreferences = new UserSharedPreferences(context);
    }

    @Override
    public AttendenceContactAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contact_item_attendence, viewGroup, false);
        return new AttendenceContactAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AttendenceContactAdapter.ViewHolder holder, int i) {
        try {
            JSONObject object = data.getJSONObject(i);

            holder.user_name.setText(data.getJSONObject(i).getString("student_name"));
            holder.due_date.setText("Next due date: " + data.getJSONObject(i).getString("due_date"));

            if (data.getJSONObject(i).getString("image").length() > 0)
                Picasso.get().load(batch_images + object.getString("image")).error(R.drawable.error_circle).fit().into(holder.userimage);

            if (((SelectAttendence) context).dataList.size() > 0) {
//                if (object.getInt("id") == )
                holder.select_attendence.setChecked(((SelectAttendence) context).checkedItems.get(i));
            }
// Mark attendence
            holder.select_attendence.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // update your model (or other business logic) based on isChecked
                    if (isChecked) {
                        try {
                            ((SelectAttendence) context).dataList.add(String.valueOf(object.getInt("id")));
                            ((SelectAttendence) context).image_array.add(object.getString("image"));
                        } catch (JSONException e) {
                            Log.e("onClick: ", e + "");
                            e.printStackTrace();
                        }
//                        Collections.sort(((SelectAttendence) context).dataList);
                        Log.e("array1_id ", ((SelectAttendence) context).dataList + "");

                        String arrayid = String.valueOf(((SelectAttendence) context).dataList);
                        String newString = arrayid.replace("[", " ");
//                        new_attendence_ = newString.replace("]", " ");
                        new_attendence_array = newString.replace("]", " ");
//                        Log.e("arra_ image", ((SelectAttendence) context).image_array + "");

                        Log.e("image_array_if", new_attendence_array + "");

                        if (((SelectAttendence) context).image_array.size() < 6) {
                            Log.e("imageadapter", "if");
                            ((SelectAttendence) context).all_users_count.setVisibility(View.GONE);
                            ((SelectAttendence) context).setImageAdapter(((SelectAttendence) context).image_array);
                        } else {
                            Log.e("imageadapter", "else");
                            ((SelectAttendence) context).all_users_count.setVisibility(View.VISIBLE);
                            int value = ((SelectAttendence) context).image_array.size() - 5;
                            ((SelectAttendence) context).all_users_count.setText("+" + value + "");
                        }

                        ((SelectAttendence) context).checkedItems.put(i, true);

                    } else {

                        ((SelectAttendence) context).checkedItems.delete(i);

                        Log.e("unchecked", "else");
                        try {



                            if (((SelectAttendence) context).image_array.size() < 6) {
                                ((SelectAttendence) context).all_users_count.setVisibility(View.GONE);
                                ((SelectAttendence) context).setImageAdapter(((SelectAttendence) context).image_array);

                            } else if (((SelectAttendence) context).image_array.size() >= 6) {

                                ((SelectAttendence) context).all_users_count.setVisibility(View.VISIBLE);
                                int value = ((SelectAttendence) context).image_array.size() - 5;
                                ((SelectAttendence) context).all_users_count.setText("+" + value + "");
                                ((SelectAttendence) context).adapter.notifyDataSetChanged();
                            }

                            ((SelectAttendence) context).dataList.remove(String.valueOf(object.getInt("id")));
                            ((SelectAttendence) context).image_array.remove(object.getString("image"));

                            String arrayid = String.valueOf(((SelectAttendence) context).dataList);
                            String newString = arrayid.replace("[", " ");
//                        new_attendence_ = newString.replace("]", " ");
                            new_attendence_array = newString.replace("]", " ");

                            Log.e("image_array_else", new_attendence_array + "");
                            Log.e("array_else_image", ((SelectAttendence) context).image_array + "");
                            Log.e("id_array_else", ((SelectAttendence) context).dataList + "");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            submit_attnd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(context);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView userimage;
        TextView user_name, due_date;
        CheckBox select_attendence;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            userimage = (ImageView) itemView.findViewById(R.id.image_user);
            user_name = (TextView) itemView.findViewById(R.id.name_user);
            due_date = (TextView) itemView.findViewById(R.id.due_dae);
            select_attendence = (CheckBox) itemView.findViewById(R.id.attendance_box);


        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            select_attendence.setOnClickListener(onClickListener);
        }
    }

    private void setAttendence(Dialog dialog) {
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(context);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, add_attendence, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response_addAttendence", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar.setVisibility(View.GONE);
                        // Launch login activity
                        Log.e("success", "su");
                        dialog.cancel();
                        ((SelectAttendence) context).finish();
                        Toast.makeText(context, "Attendence submitted.", Toast.LENGTH_SHORT).show();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(context, "Sorry! Attendance already taken!", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", sharedPreferences.getuserID());
                params.put("event_id", sharedPreferences.geteventId());
                params.put("batch_id", batch_id);
                params.put("time", getCurrentTime() + "");

                if (new_attendence_array == null){
                    params.put("attendance", "");
                }else
                    params.put("attendance", new_attendence_array);


                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    private static String getCurrentTime() {
        //date output format
        DateFormat dateFormat = new SimpleDateFormat("hh:mm aa");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    // Conformation View and Dialog
    public class ViewDialog {
        void showDialog(Context activity) {
            Dialog dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.attendance_submission_popup);

            progressBar= dialog.findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.GONE);
            TextView cancel = (TextView) dialog.findViewById(R.id.delete_btn);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            TextView submit = (TextView) dialog.findViewById(R.id.submit_button);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setAttendence(dialog);
                }
            });
            dialog.show();
        }
    }
}