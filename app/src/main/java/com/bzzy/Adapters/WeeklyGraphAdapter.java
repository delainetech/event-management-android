package com.bzzy.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.bzzy.Activities.MyExpenses;
import com.bzzy.Activities.MyExpensesDetails;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;
//import com.google.gson.JsonArray;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class WeeklyGraphAdapter extends RecyclerView.Adapter<WeeklyGraphAdapter.ViewHolder> implements ApiCalls {

    Context context;
    Double amount;
    int k, current_day;
    JSONObject data;
    CardView expenses_detail;
    int position=0  ;
    private int selectedItem = -1;

    public WeeklyGraphAdapter(Context context, JSONObject data,
                              int current_day, Double amount, CardView expenses_detail) {
        this.context = context;
        this.current_day = current_day;
        this.data = data;
        this.expenses_detail=expenses_detail;
        this.amount = amount;
    }


    @Override
    public WeeklyGraphAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.week_bar, viewGroup, false);
        return new WeeklyGraphAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder( WeeklyGraphAdapter.ViewHolder holder,final int i) {

        k=0;

        k = i + 1;
//        Log.e("pos", i + "");
        try {
            data.getJSONObject("days").getString(k + "");


            Iterator i1 = data.getJSONObject("days").keys();
            String key = "";
//
//            String ka = "";
//
            while (i1.hasNext()) {
                key = i1.next().toString();
//
                holder.dayslist.add(key);
//                String k = key;
//                Log.e("node key", key+"12");
//                k = k.replaceAll("[^\\d.]", "");
//
//                Log.e("node", k + "");
//
//                JsonArray array=new JsonArray();
//
////                users = users + k + ",";
////                array.add(users + k);
////                Log.e(TAG, "doOnSuccess: ", );
////                Log.e("doOnSuccess: ", users);
////                Log.e( "al: ",al+"" );
            }
//            Log.e("k", k + " " + data.getJSONObject("days").getString(k + ""));

// Check the amount if it is 0 or not
            if (data.getJSONObject("days").getString(k + "").equalsIgnoreCase("0")) {
//                expenses_detail.setVisibility(View.GONE);
                holder.set_amount.setVisibility(View.GONE);
            } else if (current_day == k) {
//                expenses_detail.setVisibility(View.VISIBLE);
                holder.set_amount.setVisibility(View.VISIBLE);
//                holder.set_amount.setText("₹ " + data.getJSONObject("days").getString(k + ""));
            }

            holder.day.setText(k + "");
//            Log.e("kkkkk",k+"");

            // Set the progress of days
            float progress = 0;
            if (amount != 0) {
                progress = Float.parseFloat(((Double.parseDouble(data.getJSONObject("days").getString(k + "")) * 100) / amount)+"");
            }

            holder.day_progress.setProgress(progress);

//            Log.e("progress", progress + "");

//            Log.e("Current_day", current_day + "");
            float progress1 = holder.day_progress.getProgress();



//            if (current_day > k) {
//                holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progress_off_color));
//            } else if (current_day == k) {
//                holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progess_color_current));
//            }


            if (i == selectedItem) {
                holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progess_color_current));
                holder.set_amount.setVisibility(View.VISIBLE);
                try {
                    ((MyExpenses)context).amount_type.setText("₹ " + data.getJSONObject("days").getString(i + 1 + ""));
//                    holder.set_amount.setText("₹ " + data.getJSONObject("days").getString(i + 1 + ""));
//                    Log.e("amount", data.getJSONObject("days").getString(i + 1 + "") + "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                holder.set_amount.setVisibility(View.GONE);
                holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progress_off_color));
            }

// Progress to check the progress of Per day
            holder.main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    expenses_detail.setVisibility(View.GONE);

                    Log.e("position",holder.day.getText().toString()+" "+i);



                    position=i;
                    if (selectedItem == i) {
                        holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progress_off_color));
                        selectedItem = -1;
                        expenses_detail.setVisibility(View.GONE);
                        holder.set_amount.setVisibility(View.GONE);
                        ((MyExpenses)context).amount_type.setText("₹ " + "0" );
                    } else {
                        selectedItem = i;
                        try {
                            if (!data.getJSONObject("days").getString((position+1) + "").equalsIgnoreCase("0")) {

                                expenses_detail.setVisibility(View.VISIBLE);

                            }else {


                                expenses_detail.setVisibility(View.GONE);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progess_color_current));

                        notifyDataSetChanged();
                    }
                }
            });
            expenses_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.e("date",(position+1)+" d"+((MyExpenses)context).month_count+"m "+((MyExpenses)context).year+ "y " );
                    try {

                        if (!data.getJSONObject("days").getString((position+1) + "").equalsIgnoreCase("0")) {


                                Intent intent=new Intent(context, MyExpensesDetails.class);
                                intent.putExtra("day",(position+1)+"");
                                intent.putExtra("month",((MyExpenses)context).month_count+"");
                                intent.putExtra("year",((MyExpenses)context).year+"");
                                intent.putExtra("type","day");
                                intent.putExtra("first_date","1-1-2019");
                                intent.putExtra("second_date","7-1-219");
                                context.startActivity(intent);
                    }else {

                            Toast.makeText(context, "No data Found", Toast.LENGTH_SHORT).show();
                        }

                        Log.e("khhh", position + " " + data.getJSONObject("days").getString((position + 1)+""));
                    } catch (JSONException e) {
                        Log.e("eee",e.toString());
                        e.printStackTrace();
                    }


                }
            });

        } catch (JSONException e) {
            Log.e("eeee",e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return current_day;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView day, set_amount;
        RelativeLayout main;
        RoundCornerProgressBar day_progress;
        ArrayList<String> dayslist=new ArrayList<>();

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            main = (RelativeLayout) itemView.findViewById(R.id.main_layout);
            day = (TextView) itemView.findViewById(R.id.day);
            day_progress = (RoundCornerProgressBar) itemView.findViewById(R.id.day_amount_progress);
            set_amount = itemView.findViewById(R.id.set_amount);
        }
    }
}