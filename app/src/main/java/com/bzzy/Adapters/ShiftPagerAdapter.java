package com.bzzy.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.AddBatch;
import com.bzzy.Activities.Suggestion;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.bzzy.Api.ApiCalls.add_batch;
import static com.bzzy.Api.ApiCalls.disable_batch;

public class ShiftPagerAdapter extends PagerAdapter {

    Context context;
    private String time;

    UserSharedPreferences sharedPreferences;
    JSONArray array;
    RequestQueue MyRequestQueue;
    StringRequest MyStringRequest;
    Dialog dialog;

    int[] backgrounds = {R.drawable.shift_card_first,
            R.drawable.shift_background_10,
            R.drawable.shift_screen_3,
            R.drawable.shift_screen_4,
            R.drawable.shift_card_first,
            R.drawable.shift_background_10,
            R.drawable.shift_screen_3,
            R.drawable.shift_screen_4
    };

    int[] text_background = {
            R.drawable.shift_edittext_back,
            R.drawable.shift_back_2,
            R.drawable.shift_back_3,
            R.drawable.shift_back_4,
            R.drawable.shift_edittext_back,
            R.drawable.shift_back_2,
            R.drawable.shift_back_3,
            R.drawable.shift_back_4
    };

    private int from = 0, to = 1;

    public ShiftPagerAdapter(AddBatch addBatch, int[] backgrounds, int[] text_back) {
        this.context = addBatch;
        this.backgrounds = backgrounds;
        this.text_background = text_back;
        sharedPreferences = new UserSharedPreferences(context);
    }

    public ShiftPagerAdapter(AddBatch addBatch, JSONArray array) {
        this.context = addBatch;
        this.array = array;
        sharedPreferences = new UserSharedPreferences(context);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = LayoutInflater.from(context).inflate(R.layout.shift_design, container, false);

        // Initializations
        RelativeLayout main = (RelativeLayout) view.findViewById(R.id.main_layout);
        main.setBackground(context.getResources().getDrawable(backgrounds[position]));


// Add New Batch
        if (array.length()==0) {
            Log.e("in", "if");
            ImageView ok = (ImageView) view.findViewById(R.id.ok_button);
            ImageView saved_btn = (ImageView) view.findViewById(R.id.done);
            ImageView delete_batch = (ImageView) view.findViewById(R.id.delete_batch);

            EditText batch_name = (EditText) view.findViewById(R.id.enter_batch_name);
            TextView  from_time = (TextView) view.findViewById(R.id.timefrom);
            TextView to_time = (TextView) view.findViewById(R.id.timeto);

            EditText total_seats = (EditText) view.findViewById(R.id.total_seats);
            EditText fee = (EditText) view.findViewById(R.id.event_amount);

// Backgrounds of card Texts
            from_time.setBackground(context.getResources().getDrawable(text_background[position]));
            to_time.setBackground(context.getResources().getDrawable(text_background[position]));
            total_seats.setBackground(context.getResources().getDrawable(text_background[position]));


            ((AddBatch) context).add_batch.setVisibility(View.INVISIBLE);

            //  Select Time
            to_time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("Click", "clicking");
//                to_time.setText("");
//                selectTime(to, to_time, from_time.getText().toString().trim());
                    selectTime(to_time);
                }
            });

            from_time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                selectTime(from, from_time, to_time);
                    selectTime(from_time);
                }
            });


// validations and Button to add batch and Add User Api Anonymous method
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("click", "ok");
                    if (batch_name.getText().toString().trim().equalsIgnoreCase("")) {
                        batch_name.setError("Please enter Batch name");
                    } else if (total_seats.getText().toString().trim().equalsIgnoreCase("")) {
                        total_seats.setError("You must provide total seats.");
                    }else if (Integer.parseInt(total_seats.getText().toString().trim()) == 0) {
                        total_seats.setError("You must provide total seats.");
                    } else if (fee.getText().toString().trim().equalsIgnoreCase("")) {
                        fee.setError("Please provide charges");
                    } else if (from_time.getText().toString().trim().equalsIgnoreCase("")) {
                        Toast.makeText(context, "Please select start time", Toast.LENGTH_SHORT).show();
                    } else if (to_time.getText().toString().trim().equalsIgnoreCase("")) {
                        Toast.makeText(context, "Please select end time", Toast.LENGTH_SHORT).show();
                    } else {
                        addBatchAPICall();
                    }
                }

                // Api Call for New Batch
                private void addBatchAPICall() {
                    MyRequestQueue = Volley.newRequestQueue(context);
                    MyStringRequest = new StringRequest(Request.Method.POST, add_batch, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Response", response + " ");
                            //This code is executed if the server responds, whether or not the response contains data.
                            try {
                                JSONObject jObj = new JSONObject(response);
                                String status = jObj.getString("status");
                                if (status.equals("success")) {
                                    // Launch login activity
                                    JSONObject object = jObj.getJSONObject("data");
                                    Log.e("Data in Shift", object + "");
                                    delete_batch.setVisibility(View.VISIBLE);
                                    saved_btn.setVisibility(View.VISIBLE);
                                    ok.setVisibility(View.INVISIBLE);
                                    from_time.setClickable(false);
                                    to_time.setClickable(false);
                                    total_seats.setFocusable(false);
                                    fee.setFocusable(false);
                                    batch_name.setFocusable(false);
//
                                    ((AppCompatActivity)context).finish();
                                    Intent intent=new Intent(context,AddBatch.class);
                                    context.startActivity(intent);
//                                    ((AddBatch)context).getBatchData();


                                    Toast.makeText(context, "Congrats Shift is added successfully.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e1) {
                                Log.e("Exception", e1 + " ");
                                e1.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        //Create an error listener to handle errors appropriately.
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_LONG).show();
                            Log.e("Error", error + "");
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();

                            params.put("id", "");
                            params.put("name", batch_name.getText().toString().trim());
                            params.put("start_time", from_time.getText().toString().trim());
                            params.put("end_time", to_time.getText().toString().trim());
                            params.put("seats", total_seats.getText().toString().trim());
                            params.put("fee", fee.getText().toString().trim());
                            params.put("event_id", sharedPreferences.geteventId());
                            params.put("user_id", sharedPreferences.getuserID());

                            Log.e("getParams: ", params + "");
                            return params;
                        }
                    };
                    MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    MyRequestQueue.add(MyStringRequest);
                }
            });

// Else part for editing Existing batch
        } else {

            JSONObject object= null;
            try {
                object = array.getJSONObject(position);
            } catch (JSONException e) {
                Log.e("eeee",e.toString());
                e.printStackTrace();
            }

            Log.e("in", "else");

            ImageView ok = (ImageView) view.findViewById(R.id.ok_button);
            ImageView  saved_btn = (ImageView) view.findViewById(R.id.done);
            ImageView  delete_batch = (ImageView) view.findViewById(R.id.delete_batch);

            EditText batch_name = (EditText) view.findViewById(R.id.enter_batch_name);
            TextView  from_time = (TextView) view.findViewById(R.id.timefrom);
            TextView to_time = (TextView) view.findViewById(R.id.timeto);

            EditText total_seats = (EditText) view.findViewById(R.id.total_seats);
            EditText fee = (EditText) view.findViewById(R.id.event_amount);

// Backgrounds of card Texts
            from_time.setBackground(context.getResources().getDrawable(text_background[position]));
            to_time.setBackground(context.getResources().getDrawable(text_background[position]));
            total_seats.setBackground(context.getResources().getDrawable(text_background[position]));


            ((AddBatch) context).add_batch.setVisibility(View.VISIBLE);
            ((AddBatch) context).add_batch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, AddBatch.class);
                    intent.putExtra("status",true);
                    context.startActivity(intent);

                    ((AddBatch) context).finish();
                }
            });

//  Select Time
            to_time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("Click", "clicking");
//                to_time.setText("");
//                selectTime(to, to_time, from_time.getText().toString().trim());
                    selectTime(to_time);
                }
            });

            from_time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                selectTime(from, from_time, to_time);
                    selectTime(from_time);
                }
            });


            ok.setVisibility(View.GONE);
            saved_btn.setVisibility(View.VISIBLE);
            delete_batch.setVisibility(View.VISIBLE);

// SetValues of existing batch
            try {
                batch_name.setText(object.getString("name"));
                from_time.setText(object.getString("start_time"));
                to_time.setText(object.getString("end_time"));
                fee.setText(object.getString("fee"));
                total_seats.setText(object.getString("seats"));
            } catch (Exception e) {
                Log.e("exception", e + "");
            }

// validations and Button to add batch and Add User Api Anonymous method
            JSONObject finalObject = object;
            saved_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (batch_name.getText().toString().trim().equalsIgnoreCase("")) {
                        batch_name.setError("Please enter Batch name");
                    } else if (total_seats.getText().toString().trim().equalsIgnoreCase("")) {
                        total_seats.setError("You must provide total seats.");
                    } else if (Integer.parseInt(total_seats.getText().toString().trim()) == 0) {
                        total_seats.setError("You must provide total seats.");
                    } else if (fee.getText().toString().trim().equalsIgnoreCase("")) {
                        fee.setError("Please provide charges");
                    } else if (from_time.getText().toString().trim().equalsIgnoreCase("")) {
                        Toast.makeText(context, "Please select start time", Toast.LENGTH_SHORT).show();
                    } else if (to_time.getText().toString().trim().equalsIgnoreCase("")) {
                        Toast.makeText(context, "Please select end time", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            addBatchAPICall(finalObject.getString("id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                // Api Call for Update Batch
                private void addBatchAPICall(String id) {
                    MyRequestQueue = Volley.newRequestQueue(context);
                    MyStringRequest = new StringRequest(Request.Method.POST, add_batch, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("Response", response + " ");
                            //This code is executed if the server responds, whether or not the response contains data.
                            try {
                                JSONObject jObj = new JSONObject(response);
                                String status = jObj.getString("status");
                                if (status.equals("success")) {
                                    // Launch login activity
                                    JSONObject object = jObj.getJSONObject("data");
                                    Log.e("Data in Shift", object + "");
                                    delete_batch.setVisibility(View.VISIBLE);
                                    saved_btn.setVisibility(View.VISIBLE);
                                    ok.setVisibility(View.INVISIBLE);
                                    from_time.setClickable(false);
                                    to_time.setClickable(false);
                                    total_seats.setFocusable(false);
                                    fee.setFocusable(false);
                                    batch_name.setFocusable(false);

                                    Toast.makeText(context, "Congrats Batch is updated successfully.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e1) {
                                Log.e("Exception", e1 + " ");
                                e1.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        //Create an error listener to handle errors appropriately.
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_LONG).show();
                            Log.e("Error", error + "");
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();

                            params.put("id", id);
                            params.put("name", batch_name.getText().toString().trim());
                            params.put("start_time", from_time.getText().toString().trim());
                            params.put("end_time", to_time.getText().toString().trim());
                            params.put("seats", total_seats.getText().toString().trim());
                            params.put("fee", fee.getText().toString().trim());
                            params.put("event_id", sharedPreferences.geteventId());
                            params.put("user_id", sharedPreferences.getuserID());

                            Log.e("getParams: ", params + "");
                            return params;
                        }
                    };
                    MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    MyRequestQueue.add(MyStringRequest);
                }
            });

            JSONObject finalObject1 = object;
            delete_batch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        OpenDialog(finalObject1.getString("id"),
                                saved_btn,position,from_time,to_time,total_seats,fee,batch_name);
//                        DisableBatch alert = new DisableBatch();
//                        alert.showDialog(((AddBatch) context), finalObject1.getString("id"), saved_btn);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            from_time.setClickable(false);
            to_time.setClickable(false);

            total_seats.setEnabled(false);
            fee.setEnabled(false);
            batch_name.setEnabled(false);
            saved_btn.setClickable(false);
        }

        container.addView(view);
        return view;
    }

    //Disable Dialog
    public void OpenDialog(String id, ImageView btn, int position, TextView from_time, TextView to_time, EditText total_seats, EditText fee, EditText batch_name) {
            dialog = new Dialog(context);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.delete_batch_popup);

            ImageView edit = (ImageView) dialog.findViewById(R.id.edit_btn);
            ImageView delete_batch = (ImageView) dialog.findViewById(R.id.delete_btn);

            Log.e("Batch_id", id + "");
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("Batch_idedit", id + "");

                    from_time.setClickable(true);

                    Log.e("position",position+"");

                    Log.e("iasdbfj",batch_name.getText().toString());
                    to_time.setClickable(true);
                    total_seats.setEnabled(true);
                    fee.setEnabled(true);
                    batch_name.setEnabled(true);
                    btn.setClickable(true);





                    dialog.cancel();
                }
            });

            delete_batch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disableBatch(id);
                }
            });

            dialog.show();
        }


    // Method to set Time from
    private void selectTime(TextView textView) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                datetime.set(Calendar.MINUTE, selectedMinute);

                if (selectedHour >= 00 && selectedHour < 12) {
                    time = String.format("%02d:%02d", selectedHour, selectedMinute) + " AM";
                } else {
                    if (selectedHour == 12) {
                        time = String.format("%02d:%02d", selectedHour, selectedMinute) + " PM";
                    } else {
                        selectedHour = selectedHour - 12;
                        time = String.format("%02d:%02d", selectedHour, selectedMinute) + " PM";
                    }
                }

                Log.e("date time",time);
                textView.setText(time);
            }
        }, hour, minute, false);
        mTimePicker.show();
    }


    // Check past time
    private int checkTimings(String time, String endtime) {

        Log.e("start", time);
        Log.e("end", endtime);
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        Log.e("curr_date", date);
        int status = 0;

        String pattern = "hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
//        SimpleDateFormat sdf1 = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            Log.e("Date1", date1 + "");
            Log.e("Date2", date2 + "");
            if (date1.before(date2)) {
//                Toast.makeText(context, "true", Toast.LENGTH_SHORT).show();
                status = 1;
            } else {
//                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                Log.e("checkTimings: ", "false");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {

        if (array.length()>0) {
            return array.length();
        } else {
            return 8;
        }

    }

    // Api to disable batch
    private void disableBatch(String id) {
        MyRequestQueue = Volley.newRequestQueue(context);
        MyStringRequest = new StringRequest(Request.Method.POST, disable_batch, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        dialog.dismiss();
                        ((AddBatch) context).finish();
                        Toast.makeText(context, "Batch is deleted successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Sorry! batch no found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("batch_id", id);

                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

}