package com.bzzy.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MessagePreviousAdapter extends RecyclerView.Adapter<MessagePreviousAdapter.ViewHolder> implements ApiCalls {
    Context context;
    JSONArray user_array;
    JSONObject notification_data;
    Dialog dialog;
    UserSharedPreferences sharedPreferences;

    public MessagePreviousAdapter(Context context) {
        this.context = context;
    }

    public MessagePreviousAdapter(Context context,  JSONArray data) {
        this.context = context;
        this.user_array = data;
        sharedPreferences = new UserSharedPreferences(context);
    }

    @Override
    public MessagePreviousAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.previous_message, viewGroup, false);
        return new MessagePreviousAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MessagePreviousAdapter.ViewHolder holder, int i) {

        try {
            JSONObject user_data = user_array.getJSONObject(i);

            holder.previous_message.setText(user_data.getString("message"));
            holder.date.setText(user_data.getString("date"));
            holder.domain_name.setText(user_data.getString("domain_name"));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return user_array.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView previous_message, domain_name, date;
        ImageView image_message;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();

            previous_message = (TextView) itemView.findViewById(R.id.name_user);
            domain_name = (TextView) itemView.findViewById(R.id.category);
            date= (TextView) itemView.findViewById(R.id.due_dae);

        }

    }

}