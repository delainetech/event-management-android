package com.bzzy.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bzzy.Activities.AddBatch;
import com.bzzy.Activities.AdminProfile;
import com.bzzy.Activities.Dashboard;
import com.bzzy.Activities.ExpenditureActivity;
import com.bzzy.Activities.UserProfile;
import com.bzzy.Activities.ViewExpenditureBills;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AppSearchAdapter extends RecyclerView.Adapter<AppSearchAdapter.ViewHolder> implements ApiCalls {
    Context context;
    JSONArray data;
    Dialog dialog;
    UserSharedPreferences sharedPreferences;

    int year;

    public AppSearchAdapter(Context context) {
        this.context = context;
    }

    public AppSearchAdapter(Context context, JSONArray array) {
        this.context = context;
        this.data = array;
        sharedPreferences = new UserSharedPreferences(context);
    }

    @Override
    public AppSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_item, viewGroup, false);
        return new AppSearchAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AppSearchAdapter.ViewHolder holder, int i) {

        try {
            JSONObject object = data.getJSONObject(i);

            if (object.getString("type").equalsIgnoreCase("batch")) {
                holder.search_item.setText(data.getJSONObject(i).getString("name"));
                holder.type.setText("in "+data.getJSONObject(i).getString("type"));
                holder.search_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(context, AddBatch.class);
                        intent.putExtra("status",false);
                        context.startActivity(intent);
                        ((Dashboard)context).search_recycler.setVisibility(View.GONE);
                        ((Dashboard)context).searchView.setQuery("",false);
                        ((Dashboard)context).searchView.clearFocus();
                    }
                });

            }else if (object.getString("type").equalsIgnoreCase("batch_users")){
                holder.search_item.setText(data.getJSONObject(i).getString("student_name"));
                holder.type.setText("in "+"users");
                holder.search_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, UserProfile.class);
                        try {
                            intent.putExtra("user_id", data.getJSONObject(i).getString("id"));
                            intent.putExtra("batch_id", data.getJSONObject(i).getString("batch_id"));
                            context.startActivity(intent);
                            ((Dashboard)context).search_recycler.setVisibility(View.GONE);
                            ((Dashboard)context).searchView.setQuery("",false);
                            ((Dashboard)context).searchView.clearFocus();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });


            }else if (object.getString("type").equalsIgnoreCase("partner")){
                holder.search_item.setText(data.getJSONObject(i).getString("name"));
                holder.type.setText("in "+ "Partner Profile");
                holder.search_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(context, AdminProfile.class);
                        context.startActivity(intent);
                        ((Dashboard)context).search_recycler.setVisibility(View.GONE);
                        ((Dashboard)context).searchView.setQuery("",false);
                        ((Dashboard)context).searchView.clearFocus();
                    }
                });


            }else if (object.getString("type").equalsIgnoreCase("expenditures")){

                if(data.getJSONObject(i).getString("sub_type").equalsIgnoreCase("")){
                    holder.search_item.setText(data.getJSONObject(i).getString("description"));
                }else
                holder.search_item.setText(data.getJSONObject(i).getString("sub_type"));


                holder.type.setText("in "+ "Expenditure");

                holder.search_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(context, ExpenditureActivity.class);
                        context.startActivity(intent);
                        ((Dashboard)context).search_recycler.setVisibility(View.GONE);
                        ((Dashboard)context).searchView.setQuery("",false);
                        ((Dashboard)context).searchView.clearFocus();
                    }
                });

            }else if (object.getString("type").equalsIgnoreCase("category")){
                holder.search_item.setText(data.getJSONObject(i).getString("name"));

                holder.type.setText("in "+ "Categories");

                holder.search_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(context, ViewExpenditureBills.class);
                        context.startActivity(intent);
                        ((Dashboard)context).search_recycler.setVisibility(View.GONE);
                        ((Dashboard)context).searchView.setQuery("",false);
                        ((Dashboard)context).searchView.clearFocus();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView search_item, type;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            search_item = (TextView) itemView.findViewById(R.id.search_text);
            type=(TextView) itemView.findViewById(R.id.type);

        }
    }
}