package com.bzzy.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.bzzy.Activities.MyEarnings;
import com.bzzy.Activities.MyEarningsDetails;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;

import org.json.JSONException;
import org.json.JSONObject;

public class WeekGraphAdapter2 extends RecyclerView.Adapter<WeekGraphAdapter2.ViewHolder> implements ApiCalls {

    Context context;
    int amount;
    int k, current_day;
    JSONObject data;
    int position;
    private int selectedItem = -1;
    CardView earning_detail;
    public WeekGraphAdapter2(Context context, JSONObject data, int current_day,
                             int amount, CardView earning_detail) {
        this.context = context;
        this.current_day = current_day;
        this.data = data;
        this.amount = amount;
        this.earning_detail=earning_detail;
    }


    @Override
    public WeekGraphAdapter2.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.week_bar, viewGroup, false);
        return new WeekGraphAdapter2.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final WeekGraphAdapter2.ViewHolder holder, int i) {

        k = i + 1;
//        Log.e("data", data + "");
        try {
            data.getJSONObject("days").getString(k + "");
//            Log.e("k", k + " " + data.getJSONObject("days").getString(k + ""));

// Check the amount if it is 0 or not
            if (data.getJSONObject("days").getString(k + "").equalsIgnoreCase("0")) {
                holder.set_amount.setVisibility(View.GONE);
            } else if (current_day == k) {
                holder.set_amount.setVisibility(View.VISIBLE);
//                holder.set_amount.setText("₹ " + data.getJSONObject("days").getString(k + ""));
            }

            holder.day.setText(k + "");

            // Set the progress of days
            float progress = 0;
            if (amount != 0) {
                progress = (Integer.parseInt(data.getJSONObject("days").getString(k + "")) * 100) / amount;
            }



            holder.day_progress.setProgress(progress);

            Log.e("progress", progress + "");

            Log.e("Current_day", current_day + "");
            float progress1 = holder.day_progress.getProgress();

//            if (current_day > k) {
//                holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progress_off_color));
//            } else if (current_day == k) {
//                holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progess_color_current));
//            }


            if (i == selectedItem) {
//                earning_detail.setVisibility(View.VISIBLE);
                holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progess_color_current));
                holder.set_amount.setVisibility(View.VISIBLE);
                ((MyEarnings)context).amount_type.setText("₹ " + data.getJSONObject("days").getString(i + 1 + ""));
                try {
//                    holder.set_amount.setText("₹ " + data.getJSONObject("days").getString(i + 1 + ""));
                    Log.e("amount", data.getJSONObject("days").getString(i + 1 + "") + "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
//                earning_detail.setVisibility(View.GONE);
                holder.set_amount.setVisibility(View.GONE);
                holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progress_off_color));
            }


// Progress to check the progress of Per day
            holder.main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    position=i;
                    if (selectedItem == i) {
                        earning_detail.setVisibility(View.GONE);
                        holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progress_off_color));
                        selectedItem = -1;
//                        holder.set_amount.setVisibility(View.GONE);
                        ((MyEarnings)context).amount_type.setText("₹ " + "0" );
                    } else {
                        selectedItem = i;
                        earning_detail.setVisibility(View.VISIBLE);
                        holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progess_color_current));
                        try {
                            if (!data.getJSONObject("days").getString((position+1) + "").equalsIgnoreCase("0")) {

                                earning_detail.setVisibility(View.VISIBLE);

                            }else {


                                earning_detail.setVisibility(View.GONE);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        notifyDataSetChanged();
                    }
                }
            });

            earning_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.e("date",(position+1)+" d"+((MyEarnings)context).month_count+"m "+((MyEarnings)context).year+ "y " );
                    try {

                        if (!data.getJSONObject("days").getString((position+1) + "").equalsIgnoreCase("0")) {


                            Intent intent=new Intent(context, MyEarningsDetails.class);
                            intent.putExtra("day",(position+1)+"");
                            intent.putExtra("month",((MyEarnings)context).month_count+"");
                            intent.putExtra("year",((MyEarnings)context).year+"");
                            intent.putExtra("type","day");
                            intent.putExtra("first_date","1-1-2019");
                            intent.putExtra("second_date","7-1-219");
                            context.startActivity(intent);
                        }else {

                            Toast.makeText(context, "No data Found", Toast.LENGTH_SHORT).show();
                        }

                        Log.e("khhh", position + " " + data.getJSONObject("days").getString((position + 1)+""));
                    } catch (JSONException e) {
                        Log.e("eee",e.toString());
                        e.printStackTrace();
                    }
//                    Intent intent=new Intent(context, MyEarningsDetails.class);
//                    intent.putExtra("day",k+"");
//                    context.startActivity(intent);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return current_day;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView day, set_amount;
        RelativeLayout main;
        RoundCornerProgressBar day_progress;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            main = (RelativeLayout) itemView.findViewById(R.id.main_layout);
            day = (TextView) itemView.findViewById(R.id.day);
            day_progress = (RoundCornerProgressBar) itemView.findViewById(R.id.day_amount_progress);
            set_amount = itemView.findViewById(R.id.set_amount);
        }
    }
}