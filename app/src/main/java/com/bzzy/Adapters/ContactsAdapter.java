package com.bzzy.Adapters;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bzzy.Activities.BatchListActivity;
import com.bzzy.Activities.SelectMemberActivity;
import com.bzzy.Activities.UserProfile;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

//import com.eventmanagementtool.Models.ClickInterface;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> implements ApiCalls {
    Context context;
    JSONArray data;
//    private Spinner renew_spinner;
    private ArrayList<String> renew_arraylist = new ArrayList<>();
    private String select_month;
    String month_id="";
    int feess,amount;
    StringRequest MyStringRequest;
    RequestQueue MyRequestQueue;
    EditText fee;
    TextView total_fee,tv_datefrom,tv_dateto;
    EditText registration_fee;
    ImageView renew_user;
    Dialog dialog;
    UserSharedPreferences sharedPreferences;
    int last_pos;
    ProgressBar progressBar;
    ProgressDialog progressDialog;
    boolean isclick;
    String date,st_datefrom="",st_dateto="";

    int month1,day1,year1;
    private int selectedItem = -1;
    //    private ClickInterface clickInterface;
    SparseBooleanArray checkedItems = new SparseBooleanArray();

    public ContactsAdapter(Context context, JSONArray array, String date, boolean isclick) {
        this.context = context;
        this.data = array;
        this.date = date;
        sharedPreferences = new UserSharedPreferences(context);
        this.isclick=isclick;
        Calendar c=Calendar.getInstance();
        month1=(c.get(Calendar.MONTH)+1);
        day1=c.get(Calendar.DAY_OF_MONTH);
        year1=c.get(Calendar.YEAR);
    }

    @Override
    public ContactsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contact_item, viewGroup, false);
            return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ContactsAdapter.ViewHolder holder, int i) {
//        ModelTest modelTest = modelTestList.get(i);

            try {
                JSONObject object = data.getJSONObject(i);
                Log.e("profile pic",batch_images + data.getJSONObject(i).getString("image"));

// User name
                holder.user_name.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                holder.user_name.setText(data.getJSONObject(i).getString("student_name"));
// User images
                if (data.getJSONObject(i).getString("image").length() > 4) {

                    Log.e("profile pic",batch_images + data.getJSONObject(i).getString("image"));
                    Glide.with(context)
                            .load(batch_images + data.getJSONObject(i).getString("image"))
                            .centerCrop()
                            .placeholder(R.drawable.error_circle)
                            .into(holder.userimage);
                    Glide.with(context)
                            .load(batch_images + data.getJSONObject(i).getString("image"))
                            .centerCrop()
                            .placeholder(R.drawable.error_circle)
                            .into(holder.imageInside);
//                    Picasso.get().load(batch_images + data.getJSONObject(i).getString("image")).fit().error(R.drawable.error_circle)
//                            .into(holder.userimage);
//                    Picasso.get().load(batch_images + data.getJSONObject(i).getString("image")).fit().error(R.drawable.error_circle)
//                            .into(holder.imageInside);
                }else {
                    holder.userimage.setImageDrawable(context.getResources().getDrawable(R.drawable.error_circle));
                    holder.imageInside.setImageDrawable(context.getResources().getDrawable(R.drawable.error_circle));
                }

                holder.due_date.setText("Next due date :" + " " + data.getJSONObject(i).getString("due_date"));

// Click user to expand list

                if (i == selectedItem) {
                    holder.expandableLayout.toggle(true);
                }else {
                    holder.expandableLayout.collapse(true);
                }

                holder.userimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (selectedItem == i) {
                            holder.expandableLayout.collapse(true);
                            selectedItem=-1;
                        } else {
                            selectedItem = i;
                            notifyDataSetChanged();
                            holder.expandableLayout.toggle(true);
                        }
                    }
                });

                holder.expandableLayout.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
                    @Override
                    public void onExpansionUpdate(float expansionFraction, int state) {
//                    Log.e("ExpandableLayout", "State: " + state);
                        holder.userimage.setRotation(expansionFraction);

                        try {
                            if (Integer.parseInt(data.getJSONObject(i).getString("due_status")) == 0) {
                                if (state == 2) {
                                    holder.due_status.setVisibility(View.GONE);
                                } else if (state == 0) {
                                    holder.due_status.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

// Click to open user profile

                holder.user_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            holder.expandableLayout.collapse(true);
                            if (!isclick) {
                                Intent intent = new Intent(context, UserProfile.class);
                                intent.putExtra("user_id", data.getJSONObject(i).getString("id"));
                                intent.putExtra("batch_id", data.getJSONObject(i).getString("batch_id"));
                                context.startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                holder.main.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            holder.expandableLayout.collapse(true);
                            if (!isclick) {
                                Intent intent = new Intent(context, UserProfile.class);
                                intent.putExtra("user_id", data.getJSONObject(i).getString("id"));
                                intent.putExtra("batch_id", data.getJSONObject(i).getString("batch_id"));
                                context.startActivity(intent);
//                            ((SelectMemberActivity) context).finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

// Renew User
                holder.renew_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        renew_arraylist.clear();

                        if (!isclick) {
                        try {
                            getRenewDialog alert = new getRenewDialog();
                            alert.showDialog((SelectMemberActivity) context, data.getJSONObject(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        holder.expandableLayout.collapse();
                    }else {
                            try {
                                getRenewDialog alert = new getRenewDialog();
                                alert.showDialog((BatchListActivity) context, data.getJSONObject(i));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }
                });

// Check fees status
                if (Integer.parseInt(data.getJSONObject(i).getString("due_status")) == 0) {
                    holder.due_status.setVisibility(View.VISIBLE);
                }else
                    holder.due_status.setVisibility(View.GONE);
// To call user
                holder.call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.expandableLayout.collapse();
                        Uri uri = null;
                        try {
                            uri = Uri.parse("tel:" + object.getString("student_phone"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent DialIntent = new Intent(Intent.ACTION_DIAL, uri);
                        DialIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(DialIntent);
                    }
                });

// To open sms
                holder.message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            holder.expandableLayout.collapse();
                            Uri sms_uri = Uri.parse("smsto:" + object.getString("student_phone"));
                            Intent intent = new Intent(Intent.ACTION_SENDTO, sms_uri);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
//                        intent.setType("vnd.android-dir/mms-sms");
                            intent.putExtra("address", object.getString("student_phone"));
                            intent.putExtra("sms_body", " ");
                            context.startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        public ImageView call, renew_user, message;
        public ImageView userimage, imageInside, due_status;
        public ExpandableLayout expandableLayout;
        public TextView user_name, due_date;
        ConstraintLayout main;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            main = (ConstraintLayout) itemView.findViewById(R.id.main_layout);
            userimage = (ImageView) itemView.findViewById(R.id.image_user);
            imageInside = (ImageView) itemView.findViewById(R.id.image_user1);
            call = (ImageView) itemView.findViewById(R.id.call);
            expandableLayout = itemView.findViewById(R.id.expandable_layout);
            user_name = (TextView) itemView.findViewById(R.id.name_user);
            due_date = (TextView) itemView.findViewById(R.id.due_dae);
            due_status = (ImageView) itemView.findViewById(R.id.trail_status);
            renew_user = (ImageView) itemView.findViewById(R.id.renew_user);
            message = (ImageView) itemView.findViewById(R.id.message);
        }

    }

    // GetData Dialog
    public class getRenewDialog {
        void showDialog(Activity activity, JSONObject object) throws JSONException {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.renew_dialog);

            renew_arraylist.clear();

            Log.e("user_data", object + "");

//            int time_differ=  checkTimings(date, object.getString("due_date"));
//            Log.e("status_time_differ", time_differ+"");

            ImageView profile_pic = dialog.findViewById(R.id.user_image);
            Picasso.get().load(batch_images + object.getString("image")).error(R.drawable.error_circle).into(profile_pic);
            fee = (EditText) dialog.findViewById(R.id.enter_fee);

//            fee.setText(object.getString("amount"));

            total_fee = (TextView) dialog.findViewById(R.id.total_fee);
            registration_fee = (EditText) dialog.findViewById(R.id.registration_fee);

            tv_datefrom=(TextView)dialog.findViewById(R.id.tv_from);
            tv_dateto=(TextView)dialog.findViewById(R.id.tv_to);

            progressBar = dialog.findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.GONE);
            renew_user = (ImageView) dialog.findViewById(R.id.renew_user);

            st_datefrom=object.getString("due_date");
            tv_datefrom.setText(convertDate(object.getString("due_date")+" 00:00:00","dd-MMM-yyyy"));

            tv_dateto.setHint("dd-mm-yyyy");
//            renew_spinner = (Spinner) dialog.findViewById(R.id.month_spinner);

            // Create an ArrayAdapter that will contain all list items
//            ArrayAdapter<String> adapter;

//            String[] menuArray = context.getResources().getStringArray(R.array.Renew_array);
//
//            for (int k = 0; k < menuArray.length; k++) {
//                renew_arraylist.add(menuArray[k]);
//                adapter = new ArrayAdapter<>(context, R.layout.expense_spinner, renew_arraylist);
//                renew_spinner.setAdapter(adapter);
//            }

// Condition for dates whether it is after or before
            try {
                Log.e("date", date + "");
                Log.e("date_end", object.getString("due_date") + "");

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Log.e("Dates", sdf.parse(date).equals(sdf.parse(object.getString("due_date"))) + "");

                if (sdf.parse(date).equals(sdf.parse(object.getString("due_date")))) {
                    registration_fee.setText("0");
                } else if (sdf.parse(date).after(sdf.parse(object.getString("due_date")))) {
                    registration_fee.setText("50");
                } else if (sdf.parse(date).before(sdf.parse(object.getString("due_date")))) {
                    registration_fee.setText("0");
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


            tv_datefrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDate(tv_datefrom,day1,month1,year1,true);
                }
            });

            tv_dateto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDate(tv_dateto,day1,month1,year1,false);
                }
            });

// Get spinner value

//            renew_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                    select_month = String.valueOf(renew_spinner.getSelectedItem());
//
//                    if (select_month.equalsIgnoreCase("1 Month")) {
//                        if (fee.getText().toString().equalsIgnoreCase("")) {
//                            feess = 0;
//                        } else {
//                            feess = Integer.valueOf(fee.getText().toString().trim());
//                            Log.e("feees", feess + "");
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                            month_id = String.valueOf(renew_spinner.getSelectedItemId() + 1);
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
//                    } else if (select_month.equalsIgnoreCase("Quarterly")) {
//                        if (fee.getText().toString().equalsIgnoreCase("")) {
//                            feess = 0;
//                        } else {
//                            feess = Integer.valueOf(fee.getText().toString().trim());
//                            Log.e("feees", feess + "");
//                            feess = feess * 3;
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                            month_id = String.valueOf(renew_spinner.getSelectedItemId() + 2);
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
//                    } else if (select_month.equalsIgnoreCase("Half-yearly")) {
//                        if (fee.getText().toString().equalsIgnoreCase("")) {
//                            feess = 0;
//                        } else {
//                            feess = Integer.valueOf(fee.getText().toString().trim());
//                            Log.e("feees", feess + "");
//                            feess = feess * 6;
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                            month_id = String.valueOf(renew_spinner.getSelectedItemId() + 4);
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
//                    } else if (select_month.equalsIgnoreCase("Yearly")) {
//                        if (fee.getText().toString().equalsIgnoreCase("")) {
//                            feess = 0;
//                        } else {
//                            feess = Integer.valueOf(fee.getText().toString().trim());
//                            Log.e("feees", feess + "");
//                            feess = feess * 12;
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                            month_id = String.valueOf(renew_spinner.getSelectedItemId() + 9);
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
//                    }
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> adapterView) {
//                    Toast.makeText(context, "Nothing selected", Toast.LENGTH_SHORT).show();
//                }
//            });

// Edit Text TextChange Listener

            fee.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

// For 1 month Renew
//                    if (select_month.equalsIgnoreCase("1 Month")) {
//                        if ("".equalsIgnoreCase(String.valueOf(s))) {
//                            feess = 0 + Integer.parseInt(registration_fee.getText().toString().trim());
//                            Log.e("value of s", s + "");
//                        } else {
//                            feess = Integer.valueOf(String.valueOf(s));
//                            Log.e("feees", feess + "");
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
////// For Quaterly Renew
//                    } else if (select_month.equalsIgnoreCase("Quarterly")) {
//                        if ("".equalsIgnoreCase(String.valueOf(s))) {
//                            feess = 0 + Integer.parseInt(registration_fee.getText().toString().trim());
//                            Log.e("value of s", s + "");
//                        } else {
//                            feess = Integer.valueOf(String.valueOf(s));
////                             feess = Integer.parseInt(registration_fee.getText().toString().trim()) + Integer.parseInt(fee.getText().toString());
//                            Log.e("feees", feess + "");
//                            feess = feess * 3;
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
//// For Half-yearly Renew
//                    } else if (select_month.equalsIgnoreCase("Half-yearly")) {
//                        if ("".equalsIgnoreCase(String.valueOf(s))) {
//                            feess = 0 + Integer.parseInt(registration_fee.getText().toString().trim());
//                            Log.e("value of s", s + "");
//                        } else {
//                            feess = Integer.valueOf(String.valueOf(s));
////                             feess = Integer.parseInt(registration_fee.getText().toString().trim()) + Integer.parseInt(fee.getText().toString());
//                            Log.e("feees", feess + "");
//                            feess = feess * 6;
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
//// For yearly Renew
//                    } else if (select_month.equalsIgnoreCase("Yearly")) {

                        if ("".equalsIgnoreCase(String.valueOf(s))) {
                           amount= feess = 0 + Integer.parseInt(registration_fee.getText().toString().trim());
                            Log.e("value of s", s + "");
                        } else {
//
                            amount=feess = Integer.valueOf(String.valueOf(s));
//                             feess = Integer.parseInt(registration_fee.getText().toString().trim()) + Integer.parseInt(fee.getText().toString());
                            Log.e("feees", feess + "");
//                            feess = feess * 12;
                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
                        }
                        total_fee.setText("Total Fees ₹ " + feess);
//                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            fee.setText(object.getString("amount"));

            renew_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fee.getText().toString().trim().equalsIgnoreCase("")) {
                        fee.setError("Please enter renew amount.");
                    } else if(st_datefrom.length()>0&st_dateto.length()>0)
                        renewUser(object);
                    else
                        Toast.makeText(context,"Please select date",Toast.LENGTH_SHORT).show();
                }
            });
            dialog.show();
        }
    }

    public String addMonth(String d ){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//        String d = "2000-01-30";
        Date date= null;
        try {
            date = new Date(sdf.parse(d).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        date.setMonth(date.getMonth() + 1);

        DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");

        st_dateto=dateFormat1.format(date);

        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

        return dateFormat.format(date);
//        Log.e("dddddd",date+" "+strDate+" "+date.getDate()+" "+date.getYear()+" "+date.getMonth()+" "+date.getDay());
    }

    public void getDate(final TextView et, final int day, int month, int year,boolean b){

        DatePickerDialog datePickerDialog =new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String d=dayOfMonth+"-"+(month+1)+"-"+year+" 00:00:00";

                    if(b) {

                        day1 = dayOfMonth;
                        month1 = month + 1;
                        year1 = year;

                        st_datefrom=convertDate(d,"dd-MM-yyyy");
                    }
                    else
                    st_dateto=convertDate(d,"dd-MM-yyyy");

//                    tv_to.setText(utl.convertDate(d,"dd MMM-yyyy"));

                et.setText(convertDate(d,"dd-MMM-yyyy"));
            }
        },year,month-1,day);

        if(b){

        }
//            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        else{

//            String myDate = year+"/"+month+"/"+day;

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

            Date date = null;
            try {
                date = sdf.parse(st_datefrom);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long millis = date.getTime();

            datePickerDialog.getDatePicker().setMinDate(millis);
        }

        datePickerDialog.show();
    }

    private void renewUser(JSONObject obj) {
        renew_user.setClickable(false);
        renew_user.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        MyRequestQueue = Volley.newRequestQueue(context);
        MyStringRequest = new StringRequest(Request.Method.POST, renew_student, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {

                        // Launch login activity
                        progressBar.setVisibility(View.GONE);
                        ((SelectMemberActivity) context).getMemberList("","");
                        dialog.dismiss();

                        Toast.makeText(context,jObj.getString("message"), Toast.LENGTH_SHORT).show();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(context, jObj.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                try {
//                    int amount = Integer.parseInt(fee.getText().toString().trim()) * Integer.parseInt(month_id);
                    params.put("user_id", sharedPreferences.getuserID());
                    params.put("batch_id", obj.getString("batch_id"));
                    params.put("event_id", sharedPreferences.geteventId());
                    params.put("amount",  amount+"");
                    params.put("student_id", obj.getString("id"));
                    params.put("months", month_id+"");
                    params.put("date", date);
                    params.put("from",st_datefrom);
                    params.put("to",st_dateto);
                    params.put("late_fee", registration_fee.getText().toString().trim() + "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public String convertDate(String dt, String ft){

//        log("daaaaa",dt+"");
        if(dt.equalsIgnoreCase(null)|dt.equalsIgnoreCase("null")|dt.equalsIgnoreCase(""))
            return "";

        Date date1 = null;
        try {
            date1=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat formatter = new SimpleDateFormat(ft);
//        Date date = new Date();

        return formatter.format(date1);

    }

}