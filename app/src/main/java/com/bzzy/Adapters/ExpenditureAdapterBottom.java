package com.bzzy.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.AddBatch;
import com.bzzy.Activities.AdminProfile;
import com.bzzy.Activities.AllMessage;
import com.bzzy.Activities.Attendence;
import com.bzzy.Activities.Dashboard;
import com.bzzy.Activities.ViewExpenditureBills;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.bzzy.Api.ApiCalls.getBatchList;

public class ExpenditureAdapterBottom extends RecyclerView.Adapter<ExpenditureAdapterBottom.ViewHolder> {
    Context context;
    ArrayList<String> productimages = new ArrayList<>();
    String ima;
    int[] backgrounds;
    JSONObject object;
        UserSharedPreferences sharedPreferences;
    JSONArray array;


    public ExpenditureAdapterBottom(Context context, int[] backgrounds, JSONObject object) {
        this.context = context;
        this.backgrounds = backgrounds;
        this.object = object;
        sharedPreferences=new UserSharedPreferences(context);
        array=new JSONArray();
        getBatchData();
    }

    @NonNull
    @Override
    public ExpenditureAdapterBottom.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.static_exp_dataitem, parent, false);
        return new ExpenditureAdapterBottom.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ExpenditureAdapterBottom.ViewHolder holder, final int i) {
        holder.main.setBackground(context.getResources().getDrawable(backgrounds[i]));
        // To show Pending approvals
        holder.approvals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdminProfile.class);
                context.startActivity(intent);
            }
        });
        // For attendence
        holder.attendence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Attendence.class);
                context.startActivity(intent);

            }
        });

        // View Expenditure List
        holder.expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ViewExpenditureBills.class);
                context.startActivity(intent);

            }
        });

        // My Account Details
        holder.my_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdminProfile.class);
                context.startActivity(intent);

            }
        });

        //add batch
        holder.add_batch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, AddBatch.class);
                intent.putExtra("array",array+"");
                intent.putExtra("status",false);
                context.startActivity(intent);
            }
        });

        // Message
        holder.message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AllMessage.class);
                context.startActivity(intent);
//         Intent intent = new Intent(Intent.ACTION_MAIN);
//         intent.addCategory(Intent.CATEGORY_APP_MESSAGING);
//         context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView attendence, message, my_account, approvals, expense,add_batch;
        RelativeLayout main;

        public ViewHolder(View itemView) {
            super(itemView);
            main = (RelativeLayout) itemView.findViewById(R.id.main_layout);
            attendence = itemView.findViewById(R.id.attendance);
            message = itemView.findViewById(R.id.message);
            my_account = itemView.findViewById(R.id.my_account);
            approvals = itemView.findViewById(R.id.approvals);
            expense=itemView.findViewById(R.id.expense);
            add_batch=itemView.findViewById(R.id.add_batch);
        }
    }

    public void getBatchData() {


        RequestQueue MyRequestQueue = Volley.newRequestQueue(context);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, getBatchList, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response BatchList", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        array = jObj.getJSONArray("data");
                        Log.e("GetList array  ", array + "");


                    } else {

                    }
                } catch (JSONException e1) {

                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(Dashboard.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id", sharedPreferences.geteventId());
                Log.e("getParams: ", params + "");

                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }
}
