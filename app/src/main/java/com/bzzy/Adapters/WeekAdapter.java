package com.bzzy.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.bzzy.Activities.MyEarnings;
import com.bzzy.Activities.MyEarningsDetails;
import com.bzzy.Activities.MyExpenses;
import com.bzzy.Activities.MyExpensesDetails;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WeekAdapter extends RecyclerView.Adapter<WeekAdapter.ViewHolder> implements ApiCalls {

    Context context;
    Double amount;
    int k;
    JSONObject data;
    private int selectedItem = -1;
    ArrayList<String> header_week;
    ArrayList<String> weekly_amt;
    int status;
    String first_date="",secound_date="";
    int position;
    boolean isearning;
    CardView expenses_detail;
    public  WeekAdapter(Context context, JSONObject data, Double amount,
                        ArrayList<String> string, ArrayList<String> header, int k,
                        CardView expenses_detail, boolean isearning) {
        this.context = context;
        this.data = data;
        this.expenses_detail=expenses_detail;
        this.amount = amount;
        this.weekly_amt = string;
        this.header_week = header;
        this.status=k;
        this.isearning=isearning;
    }


    @Override
    public WeekAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.week_days, viewGroup, false);
        return new WeekAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final WeekAdapter.ViewHolder holder, int i) {
        k=i+1;

        if (k > getItemCount()) {
            holder.set_amount.setVisibility(View.GONE);
        } else if (k == getItemCount()) {
            holder.set_amount.setVisibility(View.VISIBLE);
//            holder.set_amount.setText("₹ " + weekly_amt.get(i)+"");
        }

// Date String to set
        String currentString = header_week.get(i);
        String[] separated = currentString.split(" to ");

        String first=separated[0]; // this will contain first week
        String second= separated[1]; //  this will contain 2nd week
        String [] month_separated=second.split("/");

        String month_name= month_separated[1];
        Log.e("month_separted",first+" "+second+" "+month_name);
        Log.e("secnd date",second.substring(0, 2).trim()+" // "+month_separated[0]+" // "+month_separated[1]);
        holder.day.setText(first.substring(0, 2).trim()+"-"+second.substring(0, 2).trim());
        holder.month.setText(month_name.substring(0,3)+"");

        double progress = 0;
        if (amount != 0) {
            progress = (Double.parseDouble(weekly_amt.get(i)) * 100) / amount;
        }
            Log.e("progress",progress+"");
        holder.day_progress.setProgress(Float.parseFloat(progress+""));
        holder.day_progress.getProgress();


        if (i == selectedItem) {
            holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progess_color_current));
            holder.set_amount.setVisibility(View.VISIBLE);
            try {
                if (status==1){
                    ((MyEarnings)context).amount_type.setText("₹ " + weekly_amt.get(i) );
                }else if (status==0){
                    ((MyExpenses)context).amount_type.setText("₹ " + weekly_amt.get(i) );
                }

//                holder.set_amount.setText("₹ " + weekly_amt.get(i) );
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            holder.set_amount.setVisibility(View.GONE);
            // Set Progress color
//            if (k > getItemCount()) {
//                holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progress_off_color));
//            } else if (k== getItemCount()) {
//                holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progess_color_current));
//            }
        }

// click graph to see the amount of month
        holder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                position=i;
                if (selectedItem == i) {
                    first_date="";
                    secound_date="";
                    holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progress_off_color));
                    selectedItem = -1;
//                    holder.set_amount.setVisibility(View.GONE);
                    if (status==1){
                        ((MyEarnings)context).amount_type.setText("₹ " + "0" );
                    }else if (status==0){
                        ((MyExpenses)context).amount_type.setText("₹ " + "0" );
                    }
                    expenses_detail.setVisibility(View.GONE);
                } else {

                    selectedItem = i;
                    first_date=first;
                    secound_date=month_separated[0];
                    expenses_detail.setVisibility(View.VISIBLE);
                    holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progess_color_current));

                    notifyDataSetChanged();
                }
            }
        });

        expenses_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("date",first_date+" - "+secound_date );

                if (isearning){
                    if (first_date.length() > 0 && secound_date.length() > 0) {


                        Intent intent = new Intent(context, MyEarningsDetails.class);
                        intent.putExtra("day", (position + 1) + "");
                        intent.putExtra("month", ((MyEarnings) context).month_count + "");
                        intent.putExtra("year", ((MyEarnings) context).year + "");
                        intent.putExtra("first_date", first_date);
                        intent.putExtra("second_date", secound_date);
                        intent.putExtra("type", "week");
                        context.startActivity(intent);
                    } else {

                        Toast.makeText(context, "No data Found", Toast.LENGTH_SHORT).show();
                    }

                }else {

                    if (first_date.length() > 0 && secound_date.length() > 0) {


                        Intent intent = new Intent(context, MyExpensesDetails.class);
                        intent.putExtra("day", (position + 1) + "");
                        intent.putExtra("month", ((MyExpenses) context).month_count + "");
                        intent.putExtra("year", ((MyExpenses) context).year + "");
                        intent.putExtra("first_date", first_date);
                        intent.putExtra("second_date", secound_date);
                        intent.putExtra("type", "week");
                        context.startActivity(intent);
                    } else {

                        Toast.makeText(context, "No data Found", Toast.LENGTH_SHORT).show();
                    }
                }
//                    Log.e("khhh", position + " " + data.getJSONObject("days").getString((position + 1)+""));


            }
        });


    }

    @Override
    public int getItemCount() {
//        Log.e("size",weekly_amt.size()+"");
        return weekly_amt.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView day, set_amount, month;
        RelativeLayout main;
        RoundCornerProgressBar day_progress;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            main = (RelativeLayout) itemView.findViewById(R.id.main_layout);
            day = (TextView) itemView.findViewById(R.id.day);
            day_progress = (RoundCornerProgressBar) itemView.findViewById(R.id.day_amount_progress);
            set_amount = itemView.findViewById(R.id.set_amount);
            month=itemView.findViewById(R.id.month);
        }
    }
}