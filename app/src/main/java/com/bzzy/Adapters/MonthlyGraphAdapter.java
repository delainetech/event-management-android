package com.bzzy.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.bzzy.Activities.MyEarnings;
import com.bzzy.Activities.MyEarningsDetails;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;

import org.json.JSONException;
import org.json.JSONObject;

public class MonthlyGraphAdapter extends RecyclerView.Adapter<MonthlyGraphAdapter.ViewHolder> implements ApiCalls {

    Context context;
    int amount;
    int k, current_day;
    JSONObject data;
    private int selectedItem = -1;
    CardView earning_detail;
    int position;
    public MonthlyGraphAdapter(Context context, JSONObject data, int current_day, int amount,CardView earning_detail) {
        this.context = context;
        this.current_day = current_day;
        this.data = data;
        this.amount = amount;
        this.earning_detail=earning_detail;
    }


    @Override
    public MonthlyGraphAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.week_bar, viewGroup, false);
        return new MonthlyGraphAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MonthlyGraphAdapter.ViewHolder holder, int i) {

        k = i;
//        Log.e("data", data + "");
        try {
            data.getJSONObject("months").getString(((MyEarnings) context).xVals.get(k).toString());
//            Log.e("k", k + " " + data.getJSONObject("months").getString(((MyEarnings)context).xVals.get(k).toString()));
            int l = 1 + i;
            if (data.getJSONObject("months").getString(((MyEarnings) context).xVals.get(k).toString()).equalsIgnoreCase("0")) {
                holder.set_amount.setVisibility(View.GONE);
            } else if (current_day == l) {
                holder.set_amount.setVisibility(View.VISIBLE);
//                holder.set_amount.setText("₹ " + data.getJSONObject("months").getString(((MyEarnings) context).xVals.get(k).toString()));
            }

            holder.day.setText(((MyEarnings) context).xVals.get(k).toString());

            float progress = 0;
            if (amount != 0) {
                progress = (Integer.parseInt(data.getJSONObject("months").getString(((MyEarnings) context).xVals.get(k).toString())) * 100) / amount;
            }
//            Log.e("progress",progress+"");
            holder.day_progress.setProgress(progress);
            holder.day_progress.getProgress();



            if (i == selectedItem) {
                holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progess_color_current));
                holder.set_amount.setVisibility(View.VISIBLE);
                try {
                    ((MyEarnings)context).amount_type.setText("₹ " + data.getJSONObject("months").getString(((MyEarnings) context).xVals.get(i).toString()));
//                    holder.set_amount.setText("₹ " + data.getJSONObject("months").getString(((MyEarnings) context).xVals.get(i).toString()));
                    Log.e("amount",data.getJSONObject("days").getString(i+1 + "")+"");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                holder.set_amount.setVisibility(View.GONE);
                holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progress_off_color));
//                if (current_day > l) {
//                    holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progress_off_color));
//                } else if (current_day == l) {
//                    holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progess_color_current));
//                    holder.set_amount.setVisibility(View.VISIBLE);
//                    try {
//                        holder.set_amount.setText("₹ " + data.getJSONObject("months").getString(((MyEarnings) context).xVals.get(i).toString()));
//                        Log.e("amount",data.getJSONObject("days").getString(i+1 + "")+"");
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
            }

// click graph to see the amount of month
            holder.main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    position=i;
                    if (selectedItem == i) {
                        holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progress_off_color));
                        selectedItem = -1;
//                        holder.set_amount.setVisibility(View.GONE);
                        ((MyEarnings)context).amount_type.setText("₹ " + "0" );
                        earning_detail.setVisibility(View.GONE);
                    } else {
                        selectedItem = i;
                        holder.day_progress.setProgressColor(context.getResources().getColor(R.color.progess_color_current));



                        try {
                            Log.e("ejgrhus;odhgousdbn",data.getJSONObject("months").getString(((MyEarnings) context).xVals.get(i)));
                            if (!data.getJSONObject("months").getString(((MyEarnings) context).xVals.get(i)).equalsIgnoreCase("0")) {

                                earning_detail.setVisibility(View.VISIBLE);

                            }else {


                                earning_detail.setVisibility(View.GONE);

                            }
                        } catch (JSONException e) {
                            Log.e("eeeee",e.toString());
                            e.printStackTrace();
                        }
                        notifyDataSetChanged();
                    }
                }
            });

            earning_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        Log.e("date",(position+1)+" d"+(position+1)+"m "+((MyEarnings)context).year+ "y "+data.getJSONObject("months").getString(((MyEarnings) context).xVals.get(position)) );

                        if (!data.getJSONObject("months").getString(((MyEarnings) context).xVals.get(position)).equalsIgnoreCase("0")) {

//
                            Intent intent=new Intent(context, MyEarningsDetails.class);
                            intent.putExtra("day",(position+1)+"");
                            intent.putExtra("month",(position+1)+"");
                            intent.putExtra("year",((MyEarnings)context).year+"");
                            intent.putExtra("type","month");
                            intent.putExtra("first_date","1-1-2019");
                            intent.putExtra("second_date","7-1-219");
                            context.startActivity(intent);
                        }else {

                            Toast.makeText(context, "No data Found", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        Log.e("eee",e.toString());
                        e.printStackTrace();
                    }



                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return current_day;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView day, set_amount;
        RelativeLayout main;
        RoundCornerProgressBar day_progress;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            main = (RelativeLayout) itemView.findViewById(R.id.main_layout);
            day = (TextView) itemView.findViewById(R.id.day);
            day_progress = (RoundCornerProgressBar) itemView.findViewById(R.id.day_amount_progress);
            set_amount = itemView.findViewById(R.id.set_amount);
        }
    }

}