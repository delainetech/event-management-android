package com.bzzy.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bzzy.Activities.UserProfile;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class PresentAdapter extends RecyclerView.Adapter<PresentAdapter.ViewHolder> implements ApiCalls {
    List<String> dataList;
    String letter;
    Context context;
    JSONArray data;

    public PresentAdapter(Context context) {
        this.context = context;
    }

    public PresentAdapter(Context context, JSONArray array) {
        this.context = context;
        this.data = array;
    }

    @Override
    public PresentAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.prtabst_item, viewGroup, false);
        return new PresentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PresentAdapter.ViewHolder holder, int i) {

        try {
            JSONObject object=data.getJSONObject(i);

            holder.user_name.setText(object.getString("student_name"));

            if(object.getString("last_seen_day").equalsIgnoreCase("0")){
                holder.due_date.setText("Next due date : "+object.getString("due_date"));
            }

            holder.main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context,UserProfile.class);
                    try {
                        intent.putExtra("user_id", data.getJSONObject(i).getString("id"));
                        intent.putExtra("batch_id", data.getJSONObject(i).getString("batch_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    context.startActivity(intent);
                }
            });

            if (object.getString("image").length()>4){
                Picasso.get().load(batch_images+object.getString("image")).fit().error(R.drawable.error_circle).into(holder.userimage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView userimage;
        TextView user_name, due_date;
        public ImageView call, renew_user, message;
        ImageView imageInside ;
        ExpandableLayout expandableLayout;
        ConstraintLayout main;

        public ViewHolder(View itemView) {
            super(itemView);

            setIsRecyclable(false);
            isRecyclable();
            main=itemView.findViewById(R.id.main_layout);
            userimage = (ImageView) itemView.findViewById(R.id.image_user);
            user_name = (TextView) itemView.findViewById(R.id.name_user);
            due_date = (TextView) itemView.findViewById(R.id.due_dae);
            imageInside = (ImageView) itemView.findViewById(R.id.image_user1);
            call = (ImageView) itemView.findViewById(R.id.call);
            expandableLayout = itemView.findViewById(R.id.expandable_layout);
            message = (ImageView) itemView.findViewById(R.id.message);
        }
    }

}