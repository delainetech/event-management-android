package com.bzzy.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bzzy.Api.ApiCalls;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class RegistrationAdapter extends RecyclerView.Adapter<RegistrationAdapter.ViewHolder> implements ApiCalls {
    Context context;
    ArrayList<String> productimages = new ArrayList<>();
    String ima;
    JSONArray array;

    public RegistrationAdapter(Context context) {
        this.context = context;
    }

    public RegistrationAdapter(Context context, JSONArray array) {
        this.context = context;
        this.array = array;
    }

    @NonNull
    @Override
    public RegistrationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_image_items, parent, false);

        return new RegistrationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RegistrationAdapter.ViewHolder holder, final int i) {

        try {
            Log.e("arraylenght", array.getString(i) + "");
            if (array.getString(i).equals("null")) {
                Log.e("array", "if" + "");
                holder.user_img.setImageDrawable(context.getResources().getDrawable(R.drawable.error_circle));
            } else{
                Log.e("array", "else" + "");
                Picasso.get().load(batch_images + array.getString(i)).error(R.drawable.error_circle).fit().into(holder.user_img);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return array.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView user_img;

        ViewHolder(View itemView) {
            super(itemView);
            user_img = itemView.findViewById(R.id.user_image);
//            textView=itemView.findViewById(R.id.shoptxt);
//            textView1=itemView.findViewById(R.id.price);
        }
    }
}
