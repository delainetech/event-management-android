package com.bzzy.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ExpensesDetailAdapter extends RecyclerView.Adapter<ExpensesDetailAdapter.ViewHolder>
        implements ApiCalls {
    List<String> dataList;
    String cat_name;
    Context context;
    JSONArray data;
    Dialog dialog;
    JSONArray array;
    ProgressBar progressBar;


    public ExpensesDetailAdapter(Context context, JSONArray array) {
        this.context = context;
        this.array=array;
    }

    @Override
    public ExpensesDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.expenses_detail_adapter, viewGroup, false);
        return new ExpensesDetailAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ExpensesDetailAdapter.ViewHolder holder, int i) {

        try {
            JSONObject object=array.getJSONObject(i);
            holder.price.setText(object.getString("amount"));
            holder.sub_category.setText(object.getString("sub_type"));
            holder.date.setText(object.getString("date"));
            if (object.getString("remark") != null && !object.getString("remark").isEmpty()
                    && !object.getString("remark").equals("null")){
                {

                    holder.remark.setText(object.getString("remark"));
                }

            }
                if (object.getString("image").length() > 4) {

                    Glide.with(context)
                            .load(expenditure_images + object.getString("image"))
                            .centerCrop()
                            .placeholder(R.drawable.error_circle)
                            .into(holder.user_image);
                    }else {

                    Picasso.get().load(expenditure_images +
                            object.getString("image")).error(R.drawable.error_circle).fit().into(holder.user_image);

                }

                Log.e("image",expenditure_images + object.getString("image"));


        } catch (JSONException e) {
            Log.e("ee",e.toString());
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return array.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView price,date,sub_category,remark;
        CircleImageView user_image;
        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();

            remark=itemView.findViewById(R.id.remark);
            sub_category=itemView.findViewById(R.id.sub_category);
            user_image=itemView.findViewById(R.id.user_image);
            price=itemView.findViewById(R.id.price);
            date=itemView.findViewById(R.id.date);
        }
    }

    public class ViewDialog {
        void showDialog(Activity activity, String image) {
            dialog = new Dialog(activity);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.view_bill);

            Log.e("imagename", image);
            progressBar = dialog.findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.VISIBLE);

            ImageView bill_image = (ImageView) dialog.findViewById(R.id.bill_image);
            TextView error_text = (TextView) dialog.findViewById(R.id.error_text);

            if (image.length() > 4) {
                Picasso.get().load(expenditure_images + image).fit().into(bill_image);
                progressBar.setVisibility(View.GONE);
            } else {
                bill_image.setVisibility(View.GONE);
                error_text.setVisibility(View.VISIBLE);
            }
            dialog.show();
        }
    }
}
