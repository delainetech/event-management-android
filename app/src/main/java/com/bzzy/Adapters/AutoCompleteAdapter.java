package com.bzzy.Adapters;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.bzzy.Models.searchresult;
import com.bzzy.R;

public class AutoCompleteAdapter extends ArrayAdapter<searchresult> {
    private ArrayList<searchresult> items;
    private ArrayList<searchresult> itemsAll;
    private ArrayList<searchresult> suggestions;
    private int viewResourceId;

    @SuppressWarnings("unchecked")
    public AutoCompleteAdapter(Context context, int viewResourceId,
                                ArrayList<searchresult> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.itemsAll = (ArrayList<searchresult>) items.clone();
        this.suggestions = new ArrayList<searchresult>();
        this.viewResourceId = viewResourceId;

        Log.e("dfjsakdfjsdf",this.items.size()+"");

    }
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.auto_complete_search, null);
        }
        Log.e("getview",position+"");
        searchresult product = items.get(position);

        if (product != null) {
            TextView productLabel = (TextView)  v.findViewById(R.id.search_text);
            TextView type=(TextView)v.findViewById(R.id.type);
            type.setVisibility(View.GONE);

            if (productLabel != null) {
                Log.e("item",product.getStatus());

                type.setText(product.getStatus());
                if (product.getStatus().equals("true")) {

                    Log.e("sheat",product.getStatus());
                    productLabel.setTextColor(Color.parseColor("#bcbcbc"));

                }else {
                    productLabel.setTextColor(Color.parseColor("#000000"));

                }
                productLabel.setText(product.getId());
            }
        }else {

            Log.e("product is ","null");
        }
        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        public String convertResultToString(Object resultValue) {
            String str = ((searchresult) (resultValue)).getId();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (searchresult product : itemsAll) {
                    if (product.getId().toLowerCase()
                            .startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(product);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            @SuppressWarnings("unchecked")
            ArrayList<searchresult> filteredList = (ArrayList<searchresult>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (searchresult c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };

}