package com.bzzy.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bzzy.Activities.MyExpensesDetails;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ExpensesDetailDayAdapter extends RecyclerView.Adapter<ExpensesDetailDayAdapter.ViewHolder> implements ApiCalls {
    List<String> dataList;
    String cat_name;
    Context context;

    Dialog dialog;
    int k, current_day;
    ProgressBar progressBar;
    String inyear;
    String type;
    String inday;
    String inmonth;
    int day;

    public ExpensesDetailDayAdapter(Context context,
                                    int days, String type, String inday, String inmonth, String inyear) {
        this.context = context;

        this.day=days;
        this.inday=inday;
        this.inyear=inyear;
        this.inmonth=inmonth;
        this.type=type;
    }

    @Override
    public ExpensesDetailDayAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.expenses_details_day_adapter, viewGroup, false);
        return new ExpensesDetailDayAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ExpensesDetailDayAdapter.ViewHolder holder, int i) {

        k = i + 1;
//        ((MyEarningsDetails)context)

        if (type.equals("day")){
            holder.day.setText(k + "");


        }else if (type.equals("month")){


            holder.day.setText(((MyExpensesDetails) context).xVals.get(i).toString());


        }
        Log.e("day",inday);
        if ( k==Integer.parseInt(inday)){

            Log.e("current","current");
            holder.layout.setBackgroundResource(R.drawable.black_circle);
            holder.day.setTextColor(Color.parseColor("#ffffff"));
        }else {

            Log.e("current","not current");
            holder.layout.setBackgroundResource(R.drawable.white_circle);
            holder.day.setTextColor(Color.parseColor("#000000"));
        }

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                        notifyDataSetChanged();

//                holder.layout.setBackgroundResource(R.drawable.black_circle);
//                holder.day.setTextColor(Color.parseColor("#ffffff"));


                Log.e("position",(i+1)+"");
                if (type.equals("day")) {
                    ((MyExpensesDetails) context).getExpense((i+1)+"",inmonth,inyear,type,"","",true);
                }else if (type.equals("month")){

                    ((MyExpensesDetails) context).getExpense((i+1)+"",(i+1)+"",inyear,type,"","",true);


                }
            }
        });

    }

    @Override
    public int getItemCount() {

        Log.e("day count",day+"");
        return day;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView product_name, bill_date, remark, view_bill, day;
        ImageView product_image;
        RelativeLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            day = (TextView) itemView.findViewById(R.id.day);
            layout=(RelativeLayout)itemView.findViewById(R.id.layout);
        }
    }

    public class ViewDialog {
        void showDialog(Activity activity, String image) {
            dialog = new Dialog(activity);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.view_bill);

            Log.e("imagename", image);
            progressBar = dialog.findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.VISIBLE);

            ImageView bill_image = (ImageView) dialog.findViewById(R.id.bill_image);
            TextView error_text = (TextView) dialog.findViewById(R.id.error_text);

            if (image.length() > 4) {
                Picasso.get().load(expenditure_images + image).fit().into(bill_image);
                progressBar.setVisibility(View.GONE);
            } else {
                bill_image.setVisibility(View.GONE);
                error_text.setVisibility(View.VISIBLE);
            }
            dialog.show();
        }
    }
}
