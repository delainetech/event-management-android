package com.bzzy.Adapters;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.MyExpenses;
import com.bzzy.Activities.SelectMemberActivity;
import com.bzzy.Activities.UserProfile;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DueDateAdapter extends RecyclerView.Adapter<DueDateAdapter.ViewHolder> implements ApiCalls {
    Context context;
    ArrayList<String> productimages = new ArrayList<>();
    String ima;
    JSONArray data;
    Dialog dialog;
    StringRequest MyStringRequest;
    RequestQueue MyRequestQueue;
    private ArrayList<String> renew_arraylist = new ArrayList<>();
    private String select_month;
    String month_id="";
    int feess,amount;
    int month1,day1,year1;
    EditText fee;
    TextView total_fee,tv_datefrom,tv_dateto;
//    private Spinner renew_spinner;
    EditText registration_fee;
    ImageView renew_user;
    ProgressBar progressBar;
    UserSharedPreferences sharedPreferences;
    int year,month,current_day;
    String date="",st_datefrom="",st_dateto="";

    public DueDateAdapter(Context context, JSONArray array, String date) {
        this.context = context;
        this.data = array;
        this.date=date;
        sharedPreferences = new UserSharedPreferences(context);

        Calendar c=Calendar.getInstance();
        month1=(c.get(Calendar.MONTH)+1);
        day1=c.get(Calendar.DAY_OF_MONTH);
        year1=c.get(Calendar.YEAR);
    }

    @NonNull
    @Override
    public DueDateAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_due, parent, false);
        return new DueDateAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DueDateAdapter.ViewHolder holder, final int i) {

        try {
            JSONObject object = data.getJSONObject(i);

            holder.username.setText(object.getString("student_name"));
            holder.due_date.setText(object.getString("due_date"));
            if (object.getString("image").length() > 4) {
                Picasso.get().load(batch_images + object.getString("image")).fit().error(R.drawable.error_circle).into(holder.user_image);
            }

            holder.user_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, UserProfile.class);
                    try {
                        intent.putExtra("user_id", data.getJSONObject(i).getString("id"));
                        intent.putExtra("batch_id", data.getJSONObject(i).getString("batch_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    context.startActivity(intent);
                }
            });

            holder.username.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            holder.main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    renew_arraylist.clear();

                    try {
                        getRenewDialog alert = new getRenewDialog();
                        alert.showDialog((SelectMemberActivity) context, data.getJSONObject(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView user_image;
        TextView username, due_date;
        LinearLayout main;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();

            user_image = itemView.findViewById(R.id.user_image);
            username = itemView.findViewById(R.id.user_name);
            due_date = itemView.findViewById(R.id.due_date);
            main = (LinearLayout) itemView.findViewById(R.id.main_layout);
        }
    }

    // GetData Dialog
    public class getRenewDialog {
        void showDialog(Activity activity, JSONObject object) throws JSONException {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.renew_dialog);

            renew_arraylist.clear();

            Log.e("user_data", object + "");

//            int time_differ=  checkTimings(date, object.getString("due_date"));
//            Log.e("status_time_differ", time_differ+"");

            ImageView profile_pic = dialog.findViewById(R.id.user_image);
            Picasso.get().load(batch_images + object.getString("image")).error(R.drawable.error_circle).into(profile_pic);
            fee = (EditText) dialog.findViewById(R.id.enter_fee);

//            fee.setText(object.getString("amount"));

            total_fee = (TextView) dialog.findViewById(R.id.total_fee);
            registration_fee = (EditText) dialog.findViewById(R.id.registration_fee);

            tv_datefrom=(TextView)dialog.findViewById(R.id.tv_from);
            tv_dateto=(TextView)dialog.findViewById(R.id.tv_to);

            progressBar = dialog.findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.GONE);
            renew_user = (ImageView) dialog.findViewById(R.id.renew_user);

            st_datefrom=object.getString("due_date");
            tv_datefrom.setText(convertDate(object.getString("due_date")+" 00:00:00","dd-MMM-yyyy"));

            tv_dateto.setHint("dd-mm-yyyy");
//            renew_spinner = (Spinner) dialog.findViewById(R.id.month_spinner);

            // Create an ArrayAdapter that will contain all list items
//            ArrayAdapter<String> adapter;

//            String[] menuArray = context.getResources().getStringArray(R.array.Renew_array);
//
//            for (int k = 0; k < menuArray.length; k++) {
//                renew_arraylist.add(menuArray[k]);
//                adapter = new ArrayAdapter<>(context, R.layout.expense_spinner, renew_arraylist);
//                renew_spinner.setAdapter(adapter);
//            }

// Condition for dates whether it is after or before
            try {
                Log.e("date", date + "");
                Log.e("date_end", object.getString("due_date") + "");

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Log.e("Dates", sdf.parse(date).equals(sdf.parse(object.getString("due_date"))) + "");

                if (sdf.parse(date).equals(sdf.parse(object.getString("due_date")))) {
                    registration_fee.setText("0");
                } else if (sdf.parse(date).after(sdf.parse(object.getString("due_date")))) {
                    registration_fee.setText("50");
                } else if (sdf.parse(date).before(sdf.parse(object.getString("due_date")))) {
                    registration_fee.setText("0");
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


            tv_datefrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDate(tv_datefrom,day1,month1,year1,true);
                }
            });

            tv_dateto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDate(tv_dateto,day1,month1,year1,false);
                }
            });

// Get spinner value

//            renew_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                    select_month = String.valueOf(renew_spinner.getSelectedItem());
//
//                    if (select_month.equalsIgnoreCase("1 Month")) {
//                        if (fee.getText().toString().equalsIgnoreCase("")) {
//                            feess = 0;
//                        } else {
//                            feess = Integer.valueOf(fee.getText().toString().trim());
//                            Log.e("feees", feess + "");
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                            month_id = String.valueOf(renew_spinner.getSelectedItemId() + 1);
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
//                    } else if (select_month.equalsIgnoreCase("Quarterly")) {
//                        if (fee.getText().toString().equalsIgnoreCase("")) {
//                            feess = 0;
//                        } else {
//                            feess = Integer.valueOf(fee.getText().toString().trim());
//                            Log.e("feees", feess + "");
//                            feess = feess * 3;
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                            month_id = String.valueOf(renew_spinner.getSelectedItemId() + 2);
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
//                    } else if (select_month.equalsIgnoreCase("Half-yearly")) {
//                        if (fee.getText().toString().equalsIgnoreCase("")) {
//                            feess = 0;
//                        } else {
//                            feess = Integer.valueOf(fee.getText().toString().trim());
//                            Log.e("feees", feess + "");
//                            feess = feess * 6;
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                            month_id = String.valueOf(renew_spinner.getSelectedItemId() + 4);
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
//                    } else if (select_month.equalsIgnoreCase("Yearly")) {
//                        if (fee.getText().toString().equalsIgnoreCase("")) {
//                            feess = 0;
//                        } else {
//                            feess = Integer.valueOf(fee.getText().toString().trim());
//                            Log.e("feees", feess + "");
//                            feess = feess * 12;
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                            month_id = String.valueOf(renew_spinner.getSelectedItemId() + 9);
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
//                    }
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> adapterView) {
//                    Toast.makeText(context, "Nothing selected", Toast.LENGTH_SHORT).show();
//                }
//            });

// Edit Text TextChange Listener
            fee.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
// For 1 month Renew
//                    if (select_month.equalsIgnoreCase("1 Month")) {
//                        if ("".equalsIgnoreCase(String.valueOf(s))) {
//                            feess = 0 + Integer.parseInt(registration_fee.getText().toString().trim());
//                            Log.e("value of s", s + "");
//                        } else {
//                            feess = Integer.valueOf(String.valueOf(s));
//                            Log.e("feees", feess + "");
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
////// For Quaterly Renew
//                    } else if (select_month.equalsIgnoreCase("Quarterly")) {
//                        if ("".equalsIgnoreCase(String.valueOf(s))) {
//                            feess = 0 + Integer.parseInt(registration_fee.getText().toString().trim());
//                            Log.e("value of s", s + "");
//                        } else {
//                            feess = Integer.valueOf(String.valueOf(s));
////                             feess = Integer.parseInt(registration_fee.getText().toString().trim()) + Integer.parseInt(fee.getText().toString());
//                            Log.e("feees", feess + "");
//                            feess = feess * 3;
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
//// For Half-yearly Renew
//                    } else if (select_month.equalsIgnoreCase("Half-yearly")) {
//                        if ("".equalsIgnoreCase(String.valueOf(s))) {
//                            feess = 0 + Integer.parseInt(registration_fee.getText().toString().trim());
//                            Log.e("value of s", s + "");
//                        } else {
//                            feess = Integer.valueOf(String.valueOf(s));
////                             feess = Integer.parseInt(registration_fee.getText().toString().trim()) + Integer.parseInt(fee.getText().toString());
//                            Log.e("feees", feess + "");
//                            feess = feess * 6;
//                            feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
//                        }
//                        total_fee.setText("Total Fees ₹ " + feess);
//// For yearly Renew
//                    } else if (select_month.equalsIgnoreCase("Yearly")) {
                    if ("".equalsIgnoreCase(String.valueOf(s))) {
                      amount=  feess = 0 + Integer.parseInt(registration_fee.getText().toString().trim());

                        Log.e("value of s", s + "");
                    } else {
//
                       amount= feess = Integer.valueOf(String.valueOf(s));
//                             feess = Integer.parseInt(registration_fee.getText().toString().trim()) + Integer.parseInt(fee.getText().toString());
                        Log.e("feees", feess + "");
//                            feess = feess * 12;
                        feess = feess + Integer.parseInt(registration_fee.getText().toString().trim());
                    }
                    total_fee.setText("Total Fees ₹ " + feess);
//                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            fee.setText(object.getString("amount"));

            renew_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fee.getText().toString().trim().equalsIgnoreCase("")) {
                        fee.setError("Please enter renew amount.");
                    } else if(st_datefrom.length()>0&st_dateto.length()>0)
                        renewUser(object);
                    else
                        Toast.makeText(context,"Please select date",Toast.LENGTH_SHORT).show();
                }
            });
            dialog.show();
        }
    }


    public String convertDate(String dt, String ft){

//        log("daaaaa",dt+"");
        if(dt.equalsIgnoreCase(null)|dt.equalsIgnoreCase("null")|dt.equalsIgnoreCase(""))
            return "";

        Date date1 = null;
        try {
            date1=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat formatter = new SimpleDateFormat(ft);
//        Date date = new Date();

        return formatter.format(date1);

    }

    public void getDate(final TextView et, final int day, int month, int year,boolean b){

        DatePickerDialog datePickerDialog =new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String d=dayOfMonth+"-"+(month+1)+"-"+year+" 00:00:00";

                if(b) {

                    day1 = dayOfMonth;
                    month1 = month + 1;
                    year1 = year;

                    st_datefrom=convertDate(d,"dd-MM-yyyy");
                }
                else
                    st_dateto=convertDate(d,"dd-MM-yyyy");

//                    tv_to.setText(utl.convertDate(d,"dd MMM-yyyy"));

                et.setText(convertDate(d,"dd-MMM-yyyy"));
            }
        },year,month-1,day);

        if(b){

        }
//            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        else{

//            String myDate = year+"/"+month+"/"+day;

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

            Date date = null;
            try {
                date = sdf.parse(st_datefrom);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long millis = date.getTime();

            datePickerDialog.getDatePicker().setMinDate(millis);
        }

        datePickerDialog.show();
    }

    private void renewUser(JSONObject obj) {
        progressBar.setVisibility(View.VISIBLE);
        MyRequestQueue = Volley.newRequestQueue(context);
        MyStringRequest = new StringRequest(Request.Method.POST, renew_student, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        progressBar.setVisibility(View.GONE);
                        ((SelectMemberActivity) context).getMemberList("","");
                        dialog.dismiss();

                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(context, "Sorry! Unable to renew.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            progressBar.setVisibility(View.GONE);
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//                Toast.makeText(AdminProfile.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", volleyError + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                try {
//                    int amount=Integer.parseInt(fee.getText().toString().trim())* Integer.parseInt(month_id);
                    params.put("user_id", sharedPreferences.getuserID());
                    params.put("batch_id", obj.getString("batch_id"));
                    params.put("event_id", sharedPreferences.geteventId());
                    params.put("amount", amount+"" );
                    params.put("student_id", obj.getString("id"));
                    params.put("months", month_id);
                    params.put("from",st_datefrom);
                    params.put("to",st_dateto);
                    params.put("date", date);
                    params.put("late_fee", registration_fee.getText().toString().trim() + "");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("getParams: ", params + "");

                return params;

            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

}
