package com.bzzy.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bzzy.Activities.AddBatch;
import com.bzzy.Activities.AdminProfile;
import com.bzzy.Activities.Dashboard;
import com.bzzy.Activities.ExpenditureActivity;
import com.bzzy.Activities.UserProfile;
import com.bzzy.Activities.ViewExpenditureBills;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SheatSearchAdapter extends RecyclerView.Adapter<SheatSearchAdapter.ViewHolder> implements ApiCalls {
    Context context;
    JSONArray data;
    Dialog dialog;
    UserSharedPreferences sharedPreferences;

    int year;

    public SheatSearchAdapter(Context context) {
        this.context = context;
    }

    public SheatSearchAdapter(Context context, JSONArray array) {
        this.context = context;
        this.data = array;
        sharedPreferences = new UserSharedPreferences(context);
    }

    @Override
    public SheatSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_autotext, viewGroup, false);
        return new SheatSearchAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SheatSearchAdapter.ViewHolder holder, int i) {

        try {
            JSONObject object = data.getJSONObject(i);

            holder.search_item.setText(object.getString("id"));


            if (object.getString("status").equals("true")) {

                holder.search_item.setTextColor(Color.parseColor("#bcbcbc"));

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView search_item, type;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            search_item = (TextView) itemView.findViewById(R.id.search_text);
            type=(TextView) itemView.findViewById(R.id.type);

        }
    }
}
