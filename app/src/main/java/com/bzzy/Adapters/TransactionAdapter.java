package com.bzzy.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> implements ApiCalls {
    List<String> dataList;
    String letter;
    Context context;
    JSONArray data;
    Dialog dialog;
    UserSharedPreferences sharedPreferences;

    public TransactionAdapter(Context context, JSONArray array) {
        this.context = context;
        this.data = array;
        sharedPreferences = new UserSharedPreferences(context);
    }

    @Override
    public TransactionAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.transaction_fees_item, viewGroup, false);
        return new TransactionAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TransactionAdapter.ViewHolder holder, int i) {
        try {
            JSONObject object = data.getJSONObject(i);
// Set details of expense
            holder.bill_date.setText(object.getString("date"));

            holder.amount.setText("₹ " + object.getString("amount"));

            if (!object.getString("from").equalsIgnoreCase("null")) {
                holder.from_date.setText(object.getString("from"));
                holder.to_date.setText(object.getString("to"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView to_date, bill_date, remark, amount, from_date, view_bill, status;
        ImageView partner_image;
        CardView main;

        public ViewHolder(View itemView) {
            super(itemView);
            from_date = (TextView) itemView.findViewById(R.id.from_date);
            bill_date = (TextView) itemView.findViewById(R.id.paid_date);
            to_date = (TextView) itemView.findViewById(R.id.to_date);
            amount = (TextView) itemView.findViewById(R.id.fees_amount);
            view_bill = (TextView) itemView.findViewById(R.id.view_bill);
        }
    }

    // Confirm bill
    public class confirmationDialog {
        void showDialog(Activity activity, String exp_id, int status, int pos) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.view_bill);

            dialog.show();
        }
    }

}