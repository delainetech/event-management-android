package com.bzzy.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bzzy.Api.ApiCalls;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;

public class ImageViewerAdapter extends RecyclerView.Adapter<ImageViewerAdapter.ViewHolder> implements ApiCalls {
    Context context;
    ArrayList<String> productimages = new ArrayList<>();
    String ima;
    JSONArray array;

    public ImageViewerAdapter(Context context) {
        this.context = context;
    }

    public ImageViewerAdapter(Context context, ArrayList<String> array) {
        this.context = context;
        this.productimages = array;
    }

    @NonNull
    @Override
    public ImageViewerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_image_items, parent, false);

        return new ImageViewerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageViewerAdapter.ViewHolder holder, final int i) {

        if (productimages.get(i).equals("null")) {
            Log.e("if","in imageadpt");
            holder.user_img.setImageDrawable(context.getResources().getDrawable(R.drawable.error_circle));
        } else {
            Log.e("else", "start imageadpt");
            Picasso.get().load(batch_images + productimages.get(i)).error(R.drawable.error_circle).fit().into(holder.user_img);

        }
    }

    @Override
    public int getItemCount() {
        return productimages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView user_img;

        ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            user_img = itemView.findViewById(R.id.user_image);

        }
    }
}
