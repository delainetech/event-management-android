package com.bzzy.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> implements ApiCalls {
    Context context;
    JSONArray user_array;
    JSONObject notification_data;
    Dialog dialog;
    UserSharedPreferences sharedPreferences;

    int year;

    public NotificationAdapter(Context context) {
        this.context = context;
    }

    public NotificationAdapter(Context context, JSONObject array, JSONArray data) {
        this.context = context;
        this.notification_data = array;
        this.user_array = data;
        sharedPreferences = new UserSharedPreferences(context);
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notifications, viewGroup, false);
        return new NotificationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.ViewHolder holder, int i) {

        try {
            JSONObject user_data = user_array.getJSONObject(i);

            if (Integer.parseInt(user_array.getJSONObject(i).getString("pendings")) > 0) {
                holder.notication_image.setVisibility(View.VISIBLE);
                Log.e("in else", "if");
                holder.notication_text.setText(user_data.getString("name")+" : "+user_data.getString("pendings") + " expenditure requests are pending. "  );
                holder.notication_image.setImageDrawable(context.getDrawable(R.drawable.app_iicon));
            }else {
                holder.notication_image.setVisibility(View.GONE);
                holder.notication_image.setVisibility(View.GONE);
                Log.e("in else", "else");

            }

        } catch (JSONException e) {
            Log.e("eee adapter",e+"");
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        Log.e("size user arrya",user_array.length()+"");
        return user_array.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView notication_text;
        ImageView notication_image;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();

            notication_text = (TextView) itemView.findViewById(R.id.notification_text);
            notication_image = (ImageView) itemView.findViewById(R.id.notification_image);

        }

    }

}