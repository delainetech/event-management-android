package com.bzzy.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bzzy.Activities.PendingApprovals;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ApprovalAdminAdapter extends RecyclerView.Adapter<ApprovalAdminAdapter.ViewHolder> implements ApiCalls {
    List<String> dataList;
    String letter;
    Context context;
    JSONArray array;

    public ApprovalAdminAdapter(Context context) {
        this.context = context;
    }

    public ApprovalAdminAdapter(Context context, JSONArray array) {
        this.context = context;
        this.array = array;
    }

    @Override
    public ApprovalAdminAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.admin_profile_pending_item, viewGroup, false);
        return new ApprovalAdminAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ApprovalAdminAdapter.ViewHolder holder, int i) {
        try {
            JSONObject object = array.getJSONObject(i);

            holder.user_name.setText(object.getString("name"));

            if (object.getString("image").length() > 4)
                Picasso.get().load(advertising_images + object.getString("image")).error(R.drawable.error_circle).fit().into(holder.user_image);

            if (object.getString("pending_status").equalsIgnoreCase("1")) {
                holder.status.setVisibility(View.VISIBLE);
            } else holder.status.setVisibility(View.GONE);
            holder.main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, PendingApprovals.class);
                    intent.putExtra("jsonObject", object.toString());
                    context.startActivity(intent);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return array.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CardView main;
        ImageView user_image, status;
        TextView user_name;

        public ViewHolder(View itemView) {
            super(itemView);
            user_image = (ImageView) itemView.findViewById(R.id.user_image);
            status = (ImageView) itemView.findViewById(R.id.status);
            user_name = (TextView) itemView.findViewById(R.id.user_name);
            main = itemView.findViewById(R.id.main_layout);
        }
    }

}