package com.bzzy.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.AdminProfile;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AdminCardsAdapter extends PagerAdapter implements ApiCalls {

    Context context;
    int[] backgrounds;
    JSONArray data;
    View view = null;
    public static TextView admission_total, trials_total, conversion, click_here, percentage;
    ImageView percent_graph, percent_decrease;
    JSONObject main_object;
    Dialog dialog;
    ProgressBar progressBar;
    RecyclerView conversion_recycler;
    ConversionAdapter conversionAdapter;
    Spinner dialog_year_spinner;
    private ArrayList<String> year_list = new ArrayList<>();
    int year;
    ArrayAdapter<String> adapter;
    UserSharedPreferences sharedPreferences;

    public AdminCardsAdapter(Context context, JSONArray obj, JSONObject jsonObject) {
        this.context = context;
        this.data = obj;
        this.main_object = jsonObject;
        getDate();
        sharedPreferences = new UserSharedPreferences(context);
    }

    public AdminCardsAdapter(AdminProfile adminProfile, int[] backgrounds, JSONArray array) {
        this.context = adminProfile;
        this.backgrounds = backgrounds;
        this.data = array;

    }

    @Override
    public Object instantiateItem(ViewGroup container, int i) {

// First View ................................
        if (i == 0) {
            view = LayoutInflater.from(context).inflate(R.layout.admin_card, null, false);
            admission_total = view.findViewById(R.id.no_of_admissions);
            conversion = view.findViewById(R.id.conversion_details);
            try {
//                    if (data.getJSONObject(i).getString("current_month").equalsIgnoreCase("1")) {
                if (Integer.parseInt(main_object.getString("all_total_admission")) <= 9) {
                    admission_total.setText("00" + main_object.getString("all_total_admission"));
                } else if (Integer.parseInt(main_object.getString("all_total_admission")) <= 99) {
                    admission_total.setText("0" + main_object.getString("all_total_admission"));
                } else
                    admission_total.setText(main_object.getString("all_total_admission"));

                float value_monthly = (Float.parseFloat(main_object.getString("all_total_admission")) * 100) / Integer.parseInt(main_object.getString("all_total_trials"));

                String value = String.format("%.2f", value_monthly);

                conversion.setText(value + " %" + " Conversion");
//                    }
            } catch (Exception e) {
                e.printStackTrace();
            }


// Second View ................................
        } else if (i == 1) {
            view = LayoutInflater.from(context).inflate(R.layout.admin_card_sec, null, false);
            trials_total = view.findViewById(R.id.no_of_trials);

            try {
                if (Integer.parseInt(main_object.getString("all_total_trials")) <= 9) {
                    trials_total.setText("00" + main_object.getString("all_total_trials"));
                } else if (Integer.parseInt(main_object.getString("all_total_trials")) <= 99) {
                    trials_total.setText("0" + main_object.getString("all_total_trials"));
                } else
                    trials_total.setText(main_object.getString("all_total_trials"));

            } catch (Exception e) {
                e.printStackTrace();
            }
// Third View ................................
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.admin_card_third, null, false);

            percent_graph = view.findViewById(R.id.percent);
            percent_decrease = view.findViewById(R.id.percent_dec);
            percentage = view.findViewById(R.id.percentage);
            click_here = view.findViewById(R.id.click_here);

            try {

                String value = String.format("%.2f", Float.parseFloat(main_object.getString("curr_pre_admission_percentage")));

                if (Float.parseFloat(main_object.getString("curr_pre_admission_percentage")) >= 0) {
//                    Log.e("ss","ss");
                    percent_graph.setVisibility(View.VISIBLE);
                    percent_decrease.setVisibility(View.GONE);
                    percentage.setText(value + " %");
                } else {
//                    Log.e("esle","s");
                    String newString = value.replace("-", " ");
                    percentage.setText(newString + " %");

                    percent_graph.setVisibility(View.GONE);
                    percent_decrease.setVisibility(View.VISIBLE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


            click_here.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    conversionDialog alert = new conversionDialog();
                    alert.showDialog((AdminProfile) context);
                }
            });
        }

        container.addView(view);
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    // Add partner dialog
    public class conversionDialog {
        void showDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);

            dialog.setContentView(R.layout.conversion);

            year_list.clear();

            progressBar = dialog.findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.GONE);

            conversion_recycler = dialog.findViewById(R.id.conversion_recycler);
            conversion_recycler.setItemAnimator(new DefaultItemAnimator());
            LinearLayoutManager horizontalLayoutManager3 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            conversion_recycler.setLayoutManager(horizontalLayoutManager3);


            dialog_year_spinner = dialog.findViewById(R.id.year_spinner);

            ArrayAdapter<String> adapter;
            String[] menuArray = context.getResources().getStringArray(R.array.years);


            conversion();

            dialog.show();
        }
    }

    // Conversion
    private void conversion() {
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(context);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, conversion_report, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar.setVisibility(View.GONE);
                        // Launch login activity
                        JSONArray month_report = jObj.getJSONArray("months");
                        JSONArray years = jObj.getJSONArray("years");
                        for (int k = 0; k < years.length(); k++) {
                            year_list.add(years.getString(k));
                            adapter = new ArrayAdapter<>(context, R.layout.cat_spinner, year_list);
                            dialog_year_spinner.setAdapter(adapter);
                        }
                        setData(years, month_report);
                    } else {

                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(context, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", sharedPreferences.getuserID());
                params.put("event_id", sharedPreferences.geteventId());
                params.put("year", year + "");
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    public void setData(JSONArray year, JSONArray data) {
        dialog_year_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String month = String.valueOf(dialog_year_spinner.getSelectedItem());
                Log.e("spinner", "selected");

                    conversionAdapter = new ConversionAdapter(context, data);
                    conversion_recycler.setAdapter(conversionAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void getDate() {
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
    }
}