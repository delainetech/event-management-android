package com.bzzy.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bzzy.Activities.AdminProfile;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BannerAdapter extends PagerAdapter implements ApiCalls {

    Context context;
    private int from = 0, to = 1;
    int[] backgrounds;
    private JSONObject object;
    JSONArray data;
    RelativeLayout main;
    ImageView banner,outer_banner;
    CardView cardView;

    public BannerAdapter(Context context, int[] backgrounds) {
        this.context = context;
        this.backgrounds = backgrounds;
    }

    public BannerAdapter(AdminProfile adminProfile, int[] backgrounds, JSONArray array) {
        this.context = adminProfile;
        this.backgrounds = backgrounds;
        this.data = array;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int i) {

        View view = null;

        view = LayoutInflater.from(context).inflate(R.layout.banner, null, false);
        cardView=(CardView) view.findViewById(R.id.card);
        banner = (ImageView) view.findViewById(R.id.image_banner);
        outer_banner = (ImageView) view.findViewById(R.id.image_banner_outer);

        if (data != null) {
            try {
                cardView.setVisibility(View.VISIBLE);
                outer_banner.setVisibility(View.GONE);
                JSONObject object = data.getJSONObject(i);

                RelativeLayout main = (RelativeLayout) view.findViewById(R.id.main_layout);
                main.setBackgroundResource((backgrounds[i]));

                if (object.getString("image").length() > 4) {
                    Picasso.get().load(advertising_images + object.getString("image")).fit().into(banner);
                }

                banner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri uri = null; // missing 'http://' will cause crashed
                        try {
                            uri = Uri.parse("http://" + object.getString("url"));
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            context.startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            cardView.setVisibility(View.GONE);
            outer_banner.setVisibility(View.VISIBLE);
            outer_banner.setImageResource(backgrounds[i]);
        }
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }


}