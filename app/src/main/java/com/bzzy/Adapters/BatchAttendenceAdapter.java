package com.bzzy.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bzzy.Activities.Attendence;
import com.bzzy.Activities.PresentAbsentActivity;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BatchAttendenceAdapter extends RecyclerView.Adapter<BatchAttendenceAdapter.ViewHolder> implements ApiCalls {

    Context context;
    JSONArray data;
    int backgrounds[];

    int k = 0;

    public BatchAttendenceAdapter(Attendence context, JSONArray array, int[] background) {
        this.context = context;
        this.data = array;
        this.backgrounds = background;
    }


    public BatchAttendenceAdapter(Attendence context) {
        this.context = context;
    }

    @Override
    public BatchAttendenceAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.batch_view, viewGroup, false);
        return new BatchAttendenceAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BatchAttendenceAdapter.ViewHolder holder, int i) {

        JSONObject object = null;
        try {
            object = data.getJSONObject(i);

            holder.batch_name.setText(object.getString("name"));
            holder.present_members.setText(object.getString("present") + " Present");
            holder.absent_members.setText(object.getString("absent") + " Absent");

            Log.e("attendence", object.getString("time") + "  " + object.getString("attendance_date"));
            if (!object.getString("attendance_date").equalsIgnoreCase(" ")) {
                holder.attendance_date.setText(object.getString("attendance_date"));
                holder.attendance_time.setText(object.getString("time"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (k == 0) {
            holder.main.setBackground(context.getResources().getDrawable(R.drawable.card_attendence_pink));
            k++;
        } else if (k == 1) {
            holder.main.setBackground(context.getResources().getDrawable(R.drawable.voilet_square_card));
            k++;
        } else if (k == 2) {
            holder.main.setBackground(context.getResources().getDrawable(R.drawable.light_green_square_card));
            k++;
        } else if (k == 3) {
            holder.main.setBackground(context.getResources().getDrawable(R.drawable.blue_square_card));
            k=0;
        }


        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(context, PresentAbsentActivity.class);
                    intent.putExtra("batch_id", data.getJSONObject(i).getString("batch_id"));
                    intent.putExtra("attendance_id",data.getJSONObject(i).getString("id"));
                    context.startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView present_members, absent_members, attendance_date, attendance_time, batch_name;
        RelativeLayout main;
        CardView parent;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();
            main = (RelativeLayout) itemView.findViewById(R.id.child_layout);
            parent = (CardView) itemView.findViewById(R.id.parent_card);
            present_members = (TextView) itemView.findViewById(R.id.present);
            absent_members = (TextView) itemView.findViewById(R.id.absent);
            attendance_date = (TextView) itemView.findViewById(R.id.batch_date);
            attendance_time = (TextView) itemView.findViewById(R.id.batch_time);
            batch_name = (TextView) itemView.findViewById(R.id.batch_name);
//            due_status = (ImageView) itemView.findViewById(R.id.trail_status);

        }
    }

}