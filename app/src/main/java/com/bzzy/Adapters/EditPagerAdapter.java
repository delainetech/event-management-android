package com.bzzy.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.AddUserActivity;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.Constants.VolleyMultipartRequest;
import com.bzzy.R;
import com.bzzy.utils.Common;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import static com.bzzy.Activities.AddUserActivity.getFileDataFromDrawable;

public class EditPagerAdapter extends PagerAdapter implements ApiCalls {

    Context context;
    private int from = 0, to = 1;
    private String time;
    private JSONObject object;
    static EditText time_from = null, time_to = null, fee = null, user_name = null, enter_mobile = null,
            enter_email = null,enter_father=null;;
    static Spinner enter_topic_spinner = null;
    EditText trail_spinner;
    static String selected_trail1;
    public static ImageView user_image, id_card;
    public static TextView upload_id, done;
    public RelativeLayout main_card;
    Bitmap bitmap, bitmap1;
    UserSharedPreferences sharedPreferences;
    private ArrayList<String> trails_name = new ArrayList<>();
    ProgressDialog pDialog;
    ImageView add_user = null;
    EditText id_number;
    View view = null;
    JSONArray domain_arr;
    ArrayList<String> domain_name = new ArrayList<>();
    public ArrayAdapter<String> arrayAdapter;
    String domain_name_select;
    JSONObject batch_object;


    public EditPagerAdapter(Context context, JSONObject obj, JSONArray domain_arr, JSONObject object) {
        this.context = context;
        this.object = obj;
        this.domain_arr = domain_arr;
        this.batch_object = object;
        sharedPreferences = new UserSharedPreferences(context);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int i) {


// First View ................................
        if (i == 0) {
            view = LayoutInflater.from(context).inflate(R.layout.time_set, null, false);
            time_from = (EditText) view.findViewById(R.id.time_from);
            time_to = (EditText) view.findViewById(R.id.time_to);
            fee = (EditText) view.findViewById(R.id.batch_fee);
            trail_spinner = (EditText) view.findViewById(R.id.trails_spinner);

            //            spinner
            enter_topic_spinner = (Spinner) view.findViewById(R.id.enter_topic);

            JSONObject object1 = null;
            try {

                for (int k = 0; k < domain_arr.length(); k++) {
                    object1 = domain_arr.getJSONObject(k);
                    domain_name.add(object1.getString("name"));
                    arrayAdapter = new ArrayAdapter<>(context, R.layout.domain_spinner, domain_name);
                    enter_topic_spinner.setAdapter(arrayAdapter);
                }

                String name = object.getString("domain_name");
                enter_topic_spinner.setSelection(domain_name.indexOf(name));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("GetList object", object + "");

            enter_topic_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    try {
//                        if (domain_name.contains(object.getString("domain_name"))) {
                        Log.e("if", "start");
//                        }
                        domain_name_select = domain_arr.getJSONObject(position).getString("id");
//                        else{
//                            Log.e("else","start");
//                            domain_name_select = domain_arr.getJSONObject(position).getString("id");
//                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("domain_name_select", domain_name_select + "");
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            try {
                Log.e("Object", object + "");

                trail_spinner.setText(object.getString("due_date"));

                fee.setText(object.getString("amount"));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                if (!batch_object.getString("start_time").equalsIgnoreCase("null")) {

                    Log.e("lower timer",batch_object.getString("start_time") + " to " + batch_object.getString("end_time"));
                    time_from.setText(batch_object.getString("start_time"));
                    time_to.setText(batch_object.getString("end_time"));
                }

                fee.setText(batch_object.getString("fee"));
                fee.setEnabled(false);

                time_from.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectTime(time_from);
                    }
                });

                time_to.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectTime(time_to);
                    }
                });
            } catch (JSONException e) {
                Log.e("eee",e.toString());
                e.printStackTrace();
            }
// Second View ................................
        } else if (i == 1) {
            view = LayoutInflater.from(context).inflate(R.layout.personal_details, null, false);
            user_name = (EditText) view.findViewById(R.id.enter_username);
            enter_mobile = (EditText) view.findViewById(R.id.enter_phone);
            enter_email = (EditText) view.findViewById(R.id.enter_email);
            enter_father = (EditText) view.findViewById(R.id.enter_father);

            try {
                user_name.setText(object.getString("student_name"));
                enter_mobile.setText(object.getString("student_phone"));
                enter_email.setText(Common.ParseString(object,"student_email"));
                if (!object.isNull("father_name")) {
                    enter_father.setText(object.getString("father_name"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


// Third View ................................
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.upload_image, null, false);

            id_number = (EditText) view.findViewById(R.id.id_number);
            upload_id = (TextView) view.findViewById(R.id.upload_id);
            user_image = (ImageView) view.findViewById(R.id.user_image);
            add_user = (ImageView) view.findViewById(R.id.submit_details);

            id_card = (ImageView) view.findViewById(R.id.id_card);

            try {
                if(!object.getString("id_number").equalsIgnoreCase("null")) {
                    Log.e("id","number "+ object.getString("id_number"));
                    id_number.setText(object.getString("id_number"));
                }


                Picasso.get().load(batch_images + object.getString("image")).fit().error(R.drawable.ic_upload_photo).into(user_image);
                Picasso.get().load(batch_images + object.getString("id_card")).fit().error(R.drawable.card_background_orange).into(id_card);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            upload_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON).setAspectRatio(512, 350)
                            .getIntent(context);
                    ((AddUserActivity) context).startActivityForResult(intent, 250);
                }
            });

            user_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).setAspectRatio(400, 580)
                            .getIntent(context);
                    ((AddUserActivity) context).startActivityForResult(intent, 211);

                }
            });

            add_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    validations();
                }

                // Validations
                private void validations() {
//                    if (id_number.getText().toString().trim().equalsIgnoreCase("")) {
//                        id_number.setError("Enter ID number.");
//                    } else
                    if (time_from.getText().toString().trim().equalsIgnoreCase("")) {
                        time_from.setError("Please check start time");
                    } else if (time_to.getText().toString().trim().equalsIgnoreCase("")) {
                        time_to.setError("Please check end time.");
                    } else if (fee.getText().toString().trim().equalsIgnoreCase("")) {
                        fee.setError("Please provide fee");
                    } else if (user_name.getText().toString().trim().equalsIgnoreCase("")) {
                        user_name.setError("Add username");
                        Toast.makeText(context, "Add username", Toast.LENGTH_SHORT).show();
                    }else if (enter_father.getText().toString().trim().equalsIgnoreCase("")) {
                        enter_father.setError("Add father name");
                        Toast.makeText(context, "Add father name", Toast.LENGTH_SHORT).show();
                    } else if (enter_mobile.getText().toString().trim().equalsIgnoreCase("")) {
                        enter_mobile.setError("Enter mobile no.");
                        Toast.makeText(context, "Enter mobile no.", Toast.LENGTH_SHORT).show();
                    } else if (!isValidMobile(enter_mobile.getText().toString().trim())) {
                        enter_mobile.setError("Incorrect Mobile no.");
                        Toast.makeText(context, "Incorrect Mobile no.", Toast.LENGTH_SHORT).show();
                    } else if ((enter_email.getText().toString().trim().length()>0) && !isValidEmailId(enter_email.getText().toString().trim())) {

                            enter_email.setError("Email is not valid.");
                            Toast.makeText(context, "Incorrect Email Id", Toast.LENGTH_SHORT).show();

                    } else if (enter_topic_spinner.equals("")) {
                        Toast.makeText(context, "Please enter topic", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            if (object.getString("batch_id").equals(batch_object.getString("id"))) {
                                addUser(((AddUserActivity) context).bmap, ((AddUserActivity) context).userBitmap_edit);
                            } else {

                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setIcon(android.R.drawable.ic_dialog_info);
                                builder.setTitle("Wait!");
                                builder.setMessage("Change Member's Batch?");
                                builder.setPositiveButton("Ok",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                arg0.cancel();
                             addUser(((AddUserActivity) context).bmap, ((AddUserActivity) context).userBitmap_edit);

                                            }
                                        });
                                builder.setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                             arg0.cancel();
                                            }
                                        });
                                final AlertDialog dialog = builder.create();
                                dialog.show();

                            }
                        }catch (Exception e){
                            Log.e("exception","e",e);
                        }
                        }
                }
            });
        }

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    private boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    private boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }


    private void selectTime(EditText setTime) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                datetime.set(Calendar.MINUTE, selectedMinute);

                if (selectedHour >= 00 && selectedHour < 12) {
                    time = String.format("%02d:%02d", selectedHour, selectedMinute) + " AM";
                } else {
                    if (selectedHour == 12) {
                        time = String.format("%02d:%02d", selectedHour, selectedMinute) + " PM";
                    } else {
                        selectedHour = selectedHour - 12;
                        time = String.format("%02d:%02d", selectedHour, selectedMinute) + " PM";
                    }
                }
                setTime.setText(time);
            }
        }, hour, minute, false);
        mTimePicker.show();
    }

    private void addUser(Bitmap id_card, Bitmap user_image_bit) {

        Log.e("gjg", "hgjg");
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading...");
        pDialog.show();
        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, add_new_user,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        Log.e("Responseedituser", new String(response.data) + " ");
                        //This code is executed if the server responds, whether or not the response contains data.
                        try {
                            JSONObject jObj = new JSONObject(new String(response.data));
                            String status = jObj.getString("status");
                            if (status.equals("success")) {
                                pDialog.dismiss();
                                JSONObject object = jObj.getJSONObject("data");

                                ((AddUserActivity) context).bmap = null;
                                ((AddUserActivity) context).userBitmap_edit = null;

                                ((AddUserActivity) context).arrayAdapter.notifyDataSetChanged();
                                Intent i=new Intent();
                                i.putExtra("data", jObj.getString("new_id")+"");
                                ((AddUserActivity) context).setResult(Activity.RESULT_OK,i);
                                ((AddUserActivity) context).finish();
                                Toast.makeText(context, "User updated successfully.", Toast.LENGTH_SHORT).show();
                            } else {
                                pDialog.dismiss();
                                Toast.makeText(context, "Sorry! error occured. ", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e1) {
                            pDialog.dismiss();
                            Log.e("Exception", e1 + " ");
                            e1.printStackTrace();
                        }
                    }
                },
                error -> {
                    pDialog.dismiss();


                    Log.e("onErrorResponse: ", error + "");
                    NetworkResponse networkResponse = error.networkResponse;
                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";
                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";

                        }
                        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


                params.put("event_id", sharedPreferences.geteventId());
                params.put("user_id", sharedPreferences.getuserID());
                params.put("due_date", " ");
                params.put("student_name", user_name.getText().toString().trim());
                try {
                    params.put("father_name", enter_father.getText().toString().trim());

                    params.put("start_time", time_from.getText().toString().trim());
                    params.put("end_time", time_to.getText().toString().trim());
                    params.put("student_id", object.getString("id"));
                    params.put("amount", fee.getText().toString().trim());
                    params.put("batch_id", batch_object.getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                params.put("topic", domain_name_select);
                params.put("student_phone", enter_mobile.getText().toString().trim());
                params.put("student_email", enter_email.getText().toString().trim());
                params.put("id_number", id_number.getText().toString().trim());
                params.put("date", " ");
                try {
                    if (user_image_bit == null) {
                        params.put("user_image", "");
                    }

                    if (id_card == null) {
                        params.put("id_card", "");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e("getParams: ", params + "");
                return params;
            }


            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();

                if (id_card != null) {
                    params.put("id_card", new DataPart(imagename + ".png", getFileDataFromDrawable(id_card)));
                }
                if (user_image_bit != null) {
                    params.put("user_image", new DataPart("profile_" + UUID.randomUUID().toString() + ".png", getFileDataFromDrawable(user_image_bit)));
                }
                Log.e("params", params + "");
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //adding the request to volley
        Volley.newRequestQueue(context).add(volleyMultipartRequest);
    }

    public interface setIdImage {
        static void sIdImage(Bitmap uri) {
            id_card.setImageBitmap(uri);
        }
    }

    public interface setUserImage {
        static void sUserImage(Bitmap uri) {
            user_image.setImageBitmap(uri);
        }
    }




}
