package com.bzzy.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.bzzy.Activities.AddBatch;
import com.bzzy.Activities.BatchListActivity;
import com.bzzy.Api.ApiCalls;
import com.bzzy.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DashboardBarAdapter extends RecyclerView.Adapter<DashboardBarAdapter.ViewHolder> implements ApiCalls {

    Context context;
    JSONArray data;

    public DashboardBarAdapter(Context context, JSONArray array) {
        this.context = context;
        this.data = array;
    }


    @Override
    public DashboardBarAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.batch_bar, viewGroup, false);
        return new DashboardBarAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DashboardBarAdapter.ViewHolder holder, int i) {

        try {
            JSONObject object = data.getJSONObject(i);

            Log.e("Batch Data",object+"");

            holder.batch_name.setText(object.getString("name"));

            if(Integer.parseInt(object.getString("occupied"))<=9){
                holder.filled_seats.setText("0"+object.getString("occupied"));
            }else     holder.filled_seats.setText(object.getString("occupied"));

            if(Integer.parseInt(object.getString("occupied")) > Integer.parseInt(object.getString("seats"))){
                holder.filled_seats.setTextColor(context.getResources().getColor(R.color.progress_color));
            }

            if(Integer.parseInt(object.getString("seats"))<=9) {
                holder.total_seats.setText("0"+object.getString("seats"));
            }else
                holder.total_seats.setText(object.getString("seats"));

            holder.total.setMax(Float.parseFloat(data.getJSONObject(i).getString("seats")));
            holder.total.getMax();

            int value= (int) holder.total.getMax();
            holder.total.setProgress(value);
            int total_filled=0;
        if (value!=0) {
            total_filled = (Integer.parseInt(data.getJSONObject(i).getString("occupied")) * 100) / value;
        }
            holder.filled.setProgress(total_filled);
            holder.filled.getProgress();

            holder.filled.invalidateDrawable(context.getResources().getDrawable(R.drawable.progress_bar_dashboard));
            holder.filled.getAnimation();

            holder.main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, BatchListActivity.class);
                    intent.putExtra("position",i+"");
                    context.startActivity(intent);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView batch_name, total_seats, filled_seats;
        RelativeLayout main;
        RoundCornerProgressBar total, filled;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            main = (RelativeLayout) itemView.findViewById(R.id.main_layout);
            batch_name = (TextView) itemView.findViewById(R.id.batch_name);
            total_seats = (TextView) itemView.findViewById(R.id.total_seat);
            filled_seats = (TextView) itemView.findViewById(R.id.filled_seat);
            total = (RoundCornerProgressBar) itemView.findViewById(R.id.total_seats);
            filled = (RoundCornerProgressBar) itemView.findViewById(R.id.filled_seats);

        }
    }

}