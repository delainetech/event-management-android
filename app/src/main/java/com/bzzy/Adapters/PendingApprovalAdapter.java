package com.bzzy.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.PendingApprovals;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;
import com.bzzy.utils.util;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PendingApprovalAdapter extends RecyclerView.Adapter<PendingApprovalAdapter.ViewHolder> implements ApiCalls {
    List<String> dataList;
    String letter;
    Context context;
    JSONArray data;
    Dialog dialog;
    UserSharedPreferences sharedPreferences;

    public PendingApprovalAdapter(Context context, JSONArray array) {
        this.context = context;
        this.data = array;
        sharedPreferences = new UserSharedPreferences(context);
    }

    @Override
    public PendingApprovalAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pending_item, viewGroup, false);
        return new PendingApprovalAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PendingApprovalAdapter.ViewHolder holder, int i) {
        try {
            JSONObject object = data.getJSONObject(i);
            Log.e("object_pending", object + "");
// Set details of expense
            if (!object.getString("description").equalsIgnoreCase("null")) {
                holder.product_name.setText(object.getString("description"));
            } else if (!object.getString("sub_type").equalsIgnoreCase("null")) {
                if (object.getString("sub_type").equalsIgnoreCase("other")) {
                    holder.product_name.setText(object.getString("sub_type") + "(" + object.getString("name") + ")");
                } else
                    holder.product_name.setText(object.getString("sub_type"));
            } else
                holder.product_name.setText(object.getString("cat_name"));
// Product image
            if (object.getString("cat_image").length() > 4) {
                Picasso.get().load(advertising_images + object.getString("cat_image")).fit().error(R.drawable.other_icon).into(holder.product_image);
            } else
                holder.product_image.setImageDrawable(context.getResources().getDrawable(R.drawable.other_icon));

// Bill date
            holder.bill_date.setText(object.getString("date"));
// remarks
            if (object.getString("remark").equalsIgnoreCase("null")) {
                holder.remark.setText("");
            } else
                holder.remark.setText(object.getString("remark"));

            holder.amount.setText("₹ " + object.getString("amount"));
// Approve bill
            holder.approve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmationDialog alert = new confirmationDialog();
                    try {
                        alert.showDialog((PendingApprovals) context, data.getJSONObject(i).getString("id"), 1, i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
// Decline bill
            holder.decline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    declineDialog alert = new declineDialog();
                    try {
                        alert.showDialog((PendingApprovals) context, data.getJSONObject(i).getString("id"), 2, i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
// Check bill image

            if (data.getJSONObject(i).getString("image").length() > 4) {
                holder.view_bill.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("boolean","true");
//                        ViewDialog alert = new ViewDialog();
                        try {
                            new util(context).Zoom_Image(context,expenditure_images+data.getJSONObject(i).getString("image"),true);
//                            alert.showDialog((PendingApprovals) context, data.getJSONObject(i).getString("image"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else
                holder.view_bill.setTextColor(context.getResources().getColor(R.color.light_grey));


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return data.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView product_name, bill_date, remark, amount, decline, approve, view_bill;
        ImageView product_image;

        public ViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            isRecyclable();

            product_image = (ImageView) itemView.findViewById(R.id.product_image);
            product_name = (TextView) itemView.findViewById(R.id.product_name);
            bill_date = (TextView) itemView.findViewById(R.id.bill_date);
            remark = (TextView) itemView.findViewById(R.id.remarks);
            amount = (TextView) itemView.findViewById(R.id.bill_amount);
            decline = (TextView) itemView.findViewById(R.id.decline);
            approve = (TextView) itemView.findViewById(R.id.approve);
            view_bill = (TextView) itemView.findViewById(R.id.view_bill);
        }
    }

    public class ViewDialog {
        void showDialog(Activity activity, String image) {
            dialog = new Dialog(activity);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.view_bill);

            Log.e("imagename", image);

            ProgressBar progressBar = dialog.findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.VISIBLE);
            ImageView bill_image = (ImageView) dialog.findViewById(R.id.bill_image);
            TextView error_text = (TextView) dialog.findViewById(R.id.error_text);

            if (image.length() > 4) {
                progressBar.setVisibility(View.GONE);
                Picasso.get().load(expenditure_images + image).fit().into(bill_image);
            } else {
                bill_image.setVisibility(View.GONE);
                error_text.setVisibility(View.VISIBLE);
            }
            dialog.show();
        }
    }

    // Confirm bill
    public class confirmationDialog {
        void showDialog(Activity activity, String exp_id, int status, int pos) {
            dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.confirmation_dialog);


            TextView approve = (TextView) dialog.findViewById(R.id.submit_button);
            TextView cancel = (TextView) dialog.findViewById(R.id.delete_btn);

            approve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pendingApprovals(exp_id, status, pos);
                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    // Decline bill
    public class declineDialog {
        void showDialog(Activity activity, String exp_id, int status, int pos) {
            dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;
            wlmp.dimAmount = 0.0F;
            dialog.getWindow().setAttributes(wlmp);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow()
                    .getAttributes().windowAnimations = R.style.DialogAnimation;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 1.00);
            int screenHeight = (int) (metrics.heightPixels * 1.00);
            dialog.getWindow().setLayout(screenWidth, screenHeight);
            dialog.setContentView(R.layout.decline_dialog);


            TextView decline = (TextView) dialog.findViewById(R.id.submit_button);
            TextView cancel = (TextView) dialog.findViewById(R.id.delete_btn);

            decline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pendingApprovals(exp_id, status, pos);
                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }


    private void pendingApprovals(String exp_id, int status, int pos) {
        RequestQueue MyRequestQueue = Volley.newRequestQueue(context);
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, respond_approvals, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        // Launch login activity
                        JSONObject object = jObj.getJSONObject("data");

                        data.remove(pos);
                        dialog.dismiss();
                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, "Sorry! no data found.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("expenditure_id", exp_id);
                params.put("user_id", sharedPreferences.getuserID());
                params.put("status", status + "");
                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }
}