package com.bzzy.Popus;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.ViewExpenditureBills;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.Constants.VolleyMultipartRequest;
import com.bzzy.R;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class ExpensBillEdit extends AppCompatDialogFragment implements View.OnClickListener, ApiCalls {

public Activity c;
        Resources resources;
        View view;
        TextView view_bill, image_path;
        EditText bill_date, other_products, amount, remark;
        int dayFinal, monthFinal, yearFinal;
        int day, month, year;
        String date_string;
        Bitmap bitmap = null, bitmap1 = null;
        ImageView profile_picture, upload_bill, submit_expense;
        Spinner category_spinner, sub_category_spinner;
        ArrayList<String> cat_name = new ArrayList<>();
        ArrayAdapter<String> arrayAdapter;
        UserSharedPreferences sharedPreferences;
        ArrayList<String> sub_cat_name = new ArrayList<>();
        String item_id, item_name, type, image,cat_id;
        ProgressDialog pDialog;
        String sub_cat;
        ImageView product_image;
        boolean status=false;
        String item_object;
        JSONObject object;
@Override
public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
        .getAttributes().windowAnimations = R.style.DialogAnimation;

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 1.00);
        int screenHeight = (int) (metrics.heightPixels * 1.00);
        getDialog().getWindow().setLayout(screenWidth, screenHeight);
        }

        int value, flag;

@Nullable
@Override
public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
        view = inflater.inflate(R.layout.expense_bill, container, false);
        c = getActivity();
                assert c != null;
                sharedPreferences = new UserSharedPreferences(c);
        resources = c.getResources();
                initialize();
        assert this.getArguments() != null;
        item_object=this.getArguments().getString("object");
            try {
                object=new JSONObject(item_object);

                item_id=object.getString("id");
                    item_name=object.getString("cat_name");
                    bill_date.setText(object.getString("date"));
                    amount.setText(object.getString("amount"));
                    remark.setText(object.getString("remark"));
                    cat_id=object.getString("cat_id");
                    type=object.getString("exp_type");
                    other_products.setText(object.getString("description"));


                Log.e("objrct",object.toString());
            } catch (JSONException e) {
                Log.e("exe",e.toString());
                e.printStackTrace();
            }


            Log.e("type", type + "");


        }
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return view;
        }


// Initializations of components
    private void initialize() {
        product_image = (ImageView) view.findViewById(R.id.image_product);
        Picasso.get().load(advertising_images + image).error(R.drawable.other_icon).into(product_image);
        remark = (EditText) view.findViewById(R.id.enter_remark);
        amount = (EditText) view.findViewById(R.id.amount);
        submit_expense = (ImageView) view.findViewById(R.id.submit_button);
        submit_expense.setOnClickListener(this);
        image_path = (TextView) view.findViewById(R.id.image_path);
        view_bill = (TextView) view.findViewById(R.id.view_bills);
        view_bill.setOnClickListener(this);

        bill_date = (EditText) view.findViewById(R.id.bill_date);
        bill_date.setOnClickListener(this);

        profile_picture = (ImageView) view.findViewById(R.id.bill);

        upload_bill = (ImageView) view.findViewById(R.id.upload_bill);
        upload_bill.setOnClickListener(this);

        category_spinner = (Spinner) view.findViewById(R.id.category);
        sub_category_spinner = (Spinner) view.findViewById(R.id.sub_category);

        other_products = (EditText) view.findViewById(R.id.other_products);

        getCategoriesData();
        }

        // Click events
        @Override
        public void onClick(View v) {
                if (v == view_bill) {
                Intent intent = new Intent(getContext(), ViewExpenditureBills.class);
                startActivity(intent);
                } else if (v == bill_date) {
                selectDate();
                } else if (v == upload_bill) {
                CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                .start(getActivity(), ExpensBillEdit.this);
                } else if (v == submit_expense) {
                get_Data();
                }
                }

        private void get_Data() {
                String cat = category_spinner.getItemAtPosition(category_spinner.getSelectedItemPosition()).toString();

                Log.e("cat", cat);
                if (bill_date.getText().toString().trim().equals("") || cat.equals("") || amount.getText().toString().trim().equalsIgnoreCase("")) {
                Toast.makeText(getContext(), "Please fill all the necessary details!", Toast.LENGTH_SHORT).show();
                } else {
                addExpenditure(bitmap);
                }
                }

        // Select date for bill update
        public void selectDate() {
        final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                yearFinal = year;
                monthFinal = month + 1;
                dayFinal = dayOfMonth;
                date_string = String.format("%02d-%02d-%02d", dayFinal, monthFinal, yearFinal);
                bill_date.setText(date_string);
                }
                }, year, month, day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
                }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
                super.onActivityResult(requestCode, resultCode, data);

                Log.e("codes", requestCode + "," + resultCode + "," + data);
                if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.e("uri", resultUri + "");
                image_path.setVisibility(View.VISIBLE);
                image_path.setText(resultUri + "");
        //                profile_picture.setImageURI(resultUri);
                try {
                bitmap1 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), resultUri);
                bitmap = getResizedBitmap(bitmap1, 512);
                } catch (IOException e) {
                e.printStackTrace();
                }
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("in", error + "");
                }
                }
                }


        public static byte[] getFileDataFromDrawable(Bitmap bitmap) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                return byteArrayOutputStream.toByteArray();
                }

        public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
                int width = image.getWidth();
                int height = image.getHeight();

                float bitmapRatio = (float) width / (float) height;
                if (bitmapRatio > 1) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
                } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
                }

                return Bitmap.createScaledBitmap(image, width, height, true);
                }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
                Dialog dialog = super.onCreateDialog(savedInstanceState);
                WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
                wlmp.gravity = Gravity.TOP;
                wlmp.dimAmount = 0.0F;
                dialog.getWindow().setAttributes(wlmp);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                return dialog;
                }


        public void getCategoriesData() {
                cat_name.clear();
                RequestQueue MyRequestQueue = Volley.newRequestQueue(c);
                StringRequest MyStringRequest = new StringRequest(Request.Method.POST, categories, new Response.Listener<String>() {
                        @Override
                    public void onResponse(String response) {
                Log.e("Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                JSONObject jObj = new JSONObject(response);
                String status = jObj.getString("status");
                if (status.equals("success")) {
                // Launch login activity
                JSONArray array = jObj.getJSONArray("data");
        //                        Log.e("GetList array  ", array + "");
                JSONObject object = null;
        //                        cat_name.add("Select Category");
                for (int i = 0; i < array.length(); i++) {
                object = array.getJSONObject(i);
                                    Log.e("GetList object", object + "");

                cat_name.add(object.getString("name"));

                }
                        arrayAdapter = new ArrayAdapter<>(c, R.layout.cat_spinner, cat_name);
                        category_spinner.setAdapter(arrayAdapter);
                setDataSpinner(array);

        //                        Toast.makeText(AddUserActivity.this, "Congrats Shift is added successfully.", Toast.LENGTH_SHORT).show();
                } else {
                Toast.makeText(c, "Sorry! event is not found.", Toast.LENGTH_SHORT).show();
                }
                } catch (JSONException e1) {
                Log.e("Exception", e1 + " ");
                e1.printStackTrace();
                }
                }
                }, new Response.ErrorListener() {
        //Create an error listener to handle errors appropriately.
        @Override
        public void onErrorResponse(VolleyError error) {
                Toast.makeText(c, "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
                }
                }) {
        @Override
        protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("event_id", sharedPreferences.geteventId());
                Log.e("getParams: ", params + "");

                return params;
                }
                };
                MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MyRequestQueue.add(MyStringRequest);
                }

        // Get Values from Array through spinner
        public void setDataSpinner(JSONArray array) {

                category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                sub_cat_name.clear();
                String category = null;
                String category_selected = null;
                if (cat_name.contains(item_name)) {
                category_selected = item_name;
                int id = cat_name.indexOf(item_name);
                Log.e("selectionid", id + "");
                category_spinner.setSelection(id);
                Log.e("categoryselect", category_selected + "");

                JSONObject object = null;

                try {
                object = array.getJSONObject(id);
                category = object.getString("sub_cat");
                Log.e("sub", category + "");
                } catch (JSONException e) {
                        Log.e("ee",e.toString());
                e.printStackTrace();
                }

                } else {
                Log.e("categoryselect", "else");
                }

                assert category != null;
                if (!category.equalsIgnoreCase("[]")) {
                Log.e("sub", "v");
                sub_category_spinner.setVisibility(View.VISIBLE);
                JSONArray sub_cat_arry = new JSONArray(category);
                for (int k = 0; k < sub_cat_arry.length(); k++) {
                sub_cat_name.add(sub_cat_arry.getString(k));
                }

                arrayAdapter = new ArrayAdapter<>(c, R.layout.cat_spinner, sub_cat_name);
                sub_category_spinner.setAdapter(arrayAdapter);
                sub_category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String category_selected = String.valueOf(sub_category_spinner.getSelectedItem());
                if (category_selected.equalsIgnoreCase("other")) {
                other_products.setVisibility(View.VISIBLE);
                } else {
                other_products.setVisibility(View.GONE);
                }
                sub_cat = sub_category_spinner.getItemAtPosition(sub_category_spinner.getSelectedItemPosition()).toString();
                Log.e("sub_cat",sub_cat+"");
                }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

                }
                });



                } else if (category_selected.equalsIgnoreCase("Other")) {
                other_products.setVisibility(View.VISIBLE);
                sub_category_spinner.setVisibility(View.GONE);
                sub_cat = "";
                Log.e("sub", "g");
                } else {
                Log.e("sub", "dfsdg");
                sub_cat = "";
                sub_category_spinner.setVisibility(View.GONE);
                other_products.setVisibility(View.GONE);
                }
                } catch (JSONException e) {
                e.printStackTrace();
                }
                }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(c, "Nothing selected", Toast.LENGTH_SHORT).show();
                }
                });
                category_spinner.setEnabled(false);
                }

        private void addExpenditure(Bitmap bitmap) {
                Log.e("gjg", "hgjg");
                pDialog = new ProgressDialog(getContext());
                pDialog.setMessage("Loading...");
                pDialog.show();
                //our custom volley request
                VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, add_expense_bill,
                new Response.Listener<NetworkResponse>() {
        @Override
        public void onResponse(NetworkResponse response) {

                Log.e("Response", new String(response.data) + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                JSONObject jObj = new JSONObject(new String(response.data));
                String status = jObj.getString("status");
                if (status.equals("success")) {
                pDialog.dismiss();
                JSONObject object = jObj.getJSONObject("data");

                if (Integer.parseInt(object.getString("verify_status")) == 1) {
                ((ViewExpenditureBills) getContext()).getCategoriesData("","","");
                } else {
                bitmap1 = null;
                Toast.makeText(c, "Expenditure is Updated successfully. wait for approval", Toast.LENGTH_SHORT).show();
                }

                dismiss();

                } else {
                pDialog.dismiss();
                Toast.makeText(c, "Sorry! expense is not added. ", Toast.LENGTH_SHORT).show();
                }
                } catch (JSONException e1) {
                pDialog.dismiss();
                Log.e("Exception", e1 + " ");
                e1.printStackTrace();
                }
                }
                },
                error -> {
                pDialog.dismiss();


                Log.e("onErrorResponse: ", error + "");
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                if (error.getClass().equals(TimeoutError.class)) {
                errorMessage = "Request timeout";
                } else if (error.getClass().equals(NoConnectionError.class)) {
                errorMessage = "Failed to connect server";

                    Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();

                }
                }
                }) {
        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("event_id", sharedPreferences.geteventId());
                params.put("user_id", sharedPreferences.getuserID());
                params.put("cat_id", cat_id);
                if (sub_cat.equalsIgnoreCase("")) {
                params.put("sub_type", "");
                } else
                params.put("sub_type", sub_cat);
                params.put("description", other_products.getText().toString().trim());
                params.put("date", bill_date.getText().toString().trim());
                params.put("amount", amount.getText().toString().trim());
                params.put("remark", remark.getText().toString().trim());
                params.put("exp_type", type);
                params.put("id",item_id);
                if (bitmap == null) {
                params.put("image", "");
                }
                Log.e("getParams: ", params + "");
                return params;
                }

        @Override
        protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                if (bitmap != null) {
                params.put("image", new DataPart(imagename + ".jpg", getFileDataFromDrawable(bitmap)));
                }
                Log.e("params", params + "");
                return params;
                }
                };
                volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                //adding the request to volley
                Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
                }
                }
