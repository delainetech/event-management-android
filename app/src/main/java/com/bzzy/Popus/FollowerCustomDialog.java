package com.bzzy.Popus;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzzy.Activities.Dashboard;
import com.bzzy.Activities.MyEarnings;
import com.bzzy.Activities.MyExpenses;
import com.bzzy.Adapters.ExpenditureAdapterBottom;
import com.bzzy.Api.ApiCalls;
import com.bzzy.Constants.UserSharedPreferences;
import com.bzzy.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class FollowerCustomDialog extends DialogFragment implements ApiCalls, View.OnClickListener {

    public Activity c;
    Resources resources;
    UserSharedPreferences sharedPreferences;
    View view;
    RecyclerView bottom_recycler;
    RecyclerView.LayoutManager layoutManager;
    ExpenditureAdapterBottom adapterBottom;
    TextView current_month, expense_amount, current_month1, income_amount;
    RelativeLayout expense_card, income_card;
    ProgressBar progressBar;


    int[] backgrounds = {
            R.drawable.monthly_card_back_blank,
    };

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 1.00);
        int screenHeight = (int) (metrics.heightPixels * 1.00);
        getDialog().getWindow().setLayout(screenWidth, screenHeight);
    }

    int value, flag;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.bottom_layout, container, false);
            sharedPreferences = new UserSharedPreferences(getContext());
            c = getActivity();
            resources = c.getResources();
            initialize();
        }
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
        wlmp.gravity = Gravity.RELATIVE_LAYOUT_DIRECTION;
        wlmp.dimAmount = 0.0F;
        dialog.getWindow().setAttributes(wlmp);
        dialog.getWindow().requestFeature(Window.FEATURE_SWIPE_TO_DISMISS);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((Dashboard) getContext()).view.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((Dashboard) getContext()).view.setVisibility(View.VISIBLE);

    }

    private void initialize() {

        progressBar= view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        bottom_recycler = (RecyclerView) view.findViewById(R.id.recycler_expenditure_bottom);

        current_month = view.findViewById(R.id.month);
        expense_amount = view.findViewById(R.id.amount1);

        current_month1 = view.findViewById(R.id.month1);
        income_amount = view.findViewById(R.id.amount);

        income_card = (RelativeLayout) view.findViewById(R.id.income_card);
        income_card.setVisibility(View.GONE);
        income_card.setOnClickListener(this);
        expense_card = (RelativeLayout) view.findViewById(R.id.expense_card);
        expense_card.setVisibility(View.GONE);
        expense_card.setOnClickListener(this);

        layoutManager = new LinearLayoutManager(getContext());
        bottom_recycler.setLayoutManager(layoutManager);

        getDataCards();
    }

    private void getDataCards() {
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue MyRequestQueue = Volley.newRequestQueue(getContext());
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, bottom_cards, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("cards Response", response + " ");
                //This code is executed if the server responds, whether or not the response contains data.
                try {
                    JSONObject jObj = new JSONObject(response);
                    String status = jObj.getString("status");
                    if (status.equals("success")) {
                        progressBar.setVisibility(View.GONE);
                        income_card.setVisibility(View.VISIBLE);
                        expense_card.setVisibility(View.VISIBLE);

                        expense_amount.setText("₹ " + jObj.getString("total_exp"));
                        current_month.setText(jObj.getString("current_month"));

                        income_amount.setText("₹ " + jObj.getString("total_income"));
                        current_month1.setText(jObj.getString("current_month"));

                        adapterBottom = new ExpenditureAdapterBottom(getContext(), backgrounds, jObj);
                        bottom_recycler.setAdapter(adapterBottom);

                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "Sorry! No data found!.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Exception", e1 + " ");
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                Log.e("Error", error + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", sharedPreferences.getuserID());
                params.put("event_id", sharedPreferences.geteventId());

                Log.e("getParams: ", params + "");
                return params;
            }
        };
        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == income_card) {
            Intent intent = new Intent(getContext(), MyEarnings.class);
            startActivity(intent);
        } else if (v == expense_card) {
            Intent intent = new Intent(getContext(), MyExpenses.class);
            startActivity(intent);
        }

    }
}
